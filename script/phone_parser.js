﻿var currentPollCollection;		
var herausforderer = [];
var searchRep = '';
function parseText(value, id, componentId, regionCode) {
	
    var lines = value.split('\n');
    var innerHTML = '';
	lastWeekPolls = '';
	var lastUpdateTime = lines[3].split(' ').clean("\r");
	if(lastUpdateTime == '') {
		noResltsInfo();
	}else {
		$("#noResltInfo").attr('style','display:none;');
	}
	
	if (state == 'Nation' ){
		$('.contentHeadline').html('LANDSOVERSIKT');
	}else if (state =='nationPolls' || state == 'Polls'){
		$('.contentHeadline').html('LANDSDEKKENDE MÅLINGER');
	}else {
		if (region == 'c00m0301') {
			$('.contentHeadline').html(" OSLO");
		}else {
			$('.contentHeadline').html(" "+getRegionName(region).toUpperCase());
		}
	}
    // only regioncode 05 nation and 04 county
	if(!isValg){
		currentPollCollection = parsePollCollectionForRegion(lines);

        if (currentPollCollection)
            parsePollCollectionResultsForRegion(lines);
	}
	
	
	
    if (componentId == 1) {
	//isFav = false;
		if ( mySelectedPollHL != '' && mySelectedPoll != 0 && mySelectedPollID != '') {
			var partyResultsRawM = currentPollCollectionResultsM[mySelectedPollID];
			innerHTML += parseMajority(partyResultsRawM, mySelectedPoll,mySelectedPollHL);
		}else {
			innerHTML += parseMajority(lines);
		}
        
        if (isValg) {
			innerHTML += parseVotesCounted(lines, regionCode);
		}/*else {
			innerHTML += parseLastWeekPolls(lines);
		}*/
		// if (id == 'divCountyData' || id == 'divMunicipalityData'){
            
		//	parseFavourites();
		
		//if(isFav == true){
		//	innerHTML += "<div id='isFavBtn' onclick=\"removeFromFavourites('"+region+"');\" >&nbsp;</div>";
		//}else {
		//	innerHTML += "<div id='isNotFavBtn' onclick='addToFavourites();'>&nbsp;</div>";
		//}
		//}
    }
    else if (componentId == 2) {
		//isFav = false;
		if ( mySelectedPollHL != '' && mySelectedPoll != 0 && mySelectedPollID != '') {
			var partyResultsRaw = currentPollCollectionResults[mySelectedPollID];
			if (partyResultsRaw){
				var partyResults = partyResultsRaw.split('|').clean("\r");
			}
			innerHTML += parseColumns(partyResults,regionCode, mySelectedPoll,mySelectedPollHL);
		}else {
			innerHTML += parseColumns(lines, regionCode, 0);
		}
        if (isValg) { 
			innerHTML += parseVotesCounted(lines, regionCode);
		}/*else {
			innerHTML += parseLastWeekPolls(lines);
		}*/

         /*if (id == 'divCountyData' || id == 'divMunicipalityData'){
            
			parseFavourites();
		
		if(isFav == true){
			innerHTML += "<div id='isFavBtn' onclick=\"removeFromFavourites('"+region+"');\" >&nbsp;</div>";
		}else {
			innerHTML += "<div id='isNotFavBtn' onclick='addToFavourites();'>&nbsp;</div>";
		}*/
		//}
    }
    else if (componentId == 3) {
	//isFav = false;
	if ( mySelectedPollHL != '' && mySelectedPoll != 0 && mySelectedPollID != '') {
		var partyResultsRaw = currentPollCollectionResults[mySelectedPollID];
		if (partyResultsRaw){
			var partyResults = partyResultsRaw.split('|').clean("\r");
		}
		innerHTML += parseMandates(partyResults,regionCode, mySelectedPoll,mySelectedPollHL);
	}else {
        innerHTML += parseMandates(lines, regionCode);
	}
        if (isValg) {
			innerHTML += parseVotesCounted(lines, regionCode);
		}/*else {
			innerHTML += parseLastWeekPolls(lines);
		}*/
		/* if (id == 'divCountyData' || id == 'divMunicipalityData'){
            
			parseFavourites();
		
			if(isFav == true){
			innerHTML += "<div id='isFavBtn' onclick=\"removeFromFavourites('"+region+"');\" >&nbsp;</div>";
		}else {
			innerHTML += "<div id='isNotFavBtn' onclick='addToFavourites();'>&nbsp;</div>";
		}
		}*/
        /*if (region.length > 3)
            parseFavourites();*/ // temp out bianca
    }
	else if (componentId == 8) {
		innerHTML += parseMajority(lines);
		if (isValg) innerHTML += parseVotesCounted(lines, regionCode);
	}
	else if (componentId == 9) {
		innerHTML += parseMandates2(lines, regionCode);
		if (isValg ) innerHTML += parseVotesCounted(lines, regionCode);
	}else if (componentId == 10){
		var galleri = document.getElementById('divRepres');
			
				inRepres = [];
				innerHTML = createRepArray(lines);
				if ( searchRep != '') {
		
			var blub = $('#candidate_'+ searchRep);
			setTimeout(function(){
				$('#candidate_'+ searchRep).addClass('activeRep');
			openPartiPanel(searchRepParty);
			openRepBox('candidate_'+ searchRep,$(blub));
			 searchRep = '';
			searchRepParty = 0;
			},200);
			
		}
			
	}
	if (componentId == 111){
		innerHTML = createRepArray(lines);
	}else {
		document.getElementById(id).innerHTML = innerHTML;
	isPollParse = false;
	}
}

function parseFavourites() {
    var innerHTML = '';
    var favourites = prefs.load(); 
		if (state == 'Fav') {
		innerHTML += "<div class='contentHeadline favHL'>FAVORITTER</div>"
    var color = '#FFFFFF';
        innerHTML += "<table class='favouriteContainer' border='0' cellpadding='0' cellspacing='0'>";

        if ( region && region.length <=8) {
           // innerHTML += "<tr onclick='addToFavourites();'>";
          //  innerHTML += "<td class='favouriteCell' style='padding-top: 8px;background-color:#F0F0F0;border-bottom:1px #C0C0C0 solid;text-align:center;'><img src='../images/favAdd.png' /></td>";
          //  innerHTML += "<td colspan='2' class='favouriteCell' style='background-color:#F0F0F0;text-align:left;border-bottom:1px #C0C0C0 solid;'>LEGG TIL SOM FAVORITT</td>";
          //  innerHTML += "</tr>";            
        }

        for (var i = 0; i < favourites.length; i++) {

            if (color == '#F0F0F0')
                color = '#FFFFFF';
            else if (color == '#FFFFFF')
                color = '#F0F0F0';

            var muni = favourites[i];

            innerHTML += "<tr style='background-color:" + color + ";'>";
            innerHTML += "<td onclick=\"goToFavourite('" + muni.id + "','" + muni.name + "');\" class='favouriteCell' style='padding-top: 8px;width:70px;text-align:center;'><img style='width:30px;' src='../images/fav_on.png' /></td>";
            innerHTML += "<td onclick=\"goToFavourite('" + muni.id + "','" + muni.name + "');\" class='favouriteCellText'>" + muni.name + "</td>";
            innerHTML += "<td onclick=\"removeFromFavourites('" + muni.id + "');\" class='favouriteCell' style='width:70px;text-align:center;'><img src='../images/favRemove.png' /></td>";
            innerHTML += "</tr>";
        }

    innerHTML += "</table>";
	innerHTML += "<br /><br /><div class='favInfo'><div class='favInfoOn'><div style='width: 45%;display:table-cell;text-align:right;vertical-align:middle;'><img style='margin-right: 7px;width:25px;' src='../images/fav_on.png' /><img style='margin-right: 7px;width:25px;' src='../images/favPfeil.png' /></div><div style='font-weight:lighter;display:table-cell;text-align:left;vertical-align:middle;font-style:italic;'>&nbsp;favoritt på</div></div><div class='favInfoOff'><div style='width: 45%;display:table-cell;text-align:right;vertical-align:middle;'><img style='margin-right: 7px;width:25px;' src='../images/fav_off.png' /><img style='margin-right: 7px;width:25px;' src='../images/favPfeil.png' /></div><div style='font-weight:lighter;display:table-cell;text-align:left;vertical-align:middle;font-style:italic;'>&nbsp;favoritt av</div></div></div><br />Gå til fylke/kommune for å velge favoritt.";
	}
    if (state == 'Fav'){
		var container = document.getElementById('divFav');
	}else if (state == 'County' || state == 'Municipality'){
		if ($.isArray(favourites)) {
			var regiona = region;
			if (regiona == 'c00m0301') {
				regiona = 'c03'
			}		
			var newFav = { name: getRegionName(regiona), id: regiona };
	        if (!eksistsInArray(newFav, favourites))
	            isFav = false;
			else 
				isFav = true;
	    }
	}
    if (container) container.innerHTML = innerHTML;
}

function parseMajority(lines,selectedPollIndex, headline) {
	//document.getElementById('divCountyMunicipalitySelect').style.display='block';
	//document.getElementById('divCountySelect').style.display='block';
    //64.2;6|35.8;2 majority
    var innerHTML = '';
    var percentRed = 0;
    var percentBlue = 0;
	var blueStart = 0;
	var values;
    // this was here before :var values = lines[7].split('|'); //only county and nation
	
	if((selectedPollIndex || selectedPollIndex == '0') && headline) {
		values = lines.split('|');
		
	}else {
	
	////////////////////////////////
	if (isValg){
		if (state == 'Nation' || state == 'County' || state == 'Storting' || state == 'Duell'){ 
			values = lines[7].split('|').clean("\r");
		}
		else if (state == 'Municipality') {
			values = lines[6].split('|').clean("\r");
		}
	}else {
		if (region.length > 3) //municipality, special case for iPad version
			values = lines[12].split('|').clean("\r"); //if  municipality has poll, majoritydata is at line 12			
		else
		    values = lines[7].split('|').clean("\r");
	}
	///////////////////////////////
	}
	if (headline) {
	var headLine = headline;
	innerHTML += "<div class='headline'>"+ headLine +"</div>";
	}else if (pollText){
		document.getElementById('divCountyMunicipalitySelect').style.display='none';
		document.getElementById('divCountySelect').style.display='none';
		if(state != 'Municipality'){
		innerHTML += "<div class='headline'>what does this nr mean?TODO</div>";
		}else {innerHTML += "<div class='headline'>"+pollText+"+</div>";}
	}
    if (values && values.length == 2) {
        var elementsRed = values[0].split(';');
        var elementsBlue = values[1].split(';');
		if(!isValg){
			if (state == 'Municipality' || state == 'County'){
				if (elementsRed.length > 0)
					percentRed = parseFloat(elementsRed[0]).toFixed(1);
				
				if (elementsBlue.length > 0)
					percentBlue = parseFloat(elementsBlue[0]).toFixed(1);
					
			}else{
				if (elementsRed.length == 2)
					percentRed = parseFloat(elementsRed[0]).toFixed(1);
				if (elementsBlue.length == 2)
					percentBlue = parseFloat(elementsBlue[0]).toFixed(1);
			}
		}else if(isValg == true && state == 'Duell'){
			if (elementsRed.length > 0 && elementsBlue.length > 0){
				percentRed = parseFloat(elementsRed[1]);
				percentBlue = parseFloat(elementsBlue[1]);
				var allMandates = percentRed + percentBlue;
				percentRed = parseFloat(percentRed * 100 / allMandates).toFixed(1);
				percentBlue = parseFloat(percentBlue * 100 / allMandates).toFixed(1);
			}
			
		}else {
			if (elementsRed.length > 0)
				percentRed = parseFloat(elementsRed[0]).toFixed(1);
			if (elementsBlue.length > 0)
				percentBlue = parseFloat(elementsBlue[0]).toFixed(1);	
		}
		
    }
	if (state == 'Duell') {
		
		var pfeilPer = 110;
		var pfeilPos;
		var posB = 0;
		//  0 % = -90deg, 50% = 0 deg, 100% = +90deg, 
		/*percentRed = 90;
		percentBlue	= 10;*/
		/** damit need to keep the real percent */
		if (!isValg) {
			var redPer = percentRed;
			var bluePer = percentBlue;
			var newProzent = parseFloat(percentRed) + parseFloat(percentBlue);
			percentRed = (newProzent * percentRed) / 100;
			percentBlue = 100 - percentRed;
			
		}else {
			var redPer = parseFloat(elementsRed[1]);
			var bluePer = parseFloat(elementsBlue[1]);
		}
		if (percentRed < 50){
			pfeilPer = -((50 - percentRed) * 1.8);
		}
		else if (percentRed == 50){
			pfeilPer = 0;
		}else {
			pfeilPer = +(( percentRed -50) * 1.8);
		}
		if ( percentRed == 'NaN' && percentBlue == 'NaN' ) {
			percentRed = 50;
			percentBlue = 50;
		}
		percentRed = percentRed * 1.8;
		percentBlue = percentBlue * 1.8;
		blueStart= percentRed + 270;
		innerHTML += "<div class='duellWrapper'>";
		
		innerHTML += "<div class='contentHeadline"+((!isValg) ?  ' maalingBtn' : '')+"'>DUELLEN</div>";
		if(redPer > bluePer || bluePer > redPer)
			innerHTML += "<div class='bigGreyHL'>"+((redPer > bluePer) ? 'RØD-GRØNT' : 'BLÅTT')+" FLERTALL</div>";
		else 
			innerHTML += "<div class='bigGreyHL'>&nbsp;</div>";	
			
		if (!isValg){
			innerHTML += "<div class='duellInfo'>"+((redPer > bluePer) ? redPer : bluePer) +"% av velgerne vil ha "+ ((redPer > bluePer) ?'RØD-GRØNN' : 'BORGERLIG')+" regjering fra høsten.</div>";
			innerHTML += "<div class='proInfo'><div class='tableRow'><div class='proRotHL'>RØD-GRØNNE:</div><div class='proBlauHL'>BORGERLIGE:</div></div><div class='tableRow'><div class='proRot'>"+redPer+"%</div><div class='proBlau'>"+bluePer+"%</div></div></div>";
		}else {
			innerHTML += "<div class='duellInfo'>&nbsp;</div>";
			innerHTML += "<div class='proInfo'><div class='tableRow'><div class='proRotHL'>RØD-GRØNNE:</div><div class='proBlauHL'>BORGERLIGE:</div></div><div class='tableRow'><div class='proRot'>"+redPer+"</div><div class='proBlau'>"+bluePer+"</div></div></div>";
		}
		
		innerHTML += "</div>";
		innerHTML += "<div class='duellContainer'><div id='duellRed' class='hold'><div class='pie' style='transform:rotate("+percentRed+"deg);-webkit-transform:rotate("+percentRed+"deg);-moz-transform:rotate("+percentRed+"deg);-o-transform:rotate("+percentRed+"deg);-ms-transform:rotate("+percentRed+"deg);'></div></div> <div id='duellBlue' style='transform:rotate("+blueStart+"deg);-webkit-transform:rotate("+blueStart+"deg);-moz-transform:rotate("+blueStart+"deg);-o-transform:rotate("+blueStart+"deg);-ms-transform:rotate("+blueStart+"deg);' class='hold'><div style='transform:rotate("+percentBlue+"deg);-webkit-transform:rotate("+percentBlue+"deg);-moz-transform:rotate("+percentBlue+"deg);-o-transform:rotate("+percentBlue+"deg);-ms-transform:rotate("+percentBlue+"deg);' class='pie'></div></div><div class='forBG'></div><div class='pfeil' style='transform:rotate("+pfeilPer+"deg);-webkit-transform:rotate("+pfeilPer+"deg);-moz-transform:rotate("+pfeilPer+"deg);-o-transform:rotate("+pfeilPer+"deg);-ms-transform:rotate("+pfeilPer+"deg);'></div></div>"
		if(isValg) {
			//innerHTML += parseVotesCounted(lines, regionCode);
		}else {
			innerHTML += "<div class='maalingInfo'>Før valgkvelden er framstilling basert på siste tilgjengelige gjennomsnitt av meningsmålingene</div>";
		}
		innerHTML += "</div>";
	}
	else {
    
	var percentRedText = parseFloat(percentRed).toFixed(1);
    var percentBlueText =parseFloat(percentBlue).toFixed(1);
	if (percentBlue == 0)
        percentBlue = 50;
    if (percentRed == 0)
        percentRed = 50;
    
    
    
    //calculate %
    percentBlue = (percentBlue - 50) < 0 ? 0 : percentBlue - 50;
    percentRed = (percentRed - 50) < 0 ? 0 : percentRed - 50;
	
    if (percentBlue != 0) percentBlue = percentBlue * 2;
    if (percentRed != 0) percentRed = percentRed * 2;
	
    innerHTML += "<table id='majorityContainer' width='100%' border='0' cellpadding='0' cellspacing='0'>";
    innerHTML += "<tr>";
    innerHTML += "<td style='text-align:center;padding-left:5px;padding-right:5px;'>" + percentRedText + "%</td>";
    innerHTML += "<td id='majorityComponentRed'><div id='red' style='width:100%;background-color:#FF0000;float:left;'><div style='width:" + percentBlue + "%;height:17px;background-color:#00AFCC;float:right;'>&nbsp;</div></div><div class='mask'></div></td>";
    innerHTML += "<td id='majorityComponentBlue'><div id='blue' style='width:100%;background-color:#00AFCC;float:right'><div style='width:" + percentRed +"%;height: 17px;background-color:#FF0000;float:left;'>&nbsp;</div></div><div class='mask'></div></td>";
    innerHTML += "<td style='text-align:center;padding-left:5px;padding-right:5px;'>" + percentBlueText + "%</td>";
    innerHTML += "</tr>";
    innerHTML += "</table>";

    innerHTML += "<table id='majorityContainer' width='100%' border='0' cellpadding='0' cellspacing='0'>";
    innerHTML += "<tr>";
    innerHTML += "<td style='padding-left:10px;padding-right:10px;text-align:left;'><i>RØD-GRØNNE</i></td>";
    innerHTML += "<td style='padding-left:10px;padding-right:10px;text-align:right;'><i>BORGERLIGE</i></td>";
    innerHTML += "</tr>";

    innerHTML += "</table>";
	
       
	if (region.length == 3 || region == 'c00m0301'){
		var regiona = region;
        if (regiona == 'c00m0301') {
			regiona = 'c03'
		}		
		isFav = false;
		parseFavourites();
		
		if(isFav == true){
		
			innerHTML += "<div id='isFavBtn' onclick=\"removeFromFavourites('"+regiona+"');\" >&nbsp;</div>";
		}else {
			innerHTML += "<div id='isNotFavBtn' onclick='addToFavourites();'>&nbsp;</div>";
		}
	}
	}
	isPollParse = false;
	if (!isValg) innerHTML += parsePollPanel(selectedPollIndex, 'blokk');
	
	
    return innerHTML;
}

function parseColumns(lines, regionCode, selectedPollIndex, headline) {

    var innerHTML = '';
    var labelOffset = 15;
    var componentHeight = (window.innerWidth > 320) ? 250 : 166;
    var interval = 10;
    var maxPercent = 0;
    var partyResults = null;

    //04 - value 3, diff 4
    //02 & 03 - value 1, diff 2
	if((selectedPollIndex || selectedPollIndex == '0') && headline) {
	//if(isPollParse == true) {
	
		partyResults = lines;
	}else {
	
		if (regionCode == '03' || regionCode == '02') //district or municipality
			if(isValg)partyResults = lines[7].split('|').clean("\r");
			else partyResults = lines[13].split('|').clean("\r");
		else if (regionCode == '05' || regionCode == '06' ) //county or nation
			partyResults = lines[8].split('|').clean("\r");
		else if (regionCode == '04')
			if (isValg == false) partyResults = lines[14].split('|').clean("\r");
			else partyResults = lines[8].split('|').clean("\r");
	}
	
	if (gotoSelectLastWeek == true)
		isPollParse = true;
	if (headline) {
	var headLine = headline;
	innerHTML += "<div class='headline'>"+ headLine +"</div>";
	}else if (pollText){
		document.getElementById('divCountyMunicipalitySelect').style.display='none';
		document.getElementById('divCountySelect').style.display='none';
		if(regionCode != '02'){
		innerHTML += "<div class='headline'>what does this nr mean?TODO</div>";
		}else {innerHTML += "<div class='headline'>"+pollText+"</div>";}
	}
    if (partyResults != null) {
        var columnWidth = (window.innerWidth > 320) ? 33 : 20;
        var columnSpace = (window.innerWidth > 320) ? 7 : 5;
        var MAX_NR_OF_COLUMNS = 13; //11 columns + 2 other partyresults
        var showOther = (regionCode == '05' || regionCode == '04' || isPollParse == true) ? true : ((partyResults.length > MAX_NR_OF_COLUMNS) ? true : false);
        var columnsLength = ((regionCode == '03' || regionCode == '02') ? ((partyResults.length > MAX_NR_OF_COLUMNS) ? 10 : partyResults.length) : 10);

        var startLeft = (window.innerWidth / 2) - ((( columnsLength == 10? (showOther ? 10 : columnsLength - 2) : columnsLength-2) * (columnWidth + columnSpace)) / 2) + 10;

        maxPercent = getColumnsMaxPercent(partyResults, columnsLength, regionCode, showOther);
        interval = (maxPercent <= 10) ? 2 : ((maxPercent < 30) ? 5 : 10);

        if (maxPercent > 0) {

            innerHTML += "<div id='columnsContainer' style='margin-bottom:" + (window.innerWidth > 320 ? "50" : "30") + "px;height:" + componentHeight + "px;'>";
            if(isPollParse == true){
			
				innerHTML += getColumnsBackground(maxPercent, componentHeight, interval);
			}else{
				
				innerHTML += getColumnsBackground(maxPercent, componentHeight, interval);
			}
            var names = [];
            var counter = 0;
			var sortedPartyResults = [];
			var newPartyResults = partyResults.clean("\r");
			sortedPartyResults = newPartyResults.slice(0).sort(sortMyResults); 
            for (var i = 0; i < columnsLength; i++) {
               if ( i == 8){
						var partyResult = sortedPartyResults[7];
					}else if (i == 4) {
						
						var partyResult = sortedPartyResults[9];
						
					}else if (i == 5){
						var partyResult = sortedPartyResults[5];
					}else if (i == 6){
						var partyResult = sortedPartyResults[4];
					}else if (i == 7){
						var partyResult = sortedPartyResults[6];
					}else if ( i == 9) {
						
						var partyResult = sortedPartyResults[10];
					}else {
						var partyResult = sortedPartyResults[i];
					}
                var partyElements = partyResult.split(';');

                if (partyElements.length > 5) {
                    var partyId = partyElements[0];

                    if (!showOther && partyId == 9)
                        continue;
                    
                    if (partyId == 1000)
                        continue;

                    var percent = ((regionCode == '03' || regionCode == '02') && (isPollParse == false)) ? partyElements[1] : partyElements[3];
                    var percentDiff = ((regionCode == '03' || regionCode == '02') && (isPollParse == false)) ? partyElements[2] : partyElements[4];
                    var left = startLeft + ((columnWidth + columnSpace) * i);//(partyId > 9 ? (i - 1) : i));
                    var height = ((parseFloat(percent) * 100 / maxPercent) * componentHeight) / 100;
					var maxHeight = maxPercent * componentHeight / 100 +45;
                    if (percentDiff == 0)
                        percentDiff = '';
                    if (percentDiff > 0)
                        percentDiff = '+' + percentDiff;

                    //logo
                    innerHTML += "<div style='margin-bottom:" + (window.innerWidth > 320 ? "-58" : "-38") + "px;height:" + (window.innerWidth > 320 ? "70" : "50") + "px;width:" + (window.innerWidth > 320 ? "37" : "22") + "px;position:absolute;bottom:0px;left:" + left + "px;'>";

                    if (partyId <= 11)
                        innerHTML += "<img style='position:absolute;bottom:17px;width:" + columnWidth + "px;' src='../images/logo/" + partyId + "_" + (window.innerWidth > 320 ? "480" : "480") + ".png' alt='logo'/>";
                    else {
                        var shortName = getPartyShortName(partyId);


                        innerHTML += "<div style='overflow:auto;position:absolute;bottom:0;width:" + columnWidth + "px;'>" + ((shortName.length > 3) ? shortName.substring(0,3) : shortName) + "</div>";
                        names.push(((shortName.length > 3) ? shortName.substring(0, 3) : shortName) + " = " + getPartyName(partyId));
                    }
					var n = parseFloat(percent).toFixed(1);
					 //value			
                    innerHTML += "<div style='font-size:" + ((window.innerWidth > 320) ? "12" : "9") + "pt;position:absolute;bottom:0px;height:20px;line-height:20px;'>" + n + "</div>";
                    innerHTML += "</div>";
                    
                    //column
					innerHTML += "<div style='position:absolute;height:"+maxHeight+"px;bottom:0px;left:" + (left-2) + "px;background: "+ ((counter%2 == 0)?' ':' -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(211,211,211,0.65)))')+";width:" + (columnWidth +4)+ "px;'></div>";
                    innerHTML += "<div style='position:absolute;bottom:0px;left:" + left + "px; background-color:" + getPartyColor(partyId) + ";height:" + height + "px;width:" + columnWidth + "px;'></div>";

                    //value			
                   // innerHTML += "<div style='font-size:" + ((window.innerWidth > 320) ? "12" : "9") + "pt;position:absolute;bottom:" + height + "px;left:" + left + "px;width:" + columnWidth + "px;height:20px;line-height:20px;'>" + percent + "</div>";

                    //diff value
                    innerHTML += "<div style='font-style:italic;font-size:" + ((window.innerWidth > 320) ? "8" : "8") + "pt;font-weight:normal;position:absolute;bottom:" + height + "px;left:" + left + "px;width:" + columnWidth + "px;height:20px;line-height:20px;'>" + percentDiff + "</div>";                           
                }  
				counter++;
            }

            innerHTML += "</div>";

            innerHTML += "<div class='partyNames'>";
            for (var k = 0; k < names.length; k++) {
                innerHTML += names[k] + "<br/>";
            }
            innerHTML += "</div>";
            
        }
    }
	if (region.length == 3 || region == 'c00m0301'){
		var regiona = region;
        if (regiona == 'c00m0301') {
			regiona = 'c03'
		}		
		isFav = false;
		parseFavourites();
		
		if(isFav == true){
		
			innerHTML += "<div id='isFavBtn' onclick=\"removeFromFavourites('"+regiona+"');\" >&nbsp;</div>";
		}else {
			innerHTML += "<div id='isNotFavBtn' onclick='addToFavourites();'>&nbsp;</div>";
		}
	}
	isPollParse == false;
	if (!isValg ) innerHTML += parsePollPanel(selectedPollIndex, 'votes');
    return innerHTML;
}

function getColumnsBackground(maxPercent, componentHeight, interval) {
    var innerHTML = '';
    var numRows = 0;
    var height = 0;
	var color = '#E6E6E6';
	
    numRows = Math.ceil(maxPercent / interval);
    height = componentHeight / numRows;

    innerHTML += "<table id='columnsBackground' cellpadding='0' cellspacing='0' border='0'>";

    for (var j = numRows; j > 0; j--) {
		 if (color == '#FFFFFF')
            color = '#E6E6E6';
        else if (color == '#E6E6E6')
            color = '#FFFFFF';
		
        innerHTML += "<tr>";

        if ((j % 1) == 0 && (j > 1)) {
            innerHTML += "<td class='columnsBackgroundRow'>" + ((j-1) * interval) + "%</td>";
            innerHTML += "<td class='columnsBackgroundRow' style='width:100%'>&nbsp;</td>";
        }
        else
            innerHTML += "<td class='columnsBackgroundRow' colspan='2' style='width:100%;'>&nbsp;</td>";

        innerHTML += "</tr>";
    }


    innerHTML += "</table>";

    return innerHTML;
}

function parseMandates(lines, regionCode, selectedPollIndex,headline ) {
    //1;0;0;1.5;-0.1;1.6;0;-0.3;120|2;2;-2;7;-4.3;11.3;4;-6;497|3;13;2;35.7;4;31.7;11;1.8;3167|4;4;1;11.3;4;7.3;3;1.8;868|5;1;1;2.3;0.5;1.8;0;1.2;293|6;2;-1;4.8;-2.9;7.7;3;1.9;910|7;5;0;15.2;0.8;14.4;5;-0.1;1359|8;7;-1;18.1;-2.7;20.8;8;-0.8;1892|9;1;0;4.1;0.6;3.5;1;0.4;361|21;0;0;0.2;-0.1;0.3;0;0;24|23;0;0;0.4;0.1;0.3;0;0;37|24;0;0;0.1;-0.2;0.3;0;-0.2;6|25;0;0;0.4;0.4;0;0;0.4;42|27;1;0;3.1;0.6;2.5;1;0.2;252|37;0;0;0;0;0;0;0;0|
    var innerHTML = '';
    var row1 = '<tr>';
    var row2 = '<tr>';
    var row3 = '<tr>';
    var row4 = '<tr>';
    var backgroundColor = '#ECECEC';
    var partyResults;
		//document.getElementById('divCountyMunicipalitySelect').style.display='block';
	//document.getElementById('divCountySelect').style.display='block';
	if((selectedPollIndex || selectedPollIndex == '0') && headline) {
		partyResults = lines;
	}else{ 
		if (regionCode == '03' || regionCode == '02') { //district or municipality
		   //partyResults = lines[7].split('|').clean("\r"); ;
		   /* if(isValg)partyResults = lines[7].split('|').clean("\r");
			else partyResults = lines[13].split('|').clean("\r");*/
		}else if (isValg == false && regionCode == '04'){
			partyResults = lines[14].split('|').clean("\r");
		
		
		} else if (regionCode == '04' || regionCode == '05' || regionCode == '06') { //county or nation
			partyResults = lines[8].split('|').clean("\r"); 
		}
	}
	if (headline) {
	var headLine = headline;
	innerHTML += "<div class='headline'>"+ headLine +"</div>";
	}else if (pollText){
		document.getElementById('divCountyMunicipalitySelect').style.display='none';
		document.getElementById('divCountySelect').style.display='none';
		if(regionCode != '03'){
		innerHTML += "<div class='headline'>what does this nr mean?TODO</div>";
		}else {innerHTML += "<div class='headline'>"+pollText+"+</div>";}
	}
    if (partyResults != null) {
	
        var MAX_NR_OF_COLUMNS = 13; //11 columns + 2 other partyresults
        var showOther = (regionCode == '05' || regionCode == '04') ? true : ((partyResults.length > MAX_NR_OF_COLUMNS) ? true : false);
        var columnsLength = ((regionCode == '03' || regionCode == '02') ? ((partyResults.length > MAX_NR_OF_COLUMNS) ? 9 : partyResults.length) : 9);
		
        innerHTML += "<table id='votesContainer' cellpadding='0' cellspacing='1' border='0'>";
        var names = [];
        var sortedPartyResults = [];
		var newPartyResults = partyResults.clean("\r");
			sortedPartyResults = newPartyResults.slice(0).sort(sortMyResults);
        for (var i = 0; i < columnsLength; i++) {
		
			if ( i == 8){
				var partyResult = sortedPartyResults[7];
			}else if (i == 4) {
				var partyResult = sortedPartyResults[9];
			}else if (i == 5){
				var partyResult = sortedPartyResults[5];
			}else if (i == 6){
				var partyResult = sortedPartyResults[4];
			}else if (i == 7){
				var partyResult = sortedPartyResults[6];
			}/*else if ( i == 9) {
				var partyResult = sortedPartyResults[10];
			}*/else {
				var partyResult = sortedPartyResults[i];
			}
		
            //var partyResult = partyResults[i];
            var partyElements = partyResult.split(';');
			
            //if (partyElements.length > 7) {
				
                var partyId = partyElements[0];

                if (!showOther && partyId == 9)
                    continue;

                if (partyId == 1000)
                    continue;

                var mandates = undefined;
                var diffMandates = undefined;
				
                if (regionCode == '03' || regionCode == '02') { //district or municipality
                    mandates = partyElements[6];
                    diffMandates = partyElements[7];
                }
                else if (regionCode == '04' || regionCode == '05' || regionCode == '06') { //county or nation
                    mandates = partyElements[1];
                    diffMandates = partyElements[2];
					
                }

                if (mandates && diffMandates) {
				
                    if (backgroundColor == '#ECECEC')
                        backgroundColor = '#FFFFFF';
                    else
                        backgroundColor = '#ECECEC';

                    row1 += "<td class='votesLogo' style='background-color:" + backgroundColor + "'>";
                        
                    if (partyId < 11) {
                        row1 += "<img src='../images/logo/" + partyId + "_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='logo' />";
                    }else if (partyId == 9) {
                        row1 += "<img src='../images/logo/107_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='logo' />";
                    }
                    else {
                        var shortName = getPartyShortName(partyId); 

                        row1 += ((shortName.length > 3) ? shortName.substring(0, 3) : shortName);
                        names.push(((shortName.length > 3) ? shortName.substring(0, 3) : shortName) + " = " + getPartyName(partyId));
                    }

                    row1 += "</td>";
                    
                    row2 += "<td class='votesValue' style='background-color:" + backgroundColor + "'>" + mandates + "</td>";
                    row3 += "<td class='votesDiff' style='background-color:" + backgroundColor + "'>" + (diffMandates > 0 ? '+' + diffMandates : diffMandates) + "</td>";
                    row4 += "<td class='votesDirection' style='background-color:" + backgroundColor + "'>";

                    if (diffMandates > 0)
                        row4 += "<img src='../images/up_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='up' />";
                    else if (diffMandates < 0)
                        row4 += "<img src='../images/down_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='down' />";
                    else
                        row4 += "&nbsp;";

                    row4 += "</td>";   
                }            
           // }
        }
    }

    innerHTML += row1 + "</tr>";
    innerHTML += row2 + "</tr>";
    innerHTML += row3 + "</tr>";
    innerHTML += row4 + "</tr>";
    innerHTML += "</table>";


    innerHTML += "</div>";

    innerHTML += "<div class='partyNames'>";
    for (var k = 0; k < names.length; k++) {
        innerHTML += names[k] + "<br/>";
    }
    innerHTML += "</div>";
	if (region.length == 3 || region == 'c00m0301'){
		var regiona = region;
        if (regiona == 'c00m0301') {
			regiona = 'c03'
		}		
		isFav = false;
		parseFavourites();
		
		if(isFav == true){
		
			innerHTML += "<div id='isFavBtn' onclick=\"removeFromFavourites('"+regiona+"');\" >&nbsp;</div>";
		}else {
			innerHTML += "<div id='isNotFavBtn' onclick='addToFavourites();'>&nbsp;</div>";
		}
	}
	isPollParse == false;
    if (!isValg ) innerHTML += parsePollPanel(selectedPollIndex, 'mandates');
    
    return innerHTML;
}

function parseVotesCounted(lines, regionCode) {
    //64.2;		
    var innerHTML = '';
    var lastUpdatedDate = lines[2]; //20070910 YYYYmmDD
	var lastUpdateTime = lines[3]; //11:50:29
    var percentCounted = lines[4];
	
	if (lastUpdateTime.length > 4){
		lastUpdateTime = lastUpdateTime.substring(0,lastUpdateTime.length -4);
	}
	
    if (lastUpdatedDate.length >= 8) {
        lastUpdatedDate = lastUpdatedDate.substring(6, 8) + '.' + lastUpdatedDate.substring(4, 6);
    }

    if ((regionCode == '03' || regionCode == '02') && ((region != 'c11m1103') && (region != 'c12m1201') && (region != 'c03m0301') && (region != 'c16m1601')))//district or municipality
    {
        /*
        "Ingen stemmer opptalt" (0)
        "Kun forhåndsstemmer" (1,2,3,4,5)
        "Et lite antall stemmer gjenstår"  (6,7)
        "Alt opptalt" (8)			
        */
        var codeText = '';

        if (percentCounted == 0)
            codeText = 'Ingen stemmer opptalt';
        else if (percentCounted == 1)
            codeText = "Kun forh&aring;ndsstemmer";
        else if (percentCounted == 2)
            codeText = "Et lite antall stemmer gjenst&aring;r";
        else if (percentCounted == 3)
            codeText = "Alt opptalt";

        innerHTML += "<table id='votesCountedContainer' width='100%' border='0' cellpadding='0' cellspacing='0'>";
        innerHTML += "<tr>";
        innerHTML += "<td style='text-align:center;' class='votesCountedHeader'>" + codeText + "</td>";
        innerHTML += "</tr>";
        innerHTML += "</table>";


    }
    else //county or nation
    {
        var widthFilled = parseFloat(percentCounted);

        if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c03m0301') || (region == 'c16m1601')) {
            var muniPercent = parseFloat(lines[16]);
            widthFilled = muniPercent;
        }

        innerHTML += "<table id='votesCountedContainer' border='0' cellpadding='0' cellspacing='0'>";

        innerHTML += "<tr>";
        innerHTML += "<td colspan='2' class='votesCountedHeader'>Stemmer opptalt:  "+ widthFilled + "%</td>";
        innerHTML += "</tr>";

        innerHTML += "<tr>";
        innerHTML += "<td colspan='2' id='votesCountedEmpty'><div id='votesCountedFilled' style='width:" + widthFilled + "%;'>&nbsp;</div></td>";
        //innerHTML += "<td class='votesCountedPercent'></td>";
        innerHTML += "</tr>";

		
		
		
		
      //  innerHTML += "<tr>";
      //  innerHTML += "<td colspan='2' class='votesCountedHeader'>Basert på fylkestingsvalget</td>";
      //  innerHTML += "</tr>";

        innerHTML += "</table>";

    }

    /* Last updated component */
    innerHTML += "<table style='font-weight:normal;text-align:right;width:100%;padding-right:10px;padding-bottom:10px;' id='lastUpdatedContainer' cellpadding='0' cellspacing='0'>";
    innerHTML += "<tr >";
    innerHTML += "<td ><img style='vertical-align: bottom;' src='../images/clock_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='clock' />&nbsp;OPPDATERT " + lastUpdatedDate +" kl "+lastUpdateTime+ "</td>";
    innerHTML += "</tr>";
    innerHTML += "</table>";

    return innerHTML;
}
function toggleStor(obj) {
	$(".storBtn").removeClass('active');
	if(obj.id == 'mandatBlockBtn') {
		$("#mandatBlockBtn").addClass('active');
		$("#mandaterBlock").attr("style","display:block;");
		$("#mandaterParti").attr("style","display:none;");
	}else {
		$("#mandatPartiBtn").addClass('active');
		$("#mandaterParti").attr("style","display:block;");
		$("#mandaterBlock").attr("style","display:none;");
	}
}
/*from tablet */
function parseMandates2(lines) {

    var innerHTML = '';
	var innerHTML2 = '';
    var partyResults;

    if (state == 'Nation') //nation
        partyResults = lines[8].split('|');
    else if (state == 'Polls')
        partyResults = lines[8].split('|');
	else if (state == 'Mandater' || state == 'MandaterBlock')
		partyResults = lines[8].split('|');
    var seatIndex = 169//0;
    var scaleX = 162;
    var scaleY = 154.36 - 27;
	//buttons to switch from parti to block
	innerHTML += "<div class='storHeader contentHeadline"+((!isValg) ?  ' maalingBtn' : '')+"'>STORTINGET</div>";
	//innerHTML += "<div class='contentHeadline"+((!isValg) ?  ' maalingBtn' : '')+"'>DUELLEN</div>";
	innerHTML += "<div class='storBtnMenu' style='display:table;width:100%;'><div id='mandatPartiBtn' onclick='toggleStor(this);' style='border: 1px solid #FFF;' class='button active storBtn'>PARTI</div><div id='mandatBlockBtn' onclick='toggleStor(this);' style='border: 1px solid #FFF;' class='button storBtn'>BLOKK</div></div>"
    innerHTML2 += "<div id='mandaterBlock' style='display:none;'>";
	innerHTML2 += parseMajorityMandates(lines,'blokk');
	innerHTML += "<div id='mandaterParti' style='display:block;'>";
	//innerHTML += parseMajorityMandates(lines,'parti');
	innerHTML2 += "<div class='rotate'><div class='mirror'>"
	innerHTML += "<div class='rotate'><div class='mirror'>"
    for (var i = 0; i < 9; i++) { //TODO: hardcoded first 9 parties (established + others)
        //var partyResult = partyResults[i];
        if ( i < 4 ) {
			var partyResult = partyResults[i];
		} else if ( i == 4 ) {
			var partyResult = partyResults[8];
		}else {
			var partyResult = partyResults[i - 1];
		}
		
		
		var partyElements = partyResult.split(';');

        var partiId = partyElements[0];

        //seats
        var mandates;

        if (state == 'Nation')
            mandates = partyElements[1]; //??
        else if (state == 'Polls')
            mandates = partyElements[1];
		else if (state == 'Mandater' || state == 'MandaterBlock')
			mandates = partyElements[1]; //??
        
		if (mandates > 0) {
			
			for (var mandate = 0; mandate < mandates; mandate++) {
                //seatIndex++;

                var x = getXCoordinateForMandateSeat(seatIndex - 1) - scaleX ;
                var y = getYCoordinateForMandateSeat(seatIndex - 1) - scaleY;
                var rot = getRotationForSeat(seatIndex);
				seatIndex--;
				//innerHTML2 += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='../images/seats/" + ((partiId > 4) ? 7 : 3) + ".png' alt='seat' />";
				innerHTML2 += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='../images/seats/" + ((partiId < 5) ? 3 : (partiId == 9 || partiId == 107)? 107: 7) + ".png' alt='seat' />";
				//innerhtml for parti

				innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='../images/seats/" + ((partiId == 9) ? 107 : partiId ) + ".png' alt='seat' />";
				
			  //  innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);width:11px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='../images/seats/" + partiId + ".png' alt='seat' />";
            }
        }else {
			if ( i == 1 ){
				for (var mandate = 0; mandate < 169; mandate++) {
					var x = getXCoordinateForMandateSeat(seatIndex - 1) - scaleX;
					var y = getYCoordinateForMandateSeat(seatIndex - 1) - scaleY;
				
					var rot = getRotationForSeat(seatIndex);
					seatIndex--;
					innerHTML2 += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='../images/seats/10000.png' alt='seat' />";
				 innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);-o-transform: rotate(" + rot + "deg);-ms-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='../images/seats/10000.png' alt='seat' />";
				}
			}
		}



        //logo and circle
		var caller = 'phone';
		if (partiId == 9) {
		
			var partyLogoSrc = getPartyLogoSrc('107', caller);
		}else {
			var partyLogoSrc = getPartyLogoSrc(partiId, caller);
		}
        //var partyLogoSrc = getPartyLogoSrc(partiId, caller);
        if (partyLogoSrc != '') {
            var logoX = getLogoXCoordinate(partiId);
            var logoY = getLogoYCoordinate(partiId);
			//innerHTML += "<div id='parti"+partiId+"' class='partiLabelStor'>";
            //circle
			//innerHTML += "<div class='rotate'>";
            //innerHTML += "<div class='mandatesLabel mirrorY' style='bottom:" + (logoY - (31 - 8)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";
			/*if (partiId < 5) {
				innerHTML += "<div class='mandatesLabel mirrorY' style='bottom:" + (logoY - ( (partiId == 1) ? 0 : 0)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";
			}else if (partiId == 5){
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:center;height: 45px;width:26px;bottom:" + (logoY - ( (partiId == 1) ? 5 : 30)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";

			}else {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:left;bottom:" + (logoY - ( (partiId == 1) ? 5 : 0)) + "px;left:" + (logoX - 25) + "px;'>" + mandates + "</div>";

			}
            //logo
            innerHTML += "<img class='logo mirrorY' style='width: 30px;height:30px;bottom:" + (logoY - ( (partiId == 1) ? 0 : 0)) + "px;left:" + logoX  + "px;' src='" + partyLogoSrc + "' alt='logo'/>";*/
			if ((partiId > 6 && partiId < 9)  || partiId == 5) {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:left;bottom:" + (logoY - ( (partiId == 9) ? 0 : 0)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";
			}/*else if (partiId == 5){
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:center;height: 45px;width:26px;bottom:" + (logoY - ( (partiId == 9) ? 5 : 30)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";

			}*/else if (partiId < 4 || partiId == 5 ) {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:right;bottom:" + (logoY - ( (partiId == 1) ? 0 : 0)) + "px;left:" + (logoX - 25 ) + "px;'>" + mandates + "</div>";

			}else {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:center;width:26px;bottom:" + (logoY - ( (partiId == 9) ? 30 : 30) +50) + "px;left:" + (logoX - 10) + "px;'>" + mandates + "</div>";
			}
            //logo
            innerHTML += "<img class='logo mirrorY' style='width: 30px;bottom:" + (logoY - ( (partiId == 1) ? 0 : 0)) + "px;left:" + logoX  + "px;' src='" + partyLogoSrc + "' alt='logo'/>";
			
			
			
			//innerHTML += "</div>";
        }
    }
	innerHTML +="</div></div>"
	innerHTML2 +="</div></div>"
	innerHTML2 += "<div class='storParteiImages tableRow'><div class='proRotPartier'><img src='../images/logo/1_480_off.png' >&nbsp;&nbsp;<img src='../images/logo/2_480_off.png' >&nbsp;&nbsp;<img src='../images/logo/3_480_off.png' >&nbsp;&nbsp;<img src='../images/logo/4_480_off.png' > </div><div class='proBlauPartier'><img src='../images/logo/5_480_off.png' >&nbsp;&nbsp;<img src='../images/logo/6_480_off.png' >&nbsp;&nbsp;<img src='../images/logo/7_480_off.png' >&nbsp;&nbsp;<img src='../images/logo/8_480_off.png' > </div></div>";
		
	
	
    //add label "169 mandater" DONT NEED BIANCA ?
	//innerHTML += "<div class='mandatesDescription' style='bottom:70px;'>169</div>";
    //innerHTML += "<div class='mandatesDescription' style='bottom:50px;'>mandater</div>";

    
	innerHTML += "</div>";
   
	innerHTML2 += "</div>";
	if (!isValg){
		innerHTML += "<div class='maalingInfo stortinget'>Før valgkvelden er framstilling basert på siste tilgjengelige gjennomsnitt av meningsmålingene</div>";
		//innerHTML2 += "<div class='maalingInfo stortinget'>Før valgkvelden er framstilling basert på siste tilgjengelige gjennomsnitt av meningsmålingene</div>";
		
	}
	innerHTML += innerHTML2;
    return innerHTML;

}
function getLogoXCoordinate(partyId) {
    switch (partyId) {
        case '9':
            //return 124;
			return 217;
            break;
        case '8':
            //return 124;
			return 124;
            break;
        case '7':
            //return 124;
			return 124;
            break;
        case '6':
            //return 160;//140;
			return 160;
            break;
        case '5':
            //return 217;
			return 124;
            break;
        case '4':
            return 274;//294;
            break;
        case '3':
            return 310;
			//return 420;
            break;
        case '2':
            return 310;
            break;
        case '1':
            return 310;
            break;
    }
}
function getLogoYCoordinate(partyId) {
    switch (partyId) {
        case '9':
            //return 5;
			return 170;
            break;
        case '8':
            //return 62;;
			return 5;
            break;
        case '7':
            //return 120;
			//return 350;
			return 62;
            break;
        case '6':
            return 170;
            break;
        case '5':
            //return 170;
			return 120;
            break;
        case '4':
            return 170
            break;
        case '3':
			return 120;
            break;
        case '2':
            return 62;//92;
            break;
        case '1':
            return 5;//25;
            break;
    }
}

function getRotationForSeat(seatId) {
    var degrees = 0;

    // first group of rotation (seats 23 - 27) - 11,25'
    if ((seatId > 25) && (seatId <= 30))
        degrees = 11, 25;

    // second group of rotation (seats 28 - 37) - 22,5'
    if ((seatId > 30) && (seatId <= 40))
        degrees = 22, 5;

    // third group of rotation (seats 38 - 42) - 33,75'	
    if ((seatId > 40) && (seatId <= 45))
        degrees = 33, 75;

    // fourth group of rotation (seats 43 - 53) - 45'
    if ((seatId > 45) && (seatId <= 56))
        degrees = 45;

    // fifth group of rotation (seats 54 - 59) - 56,75'
    if ((seatId > 56) && (seatId <= 62))
        degrees = 56, 75;

    // sixth group of rotation (seats 60 - 71) - 67,5'
    if ((seatId > 62) && (seatId <= 74))
        degrees = 67, 5;

    // seventh group of rotation (seats 72 - 76) - 78,75'
    if ((seatId > 74) && (seatId <= 79))
        degrees = 78, 75;

    // eight group of rotation (seats 77 - 82) - 90'
    if ((seatId > 79) && (seatId <= 85))
        degrees = 90;

    if ((seatId > 85) && (seatId <= 91))
        degrees = 90;

    if ((seatId > 91) && (seatId <= 96))
        degrees = 90 + 11, 25;

    if ((seatId > 96) && (seatId <= 108))
        degrees = 90 + 22, 5;

    if ((seatId > 108) && (seatId <= 114))
        degrees = 90 + 33, 75;

    if ((seatId > 114) && (seatId <= 125))
        degrees = 90 + 45;

    if ((seatId > 125) && (seatId <= 129))
        degrees = 90 + 56, 75;

    if ((seatId > 129) && (seatId <= 139))
        degrees = 90 + 67, 5;

    if ((seatId > 139) && (seatId <= 144))
        degrees = 90 + 78, 75;

    if (seatId > 144)
        degrees = 90 + 90;

    return degrees;
}
function parseMajorityMandates(lines,parti_) {
    //47.5;85|52.5;84
    var innerHTML = '';
	var parti = parti_;
    var mandatesRed = 0;
    var mandatesBlue = 0;
    var values = lines[7].split('|'); //7 county/nation, 6 municipality

    if (values.length == 2) {
        var elementsRed = values[0].split(';');
        var elementsBlue = values[1].split(';');

        if (elementsRed.length == 2)
            mandatesRed = elementsRed[1]; //mandats
        if (elementsBlue.length == 2)
            mandatesBlue = elementsBlue[1]; //mandates			
    }
	// TODO: check out how ntb wants it
	if (parti == 'blokk') {
		innerHTML += "<div class='redSeats'> "+ mandatesRed +"</div>";
		innerHTML += "<div class='blueSeats'>" + mandatesBlue + "</div>";
	}else {
		innerHTML += "<div class='redSeats'> "+ mandatesRed +"</div>";
		innerHTML += "<div class='blueSeats'>" + mandatesBlue + "</div>";
	}
    /*innerHTML += "<table class='mandatesMajority' border='0' cellpadding='0' cellspacing='0'>";
    innerHTML += "<tr>";
    innerHTML += "<td style='padding-right:5px;'>" + mandatesRed + "</td>";

    innerHTML += "<td>";
    innerHTML += "<table style='width:84.5px;border-collapse:collapse;'>";
    innerHTML += "<td id='majorityComponentRedSalen' style='width:" + (mandatesRed / 2) + "px;'>&nbsp;</td>";
    innerHTML += "<td id='majorityComponentBlueSalen' style='width:" + (mandatesBlue / 2) + "px;'>&nbsp;</td>";
    innerHTML += "</table>";
    innerHTML += "</td>";

    innerHTML += "<td style='padding-left:5px;'>" + mandatesBlue + "</td>";
    innerHTML += "</tr>";
    innerHTML += "</table>";

    //create majority centerline	
    var centerX = 234;

    //if (window.orientation == 0 || window.orientation == 180)
    //centerX = 380;

    var centerLine = createLine(centerX, 320, centerX, 345, '#000000', 1, 'centerLineMandates');
    innerHTML += centerLine.outerHTML;
	*/
    return innerHTML;
}
function createLine(x1, y1, x2, y2, color, weight, id){
	if (x2 < x1){
		var temp = x1;
		x1 = x2;
		x2 = temp;
		temp = y1;
		y1 = y2;
		y2 = temp;
	}
	
	var line = document.createElement("div");
	line.className = "line";
	
	if (id)
		line.id = id;
	
	var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	line.style.width = length + "px";
	line.style.borderColor = color;
	line.style.borderWidth = weight + "px 0px 0px 0px";


	//if (isIE){
	//	line.style.top = (y2 > y1) ? y1 + "px" : y2 + "px";
	//	line.style.left = x1 + "px";
	//	var nCos = (x2-x1)/length;
	//	var nSin = (y2-y1)/length;
	//	line.style.filter = "progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', M11=" + nCos + ", M12=" + -1*nSin + ", M21=" + nSin + ", M22=" + nCos + ")";
	//}
	//else{
		var angle = Math.atan((y2-y1)/(x2-x1));
		line.style.top = y1 + 0.5*length*Math.sin(angle) + "px";
		line.style.left = x1 - 0.5*length*(1 - Math.cos(angle)) + "px";
		line.style.MozTransform = line.style.WebkitTransform = line.style.OTransform =  "rotate(" + angle + "rad)";

		//line.style.transform = "rotate(" + angle + "rad)";
		//line.style.MozTransform = "rotate(" + angle + "rad)";
		//line.style.MozTransform = line.style.WebkitTransform = line.style.OTransform= "rotate(" + angle + "rad)";
	//}
	
	return line;
}

function createRepArray(lines) {
	var values;
	var parseRegioner2;
	countySpecialM = [];
	var tempArray = [];
	herausforderer = [];
	herausfordererMandat = [];

	//inRepres
	if (state == 'Polls' || state == 'Nation' || state== 'Repres' || state=='Search'){
		parseRegioner2 = lines[9].split('|').clean("\r"); 
	}
	
	if (parseRegioner2){
		
		for (var i = 0; i < parseRegioner2.length ; i ++){		
			
			var elements = parseRegioner2[i].split(';');
			
			var parseRegion = elements[0];
			//if (parseRegion == '01' ||parseRegion == '02'||parseRegion == '03'||parseRegion == '04'||parseRegion == '05'||parseRegion == '06'||parseRegion == '07'||parseRegion == '08'||parseRegion == '09'){
				while ( parseRegion.charAt(0) === '0')
					parseRegion = parseRegion.substr(1);
			//}
			
			if (parseRegion == '301') {
				parseRegion = '3';
			}
			
			
			
			var elementParti 	= elements[2].split('#');
			if (isValg){
			var umandatParti 	= elements[elements.length -5];
			var mistMandat 		= elements[elements.length -4];
			var vinneMandat 	= elements[elements.length -3];
			var mistStemmer 	= elements[elements.length -2];
			var vinneStemmer 	= elements[elements.length -1];
			var mAdded = false;
			var loseAddad = false;
			var vinneMAdded = false;
			}
			for ( var k = 0; k < elementParti.length ; k++) {
				var fylkePartiData 		= elementParti[k].split('$');
				var fylkesPartiId 		= fylkePartiData[0];
				/** MDG HACK **/
				if (fylkesPartiId == 9   ) { // mdg is parti id 107 but in the json from trond its 10
					fylkesPartiId = 1000000000;
				}
				else if (fylkesPartiId == 107) {
					fylkesPartiId = 10;
				}
				var fylkesPartiMandat 	= fylkePartiData[1];
				var fylkesPartiPercent	= fylkePartiData[2];
				var myPartiCode =  parseInt(fylkesPartiId);
				if(isValg){
					if ((umandatParti == fylkesPartiId && mAdded == false) || (mistMandat == fylkesPartiId && loseAddad == false)) {
						if (umandatParti == fylkesPartiId && mAdded == false) {
							mAdded = true;
							countySpecialM.push({countyId:elements[0], umandatParti:umandatParti,mistMandat:'',vinneMandat:'',mistStemmer:'',vinneStemmer:'', normalMandatforParti: fylkesPartiMandat });
						}else if (mistMandat == fylkesPartiId && loseAddad == false) {
							loseAddad = true;
							countySpecialM.push({countyId:elements[0], umandatParti:'',mistMandat:mistMandat,vinneMandat:'',mistStemmer:mistStemmer,vinneStemmer:vinneStemmer, normalMandatforParti: fylkesPartiMandat });
						}
					}
					if (vinneMandat == fylkesPartiId && vinneMAdded == false) {
						
						herausfordererMandat.push({countyId:elements[0], vinneMandat:vinneMandat,vinneStemmer:vinneStemmer, normalMandatforParti: fylkesPartiMandat });
						vinneMAdded = true;
					}
				}
				
				if(fylkesPartiMandat > 0){
				tempArray.push({parseRegion:parseRegion,myPartiCode:myPartiCode,fylkesPartiMandat:fylkesPartiMandat });
				}
			}
		}	
	}
	
	if (inRepres.length == 0) {
	$.each(allRepres , function() {
		
		for ( var l =0 ;l < tempArray.length;l++) {
			if (this.CountyId == tempArray[l].parseRegion && parseInt(this.PartiOrganizationId) == tempArray[l].myPartiCode && this.CountyPosition <= tempArray[l].fylkesPartiMandat) {
				inRepres.push(this);
			}
		}
		for (var h = 0; h < herausfordererMandat.length; h++){
			if (this.CountyId == (herausfordererMandat[h].countyId == 301 ? 3 : herausfordererMandat[h].countyId ) &&  parseFloat(this.CountyPosition) == (parseFloat(herausfordererMandat[h].normalMandatforParti) +1) && this.PartiOrganizationId == herausfordererMandat[h].vinneMandat ) {
				herausforderer.push(this);
			}
		}
	});
	}
	//console.log(inRepres.length+ ' inrepres length '+ herausforderer.length);
	if (state == 'Search'){
		return '';
	}else {
		return displayRepFylke(tempArray);
	}

}
function displayRepFylke(tempArray){
	
	/*$.each(allRepres , function() {
		
		for ( var l =0 ;l < tempArray.length;l++) {
			if (this.CountyId == tempArray[l].parseRegion && String(this.PoliticalPartyCode).toLowerCase() == tempArray[l].myPartiCode.toLowerCase() && this.CountyPosition < tempArray[l].fylkesPartiMandat) {
				inRepres.push(this);
			}
		}		
	});*/
	
	var innerHTML = '';var innerHTML1 = '';var innerHTML2 = '';var innerHTML3 = '';var innerHTML4 = '';var innerHTML5 = '';var innerHTML6 = '';var innerHTML7 = '';var innerHTML8 = '';var innerHTML9 = '';
	var parti1Counter = 0;var parti2Counter = 0;var parti3Counter = 0;var parti4Counter = 0;var parti5Counter = 0;var parti6Counter = 0;var parti7Counter = 0;var parti8Counter = 0;var parti9Counter = 0;
	inRepres.sort(sortByParti);
	
		innerHTML += "<div class='contentHeadline'>REPRESENTANTENE</div><div  class='represBtnMenu'><div id='represPartiBtn' class='button storBtn active' onclick='toggleRepres(this);'>PARTI</div><div id='represFylkeBtn' class='button storBtn' onclick='toggleRepres(this);'>FYLKE</div></div></div></div><div id='repWrapper'>";
		
		
		innerHTML1 += "<div id='Parti1' ><div class='partiHeadline' onclick='openPartiPanel(1);'><div class='partiLabel'>RØDT</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti1holder holder'>";
		
		innerHTML2 += "<div id='Parti2' ><div class='partiHeadline' onclick='openPartiPanel(2);'><div class='partiLabel'>SOSIALISTISK VENSTREPARTI</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti2holder holder'>";
		
		innerHTML3 += "<div id='Parti3' ><div class='partiHeadline' onclick='openPartiPanel(3);'><div class='partiLabel'>ARBEIDERPARTIET</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti3holder holder'>";
		
		innerHTML4 += "<div id='Parti4'><div class='partiHeadline' onclick='openPartiPanel(4);'><div class='partiLabel'>SENTERPARTIET</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti4holder holder'>";
		
		innerHTML5 += "<div id='Parti5'><div class='partiHeadline' onclick='openPartiPanel(5);'><div class='partiLabel'>VENSTRE</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti5holder holder'>";
		
		innerHTML6 += "<div id='Parti6'><div class='partiHeadline' onclick='openPartiPanel(6);'><div class='partiLabel'>KRISTELIG FOLKEPARTI</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti6holder holder'>";
		
		innerHTML7 += "<div id='Parti7'><div class='partiHeadline' onclick='openPartiPanel(7);'><div class='partiLabel'>HØYRE</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti7holder holder'>";
		
		innerHTML8 += "<div id='Parti8'><div class='partiHeadline' onclick='openPartiPanel(8);'><div class='partiLabel'>FREMSKRITTSPARTIET</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti8holder holder'>";
		
		innerHTML9 += "<div id='Parti9'><div class='partiHeadline' onclick='openPartiPanel(9);'><div class='partiLabel'>ANDRE</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='parti9holder holder'>";
	//parse 	
	$.each(inRepres , function() {
		
		
		//if(getPartiId(this.PoliticalPartyCode) == 1 || getPartiId(this.PoliticalPartyCode)  == '1'){
		if ( this.PartiOrganizationId == 1 ){
			parti1Counter ++; 
			innerHTML1 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));//(this.PoliticalPartyCode,this.CountyId,this.CandidateId);
			innerHTML1 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti1Counter == 3){
				innerHTML1 +=  createBox();
				parti1Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode)  == 2  || getPartiId(this.PoliticalPartyCode) == '2'){
		}else if ( this.PartiOrganizationId == 2 ){
			parti2Counter ++; 
			innerHTML2 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML2 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti2Counter == 3){
				innerHTML2 +=  createBox();
				parti2Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode)  == 3 || getPartiId(this.PoliticalPartyCode)  == '3'){
		}else if ( this.PartiOrganizationId == 3 ){
			parti3Counter ++; 
			innerHTML3 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML3 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti3Counter == 3){
				innerHTML3 +=  createBox();
				parti3Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode)  == 4 || getPartiId(this.PoliticalPartyCode)  == '4'){
		}else if ( this.PartiOrganizationId == 4 ){
			parti4Counter ++; 
			innerHTML4 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML4 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti4Counter == 3){
				innerHTML4 +=  createBox();
				parti4Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode)  == 5 || getPartiId(this.PoliticalPartyCode)  == '5'){
		}else if ( this.PartiOrganizationId == 5 ){
			parti5Counter ++; 
			innerHTML5 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML5 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti5Counter == 3){
				innerHTML5 +=  createBox();
				parti5Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode)  == 6 || getPartiId(this.PoliticalPartyCode)  == '6'){
		}else if ( this.PartiOrganizationId == 6 ){
			parti6Counter ++; 
			
			innerHTML6 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML6 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti6Counter == 3){
				innerHTML6 +=  createBox();
				parti6Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode) == 7 || getPartiId(this.PoliticalPartyCode)  == '7'){
		}else if ( this.PartiOrganizationId == 7 ){
			parti7Counter ++; 
			innerHTML7 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML7 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			
			if (parti7Counter == 3){
				innerHTML7 +=  createBox();
				parti7Counter = 0;
			}
		//}else if(getPartiId(this.PoliticalPartyCode)  == 8 || getPartiId(this.PoliticalPartyCode)  == '8'){
		}else if ( this.PartiOrganizationId == 8 ){
			parti8Counter ++; 
			innerHTML8 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML8 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti8Counter == 3){
				innerHTML8 +=  createBox();
				parti8Counter = 0;
			}
		}else {
			parti9Counter ++; 
			innerHTML9 += generateRepresentanterBilde(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML9 += generateRepresentanterList(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti9Counter == 3){
				innerHTML9 +=  createBox();
				parti9Counter = 0;
			}
		}
		
		if (this.CountyId == 1){
		
		}
		
	});
		innerHTML1 +=  createBox()+"</div></div>";
		innerHTML2 +=  createBox()+"</div></div>";
		innerHTML3 +=  createBox()+"</div></div>";
		innerHTML4 +=  createBox()+"</div></div>";
		innerHTML5 +=  createBox()+"</div></div>";
		innerHTML6 +=  createBox()+"</div></div>";
		innerHTML7 +=  createBox()+"</div></div>";
		innerHTML8 +=  createBox()+"</div></div>";
		innerHTML9 +=  createBox()+"</div></div>";
	innerHTML += innerHTML1 += innerHTML2 +=innerHTML3 +=innerHTML4 +=innerHTML5+=innerHTML6+=innerHTML7+=innerHTML8+=innerHTML9 ;
	innerHTML +=  createBox();
	innerHTML += "</div>";
	innerHTML += displayRepList();
	
	return innerHTML;
	

}
function displayRepList(){
	
	var innerHTML = '';var innerHTML1 = '';var innerHTML2 = '';var innerHTML3 = '';var innerHTML4 = '';var innerHTML5 = '';var innerHTML6 = '';var innerHTML7 = '';var innerHTML8 = '';var innerHTML9 = '';var innerHTML10 = '';var innerHTML11 = '';var innerHTML12 = '';var innerHTML13 = '';var innerHTML14 = '';var innerHTML15 = '';var innerHTML16 = '';var innerHTML17 = '';var innerHTML18 = '';var innerHTML19 = '';var innerHTML20 = '';
	var parti1Counter = 0;var parti2Counter = 0;var parti3Counter = 0;var parti4Counter = 0;var parti5Counter = 0;var parti6Counter = 0;var parti7Counter = 0;var parti8Counter = 0;var parti9Counter = 0;var parti10Counter = 0;var parti11Counter = 0;var parti12Counter = 0;var parti20Counter = 0;var parti14Counter = 0;var parti15Counter = 0;var parti16Counter = 0;var parti17Counter = 0;var parti18Counter = 0;var parti19Counter = 0;
	//inRepres.sort(sortByParti);
		//open the holder for each Fylke
		//<div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div>
		/*innerHTML += "<div id='repListWrapper' style='display:none;'>";
		innerHTML1 += "<div id='Fylke01' ><div class='partiHeadline' onclick='openFylkePanel(1);'><img src='../images/fylke/01_small.png' /><div class='fylkeLabel'>ØSTFOLD</div></div><div class='fylke1holder holder'>";
		innerHTML2 += "<div id='Fylke02' ><div class='partiHeadline' onclick='openFylkePanel(2);'><img src='../images/fylke/02_small.png' /><div class='fylkeLabel'>AKERSHUS</div></div><div class='fylke2holder holder'>";
		innerHTML3 += "<div id='Fylke03' ><div class='partiHeadline' onclick='openFylkePanel(3);'><img src='../images/fylke/3_1_small.png' /><div class='fylkeLabel'>OSLO</div></div><div class='fylke3holder holder'>";
		innerHTML4 += "<div id='Fylke04'><div class='partiHeadline' onclick='openFylkePanel(4);'><img src='../images/fylke/04_small.png' /><div class='fylkeLabel'>HEDMARK</div></div><div class='fylke4holder holder'>";
		innerHTML5 += "<div id='Fylke05'><div class='partiHeadline' onclick='openFylkePanel(5);'><img src='../images/fylke/05_small.png' /><div class='fylkeLabel'>OPPLAND</div></div><div class='fylke5holder holder'>";
		innerHTML6 += "<div id='Fylke06'><div class='partiHeadline' onclick='openFylkePanel(6);'><img src='../images/fylke/06_small.png' /><div class='fylkeLabel'>BUSKERUD</div></div><div class='fylke6holder holder'>";
		innerHTML7 += "<div id='Fylke07'><div class='partiHeadline' onclick='openFylkePanel(7);'><img src='../images/fylke/07_small.png' /><div class='fylkeLabel'>VESTFOLD</div></div><div class='fylke7holder holder'>";
		innerHTML8 += "<div id='Fylke08'><div class='partiHeadline' onclick='openFylkePanel(8);'><img src='../images/fylke/08_small.png' /><div class='fylkeLabel'>TELEMARK</div></div><div class='fylke8holder holder'>";
		innerHTML9 += "<div id='Fylke09'><div class='partiHeadline' onclick='openFylkePanel(9);'><img src='../images/fylke/09_small.png' /><div class='fylkeLabel'>AUST-AGDER</div></div><div class='fylke9holder holder'>";
		innerHTML10 += "<div id='Fylke10' ><div class='partiHeadline' onclick='openFylkePanel(10);'><img src='../images/fylke/10_small.png' /><div class='fylkeLabel'>VEST-AGDER</div></div><div class='fylke10holder holder'>";
		innerHTML11 += "<div id='Fylke11' ><div class='partiHeadline' onclick='openFylkePanel(11);'><img src='../images/fylke/11_small.png' /><div class='fylkeLabel'>ROGALAND</div></div><div class='fylke11holder holder'>";
		innerHTML12 += "<div id='Fylke12' ><div class='partiHeadline' onclick='openFylkePanel(12);'><img src='../images/fylke/12_small.png' /><div class='fylkeLabel'>HORDALAND</div></div><div class='fylke12holder holder'>";
		
		innerHTML14 += "<div id='Fylke14'><div class='partiHeadline' onclick='openFylkePanel(14);'><img src='../images/fylke/14_small.png' /><div class='fylkeLabel'>SOGN OG FJORDANE</div></div><div class='fylke14holder holder'>";
		innerHTML15 += "<div id='Fylke15'><div class='partiHeadline' onclick='openFylkePanel(15);'><img src='../images/fylke/15_small.png' /><div class='fylkeLabel'>MØRE OG ROMSDAL</div></div><div class='fylke15holder holder'>";
		innerHTML16 += "<div id='Fylke16'><div class='partiHeadline' onclick='openFylkePanel(16);'><img src='../images/fylke/16_small.png' /><div class='fylkeLabel'>SØR-TRØNDELAG</div></div><div class='fylke16holder holder'>";
		innerHTML17 += "<div id='Fylke17'><div class='partiHeadline' onclick='openFylkePanel(17);'><img src='../images/fylke/17_small.png' /><div class='fylkeLabel'>NORD-TRØNDELAG</div></div><div class='fylke17holder holder'>";
		innerHTML18 += "<div id='Fylke18'><div class='partiHeadline' onclick='openFylkePanel(18);'><img src='../images/fylke/18_small.png' /><div class='fylkeLabel'>NORDLAND</div></div><div class='fylke18holder holder'>";
		innerHTML19 += "<div id='Fylke19'><div class='partiHeadline' onclick='openFylkePanel(19);'><img src='../images/fylke/19_small.png' /><div class='fylkeLabel'>TROMS</div></div><div class='fylke19holder holder'>";
		innerHTML20 += "<div id='Fylke20' ><div class='partiHeadline' onclick='openFylkePanel(20);'><img src='../images/fylke/20_small.png' /><div class='fylkeLabel'>FINNMARK</div></div><div class='fylke20holder holder'>";*/
		innerHTML += "<div id='repListWrapper' style='display:none;'>";
		
		innerHTML1 += "<div id='Fylke01' ><div class='partiHeadline' onclick='openFylkePanel(1);'><div class='fylkeLabel'>ØSTFOLD</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke1holder holder'>";
		
		innerHTML2 += "<div id='Fylke02' ><div class='partiHeadline' onclick='openFylkePanel(2);'><div class='fylkeLabel'>AKERSHUS</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke2holder holder'>";
		
		innerHTML3 += "<div id='Fylke03' ><div class='partiHeadline' onclick='openFylkePanel(3);'><div class='fylkeLabel'>OSLO</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke3holder holder'>";
		
		innerHTML4 += "<div id='Fylke04'><div class='partiHeadline' onclick='openFylkePanel(4);'><div class='fylkeLabel'>HEDMARK</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke4holder holder'>";
		
		innerHTML5 += "<div id='Fylke05'><div class='partiHeadline' onclick='openFylkePanel(5);'><div class='fylkeLabel'>OPPLAND</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke5holder holder'>";
		
		innerHTML6 += "<div id='Fylke06'><div class='partiHeadline' onclick='openFylkePanel(6);'><div class='fylkeLabel'>BUSKERUD</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke6holder holder'>";
		
		innerHTML7 += "<div id='Fylke07'><div class='partiHeadline' onclick='openFylkePanel(7);'><div class='fylkeLabel'>VESTFOLD</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke7holder holder'>";
		
		innerHTML8 += "<div id='Fylke08'><div class='partiHeadline' onclick='openFylkePanel(8);'><div class='fylkeLabel'>TELEMARK</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke8holder holder'>";
		
		innerHTML9 += "<div id='Fylke09'><div class='partiHeadline' onclick='openFylkePanel(9);'><div class='fylkeLabel'>AUST-AGDER</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke9holder holder'>";
		
		innerHTML10 += "<div id='Fylke10' ><div class='partiHeadline' onclick='openFylkePanel(10);'><div class='fylkeLabel'>VEST-AGDER</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke10holder holder'>";
		
		innerHTML11 += "<div id='Fylke11' ><div class='partiHeadline' onclick='openFylkePanel(11);'><div class='fylkeLabel'>ROGALAND</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke11holder holder'>";
		
		innerHTML12 += "<div id='Fylke12' ><div class='partiHeadline' onclick='openFylkePanel(12);'><div class='fylkeLabel'>HORDALAND</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke12holder holder'>";
		
		innerHTML14 += "<div id='Fylke14'><div class='partiHeadline' onclick='openFylkePanel(14);'><div class='fylkeLabel'>SOGN OG FJORDANE</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke14holder holder'>";
		
		innerHTML15 += "<div id='Fylke15'><div class='partiHeadline' onclick='openFylkePanel(15);'><div class='fylkeLabel'>MØRE OG ROMSDAL</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke15holder holder'>";
		
		innerHTML16 += "<div id='Fylke16'><div class='partiHeadline' onclick='openFylkePanel(16);'><div class='fylkeLabel'>SØR-TRØNDELAG</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke16holder holder'>";
		
		innerHTML17 += "<div id='Fylke17'><div class='partiHeadline' onclick='openFylkePanel(17);'><div class='fylkeLabel'>NORD-TRØNDELAG</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke17holder holder'>";
		
		innerHTML18 += "<div id='Fylke18'><div class='partiHeadline' onclick='openFylkePanel(18);'><div class='fylkeLabel'>NORDLAND</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke18holder holder'>";
		
		innerHTML19 += "<div id='Fylke19'><div class='partiHeadline' onclick='openFylkePanel(19);'><div class='fylkeLabel'>TROMS</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke19holder holder'>";
		
		innerHTML20 += "<div id='Fylke20' ><div class='partiHeadline' onclick='openFylkePanel(20);'><div class='fylkeLabel'>FINNMARK</div></div><div class='miniBtnBar'><div onclick='toggleRepDisplay(this);' class='partiListBtn'></div><div onclick='toggleRepDisplay(this,1);' class='partiBildeBtn'></div></div><div class='fylke20holder holder'>";
		
	//parse 	
	$.each(inRepres , function() {
		
		
		if(this.CountyId  == 01 || this.CountyId   == '01'){
			parti1Counter ++; 
			innerHTML1 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML1 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti1Counter == 3){
				innerHTML1 +=  createBox();
				parti1Counter = 0;
			}
		}else if(this.CountyId   == 02  || this.CountyId  == '02'){
			parti2Counter ++; 
			innerHTML2 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML2 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti2Counter == 3){
				innerHTML2 +=  createBox();
				parti2Counter = 0;
			}
		}else if(this.CountyId   == 03 ||this.CountyId   == '03' ||this.CountyId   == 3 ||this.CountyId   == '3'){
		
			parti3Counter ++; 
			innerHTML3 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML3 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti3Counter == 3){
				innerHTML3 +=  createBox();
				parti3Counter = 0;
			}
		}else if(this.CountyId   == 04 || this.CountyId   == '04'){
			parti4Counter ++; 
			innerHTML4 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML4 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti4Counter == 3){
				innerHTML4 +=  createBox();
				parti4Counter = 0;
			}
		}else if(this.CountyId   == 05 || this.CountyId   == '05'){
			parti5Counter ++; 
			innerHTML5 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML5 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti5Counter == 3){
				innerHTML5 +=  createBox();
				parti5Counter = 0;
			}
		}else if(this.CountyId  == 06 || this.CountyId  == '06'){
			parti6Counter ++; 
			
			innerHTML6 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML6 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti6Counter == 3){
				innerHTML6 +=  createBox();
				parti6Counter = 0;
			}
		}else if(this.CountyId  == 07 ||this.CountyId   == '07'){
			parti7Counter ++; 
			innerHTML7 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML7 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			
			if (parti7Counter == 3){
				innerHTML7 +=  createBox();
				parti7Counter = 0;
			}
		}else if(this.CountyId  == 08 || this.CountyId   == '08'){
			parti8Counter ++; 
			innerHTML8 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML8 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti8Counter == 3){
				innerHTML8 +=  createBox();
				parti8Counter = 0;
			}
		}else if(this.CountyId   == 09 || this.CountyId   == '09'){
			parti9Counter ++; 
			innerHTML9 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML9 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti9Counter == 3){
				innerHTML9 +=  createBox();
				parti9Counter = 0;
			}
		}else if(this.CountyId  == 10 || this.CountyId   == '10'){
			parti10Counter ++; 
			innerHTML10 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML10 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti10Counter == 3){
				innerHTML10 +=  createBox();
				parti10Counter = 0;
			}
		}else if(this.CountyId   == 12  || this.CountyId  == '12'){
			parti12Counter ++; 
			innerHTML12 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML12 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti12Counter == 3){
				innerHTML12 +=  createBox();
				parti12Counter = 0;
			}
		}else if(this.CountyId   == 11 || this.CountyId   == '11'){
			parti11Counter ++; 
			innerHTML11 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML11 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti11Counter == 3){
				innerHTML11 +=  createBox();
				parti11Counter = 0;
			}
		}else if(this.CountyId   == 14 || this.CountyId   == '14'){
			parti14Counter ++; 
			innerHTML14 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML14 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti14Counter == 3){
				innerHTML14 +=  createBox();
				parti14Counter = 0;
			}
		}else if(this.CountyId   == 15 || this.CountyId   == '15'){
			parti15Counter ++; 
			innerHTML15 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML15 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti15Counter == 3){
				innerHTML15 +=  createBox();
				parti15Counter = 0;
			}
		}else if(this.CountyId   == 16 || this.CountyId   == '16'){
			parti16Counter ++; 
			
			innerHTML16 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML16 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti16Counter == 3){
				innerHTML16 +=  createBox();
				parti16Counter = 0;
			}
		}else if(this.CountyId  == 17 || this.CountyId   == '17'){
			parti17Counter ++; 
			innerHTML17 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML17 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			
			if (parti17Counter == 3){
				innerHTML17 +=  createBox();
				parti17Counter = 0;
			}
		}else if(this.CountyId  == 18 || this.CountyId  == '18'){
			parti18Counter ++; 
			innerHTML18 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML18 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti18Counter == 3){
				innerHTML18 +=  createBox();
				parti18Counter = 0;
			}
		}else if(this.CountyId  == 19 || this.CountyId  == '19'){
			parti19Counter ++; 
			innerHTML19 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML19 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti19Counter == 3){
				innerHTML19 += createBox();
				parti19Counter = 0;
			}
		}else if(this.CountyId == 20 || this.CountyId  == '20'){
			parti20Counter ++; 
			innerHTML20 += generateRepresentanterBildeFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			innerHTML20 += generateRepresentanterListFylke(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''),this.FirstName,this.LastName);
			if (parti20Counter == 3){
				innerHTML20 += createBox();
				parti20Counter = 0;
			}
		}
		
		
		
	});
		innerHTML1 += createBox() +"</div></div>";
		innerHTML2 += createBox() +"</div></div>";
		innerHTML3 += createBox() +"</div></div>";
		innerHTML4 += createBox() +"</div></div>";
		innerHTML5 += createBox() +"</div></div>";
		innerHTML6 += createBox() +"</div></div>";
		innerHTML7 += createBox() +"</div></div>";
		innerHTML8 += createBox() +"</div></div>";
		innerHTML9 += createBox() +"</div></div>";
		innerHTML10 += createBox() +"</div></div>";
		innerHTML11 += createBox() +"</div></div>";
		innerHTML12 += createBox() +"</div></div>";
		//innerHTML3 += "<div class='repTT'><div class='newlogoBox'></div><div class='repBox'><div class='repHeadline'><div class='repPartiLogo'></div><div class='repName'></div></div><div class='repContent'><div class='repBild'></div><div class='repInfo'></div></div></div></div></div></div>";
		innerHTML14 += createBox() +"</div></div>";
		innerHTML15 += createBox() +"</div></div>";
		innerHTML16 += createBox() +"</div></div>";
		innerHTML17 += createBox() +"</div></div>";
		innerHTML18 += createBox() +"</div></div>";
		innerHTML19 += createBox() +"</div></div>";
		innerHTML20 += createBox() +"</div></div>";
	innerHTML += innerHTML1 += innerHTML2 += innerHTML3 += innerHTML4 += innerHTML5 += innerHTML6 += innerHTML7 += innerHTML8 += innerHTML9 += innerHTML10 += innerHTML11 += innerHTML12 /*+=innerHTML13 */+= innerHTML14 += innerHTML15 += innerHTML16 += innerHTML17 += innerHTML18 += innerHTML19 += innerHTML20;
	innerHTML += createBox();
	innerHTML += "</div>";
	
	return innerHTML;
	

}
function getXCoordinateForMandateSeat(index) {
    return seatXCoordinates[index];
}
function getYCoordinateForMandateSeat(index) {
    return seatYCoordinates[index];
}
function createBox(){

	var innerHTML = "<div class='repTT '><div class='TT'><div class='newlogoBox'></div><div class='repBox'><div class='repHeadline'><div class='repPartiLogo'></div><div class='repName'></div></div><div class='repContent'><div class='repBild'></div><div class='repInfo'></div></div></div><div class='herrausforderer'></div></div></div>";
	return innerHTML;
}
function generateRepresentanterBilde(PartiKode,Picture,CandidateId,CountyId,repPos,isNew,firstname,Lastname){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
		if(isNew == true){
		
			extraMHTML += "<img src='../images/rep/grueneEckeBig.png'  />";
		}
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId)== CountyId){ // county fit
				if ( repPos == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( PartiKode  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='../images/rep/roteEckeBig.png' />";
					}
					if ( PartiKode  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='../images/rep/uManBig.png' />";
					}	
				}
				
			}
		}
		
	}
	extraMHTML +="</div>";
	var myURL = "../images/rep/big/"+(Picture != '' ? (Picture+'_org200.png') : 'dummy.jpg' );
	innerHTML += "<div class='repholder' id='candidate_"+CandidateId+"' ><div class='repImgBG' style='background-image:url("+myURL+");'>"+extraMHTML+"<div class='repImgBlend'></div><div class=' parti_"+PartiKode+" county_"+CountyId+"'  ></div></div></div>";
	return innerHTML;
}
function generateRepresentanterBildeFylke(PartiKode,Picture,CandidateId,CountyId,repPos,isNew,firstname,Lastname){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
		if(isNew == true){
		
			extraMHTML += "<img src='../images/rep/grueneEckeBig.png'  />";
		}
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == CountyId){ // county fit
				if ( repPos == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( PartiKode  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='../images/rep/roteEckeBig.png' />";
					}
					if ( PartiKode  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='../images/rep/uManBig.png' />";
					}	
				}
			}
		}
		
	}
	extraMHTML +="</div>";
	var myURL = "../images/rep/big/"+(Picture != '' ? (Picture+'_org200.png') : 'dummy.jpg' );
	innerHTML += "<div class='repholder' id='Fcandidate_"+CandidateId+"' ><div class='repImgBG' style='background-image:url("+myURL+");'>"+extraMHTML+"<div class='repImgBlend'></div><div class=' parti_"+PartiKode+" county_"+CountyId+"'  ></div></div></div>";
	return innerHTML;
}

function generateRepresentanterList(PartiKode,Picture,CandidateId,CountyId,repPos,isNew,firstname,Lastname){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
		if(isNew == true){
		
			extraMHTML += "<img style='width:20px;height:20px;margin-left:15px;' src='../images/rep/isNew.png'  />";
		}
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == CountyId){ // county fit
				if ( repPos == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( PartiKode  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='../images/rep/roteEcke.png' />";
					}
					if ( PartiKode  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='../images/rep/uMan.png' />";
					}	
				}
			}
		}
		
	}
	extraMHTML +="</div>";
	innerHTML = "<div id='listcandidate_"+CandidateId+"' class='listRep'><div class='listHeader'><div class='partiLogoHolder'><img style='margin-top: 9px;width:17px;height:17px;' src='../images/fylke/"+CountyId+"_small.png' /></div><div class='listRepName'>"+firstname+" "+Lastname+"</div>"+extraMHTML+"</div><div class='listRepContent'><div class='listRepBilde'><div class=' county_"+CountyId+" '  ></div></div><div class='listRepInfo'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com</div></div></div>";
	return innerHTML;
}
function generateRepresentanterListFylke(PartiKode,Picture,CandidateId,CountyId,repPos,isNew,firstname,Lastname){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
		if(isNew == true){
		
			extraMHTML += "<img style='width:20px;height:20px;margin-left:15px;' src='../images/rep/isNew.png'  />";
		}
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == CountyId){ // county fit
				if ( repPos == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( PartiKode  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='../images/rep/roteEcke.png' />";
					}
					if ( PartiKode  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='../images/rep/uMan.png' />";
					}	
				}
				
			}
		}
		
	}
	extraMHTML +="</div>";
	innerHTML = "<div id='liFycandidate_"+CandidateId+"' class='listRep'><div class='listHeader'> <div class='partiLogoHolder'><img style='margin-top: 9px;width:17px;height:17px;' src='../images/logo/parti"+PartiKode+".png' /></div><div class='listRepName'>"+firstname+" "+Lastname+"</div>"+extraMHTML+"</div><div class='listRepContent'><div class='listRepBilde'><div class=' county_"+CountyId+" '  ></div></div><div class='listRepInfo'></div></div></div>";
	return innerHTML;
}
function openPartiPanel(nr){

var containerHeight =  $('.parti'+nr+'holder').height();
	if (containerHeight > 0) {
		$('.miniBtnBar').attr('style','');
		$('.holder').attr('style','display:none;');
		
		//$('#Parti'+nr).attr('style',' ');
		$('.parti'+nr+'holder').animate({height:"0px"});
	}else {
		$('.holder').attr('style','display:none;');
		$('.miniBtnBar').attr('style','');
		$('#Parti'+nr+' .miniBtnBar').attr('style','visibility:visible;');
		$('.parti'+nr+'holder').attr('style','display:block;');
		$('.parti'+nr+'holder').animate({height:"1%"});
	}
	
	setTimeout(function(){scrollToButton(document.getElementById('Parti'+nr))},200);
}
function openFylkePanel(nr){
	var containerHeight =  $('.fylke'+nr+'holder').height();
	
	var strnr = ''+nr;
	if (strnr.length == 1){
		strnr = '0'+ strnr;
	}
	
	if (containerHeight > 0) {
		$('.miniBtnBar').attr('style','');
		$('.holder').attr('style','display:none;');
		//$('.fylke'+nr+'holder').attr('style','display:none;');
		//$('#Fylke'+nr).attr('style',' ');
		$('.fylke'+nr+'holder').animate({height:"0px"});
	}else {
		$('.holder').attr('style','display:none;');
		$('.miniBtnBar').attr('style','');
		$('#Fylke'+strnr+' .miniBtnBar').attr('style','visibility:visible;');
		$('.fylke'+nr+'holder').attr('style','display:block;');
		$('.fylke'+nr+'holder').animate({height:"1%"});
	}
	setTimeout(function(){scrollToButton(document.getElementById('Fylke'+strnr))},200);
}