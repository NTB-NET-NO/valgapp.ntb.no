﻿	var state; //global var for selected state - nation, county, municipality or search
	var region; //global var for selected region
	var regionName;
	var componentIdNation;
	var componentIdCounty;
	var componentIdMunicipality;
	var componentIdDistrict;
	var componentIdMy;
	
	var regionCodeNation = '06';
	var regionCodeNationPoll = '05';
	var regionCodeCounty = '04';
	var regionCodeDistrict = '03';
	var regionCodeMunicipality = '02';
	
	var isValg = false;
	var valgStart = new Date(2013,01,27); // januar = month 0
	//var valgStart = new Date(2013,08,27); // januar = month 0
	var idag = new Date();
	
	var countyElectionMode = true;
	var currentPollCollectionResults = {};
	var currentPollCollectionResultsM = {};
	var lastWeekPolls;
	var pollText = '';
	var isPollParse = false;
	var gotoSelectLastWeek = false;
	var mainButton = 'empty'; // to define mainbutton states
	var isFav = false; //to define if choosen kommune or county is added to fav
	var allRepres = {};
	var inRepres = [];
		countySpecialM = [];
	
	herausfordererMandat = [];
	//var dataDir = 'http://valgflash.ntb.no/prod/data/';		
	var dataDir = '../data/';		
	//var dataDir = 'http://valgflash.ntb.no/2009/prod/data/';
	function loadPage() {
		checkDate();
	    //set logo
	    var code = querySt();
		//loadRepres();
		//console.log('done here');
	    if (logoCodes[code.toUpperCase()]) {
	        var logoFileName = logoCodes[code.toUpperCase()];

	        if (!logoFileName)
	            logoFileName = "ntb.png";

	        var logoimg = document.getElementById('logo');
            if (logoimg)
                logoimg.src = '../logo/' + logoFileName;
        }else {
			logoFileName = "ntb.png";
			var logoimg = document.getElementById('logo');
			if (logoimg)
				logoimg.src = '../logo/' + logoFileName;
		}
		if(!isValg) {
			
			$('#valgBtnText').text('MÅLING');
			$('#valgBtn').addClass('notValg');
			$('#buttonNation').attr('onclick', 'toggleState("nationPolls")');
			$('#buttonConty').attr('onclick', 'toggleState("countyPolls")');
			$('#buttonMunicipality').attr('style', 'display:none');
		}
		
		
	$(".mMBtn").click(function() {
		$(".mMBtn").removeClass('activeMMBtn');
		if (mainButton == this.id){
			mainButton = 'empty';
			$(".mMBtnText").attr("style","display:table-row;");
			$(".contentDiv").attr("style","display:none;");
			$("#stemmerMeny").attr("style","display:none;");
			if(this.id == ''){
			
			}
		
		}else {
			$("#stemmerMeny").attr("style","display:none;");
			$(".contentDiv").attr("style","display:none;");
			mainButton = this.id;
			$(this).addClass('activeMMBtn');
			if(this.id == 'stortingBtn'){
				$("#mainCircle").attr("style","display:block;");
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'valgBtn'){
				$("#stemmerMeny").attr("style","display:table;");
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'buttonSearch'){
				$("#divSearch").attr("style","display:block;");
				$("#bottomBtnText").attr("style","display:none;");
				$("#upperBtnText").attr("style","display:table-row;");
			}
			
			if(this.id == 'favBtn'){
				$("#divFav").attr("style","display:block;");
				$("#bottomBtnText").attr("style","display:none;");
				$("#upperBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'duellenBtn'){
				$("#divDuell").attr("style","display:block;");
				$("#bottomBtnText").attr("style","display:none;");
				$("#upperBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'reprBtn'){
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				$("#divRepres").attr("style","display:block;");
			}
		}
		
	});
	$(".valgBtn").click(function() {
		$(".valgBtn").removeClass('active');
		$(this).addClass('active');
	});
	$("#mandatBlockBtn").click(function(){
		$("#mandatPartiBtn").removeClass('active');
		$("#mandatBlockBtn").addClass('active');
		$("#mandaterBlock").attr("style","display:block;");
		$("#mandaterParti").attr("style","display:none;");
	});
	$("#mandatPartiBtn").click(function() {
		$("#mandatBlockBtn").removeClass('active');
		$("#mandatPartiBtn").addClass('active');
		$("#mandaterParti").attr("style","display:block;");
		$("#mandaterBlock").attr("style","display:none;");
	});
	$("#divRepres").on("click",".repholder",function(e){
		$(".repholder").removeClass('activeRep');
		$(this).addClass('activeRep');
		openRepBox(this.id, this);
	});
	$("#divRepres").on("click",".listRep",function(e){
		$(".listRep").removeClass('activeRep');
		$(this).addClass('activeRep');
		openRepBoxList(this.id, this);
	});
	$("#divRepres").on("click",".repTT",function(e){
		$(".repholder").removeClass('activeRep');
		
		$(this).animate({height:"0px"});
		//var supernewObj = $(this).parent().parent();
			//$(supernewObj).animate({height: $(supernewObj).height() - 200 +'px'})
	});
	$("#blackOverlay").attr("style","display:none;");
		setTimeout(function(){window.scrollTo(0,1);},500);
	}
	function checkDate() {
	if (idag.getTime() > valgStart.getTime()) {
		isValg = true;
	}
}
	function loadRepres(){
	var myTempRep = {};
	var innerHTML= '';
	$.getJSON('../repData/candidates-min.json',function(data){
		allRepres = data;
		console.log("sucess");
		$("#blackOverlay").attr("style","display:none;");
		setTimeout(function(){window.scrollTo(0,1);},500);
		//return data;
	});
	}
	function openRepBox(repId,athis) {
	var innerHTML = '';
	var obj = athis;
	while ( repId.charAt(0) === 'F')
		repId = repId.substr(1);
	var newObj = $(obj).closest('div').nextAll('.repTT')[0];
	$('.'+$(newObj).attr('class')  +' .newLogoBox').removeClass('isNewImage');
	
	$.getJSON('../repData/'+repId+'_metainfo.json', function(data){
		
		
		if(data.ElectionYear == null ||data.ElectionYear == 'null' ||data.ElectionYear == ''  ||data.ElectionYear == '0' ||data.ElectionYear == 0 ){
			$('#'+$(newObj).attr('class')  +' .newlogoBox').addClass('isNewImage');
		}
		/** TODO add all the needed data once trond fixed my files **/
			$('.'+$(newObj).attr('class')  +' .repPartiLogo').addClass('partiLogo1').html("<img src='../images/logo/parti"+getPartiId(data.PoliticalPartyCode) +".png' />");;
			$('.'+$(newObj).attr('class')  +' .repName').html( data.FirstName.toUpperCase() + " " + data.LastName.toUpperCase() +"<br/>" +getPartyNameBySC(data.PoliticalPartyCode) +" I " + data.CountyName );
			
			$('.'+$(newObj).attr('class')  +' .repBild').html("<img src='../images/rep/"+((data.Picute != null) ? data.Picture : 'dummy.png' )+"' />");
			$('.'+$(newObj).attr('class')  +' .repInfo').html(' <div class="lineWrapper"><div class="repLeft">FØDT:</div><div class="repRight"> '+ data.DateOfBirth +'</div></div><div class="lineWrapper"> <div class="repLeft">STILLING:</div><div class="repRight"> '+ data.Occupation +'</div> </div><div class="lineWrapper"><div class="repLeft">BOSTED:</div><div class="repRight"> '+ data.HomeTown +'</div></div> <div class="blockWrapper"><div class="repFL"><span class="bold">Utdanning/Yrkesbakgrunn:</span> '+ data.Education +'</div> <div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.Description +'</div> <div class="repFL"><span class="bold">CandidateText:</span> '+ data.CandidateText +'</div> <div class="repFL"><span class="bold">E-Post:</span> '+ data.Email +'</div></div>');
			//var myHeight = $(newObj).height();
			
			//$(newObj).animate({height:myHeight+"px"});
			$(newObj).attr('style','display:block;');
			$(newObj).animate({height:"1%"});
			
		console.log(data);
		console.log("sucess rep");
	})
	.done(function(){console.log("second sucess  rep");})
	.fail(function(){console.log("error rep");})
	.always(function(){console.log("complete rep");});

}
function openRepBoxList(id, obj){
	var innerHTML = '';
	
	var newid = id.slice(4);
	$.getJSON('../repData/'+newid+'_metainfo.json', function(data){
		if(data.ElectionYear == null ||data.ElectionYear == 'null' ||data.ElectionYear == ''  ||data.ElectionYear == '0' ||data.ElectionYear == 0 ){
			//$('#'+$(newObj).attr('class')  +' .newlogoBox').addClass('isNewImage');
		}
		/** TODO add all the needed data once trond fixed my files **/
			//$('#'+id+' .listRepInfo .repPartiLogo').addClass('partiLogo1');
			//$('#'+id+' .listRepInfo .repName').html(' dummy name until files have the data');
			$('#'+id+' .listRepBilde').html("<img src='../images/rep/"+((this.Picute != null) ? this.Picture : 'dummy.png' )+"' />");
			$('#'+id+' .listRepInfo').html(' <div class="lineWrapper"><div class="repLeft">FØDT:</div><div class="repRight"> '+ data.DateOfBirth +'</div></div><div class="lineWrapper"> <div class="repLeft">STILLING:</div><div class="repRight"> '+ data.Occupation +'</div> </div><div class="lineWrapper"><div class="repLeft">BOSTED:</div><div class="repRight"> '+ data.HomeTown +'</div></div> <div class="blockWrapper"><div class="repFL"><span class="bold">Utdanning/Yrkesbakgrunn:</span> '+ data.Education +'</div> <div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.Description +'</div> <div class="repFL"><span class="bold">CandidateText:</span> '+ data.CandidateText +'</div> <div class="repFL"><span class="bold">E-Post:</span> '+ data.Email +'</div></div>');
			
			var myHeight = $('#'+id+' .listRepInfo').height();
			
			$('#'+id+' .listRepContent').animate({height:myHeight+"px"});
			//var supernewObj = $(newObj).parent().parent();
			//$(supernewObj).animate({height: $(supernewObj).height() + 200 +'px'})
		console.log(data);
		console.log("sucess rep");
	})
	.done(function(){console.log("second sucess  rep");})
	.fail(function(){console.log("error rep");})
	.always(function(){console.log("complete rep");});

}
	function toggleState(stateId, trueorfalse,polltext){
	if(polltext){	
		pollText = polltext;
		
	}else {
		pollText = '';
	}
		if (stateId == 'Mandater' || stateId == 'Duell'  ) {
		
			state = stateId;
		}else {
			removeCloseButtons();    
	    
			if (state != stateId)
	       // setCloseButton(stateId); BIANCA
		
			if (state == stateId)
				state = undefined;
			else
				state = stateId; /* 	Possible states: Nation, County, Municipality, Search, My */

			//var menuDiv = document.getElementById('stemmerMeny');
			//selectComponent( ((componentIdMunicipality)? componentIdMunicipality : 2) );
			var div = document.getElementById('div' + ((stateId == 'Polls' || stateId == 'nationPolls') ? 'Nation' : stateId));
			$('.contentDiv').attr('style','display:none');
			if (div)
			{
				if (div.style.display == 'block')
					div.style.display = 'none';
				else
					div.style.display = 'block';						
			}
			//menuDiv.style.display = 'none';
		}
		if (stateId == 'Nation') {
		    region = 'n';
			regionName = 'LANDSOVERSIKT';
			$('#buttonBarNation').attr('style','display:block;');
		    selectComponent(1); //select majority before refresh
		}
		else if (stateId == 'nationPolls'){
			state = 'Polls';
			selectNationPolls('true');
			$('#buttonBarNation').attr('style','display:block;');
		}
		else if (stateId == 'countyPolls'){
			
		}
		else if (state == 'Municipality') {
		$("#buttonBarMunicipality").attr('style','');
		$(".contentHeadline").html("VELG FYLKE");
		$('ul.municipalitySelect').empty();
		document.getElementById('divCountyMunicipalitySelect').style.display='block';
			if (mainButton == 'buttonSearch'){
				mainButton = 'valgBtn';
				$("#buttonSearch").removeClass('activeMMBtn');
				$("#divSearch").attr("style","display:none;");
				$("#valgBtn").addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
		    /*if (cookies_are_enabled())
		        //parseFavourites();	*/	    
		}
		else if (state == 'County') {
		$("#buttonBarCounty").attr('style','');
		$(".contentHeadline").html("VELG FYLKE");
		document.getElementById('divCountySelect').style.display='block';
			if (mainButton == 'buttonSearch'){
				mainButton = 'valgBtn';
				$("#buttonSearch").removeClass('activeMMBtn');
				$("#divSearch").attr("style","display:none;");
				$("#valgBtn").addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
		    /*if (cookies_are_enabled())
		        //parseFavourites();	*/	    
		}
		else if (state == 'Search')
		    resetSearch();
		else if (state == 'My') {
		    setLocation();
		    if (cookies_are_enabled())
		        parseFavourites();		    
		}
		else if (stateId == 'Mandater') {
			region = 'n';
			regionName = 'LANDSOVERSIKT';
			selectComponent(9);
		}
		else if (stateId == 'Duell') {
			region = 'n';
			regionName = 'LANDSOVERSIKT';
		    selectComponent(8); //select majority before refresh
		}
	    else if (stateId == 'Fav') {
		// scroll down to buttondiv
			//selectComponent(10);
			parseFavourites();
			//<div id="addToFav" onclick="addToFavourites();">add</div>
		}else if (stateId == 'Repres') {
			 region = 'n';
			selectComponent(10);
		}
		if (stateId == 'Mandater'){
			
			setTimeout(function () {scrollToButton(document.getElementById('mainCircle'))}, 100);;
		}else {
			setTimeout(function () {scrollToButton(document.getElementById('div' + state ))}, 100);;
		}
		//scrollToButton(document.getElementById( 'bottomMainMeny'));
	}
	
	function removeCloseButtons() {

	    var btnChild1 = document.getElementById('buttonMy');
	    if (btnChild1) {
	        var div1 = document.getElementById('closeDivMy');
	        if (div1) btnChild1.removeChild(div1);
	    }

	    var btnChild2 = document.getElementById('buttonNation');
	    if (btnChild2) {
	        var div2 = document.getElementById('closeDivNation');
	        if (div2) btnChild2.removeChild(div2);
	    }

	    var btnChild3 = document.getElementById('buttonCounty');
	    if (btnChild3) {
	        var div3 = document.getElementById('closeDivCounty');
	        if (div3) btnChild3.removeChild(div3);
	    }

	    var btnChild4 = document.getElementById('buttonMunicipality');
	    if (btnChild4) {
	        var div4 = document.getElementById('closeDivMunicipality');
	        if (div4) btnChild4.removeChild(div4);
	    }
	    
	    var btnChild5 = document.getElementById('buttonSearch');
	    if (btnChild5) {
	        var div5 = document.getElementById('closeDivSearch');
	        if (div5) btnChild5.removeChild(div5);
	    }
	    
	}

	function setCloseButton(state) {
	    var button = document.getElementById('button' + state);
		if (button == null) {
			
		}else {
	    //                <div style="float:right;padding-top:9px;padding-right:5px;"><img src="../images/kryss_480.png" /></div> 

	    var child = document.createElement('div');
	    child.id = "closeDiv" + state;
	    child.style.float = "right";
	    child.style.paddingTop = ((window.innerWidth > 320) ? '9' : '6') + "px";
	    child.style.paddingRight = "5px";
	    
	    var imgchild = document.createElement('img');
	    imgchild.src = "../images/kryss_" + ((window.innerWidth > 320) ? '480' : '320') + ".png";

	    child.appendChild(imgchild);
	    button.appendChild(child);
		}
	}
	
	function scrollToButton(obj) {		
	    if (obj) {
	        var y = obj.offsetTop;
			
	        while (obj = obj.offsetParent){
	            y += obj.offsetTop;
				
			}
			
	        setTimeout(function () { window.scrollTo(0, y); }, 200);	    
			
	    }
	}

	function resetSearch(){
		var searchBox = document.getElementById('searchBox');
		if (searchBox){
			searchBox.value = '';
			
			var searchResults= document.getElementById('searchResults');
			if (searchResults)
				searchResults.innerHTML = '';
			
			searchBox.focus();    	
		}	
	}
	
	function doSearch(){
		var searchResults= document.getElementById('searchResults');
		if (searchResults){
			searchResults.innerHTML = '';
			
			var searchBox = document.getElementById('searchBox');
			if (searchBox){				
				//everything is ok, perform search
				searchName(searchBox.value);
			}
		}	
	}
	
	
	function searchName(text) {
 		
 		var innerHTML = '';
 		var results = '';
 		var criteria = text.toLowerCase();
		var hitcount = 0;
		var regionsArray = subRegions.split('|');
		//var repsArray = countyLeaders; //TODO temporary dummy from 2011
		var counties = loadCounties(); 		
		if (criteria == ''){
			innerHTML += "<h3>Du må angi minst et søkeord.</h3><br/>";
  			innerHTML += 'Du kan søke på fylkesnavn, kommunenavn og kretsnavn i Oslo, Bergen, Stavanger og Trondheim. <br/>';
		}
		else{	
			innerHTML += "<div id='searchResSted'><div id='stedHL'>STED</div>";
			for (var i = 0; i < regionsArray.length; i++){			
  				//format: Name;type;id //Aust-Agder;4;9 //Fredrikstad;2;1_6 //Hersleb skole;3;3_1_101 //Rosenborg;3;16_1_6
  				var region = regionsArray[i];
  				var regionElements = region.split(';');
  			
				if (regionElements.length == 3 && (regionElements[0].toLowerCase().indexOf(criteria) > -1)){
			
					var regionType = region.charAt(region.indexOf(';') + 1)
					var regionIdRaw = region.substring(region.lastIndexOf(';') + 1);				
					var url = '';
			
					if (regionType == 2 || regionType == 3){
						//regionIdRaw = 12_65	or 1_6
						var regionIdElements = regionIdRaw.split('_');						
						
						if (regionIdElements.length == 2){
							var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];
							url = "toggleState('Municipality');refreshSubRegions('c" + countyId + "');selectMunicipality('" + regionIdRaw + "');";	
									
						}
					}
					else if (regionType == 4){												
						//regionIdRaw = 1 or 12
						var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;					
						url = "toggleState('County');selectCounty('c" + countyId + "');";
					}
					
  					results += "<div onclick=\"" + url + "\" class='searchResult'>" + regionElements[0];
  					
					/* switch regiontype
					4 = county
					2 = municipality
					3 = district
					*/	
					switch(regionType) {
	 					case('2'): //municipality
	 					{
	 						var countyId = region.substring(region.indexOf(';') + 3, region.indexOf("_"))
 	 						if (countyId == 3){ //only Oslo
	 							results += ' fylke/kommune';
	 						} 
	 						else{
	 							results += ' kommune i ' + counties[countyId];
	 						}
     						break;
     					}
     					case ('3'): //district
     					{
     						//switch municipalityid
     						switch(region.substring(region.indexOf(';') + 3, region.indexOf("_"))){
     							case ('3'):
     								results += ' (krets i Oslo)';
     								break;
     							case ('11'):
     								results += ' (krets i Stavanger)';
     								break;
     							case ('12'):
     								results += ' (krets i Bergen)';
     								break;
     							case ('16'):
     								results += ' (krets i Trondheim)';
     								break; 
     						} 
     						break;
     					}
     					case ('4'): //county
     					{
     						results +=' fylke'
     						break;
     					}
 					}   
		  				
  					results += "</div>";				  				
  					hitcount++;
  				}
 			}
			/**** representanter search ****/
			
 		
 			//innerHTML += '<h3>' + hitcount + ' treff på søk etter "' + text + '":</h3>';

 			//any hits?
 			if (hitcount == 0){
 				innerHTML += 'ingen treff';
  				//innerHTML += 'Du kan søke på fylkesnavn, kommunenavn og kretsnavn i Oslo, Bergen, Stavanger og Trondheim. <br/>';
 			}
 			else if (hitcount == 1){
 				//navigate directly to region?
 				innerHTML += results;
				innerHTML += "</div>";
 			}
 			else{
 				innerHTML += results;
				innerHTML +="</div>";
 			}
		
		}
		innerHTML += "<div id='searchResRep'><div id='repHL'>PERSON</div>";
	//TODO Results for PERSON
	
	$.each(allRepres, function(){
		if (this.FirstName && this.LastName){
		if(this.FirstName.toLowerCase().match('^'+criteria) || this.LastName.toLowerCase().match('^'+criteria) ){
		innerHTML += "<div class='searchResult'>"+ this.FirstName+" "+ this.LastName+" " +this.PartiKode+" "+getRegionName(this.CountyId)+"</div>";
		}
		}
	});
	innerHTML += "</div>";		
		document.getElementById("searchResults").innerHTML = innerHTML; 
 
		return false;
	}
	
	function loadCounties() {
 		//Parser fylkesnavn
 		var countyId = 1;
 		var counties = '&Oslash;stfold|Akershus|Oslo|Hedmark|Oppland|Buskerud|Vestfold|Telemark|Aust-Agder|Vest-Agder|Rogaland|Hordaland|Undef|Sogn og Fjordane|M&oslash;re og Romsdal|S&oslash;r-Tr&oslash;ndelag|Nord-Tr&oslash;ndelag|Nordland|Troms|Finnmark';
 		var countyArray = [];
 
 		while (counties.indexOf("|") > -1){
  			var pos = counties.indexOf("|");
  			countyArray[countyId] = counties.substring(0, pos);
  			counties = counties.substring(pos + 1, counties.length);
  			countyId++;
  		}
  		
  		return countyArray;
	}
	
	function closeAllOtherMenuItems(id){
		var divNation = document.getElementById('divNation');
		if (divNation && (divNation.id != id))
			divNation.style.display = 'none';
		
		var divCounty = document.getElementById('divCounty');
		if (divCounty && (divCounty.id != id))
			divCounty.style.display = 'none';
		if (divCounty && (divCounty.id == id))
			divCounty.style.display = 'block';

		var divMunicipality = document.getElementById('divMunicipality');
		if (divMunicipality && (divMunicipality.id != id))
			divMunicipality.style.display = 'none';
		if (divMunicipality && (divMunicipality.id == id))
			divMunicipality.style.display = 'block';

		var divSearch = document.getElementById('divSearch');
		if (divSearch && (divSearch.id != id))
		    divSearch.style.display = 'none';

		var divMy = document.getElementById('divMy');
		if (divMy && (divMy.id != id))
		    divMy.style.display = 'none';
			
		var divFav = document.getElementById('divFav');
		if (divFav && (divFav.id != id))
		    divFav.style.display = 'none';
	}

	function selectCounty(value, countyName, fillMuniDropDown,fillMuniId){
		region = value;
		
		countyName = countyName;
		
		//if called from searchresult
		if(fillMuniDropDown){
			//fillMunicipalityDropDown(region, fillMuniId);
			if (state == 'County'){
				//if (document.getElementById('countySelect').value != value)
			//document.getElementById('countySelect').value = value;
			countyElectionMode = true; //BIANCA ADD ountyelectionmodestuff
			region = 'c' +region;
			document.getElementById('divCountySelect').style.display='none';
			selectComponent(1);
			}else{
			
			selectMunicipality(region);
			}
		}
		else {
			//if (document.getElementById('countySelect').value != value)
			//document.getElementById('countySelect').value = value;
			countyElectionMode = true; //BIANCA ADD ountyelectionmodestuff
			document.getElementById('divCountySelect').style.display='none';
			selectComponent(1);
			$("#buttonBarCounty").attr('style','display:block;');
		}
		
	}
	
	function refreshSubRegions(value, obj){
		var selectedCountyId = value.substring(1,3);
		var regions = subRegions.split('|');
		$(".contentHeadline").html("VELG KOMMUNE:");
		//var regionsDiv = document.getElementById('municipalitySelect');
			    //var newObj = $(obj).closest('div').nextAll('.repTT')[0];
				$('ul.municipalitySelect').empty();
			var regionsDiv = $(obj).next('.municipalitySelect');
		//if called from searchresult
		if (document.getElementById('countyMunicipalitySelect').value != value)
			document.getElementById('countyMunicipalitySelect').value = value;		
		
		if (regionsDiv){
		    //regionsDiv.options.length = 0;  //clear select box
			//var initOpt = document.createElement('option');
			//regionsDiv.li.length =0;
			
			//var initOpt = document.createElement('li');
			//initOpt.text = 'Kommune';
			//initOpt.value = 0;
			//regionsDiv.add(initOpt);
			
			//Agdenes;2;16_22|Fredrikstad;2;1_6			
			for(var i=0;i < regions.length;i++){
				var region = regions[i].split(';');
				if (region.length == 3){
					var countyId = region[2].substring(0,2);					
					var type = region[1];
					
					//remove '_' and add leading zero
					if (countyId.substring(1) == '_')
						countyId = '0' + countyId.substring(0,1);			
								
					if (countyId == selectedCountyId && (type != 4)){									
						//var elOptNew = document.createElement('option');
						var elOptNew = document.createElement('li');
							if(type == 3){
								elOptNew.innerHTML = "- " +region[0].toUpperCase();
							}else {
								elOptNew.innerHTML = region[0].toUpperCase();
							}
						elOptNew.setAttribute('onclick','selectMunicipality("'+region[2]+'");');
				    	//elOptNew.value = region[2];
				    	try {
				      		//regionsDiv.add(elOptNew); // standards compliant; doesn't work in IE
							regionsDiv.append(elOptNew);
				    	}
				    	catch(ex) {
				      		//elSel.add(elOptNew, elSel.selectedIndex); // IE only
				    	}								
					}				
				}		
			}
		}
	}
	
	function selectMunicipality(value){
		// value = 1_5  
		var idElements = value.split('_');
		//if called from searchresult
		if (document.getElementById('municipalitySelect').value != value)
			document.getElementById('municipalitySelect').value = value;		
		
		$('#divCountyMunicipalitySelect').attr('style','display:none;');
		if (idElements.length >= 2){
			var countyId = idElements[0];
			var municipalityId = idElements[1];
			var id;

			if (countyId.length == 1) {
			    countyId = '0' + countyId;
			}
		
			if (municipalityId.length == 1)
				municipalityId = '0' + municipalityId;
		
			id = countyId + municipalityId;

			if (countyId == '03' && idElements.length > 2)
			    countyId = '00';
		    
			id = 'c' + countyId + 'm' + id;

			var districtId = '';
		    if (idElements.length > 2) { //district
		        districtId = idElements[2];
		        for (var i = districtId.length; i < 4; i++)
		            districtId = "0" + districtId;
		    }

		    region = id + ((districtId == '') ? '' : 'd' + districtId);
			
			//regionName = municipalityName;
			countyElectionMode = false;
			
			selectComponent( ((componentIdMunicipality)? componentIdMunicipality : 2) );	
				$("#buttonBarMunicipality").attr('style','display:block;');
		}		
	}
	
	function selectComponent(id){

	if (id != 9 && id != 8 && id != 10){ 
		if ((id == 2 || id == 1) && (state == 'Municipality' || state == 'My')) {
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' + state).className.replace( /(?:^|\s)active(?!\S)/g , '' );
			document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + state).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		
		}else {
		document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' +(state == 'Polls' ? 'Nation' : state)).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		}
	}
	    if (id == 1) {
		if (state == 'Municipality' || state == 'My') {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
			
		//        document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmerbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";
		//        document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/mandaterbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		    }
		    else {
		//    document.getElementById('buttonBarButton1_' + state).style.backgroundImage = "url('../images/blokk_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
			document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';	
			document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>MANDAT</span>';	
		//    document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmer_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//    document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/repr_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";	        
		}}
		else if (id == 2) {
		    if (state == 'Municipality' || state == 'My') {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
				
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';
		   }
		    else {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';	
				document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>MANDAT</span>';	
		    }
		}
		else if (id == 3) {
		    if (state == 'Municipality' || state == 'My') {
		        
		        if (region.length < 9) {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';
		//            document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmerbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//            document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/mandaterbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";		            
		        }
		    }
		    else{
			document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
			  document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';	
			  document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>MANDAT</span>';	
		//        document.getElementById('buttonBarButton1_' + state).style.backgroundImage = "url('../images/blokk_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//        document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmer_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//        document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/repr_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";		        
		    }
		}	    
	   
		if (state == 'Nation' || state == 'nationPolls'||state == 'Polls' ){
			componentIdNation = id;
			
			reloadContent('divNationData');
		}
		else if (state == 'County'){
			componentIdCounty = id;
			reloadContent('divCountyData');

        }
        else if (state == 'Municipality') {	
	    
		    if (!(id == 3 && region.length > 8)) {
		        componentIdMunicipality = id;
		        reloadContent('divMunicipalityData');		        
		    }else {componentIdMunicipality = id;
		        reloadContent('divMunicipalityData');
			}
        }
	    else if (state == 'My') {
		    componentIdMy = id;
		    reloadContent('divMyData');
		}
		else if (state == 'Mandater'){
		
			reloadContent('mainCircle');
		}
		else if (state == 'Duell') {
		
			reloadContent('divDuellData');
		}
		else if (state == 'Fav') {
			
		}else if (state == 'Repres'){
			reloadContent('divRepres');
			console.log('reloadcontent');
		}	
	}	

	function reloadContent(divId){
		var filePrefix = null;
		var componentId = null;
		var regionCode = null;
		////BIANCA ADD POLL
		/*if (!isValg){
			if (region == 'n')
				filePrefix = 'P05';
			else if (region.length == 3) //county
				filePrefix = 'P04';
			else { //municipality
				if (region == 'c00m0301') //Oslo
					filePrefix = 'P04';
				else
					filePrefix = 'P02';
			}
		}
		else {*/
		if (state == 'Nation' || state == 'nationPolls' || state == 'Polls'){
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = (isValg) ? regionCodeNation :  regionCodeNationPoll;;
			componentId = componentIdNation;
		}
		else if (state == 'County'){
			filePrefix = (!isValg) ? 'P'  + regionCodeCounty : (countyElectionMode) ? 'ST' + regionCodeCounty : 'ST'+ regionCodeCounty;
			regionCode = regionCodeCounty;
			
			componentId = componentIdCounty;
		}
		else if (state == 'Mandater'){
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = regionCodeNation;
			componentId = 9;
		}
		else if (state == 'Duell') {
			filePrefix = (!isValg) ? 'P' :'ST';
			filePrefix += (!isValg) ?regionCodeNationPoll :regionCodeNation;
			regionCode = (!isValg) ?regionCodeNationPoll :regionCodeNation;
			componentId = 8;
		}
		else if (state == 'Municipality'){
			filePrefix =	(!isValg) ? 'P' :'ST';
			filePrefix += ( (region.length > 8) ? regionCodeDistrict : regionCodeMunicipality);
			regionCode = ((region.length > 8) ? regionCodeDistrict : regionCodeMunicipality);
			componentId = componentIdMunicipality;
		}
		else if (state == 'District'){
			filePrefix = 'ST' + regionCodeDistrict;
			regionCode = regionCodeDistrict;
			componentId = componentIdDistrict;
		}else if (state == 'Repres' ){
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = (isValg) ? regionCodeNation :  regionCodeNationPoll;;
			componentId = 10;
		}

		if (filePrefix && componentId && regionCode){
		    var dataFile = dataDir + filePrefix + '-' + region + '.txt?m=' + Math.random();

			var xmlhttp;
			if (window.XMLHttpRequest){
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else{
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

		    xmlhttp.onreadystatechange = function() {

		        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
		            parseText(xmlhttp.responseText, divId, componentId, regionCode);
		        }
		        else if (xmlhttp.status == 404) {
		            //not found		 		
		            document.getElementById(divId).innerHTML = "<div style='width:320px;text-align:center;'>Beklager ingen data funnet</div>";
		        }
		    };

		    xmlhttp.open("GET", dataFile, true);


		    try {
		        xmlhttp.send();	
		        
		    } catch(e) {
		        document.getElementById(divId).innerHTML = "<div style='width:100%;text-align:center;'>Det oppsto en feil, browseren din støtter ikke henting av datafiler fra annet domene<br/>" + e.toString() + "</div>";
		    } 
		}
	}

	function addToFavourites() {
	    var favourites = prefs.load();
	    //finne ut om ligger der fra før!
	    if ($.isArray(favourites)) {
	        var newFav = { name: getRegionName(region), id: region };
	        if (!eksistsInArray(newFav, favourites))
	            favourites.unshift(newFav);
	    }
	    else {
	        favourites = [{ name: getRegionName(region), id: region}];
	    }

	    prefs.data = serialize(favourites);
	    prefs.save('valg2013Fav');

	    //parseFavourites(); BIANCA REMOVE INSTEAD CHANGE BUTTON
		document.getElementById('isNotFavBtn').id = 'isFavBtn';
	}

	function removeFromFavourites(id) {
	    var favourites = prefs.load();

	    if ($.isArray(favourites)) {

	        for (var i = 0; i < favourites.length; i++) {
	            var fav = favourites[i];
	            if (fav.id == id) {
	                favourites.splice(i, 1);
	                break;
	            }
	        }

	        prefs.data = serialize(favourites);
	        prefs.save('valg2013Fav');
	        parseFavourites();
	    }
	}

	function goToFavourite(id, name) {
	    region = id;
		mainButton = '';
		$(".mMBtn").removeClass('activeMMBtn');
		
	    if (state == 'My') {
	        document.getElementById('myDataHeader').innerHTML = name;
	        selectComponent(componentIdMy);	        
	    }
	    else if (state == 'Municipality'  || state == 'Fav') {
	        var countyid = region.substring(0, 3);
	        var muniid = region.substring(4);
	        //refreshSubRegions(countyid);
			alert(muniid.length+"   "+muniid);
	        if (muniid.length == 0) {
				state = 'County';alert(state);
				selectCounty(countyid);
				$('#valgBtn').addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				mainButton = 'valgBtn';
				closeAllOtherMenuItems('div' + state);
				
			}
			
	        else if (muniid.length == 4) {
				state = 'Municipality';
	            var subCountyId = muniid.substring(0, 2);
	            var subMuniId = muniid.substring(2);

	            if (subCountyId.length == 2 && subCountyId.substring(0, 1) == '0')
	                subCountyId = subCountyId.substring(1);

	            if (subMuniId.length == 2 && subMuniId.substring(0, 1) == '0')
	                subMuniId = subMuniId.substring(1);

	            muniid = subCountyId + "_" + subMuniId;
	            selectMunicipality(muniid);
				$('#valgBtn').addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				mainButton = 'valgBtn';
				closeAllOtherMenuItems('div' + state);
	        }
	        else {
				state = 'Municipality';
	            var subCountyId = muniid.substring(0, 2);
	            var subMuniId = muniid.substring(2,4);
				var subdisId = muniid.substring(5);
	            if (subCountyId.length == 2 && subCountyId.substring(0, 1) == '0')
	                subCountyId = subCountyId.substring(1);

	            if (subMuniId.length == 2 && subMuniId.substring(0, 1) == '0')
	                subMuniId = subMuniId.substring(1);
					
				if (subdisId.length == 4 && subdisId.substring(0, 1) == '0')
	                subdisId = subdisId.substring(1);
					
				muniid = subCountyId + "_" + subMuniId +"_"+subdisId;
				selectMunicipality(muniid);
				$('#valgBtn').addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				mainButton = 'valgBtn';
				closeAllOtherMenuItems('div' + state);
			}
	       //selectComponent(componentIdMunicipality);	        
	    }

	    // scroll down to buttondiv
	    scrollToButton(document.getElementById('div' + state));
	}

var _id;
function showMainCircle(id){
	_id = id;
	//document.getElementById('blackOverlay').style.display='block';
	document.getElementById('mainCircle').style.display='block';


    var circleCurrent = document.getElementById('circleCurrent');
	if (circleCurrent){
		var text = document.getElementById('circleCurrentText');
		var background = '';
		if (_id == 'votes'){
			background = "#F5F5F5 url('../images/pad/votes.png') no-repeat 37.5px 30px";
			if (text)
				text.innerHTML = "SISTE M&Aring;LING";
		}
		else if (_id == 'mandates'){
		//just for testing
		region = 'n';
			if (text){
				if (region == 'n'){			
					background = "#F5F5F5 url('../images/pad/mandates.png') no-repeat 33.5px 30px";
					text.innerHTML = "MANDATER";
				}
				else{
					background = "#F5F5F5 url('../images/pad/representatives.png') no-repeat 40px 16px";					
					text.innerHTML = "REPRESENTANTER";
				}
			}
		}
		else if (_id == 'history'){
			background = "#F5F5F5 url('../images/pad/history.png') no-repeat 27px 30px";					
			if (text)
			    text.innerHTML = "HISTORIKK";

		    openHistory();
		}
		if (_id == 'table') {
		    background = "#F5F5F5 url('../images/pad/table.png') no-repeat 23px 30px";
		    if (text)
		        text.innerHTML = "TABELL";
		}
		if (_id == 'parties') {
		    background = "#F5F5F5 url('../images/pad/parties.png') no-repeat 38.5px 30px";
		    if (text)
		        text.innerHTML = "PARTIER";
		}   	    
	    
		circleCurrent.style.background = background;	
		circleCurrent.style.display='block';
	}
	
	toggleHeaders();
	fadeInMainCircle();
}

function fadeInMainCircle(){
	$("#mainCircle").animate(
	{
		width: '678px', height: '678px', 'margin-left': '-339px', 'margin-top': '-339px'
	}, 500, finishedFadeInMainCircle);
}

function finishedFadeInMainCircle(){
	$("#" + _id).animate({opacity:1}, 400);
	$("#mainCircleHeaderText").animate({opacity:1}, 400);
	$("#mainCircleRegionText").animate({opacity:1}, 400);
	$("#mainCircleDescriptionText").animate({opacity:1}, 400);

	var pollPanelSelect = document.getElementById("pollSelect_" + _id);	
	if (pollPanelSelect){
		
		if (_id == 'votes' || _id == 'mandates')
			pollPanelSelect.style.display = "block";
		else
			pollPanelSelect.style.display = "none";			
	}
	
	$("#circleCurrent").animate({opacity:1}, 400);
	$("#divCloseButton").animate({opacity:0.8}, 400, animateMainComponent);
	
}

function hideMainCircle(){	
	$("#" + _id).animate({opacity:0}, 400);
	$("#mainCircleHeaderText").animate({opacity:0}, 400);
	$("#mainCircleDescriptionText").animate({opacity:0}, 400);
	$("#mainCircleRegionText").animate({opacity:0}, 400);
	$("#circleCurrent").animate({opacity:0}, 400);	
	$("#divCloseButton").animate({opacity:0}, 400, fadeOutMainCircle);
}

function fadeOutMainCircle(){
	$("#mainCircle").animate(
	{
		width: '0px', height: '0px', 'margin-left': '0px', 'margin-top': '0px'
	}, 500, function (){document.getElementById('blackOverlay').style.display='none';document.getElementById('mainCircle').style.display='none';document.getElementById('circleCurrent').style.display='none'});	
	
}
function fadeOutMainPanel(){
	$("#mainPanel").animate({opacity: 0}, 1, function(){toggleCircleButtons();});
}
function fadeInMainPanel(){

	$("#mainPanel").animate({opacity: 1}, 1000);
}


/* -------- Animation for components --------*/
function animateMainComponent(){

	switch (_id){
		case 'votes':
			animateVotesComponent();
			break;
		case 'mandates':
			animateMandatesComponent();
			break;
		case 'history':
			break;
		case 'parties':
			break;
		case 'table':
			break;
	}
}

function animateVotesComponent(){

}
function animateMandatesComponent(){

}

/* --------Animation for votes counted component --------*/
var t;
var maxValue = 0;
var startValue = 0;
var nrOfLoops = 10;
var increment = 0;
function animateVotesCounted(lines){

	var percentCounted = parseFloat(lines[4]);
	var widthEmpty = (100 - percentCounted);	
	
	maxValue = percentCounted;
	startValue = 0;
	increment = maxValue / nrOfLoops;
	//alert(nrOfLoops + ' - ' + increment + ' - ' + maxValue + ' - ' + startValue );
	
	clearInterval(t);
	//t = setInterval(animate, 20);
}
function animate() {
    startValue += increment;
	if(startValue < maxValue) {
	    var fillDiv = document.getElementById('votesCountedFilled');
	    if (fillDiv) {
	        fillDiv.style.width = startValue.toString() + "%";
		}
	} 
	else {
		clearInterval(t); 
	}	
}
function toggleHeaders(){

	var mainCircleRegionText = document.getElementById("mainCircleRegionText");
	var mainCircleDescriptionText = document.getElementById("mainCircleDescriptionText");
    var mainCircleHeaderText = document.getElementById('mainCircleHeaderText');
    
	if (mainCircleRegionText && mainCircleDescriptionText && mainCircleHeaderText){
	    mainCircleHeaderText.innerHTML = '';
	    mainCircleRegionText.innerHTML = '';
		mainCircleDescriptionText.innerHTML = '';

		if (state == 'Polls') {
		    mainCircleHeaderText.innerHTML = 'MENINGSM&Aring;LING';

			if (region == 'n'){
				if (_id == 'votes'){
					mainCircleDescriptionText.innerHTML = 'Partienes oppslutning basert p&aring; landsdekkende m&aring;linger';
				}
				else if (_id == 'mandates'){
					mainCircleDescriptionText.innerHTML = 'Tenkt Storting basert p&aring; siste ti landsdekkende m&aring;linger';
				}
				else if (_id == 'history'){
					mainCircleDescriptionText.innerHTML = 'Gjennomsnitt siste 10 landsdekkende m&aring;linger';
				}
			}
			else{
				if (_id == 'votes'){
					mainCircleDescriptionText.innerHTML = 'Partienes oppslutning basert p&aring; lokale m&aring;linger';
					mainCircleRegionText.innerHTML = regionName.toUpperCase();
				}
				else if (_id == 'mandates'){
					mainCircleDescriptionText.innerHTML = 'Mandataprognose basert p&aring; lokale m&aring;linger';
					mainCircleRegionText.innerHTML = regionName.toUpperCase();
				}
			}
        }
	    else {
		    if (_id == 'votes') {
                if (state == 'Nation') {
                    mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
                    mainCircleDescriptionText.innerHTML = 'Basert p&aring; fylkestingsvalget';
                    }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
                }
		        
		    }
		    else if (_id == 'mandates') {
		        if (state == 'Nation') {
					
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		            mainCircleDescriptionText.innerHTML = 'Tenkt Storting basert p&aring; fylkestingsvalget';
		        }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		           //TODO mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		        }
		    }
		    else if (_id == 'history') {
		        if (state == 'Nation') {
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		            mainCircleDescriptionText.innerHTML = 'Historikk: Partienes oppslutning i fylkestingsvalg';
		        }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		            mainCircleDescriptionText.innerHTML = 'Historikk: Partienes oppslutning i ' + ((countyElectionMode) ? 'fylkestingsvalg' : 'kommunevalg');
		        }
		        
		    }
		    else if (_id == 'parties') {
                if (state == 'Nation')
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		        else
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		    }
		    else if (_id == 'table') {
		        if (state == 'Nation') {
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		            mainCircleDescriptionText.innerHTML = 'Partienes oppslutning i fylkestingsvalg';
		        }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		        }		        
		    }
		}
	}
}
function gotoSelectLastWeekPoll(value, pollText){
	gotoSelectLastWeek = true;
	//value = ID-1200;20110629;kommune;5_28
	//pollText = Kommunestyrevalg i Østre Toten (Sentio for Oppland Arbeiderblad)
	var hL = pollText;
	var pollElements = value.split(";");
	if (pollElements.length == 4){
		var pollId = pollElements[0];
		var pollDateRaw = pollElements[1];
		var pollType = pollElements[2];
		var regionIdRaw = pollElements[3];
		//isPollParse = true;		
		//fix url
		switch (pollType){
			case 'kommune':
				//regionIdRaw = 12_65	or 1_6
				//regionIdRaw = 0 special case - spurt om kommunevalg i hele landet
				if (regionIdRaw == '0'){
					pollMenuMunicipalities.selectedIndex = 0;
					pollMenuCounties.selectedIndex = 0;
					selectNationPolls(false);					
				}
				else{
					var regionIdElements = regionIdRaw.split('_');											
					if (regionIdElements.length == 2){
						var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];							
						if (countyId == '03') 
							countyId = '00';						
						toggleState('Municipality', false, hL);
						pollMenuCounties.selectedIndex = ((regionIdElements[0] > 12)? regionIdElements[0] - 1:regionIdElements[0]);
						//state = 'Municipality';
						document.getElementById('divNation').style.display='none';
						selectCounty(regionIdRaw , '', true);
						
						var index = getIndexFromValue(regionIdElements[0], regionIdElements[1]);
						pollMenuMunicipalities.selectedIndex = index;
						//selectPollRegion();						
					}					
				}				
				break;
			case 'fylke':
				//regionIdRaw = 1 or 12
				var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;					
				toggleState('County', false, hL);
				pollMenuMunicipalities.selectedIndex = 0;
				pollMenuCounties.selectedIndex = ((regionIdRaw > 12)? regionIdRaw - 1:regionIdRaw);
				document.getElementById('divNation').style.display='none';
				selectCounty(countyId , '', true);
				//selectPollRegion();				
				break;
			case 'riks':
				pollMenuMunicipalities.selectedIndex = 0;
				pollMenuCounties.selectedIndex = 0;
				selectNationPolls(false);					
				break;
		}
	}
}
function getIndexFromValue(countyValue, value){
	var index = 0;
	var regionsDropDown = document.getElementById('pollMenuMunicipalities');
	if (regionsDropDown){
		for (var i = 0; i < regionsDropDown.options.length; i++){
			var option = regionsDropDown.options[i];
			
			if (option.value == countyValue + '_' + value){
				index = i;
				break;
			}
		}	
	}	
	
	return index;
}
function selectPoll(id, index, componentId,headline) {
	isPollParse = true;
	console.log(index);
    var partyResultsRaw = currentPollCollectionResults[id];
	var partyResultsRawM = currentPollCollectionResultsM[id];
    var partyResults = "";
	var partyResultsM = "";
    console.log(partyResultsRaw);
    if (partyResultsRaw){
    	partyResults = partyResultsRaw.split('|').clean("\r");;
		
    
	}
    else{
    	
    	index = -1;
    }
	console.log(partyResults);
	console.log(index);
	console.log(state);
	var component ;
    if (state == 'Nation' || state == 'Storting' || state == 'Polls'){
            regionCode = '05';
			component = document.getElementById('divNationData');
		}
        else if (state == 'County'){
            regionCode = '04';
			component = document.getElementById('divCountyData');
		}
        else if (state == 'Municipality'){
			component = document.getElementById('divMunicipalityData');
            regionCode = '02';
			}
    //TODO
   // var component = document.getElementById(componentId);
	//component = document.getElementById('divMunicipalityData');
	
    if (component){    
       	switch (componentId){
       		case 'votes':	
				console.log(partyResults);
       			component.innerHTML = parseColumns(partyResults,regionCode, index,headline);
       			break;
       		case 'blokk': //only used for seatsrepresentatives
       			component.innerHTML = parseMajority(partyResultsRawM, index,headline);
       			break;
			case 'mandates': //only used for seatsrepresentatives
       			component.innerHTML = parseMandates(partyResults,regionCode, index,headline);
       			break;
       	}
       	
       	var select = document.getElementById("pollSelect_" + componentId);
        if (select) select.style.display = "block";
	}
}
function selectPollRegion() {
	
    if (pollMenuMunicipalities.selectedIndex != 0) {
        selectMunicipality(pollMenuMunicipalities.value, pollMenuMunicipalities.options[pollMenuMunicipalities.selectedIndex].text);        
    }
    else if (pollMenuCounties.selectedIndex != 0) {
        selectCounty(pollMenuCounties.value, pollMenuCounties.options[pollMenuCounties.selectedIndex].text, false);        
    }

    //closePollMenu(); // BIANCA FIX
	
	
}
function selectNationPolls(toggle){

	region = 'n';
	regionName = 'LANDSOVERSIKT';
	//reloadContent('divNationData');
	selectComponent(1);
	//if (toggle)
	//    toggleState('Polls', false); //will close/open menu
}
function toggleRepDisplay(obj,nr){
	if (nr == 1){
		$('.listRep').attr('style','display:none;');
		$('.repholder').attr('style','display:inline-block;');		
	}else {

		$('.repholder').attr('style','display:none;');
		$('.listRep').attr('style','display:block;');
	}
}
function toggleRepres(obj) {
	$(".storBtn").removeClass('active');
	if(obj.id == 'represFylkeBtn'){
		$("#represFylkeBtn").addClass('active');
		$('#repWrapper').attr('style','display:none;');
		$('#repListWrapper').attr('style','display:block;');
	}else {
		$("#represPartiBtn").addClass('active');
		$('#repWrapper').attr('style','display:block;');
		$('#repListWrapper').attr('style','display:none;');
	}
}