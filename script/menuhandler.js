/*** Menu ***/
var state; //global var for selected state - nation, county, municipality or search
var region;
//var regionName;
var homePage;
var countyElectionMode = true;
var stateCheck;


function toggleState(stateId, open){
	stateCheck = '';
	/* 	Possible states: 
		Polls, Nation, County, Municipality, Valg, 
		Repr, duellen, Fav, sok 
	*/
	idag = new Date();
	checkDate();
	var d = new Date();

	$("#kartMenu").accordion('destroy').accordion({heightStyle: "content"});
	
	//$(".divMainComponent").attr("style","display: none;");	TODO: marked it out for testing
	var div = document.getElementById('topmenuButton' + stateId);		
	/*if (div){ // bianca removed
		div.style.backgroundColor = '#D0D0D0';
		
		var link = document.getElementById(div.id + 'Link');
		if (link) link.style.color = '#000000';
	}*/
	if ($(div).hasClass('activeMMBtn')){
	
	}else {
		state = stateId;
	}
	deselectAllOtherMenuItems('topmenuButton' + stateId);
	

	//try dat here
	if (pollMenuOpen())
			closePollMenu();
		
	$(".underMenu").attr("style","display: none;");
	
	if (stateId == 'Polls'){
		$('.button').removeClass('active');
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		//$("#wrapperUnderMenu").attr("style","display:block;");
		$("#pollUnderMenu").attr("style","display:table;");	
		$("#circleContainer").attr("style","display:table;");
		
		//var headerHeight = $("#header").height();
		//var menyHeight = $("#wrapperUnderMenu").height() + headerHeight
		var headerHeight = $("#header").height();
		var menyHeight = 134 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'134px'}, {duration:300,queue:false});
		});
		
		//$("#mainCircle").attr('style','top:' + menyHeight +'px;');
		
		
		
		if (($("#topmenuButtonCountyPollLink").html() != 'FYLKESVIS') ){
			$("#topmenuButtonCountyPoll").addClass("active");
		}else {
			$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
			$("#circleContainer").addClass("disabled");
			toggleState('nationPolls');
		}
		
	}
	else if (stateId == 'nationPolls'){
	$("#topmenuButtonNationPoll").addClass("active");
	$(".circle").removeClass("disabled");
	//$('#kartModulWrapper').svg('destroy');
		state = 'Polls';
		region = 'n';
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$("#pollUnderMenu").attr("style","display:table;");
		$("#circleContainer").attr("style","display:table;");
		var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
		$("#circleTable").addClass("disabled");
		if (window.innerWidth <= 750){
			//$('#kartModulWrapper').svg({loadURL: 'images/kart/map3b.svg',onLoad: mySvgLoaded});
		}else {
			//$('#kartModulWrapper').svg({loadURL: 'images/kart/map3b.svg',onLoad: mySvgLoaded});
		}
		reloadContent();
	}
	else if (stateId == 'countyPolls'){
	$('.button').removeClass('active');
	$('#topmenuButtonSok').removeClass('activeMMBtn');
	$("#topmenuButtonPolls").addClass("activeMMBtn");
	$("#topmenuButtonCountyPoll").addClass("active");
		$(".circle").removeClass("disabled");
		$("#circleTable").addClass("disabled");
		$("#circleParties").addClass("disabled");
		$("#circleHistory").addClass("disabled");
		$("#pollUnderMenu").attr("style","display:table;");	
		$("#pollMenuCounty").attr("style","display:block;");
		$("#circleContainer").attr("style","display:table;");
		/*var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;

		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		
		var headerHeight = $("#header").height();
		var menyHeight = 249 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'249px'}, {duration:300,queue:false});
		});
		
		$("#circleKart").addClass("disabled");
		$("#circleContainer").addClass("disabled");
	}
	else if (stateId == 'Nation'){
	state = 'Nation';
	$('.button').removeClass('active');
	$("#topmenuButtonNation").addClass("active");
	//$('#kartModulWrapper').svg('destroy');
	
	$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
	$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
	$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
	$(".circle").removeClass("disabled");	
		region = 'n';
		regionName = 'LANDSOVERSIKT';
	    countyElectionMode = true;
		$(".underMenu").attr("style","display: none;");
		$("#valgUnderMenu").attr("style","display:table;");
		$("#circleContainer").attr("style","display:table;");
		/*var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		var headerHeight = $("#header").height();
		var menyHeight = 134 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'134px'}, {duration:300,queue:false});
		});
		$("#circleContainer").addClass("disabled");
		if (window.innerWidth <= 750){
			
			//$('#kartModulWrapper').svg({loadURL: 'images/kart/map3b.svg',onLoad: mySvgLoaded});
		}else {
			
			//$('#kartModulWrapper').svg({loadURL: 'images/kart/map3b.svg',onLoad: mySvgLoaded});
		}
		reloadContent();
		
	}
	else if (stateId == 'Valg') {
		$("#valgUnderMenu").attr("style","display:table;");
		$("#circleContainer").attr("style","display:table;");
	/*	var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		
		var headerHeight = $("#header").height();
		var menyHeight = 134 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'134px'}, {duration:300,queue:false});
		});
		if (($("#topmenuButtonCountyLink").html() != 'FYLKEOVERSIKT') || ($("#topmenuButtonMunicipalityLink").html() != 'KOMMUNEOVERSIKT')){
			//animateMenuIn('wrapperUnderMenu');
			countyElectionMode = true;
			reloadContent();
			//alert('hmmmm' + state +' ' +stateId);
		}else {
			$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
			if ( $('#topmenuButtonNation').hasClass('active') ){
				toggleState('Nation');
			
				//animateMenuIn('wrapperUnderMenu');
			}else{
			$("#circleContainer").addClass("disabled");
			toggleState('Nation');
			//animateMenuIn('wrapperUnderMenu');
			countyElectionMode = true;
			}
		}
		
	}
	else if (stateId == 'Duellen') {
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		region = 'n';
		regionName = 'LANDSOVERSIKT';
	/*	var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
		*/
		var headerHeight = $("#header").height();
		var menyHeight = headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'0px'}, {duration:300,queue:false});
		});
		
		reloadContent();
	}
	else if (stateId == 'Storting') {
	$('#blackOverlay').attr('style','display:block;');
		state = 'Storting';
		region = 'n';
		regionName = 'LANDSOVERSIKT';
	    countyElectionMode = true;
		reloadContent();
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		$(".underMenu").attr("style","display: none;");
		$("#storUnderMenu").attr("style","display:table;");
		
		/*var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
		*/
		
		var headerHeight = $("#header").height();
		var menyHeight = (searchRep != '' ? 0 : 100 ) + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'100px'}, {duration:300,queue:false});
		});
		
		
	}else if (stateId =='Mandates'){
		
		stateId = 'Nation';
		state= 'Nation';
		
		showMainCircle('mandatesBlock');
	}
	else if (stateId == 'Repres') {
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
		$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
		$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		stateId = 'Nation';
		state= 'Nation';
		showMainCircle('gallerier');
		if (searchRep != '') {
		
			var blub = $('#candidate_'+searchRep);
			$(blub).addClass('activeRep');
			openRepBox('candidate_'+searchRep,$(blub));
			searchRep = '';
		}
		var headerHeight = $("#header").height();
		var menyHeight = headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'0px'}, {duration:300,queue:false});
		});
	}
	else if (stateId == 'Fav') {
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
		$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
		$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		//animateMenuIn('wrapperUnderMenu');
		openFavPanel('Fav');
		var headerHeight = $("#header").height();
		var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
		var ContentTop =  $("#header").height() + menyHeight;
		$(function(){
			$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
		});
		//setTimeout(function () {animateMenuIn('wrapperUnderMenu');  }, 100);
	}
	else if (stateId == 'Sok') {
		region = 'n';
		reloadContent();
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
		$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
		$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		//animateMenuIn('wrapperUnderMenu');
		openSearchPanel();
		//setTimeout(function () {animateMenuIn('wrapperUnderMenu');  }, 100);
	}
	else if (stateId == 'County'){
		$(".circle").removeClass("disabled");	
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
		$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		$(".underMenu").attr("style","display: none;");
		$("#valgUnderMenu").attr("style","display:table;");
		$("#circleContainer").attr("style","display:table;");
		
		
		/*var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		
		
		
		$("#circleContainer").addClass("disabled");
		$('.button').removeClass('active');
		$("#topmenuButtonCounty").addClass("active");
		refreshCounties(false);
		if (open)
		    openMenu('selectCounty');	
			
		
		var headerHeight = $("#header").height();
		var menyHeight = 275 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'275px'}, {duration:300,queue:false});
		});
	}
	else if (stateId == 'Municipality'){
		$(".circle").removeClass("disabled");	
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
		$('.button').removeClass('active');
		$("#topmenuButtonMunicipality").addClass("active");
		$(".underMenu").attr("style","display: none;");
		$("#valgUnderMenu").attr("style","display:table;");
		$("#circleContainer").attr("style","display:table;");
		
		
		/*var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		
		
		$("#circleContainer").addClass("disabled");	
		$("#circleKart").addClass("disabled");
		$("#circleMandates").addClass("disabled");
		$("#circleParties").addClass("disabled");
		refreshCounties(true);
		if (open)
		    openMenu('selectCounty');
			
		var headerHeight = $("#header").height();
		var menyHeight = 275 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'275px'}, {duration:300,queue:false});
		});
	}else if (stateId == 'Home'){
		var headerHeight = (window.innerWidth == 680 ? 61 : 115);//$("#header").height();
		var menyHeight = headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'0px'}, {duration:300,queue:false});
		});	
	
	//$('#mainCircle').attr('style','');
	var code = querySt();
	//code = 'VG';
	//loadRepres();
	/*if (code == 'AP') {
		toggleState('Duellen');
	
    }else */if (isValg == false && homeCodes[code.toUpperCase()]) {
        //var logoFileName = homeCodes[code.toUpperCase()];
		var homelinkstring = homeCodes[code.toUpperCase()];
        if (!homelinkstring || homelinkstring == ''){
           state = 'Duellen';
			$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
			$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
			$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
			$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
			region = 'n';
			regionName = ' ';
			/*var headerHeight = $("#header").height();
			var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
			//$("#mainCircle").animate({top: menyHeight});
			$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
			
			reloadContent();
		}else {
			stateCheck = 'Home';
			var elements = homelinkstring.split(',');
			state = elements[0];
			regionName = elements[1];
			region = elements[2];
			homePage = elements[3];
			reloadContent();
			
			
			
		}
    }else if (isValg == true && homeCodesValg[code.toUpperCase()]) {
	
		var homelinkstring = homeCodesValg[code.toUpperCase()];
        if (!homelinkstring || homelinkstring == ''){
           state = 'Duellen';
			$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
			$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
			$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
			$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
			region = 'n';
			regionName = 'LANDSOVERSIKT';
			/*var headerHeight = $("#header").height();
			var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
			//$("#mainCircle").animate({top: menyHeight});
			$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
			
			reloadContent();
		}else {
			stateCheck = 'Home';
			var elements = homelinkstring.split(',');
			state = elements[0];
			regionName = elements[1];
			region = elements[2];
			homePage = elements[3];
			reloadContent();
			
			
			
		}
	
	
	}else {
		/*logoFileName = "ntb.png";
		 var logoimg = document.getElementById('logoImg');
        if (logoimg)
            logoimg.src = 'logo/' + logoFileName;*/
	
		state = 'Duellen';
		$("#topmenuButtonMunicipalityLink").html("KOMMUNEOVERSIKT");
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonCountyPollLink").html("FYLKESVIS");
		$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
		region = 'n';
		regionName = 'LANDSOVERSIKT';
	/*	var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		reloadContent();
		}
	
	
	}else if (state == 'loadReps'){
		region = 'n';
		reloadContent();
	}
	var g = new Date();

}
function toggleMap(id){
	var myDumper= []
	
	var svgC = $('#kartModulWrapper').svg('get');
	if (id == 'kartBlokk'){
		var myDumper = mapDataBlokk;
	}else if (id == 'kartParti'){
		var myDumper = mapDataParti;
	}else if (id == 'kartDelta'){
		var myDumper = mapDataM;
	}
	$.each(myDumper , function() {
		$("[id^='"+this.reg+"']").attr({
											
											'stroke': '#fff',
											'fill': this.far
										});
	});
	$.each(myDumper , function() {
		$("path[id^='f"+this.reg+"']").attr({
											
											'stroke': '#fff',
											'fill': this.far
										});
	});
	var g = new Date();

}
/*** fylke / kommune menu ***/
function openMenu(id){	

var div = id;
	var menuitem = document.getElementById(id);
	
	/*if (menuitem){
		animateMenu(id);*/
		
		menuitem.style.display = 'block';	
		var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
		//animateMenuIn(div)	;
	//}
	var g = new Date();

}
function closeMenu(id){
var div = id; 
	var menuitem = document.getElementById(id);
	
	if (menuitem){	
		$("#"+id).attr("style","display: none;");
		
		var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
		//animateMenu(div);
		
	}
	var g = new Date();

}

/*** undermenu ***/
function toggleCircleButtons(){
	var ccwidth = '';
	var circleContainer = document.getElementById("circleContainer");
	var parties = document.getElementById("circleParties");
	var table = document.getElementById("circleTable");
	var votes = document.getElementById("circleVotes");
	var history = document.getElementById("circleHistory");
	var votesText = document.getElementById('circleVotesText');
	var mandatesText = document.getElementById('circleMandatesText');
	var mandates = document.getElementById('circleMandates');
	//var state = state;
	
	if (mandates) mandates.style.display = "table-cell";
	if (parties) parties.style.display = "table-cell";
	if (table) table.style.display = "table-cell";
	if (history) history.style.display = "table-cell";	
    
	if (state == 'Polls' || state == 'countyPolls' || (state == 'Fav' && isValg == false)){
		//var visibleButtons = 3;
		
		if (parties)
		    //parties.style.display = "none";	
		if (table)
		    //table.style.display = "none";
		
		//change votes to last poll
		if (votesText)
			votesText.innerHTML = 'SISTE M&Aring;LING';
		
		// hide history if not nation	
		if (history){			
			//history.style.display = "block";
		
			if (region != 'n'){
				//visibleButtons = 2;
				//history.style.display = "none";
			}
		}
		
		if (circleContainer){		
	
			if (visibleButtons == 3){

					ccwidth='350px';
			}
			else if (visibleButtons == 2){
					ccwidth='300px';							
			}
		}
		
	}
	else if ( state == 'Storting'){
		//var visibleButtons = 3;
	}
    else {
        var visibleButtons = 5;

        if ((state == 'County' &! countyElectionMode) || (state == 'Municipality' && countyElectionMode)) {
           // mandates.style.visibility = "hidden";
            //visibleButtons = 4;
        }
	    if (state == 'Municipality'){
			// history.style.display = "none";
		}
	    if (state == 'Municipality' && region.length > 8) { //district
	       // history.style.display = "none";
	       // mandates.style.display = "none";
	       // visibleButtons = 3;	        	        
	    }
	    
	    if (circleContainer) {
	        
            if (visibleButtons == 5) {
               ccwidth = '450px';		                
            }
	        else if (visibleButtons == 4)
	            ccwidth = '400px';
	        else if (visibleButtons == 3) {
	             ccwidth = '350px';
            }
	    }
		
		if (votesText)
			votesText.innerHTML = 'STEMMER';

		//if (history)
			//history.style.visibility = "visible";		
	}
	
	//both polls and results		
	if (mandatesText && mandates){
		if (region != 'n'){
			//mandatesText.innerHTML = "REPRESENTANTER";
			//mandates.style.backgroundImage = "url('../images/pad/representatives.png')";
			//mandates.style.backgroundPosition = "40px 16px";
		}
		else{
			//mandatesText.innerHTML = "REPR.";
			//mandatesText.innerHTML = "MANDATER";
			//mandates.style.backgroundImage = "url('../images/pad/mandates.png')";
			//mandates.style.backgroundPosition = "33.5px 30px";
		}
	}
	
	$("#wrapperUnderMenu").attr("style","display:block;");
	if ( state != 'Storting' && state != 'Duellen' && state != 'Sok'/* && state != 'Fav'*/ ){
	$("#circleContainer").attr("style","display:table;width:"+ccwidth);
	}
	var g = new Date();

}
/*** menu formåling ***/
function openPollMenu() {
    var menu = document.getElementById('pollMenu');

    if (menu) {
        closePollMenu();
        menu.style.visibility = 'visible';
    }
}
function closePollMenu() {
    var menu = document.getElementById('pollMenu');

    if (menu) {
        menu.style.visibility = 'hidden';
    }
}

function pollMenuOpen(){
	var menu = document.getElementById('pollMenu');

    if (menu) {
        return (menu.style.visibility == 'visible');
    }

	return false;
}
function selectNationPolls(toggle){

	region = 'n';
	regionName = 'LANDSOVERSIKT';
	if (state == 'Fav') {
	
	}else {
		reloadContent();
	}
	var g = new Date();

	//if (toggle)
	    //toggleState('Polls', false); //will close/open menu
}

//TODO BIANCA
function deselectAllOtherMenuItems(id){

	
	if ( id == 'topmenuButtonHome' || id == 'topmenuButtonValg' || id == 'topmenuButtonStorting' || id == 'topmenuButtonPolls' || id == 'topmenuButtonDuellen' || id == 'topmenuButtonFav' || id == 'topmenuButtonSok' ) {
		$('.mMBtn').removeClass('activeMMBtn');
		$('#'+id).addClass('activeMMBtn');
		//$('.button').removeClass('active');
		/*if (id == 'topmenuButtonValg') {
			$('#topmenuButtonNation').addClass('active');
		}*/
	}else if (id == 'topmenuButtonNation' || id == 'topmenuButtonCounty' || id == 'topmenuButtonMunicipality' || id == 'topmenuButtonNationPoll' || id == 'topmenuButtonCountyPoll' ) {
		//$('.button').removeClass('active');
		//$('#'+id).addClass('active');
	}
	
	
	
	var g = new Date();

}
function animateMenuIn(id){
	/*
	var div = id;	
	//var menyHeight = $("#" + id).height() +115;
	//$("#mainCircle").animate({top: menyHeight});
	$("#" + id).slideDown("fast");
	var headerHeight = $("#header").height();
	var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
	//$("#mainCircle").animate({top: menyHeight});
	$("#mainCircle").attr('style','top:' + menyHeight +'px;');
	
	var g = new Date();

	*/
	
	if (state == 'Fav') {
		var headerHeight = $("#header").height();
		var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
		var ContentTop =  $("#header").height() + menyHeight;
		$(function(){
			$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
		});
	}
}
function animateMenu(id) {

	var div = id;	
	//var menyHeight = $("#" + id).height();
	//$("#mainCircle").animate({top: '115px'});
	$("#" + id).slideUp("fast");
	var headerHeight = $("#header").height();
	var menyHeight =/* $("#wrapperUnderMenu").height() +*/headerHeight;
	//$("#mainCircle").animate({top: menyHeight});
	$("#mainCircle").attr('style','top:' + menyHeight +'px;');
	
	
	var g = new Date();

}
function toggleKart(obj) {
	$("#kartMenu").accordion('destroy').accordion({heightStyle: "content"});
	if (obj.id == 'kartNormal') {
		toggleMap('kartBlokk');
		if (state != "Polls") {
		$('#kartDeltaLink').removeClass('hidden');
		$('#ui-accordion-kartMenu-panel-2').removeClass('hidden');
		}
		$('#kartPartiLink').removeClass('hidden');
		$('#ui-accordion-kartMenu-panel-1').removeClass('hidden');
		$('#kartModulWrapper').attr('style','display:block;');
		$('#kartModulWrapperAlt').attr('style','display:none;');
		$('#kartAlt').attr('style','display:block;');
		$('#kartNormal').attr('style','display:none;');
		$('#kartHeadLineAlt').attr('style','display:none;');
	}
	else if(obj.id == 'kartAlt') {
		$('#kartDeltaLink').addClass('hidden');
		$('#ui-accordion-kartMenu-panel-2').addClass('hidden');
		$('#kartPartiLink').addClass('hidden');
		$('#ui-accordion-kartMenu-panel-1').addClass('hidden');
		$('#kartModulWrapper').attr('style','display:none;');
		$('#kartModulWrapperAlt').attr('style','display:block;');
		$('#kartNormal').attr('style','display:block;');
		$('#kartAlt').attr('style','display:none;');
		$('#kartHeadLineAlt').attr('style','');
		toggleMap('kartBlokk');
		$("#kartBlokkLink").addClass('ui-accordion-header-active ui-state-active');
		$("#ui-accordion-kartMenu-panel-0").addClass('ui-accordion-content-active');
		$("#ui-accordion-kartMenu-panel-0").attr('style', 'display:block;');
		$("#kartPartiLink").removeClass('ui-accordion-header-active ui-state-active');
		$("#ui-accordion-kartMenu-panel-1").removeClass('ui-accordion-content-active');
		$("#ui-accordion-kartMenu-panel-1").attr('style', 'display:none;');
		$("#kartDeltaLink").removeClass('ui-accordion-header-active ui-state-active');
		$("#ui-accordion-kartMenu-panel-2").removeClass('ui-accordion-content-active');
		$("#ui-accordion-kartMenu-panel-2").attr('style', 'display:none;');
	}
}
function toggleStor(obj){
	var d = new Date();

	if (obj.id == 'mandatPartiBtn') {

		$('#mandaterParti').animate({left:0,right:0});
		$('#mandaterBlock').animate({left:'100%',right:'-100%'});
		$('#mandaterBubble').animate({left:'200%',right:'-200%'});
	}
	else if(obj.id == 'mandatBlockBtn') {

		$('#mandaterParti').animate({left:'-100%',right:'100%'});
		$('#mandaterBlock').animate({left:0,right:0});
		$('#mandaterBubble').animate({left:'100%',right:'-100%'});
	}
	else if(obj.id == 'mandatBubbleBtn'){
	$('#bubbleWrapper').svg('destroy');
	document.getElementById('bubbleWrapper').innerHTML = '';
	
	
		//$('#mandaterParti').attr('style', 'left:-200%;right:200%;');
		//$('#mandaterBlock').attr('style', 'left:-100%;right:100%;');
		//$('#mandaterBubble').attr('style', 'left:0;right:0;');
		$('#mandaterParti').animate({left:'-200%',right:'200%'});//.attr('style', 'left:-200%;right:200%;');
		$('#mandaterBlock').animate({left:'-100%',right:'100%'});//.attr('style', 'left:-100%;right:100%;');
		$('#mandaterBubble').animate({left:0,right:0});
		var circleRV;
		var circleSV
		var colors = [
				'#F7DFE3',
				'#B01116',
				'#EC1C24',
				'#DD5E74',
				'#009A4C',
				'#CCE8F4',
				'#98B94E',
				'#FFDD2E',
				'#0091CC',
				'#005E96',
				'#18FF00'
			]
		/*var bubbles = Raphael("bubbleWrapper");
		var set = bubbles.set();
		for (var i= 0; i < myHCMandater.length  ;i++) {
			var mynumber = (myHCMandater[i][2] == 0) ? 1 : myHCMandater[i][2];
			var temp =  mynumber / 3.14159;
			temp = Math.sqrt(temp) * 19;

			var bubble = bubbles.circle(myHCMandater[i][0],myHCMandater[i][1], temp *(window.innerWidth >= 681 ? 1 : 1));
				bubble.attr("fill", colors[i]);bubble.attr("stroke", "#FFF");bubble.node.id ="node"+i;
				set.push(bubble);
		}*/
		$('#bubbleWrapper').svg();
		
		var bubbles = $('#bubbleWrapper').svg('get');
		for (var i= 0; i < myHCMandater.length  ;i++) {

			if ( i != 10 ) {
				if (i == 11){
					
					var mynumber = (myHCMandater[11][2] == 0) ? 1 : myHCMandater[11][2];
					var temp =  mynumber / 3.14159;
					temp = Math.sqrt(temp) * 19;
			
					var bubble =  bubbles.circle(myHCMandater[11][0],myHCMandater[11][1], temp *(window.innerWidth >= 681 ? 1 : 1),{ fill:colors[10], stroke: '#FFFFFF', id: 'node11' } );
				}else {
					var mynumber = (myHCMandater[i][2] == 0) ? 1 : myHCMandater[i][2];
					var temp =  mynumber / 3.14159;
					temp = Math.sqrt(temp) * 19;
			
					var bubble =  bubbles.circle(myHCMandater[i][0],myHCMandater[i][1], temp *(window.innerWidth >= 681 ? 1 : 1),{ fill:colors[i], stroke: '#FFFFFF', id: 'node'+i } );
				}
			}	
		}
		
		$("circle[id^=node]").attr("style","opacity:0;");
		var innerHTML = '';
		//setTimeout(function () {$("circle[id^=node]").fadeIn("slow"); }, 100);
		setTimeout(function () {$("circle[id^=node]").animate({opacity: '1'},'slow'); }, 100);
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[1][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[1][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[1][2]+"</div>";
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[1][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[1][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'></div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[2][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[2][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[2][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[3][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[3][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[3][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[4][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[4][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[4][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[6][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[6][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[6][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[7][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[7][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[7][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[8][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[8][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[8][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[9][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[9][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[9][2]+"</div>";
		
		innerHTML += "<div class='bubbleNr positionedObj' style='top:"+(myHCMandater[11][1] +(window.innerWidth <= 680 ? 23 :(window.innerWidth <= 800 ? 83 : 132))) +"px;left:"+(myHCMandater[11][0] + (window.innerWidth <= 680 ? 52 :(window.innerWidth <= 800 ? 105 : 215))) +"px;'>"+myHCMandater[11][2]+"</div>";
		innerHTML += "<div class='Pointer1 bubblePtr'><img src='images/logo/01Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer2 bubblePtr'><img src='images/logo/02Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer3 bubblePtr'><img src='images/logo/03Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer4 bubblePtr'><img src='images/logo/04Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer5 bubblePtr'><img src='images/logo/05Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer6 bubblePtr'><img src='images/logo/06Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer7 bubblePtr'><img src='images/logo/07Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer8 bubblePtr'><img src='images/logo/08Pointer.png' >&nbsp;</img></div>";
		innerHTML += "<div class='Pointer9 bubblePtr'><img src='images/logo/107Pointer.png' >&nbsp;</img></div>";
		//innerHTML += "<div class='Pointer09 bubblePtr'><img src='images/logo/09Pointer.png' >&nbsp;</img></div>";
		$('#bubbleWrapper').append(innerHTML);
	}
	var g = new Date();

}
function toggleRepres(text, size, partie){
	$('#blackOverlay').attr('style','display:block;');
	var galleri = document.getElementById('gallerier');
		//if (state == 'Repres') {
			if (galleri) {
				if (partie == 'parti'){
					galleri.innerHTML = displayRepPartiSmall(text, size);
					//$('#blackOverlay').attr('style','display:none;');
				}else {
					galleri.innerHTML = displayRepFylkeSmall(text, size);
					//$('#blackOverlay').attr('style','display:none;');
				}
			 }
		//}
	if (size == 'small') checkScrollPosition($('div.repInnerWrapper'));
	else checkScrollPosition($('div.repInnerWrapperBig'));
}
function changeValgWrapper(dir){
	$('#valgLeftBtn').attr('onclick', '');
	$('#valgRightBtn').attr('onclick', '');
	
	
	var box = $('#valgMaalingWrapper');
	var pos = box.position();
	var leftPos = pos.left;
	var boxwidth = $('#valgMaalingWrapper').width();
	var innerW = (window.innerWidth >= 1024) ? 1024:(window.innerWidth <= 679) ? 680 : (window.innerWidth <= 1023 && window.innerWidth >= 800) ? 800 : (window.innerWidth >= 681 && window.innerWidth <= 799) ? 680 : window.innerWidth;
	var mywindows = (state == 'Municipality' ? 2 :((state == 'Nation' || state == 'County' )? 5 : (state == 'Polls' ? 4 : 1 )));
	if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601'))
		mywindows = 4;
	if (region.length > 8)
		mywindows = 1;
		
		
	if (dir == 'left') {
	
		if ( (innerW == 1024 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15))) || (innerW == 800 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15))) || (innerW == 680 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15)))){
			
				var newLeftPos =-1 * (mywindows * innerW);
				var newRightPos = -1 * newLeftPos;
				newLeftPos = newLeftPos +'px';
				newRightPos = newRightPos +'px';
			
		}else {
		var newLeftPos = leftPos + innerW;
		var newRightPos = -1 * newLeftPos;
		newLeftPos = newLeftPos +'px';
		newRightPos = newRightPos +'px';
		}
	}else if (dir == 'right'){
	
		if ( ((region.length > 8) &&   (((innerW == 1024 && pos.left <= (innerW * -1) ) || (innerW == 800 && pos.left <= (innerW * -1)) || (innerW == 680 && pos.left <= (innerW * -1))))   ) ||((state == 'Nation' || state == 'County' )  &&  ((innerW == 1024 && pos.left <= -5120) || (innerW == 800 && pos.left <= -4000) || (innerW == 680 && pos.left <= -3400))) || (state == 'Municipality' && (((innerW == 1024 && pos.left <= -2048) || (innerW == 800 && pos.left <= -1600) || (innerW == 680 && pos.left <= -1360)))) ||(state == 'Polls' && (((innerW == 1024 && pos.left <= (innerW * -4) ) || (innerW == 800 && pos.left <= (innerW * -4)) || (innerW == 680 && pos.left <= (innerW * -4)))))
		||(state == 'countyPolls' && (((innerW == 1024 && pos.left <= -1024) || (innerW == 800 && pos.left <= -800) || (innerW == 680 && pos.left <= -680)))) || (((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')) && (((innerW == 1024 && pos.left <= (innerW * -4) ) || (innerW == 800 && pos.left <= (innerW * -4)) || (innerW == 680 && pos.left <= (innerW * -4)))) )){
			var newLeftPos = 0;
			var newRightPos = 0;
			newLeftPos = newLeftPos +'px';
			newRightPos = newRightPos +'px';
		}else {
			var newLeftPos = leftPos - innerW;//boxwidth;
			var newRightPos = -1 * newLeftPos;
			newLeftPos = newLeftPos +'px';
			newRightPos = newRightPos +'px';
		}
	}
	$('#valgMaalingWrapper').animate({left:newLeftPos,right:newRightPos}, function(){
		$('#valgLeftBtn').attr('onclick', 'changeValgWrapper("left")');
		$('#valgRightBtn').attr('onclick', 'changeValgWrapper("right")');
	});
	var g = new Date();

}
function changeStorWrapper(dir){
	$('#storRightBtn').attr('onclick', '');
	$('#storLeftBtn').attr('onclick', '');
	var box = $('#stortingWrapper');
	var pos = box.position();
	var leftPos = pos.left;
	var boxwidth = $('#stortingWrapper').width();
	var innerW = (window.innerWidth >= 1024) ? 1024:(window.innerWidth <= 679) ? 680 : (window.innerWidth <= 1023 && window.innerWidth >= 800) ? 800 : (window.innerWidth >= 681 && window.innerWidth <= 799) ? 680 : window.innerWidth;
	var mywindows = 2 ;
	
	if (dir == 'left') {
	
	
	//(innerW == 1024 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15)))
		
		
		if ( (innerW == 1024 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15))) || (innerW == 800 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15))) || (innerW == 680 && (pos.left == 0 || (pos.left <= -3 && pos.left >= -15)))){
			var newLeftPos =-1 * (mywindows * innerW);
			var newRightPos = -1 * newLeftPos;
			newLeftPos = newLeftPos +'px';
			newRightPos = newRightPos +'px';
		}else {
			var newLeftPos = leftPos + boxwidth;
			var newRightPos = -1 * newLeftPos;
			newLeftPos = newLeftPos +'px';
			newRightPos = newRightPos +'px';
		}
	}else if (dir == 'right'){
		if ((innerW == 1024 && pos.left <= -2048) || (innerW == 800 && pos.left <= -1600) || (innerW == 680 && pos.left <= -1360)){
			var newLeftPos = 0;
			var newRightPos = 0;
			newLeftPos = newLeftPos +'px';
			newRightPos = newRightPos +'px';
		}else {
			var newLeftPos = leftPos - boxwidth;
			var newRightPos = -1 * newLeftPos;
			newLeftPos = newLeftPos +'px';
			newRightPos = newRightPos +'px';
		}
	}
	$('#stortingWrapper').animate({left:newLeftPos,right:newRightPos},function(){
		$('#storLeftBtn').attr('onclick', 'changeStorWrapper("left")');
		$('#storRightBtn').attr('onclick', 'changeStorWrapper("right")');
	});
	
	var g = new Date();

}