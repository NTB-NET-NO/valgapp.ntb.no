function parsePollCollectionForRegion(lines) {
    var latest10Polls = [];

    if (region == 'n') {

        latest10Polls = lines[14].split('|');

        //add average poll at beginning of array
        latest10Polls.unshift("1;" + lines[2] + ";Gj.snitt siste 10 målinger");
    }
    else if (region.length == 3){ //county
        latest10Polls = lines[16].split('|');
		latest10Polls.unshift("1;" + lines[2] + ";Gj.snitt siste 10 målinger");
    }else //municipality BIANCA ADDED
        latest10Polls = lines[7].split('|');

    if (latest10Polls.length == 1 && latest10Polls[0].length == 1) //only linebreak returned
        latest10Polls = undefined;

    return latest10Polls;
}

function parsePollCollectionResultsForRegion(lines) {
    var startLine;

    if (region == 'n')
        startLine = 16;
    else if (region.length == 3) //county
        startLine = 18;
    else //municipality
        startLine = 11;

    currentPollCollectionResults = {}; //clears object
	currentPollCollectionResultsM = {}
    //add average poll
	if (region.length == 3){
		currentPollCollectionResults['1'] = lines[14];
		currentPollCollectionResultsM['1'] = lines[7];
	}else {
		currentPollCollectionResults['1'] = lines[8];
		currentPollCollectionResultsM['1'] = lines[7];
	}
    for (var pollResultLine = startLine; pollResultLine < ((9 * currentPollCollection.length) + startLine); pollResultLine += 9) {

        if (lines[pollResultLine] != undefined) {
            var pollResultHeader = lines[pollResultLine].replace(/(\r\n|\n|\r)/gm, "");

            if (pollResultHeader != "") {
                var pollResultId = pollResultHeader.split(';');
                if (pollResultId.length == 3)
                    pollResultId = pollResultId[0];

                var pollResult = lines[pollResultLine + 2];
				var pollResultM = lines[pollResultLine +1 ];

                if (currentPollCollectionResults)
                    currentPollCollectionResults[pollResultId] = pollResult;
					
				if(currentPollCollectionResultsM)
					currentPollCollectionResultsM[pollResultId] = pollResultM;
            }
        }
    }
}

function parseLastWeekPolls(lines) {
    var innerHTML = '';
	//lastWeekPolls = '';
    if (!lastWeekPolls) {
        if (lines.length >= 12) {
            var collection = lines[11]; //will always be present at line 12
            if (collection != '') {
                lastWeekPolls = collection.split("|");
            }
        }
    }

    if (lastWeekPolls) {
        var nrOfPolls = (lastWeekPolls[lastWeekPolls.length - 1].replace(/(\r\n|\n|\r)/gm, "") == "") ? lastWeekPolls.length - 1 : lastWeekPolls.length;
        //innerHTML += "<div id='lastWeek'><div id='lastWeekLabel'>Målinger siste uke</div><div onclick='showLastWeekPolls()' id='lastWeekCircle'>";	
        //innerHTML += nrOfPolls;
																																																												//this.options[this.selectedIndex].innerHTML);								
        innerHTML += "<div id='lastWeek'><div id='lastWeekLabel'>Målinger siste uke</div><select style='padding-left:" + ((nrOfPolls.toString().length > 1) ? "4" : "14") + "px;' onchange='gotoSelectLastWeekPoll(this.value, this.options[this.selectedIndex].innerHTML);' id='lastWeekCircleSelect'>";

        innerHTML += "<option value='0' >" + nrOfPolls + "</option>";

        for (var i = 0; i < lastWeekPolls.length; i++) {
            var poll = lastWeekPolls[i]; //ID-1200;20110629;Kommunestyrevalg i Østre Toten (Sentio for Oppland Arbeiderblad);kommune;5_28

            if (poll.replace(/(\r\n|\n|\r)/gm, "") != "") {
                var pollElements = poll.split(";");

                if (pollElements.length == 5) {
                    var pollId = pollElements[0];
                    var pollDateRaw = pollElements[1];
                    var pollName = pollElements[2];
                    var pollType = pollElements[3];
                    var regionIdRaw = pollElements[4];

                    // fix date
                    /*var pollDateStr = pollDateRaw;

                    try {
                    var dayStr = pollDateStr.substring(6, 8);
                    var monthStr = pollDateStr.substring(4, 6);
                    var year = pollDateStr.substring(0, 4);

                    pollDateStr = " " + dayStr + "." + monthStr + "." + year //use day and month strings to ensure two digits
                    }
                    catch (e) { }*/

                    innerHTML += "<option value='" + pollId + ";" + pollDateRaw + ";" + pollType + ";" + regionIdRaw + "'>" + pollName + "</option>";
                }
            }
        }

        innerHTML += "</select></div>";
    }

    return innerHTML;
}

function parsePollPanel(selectedPollIndex, componentId) {
    var innerHTML = "<select id='pollSelect_" + componentId + "' class='latestPollSelect' onchange=\"selectPoll(this.value, this.selectedIndex, '" + componentId + "',this.options[this.selectedIndex].innerHTML);\">";

    if (currentPollCollection && currentPollCollection.length > 0) {
        var startIndex = 0; //(region == 'n') ? 1 : 0;

        for (var index = startIndex; index < currentPollCollection.length; index++) {
            var poll = currentPollCollection[index].split(';');

            if (poll.length >= 3) {
                //ID-1128;20110607;Gallup for TV2
                var pollName = poll[2];

                // fix date
                var pollDateStr = poll[1];

                try {
                    var dayStr = pollDateStr.substring(6, 8);
                    var monthStr = pollDateStr.substring(4, 6);
                    var year = pollDateStr.substring(0, 4);

                    pollName += " " + dayStr + "." + monthStr + "." + year;  //use day and month strings to ensure two digits
                }
                catch (e) { }

                innerHTML += "<option " + ((index == selectedPollIndex) ? "selected='selected'" : "") + "id='" + poll[0] + "' value='" + poll[0] + "'>" + pollName + "</option>";
            }
        }
    }
    else {
        if (region != 'n') { //for nation we always have avg poll
            var pollName = 'Ingen målinger';
            var pollId = '-1';
            innerHTML += "<option id='" + pollId + "' value='" + pollId + "'>" + pollName + "</option>";
        }
    }

    innerHTML += "</select>";
	
    if (region.length == 3 && componentId == 'mandates')
        innerHTML += "<div class='pollPanelLabel maalingInfo stortinget'>Hvis ingen lokale målinger foreligger er tallene beregnet på grunnlag av målinger i andre fylker. Utjevningsmandater er ikke med.</div>";    
	else if (region.length == 3 && componentId == 'votes')
		innerHTML += "<div class='pollPanelLabel maalingInfo stortinget'>Hvis ingen lokale målinger foreligger er tallene beregnet på grunnlag av målinger i andre fylker.</div>"; 
	else if (region.length == 3 && componentId == 'blokk')
		innerHTML += "<div class='pollPanelLabel maalingInfo stortinget'>Hvis ingen lokale målinger foreligger er tallene beregnet på grunnlag av målinger i andre fylker. Utjevningsmandater er ikke med.</div>"; 	
		
    return innerHTML;
}
function getIndexFromValue(countyValue, value){
	var index = 0;
	var regionsDropDown = document.getElementById('pollMenuMunicipalities');
	if (regionsDropDown){
		for (var i = 0; i < regionsDropDown.options.length; i++){
			var option = regionsDropDown.options[i];
			
			if (option.value == countyValue + '_' + value){
				index = i;
				break;
			}
		}	
	}	
	
	return index;
}
