﻿var isValg = false;
//var valgStart = new Date(2013,01,27); // januar = month 0
//var valgStart =  new Date(2013,05,23,12,50,00);
var valgStart =  new Date(2013,08,09,20,50,00); // januar = month 0
var idag = new Date();
var vars = [], value;
var state; //global var for selected state - nation, county, municipality or search
var region;
//var dataDir = 'data/';	
var dataDir = 'http://valgapp.ntb.no/prod/data/';
//var dataDir = 'http://valgapp.ntb.no/prod/stage/data/';

function loadPage(){
	checkDate();
    //set logo
	var code = querySt();
	//code = 'VG';
	
	$('body').addClass(code.toUpperCase());
	
    if (logoCodes[code.toUpperCase()]) {
        var logoFileName = logoCodes[code.toUpperCase()];
		if (logoFileName){
			var logoArray = logoFileName.split(',');
			if (window.innerWidth <= 700)
				logoFileName = logoArray[0];
			else
				logoFileName = logoArray[1];
		}
        if (!logoFileName)
            logoFileName = "ntb.png";

        var logoimg = document.getElementById('logoImg');
        if (logoimg)
            logoimg.src = 'logo/' + logoFileName;
    }else {
		logoFileName = "ntb.png";
		 var logoimg = document.getElementById('logoImg');
        if (logoimg)
            logoimg.src = 'logo/' + logoFileName;
	}
		
	/*** prepare duellen ***/
	
	reloadContent();
}
function reloadContent() {
	var filePrefix = null;
	filePrefix = ((isValg) ? 'ST06' : 'P05');
	region = 'n';
	

		
		if (dataDir.substring(dataDir.length - 1) != '/')
				dataDir += "/";
		//if (state == 'Polls' || state == 'countyPolls' ||  isValg == false){
		//	var dataFile = dataDir + filePrefix + '-n.txt?p=' + Math.random();
		//}else {
			if (region == 'c00m0301' && state == 'countyPolls') //Oslo
				var dataFile = dataDir + filePrefix + '-c03.txt?p=' + Math.random();
			else
				var dataFile = dataDir + filePrefix + '-' + region + '.txt?p=' + Math.random();
		//}
		var xmlhttp;
		if (window.XMLHttpRequest){
			// code for IE7+, Firefox, Chrome, Opera, Safari
			if($.browser.msie && parseInt($.browser.version) == 9){
				if(window.XDomainRequest){
			var xdr = new XDomainRequest();
		
			if (xdr) {
				xdr.onload = function() {
					parseText(xdr.responseText);
				};
				xdr.onerror = function () {
					parseNotFound(xdr.responseText);
				};
				xdr.onprogress = function() {};
				xdr.open("GET",dataFile);
				xdr.send();
			}
		}
			}else{
				xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {

	        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	            parseText(xmlhttp.responseText);
	        }
	        else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
	            parseNotFound(xmlhttp.responseText);
	        }
			};
			xmlhttp.open("GET",dataFile,true);
			xmlhttp.send()
			}
		}
		else{
			// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");	
			xmlhttp.onreadystatechange = function() {

	        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	            parseText(xmlhttp.responseText);
	        }
	        else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
	            parseNotFound(xmlhttp.responseText);
	        }
			};
			xmlhttp.open("GET",dataFile,true);
			xmlhttp.send();
		}
}

function parseText(value) {
	var lines = value.split('\n');
	var reloadable = document.getElementById('reloadableComponent');
    if (reloadable) {
        reloadable.innerHTML = parseMajority(lines);
		
    }
}
function parseMajority(lines){
	//47.5;85|52.5;84
	var innerHTML = '';		
	var percentRed = 0;
	var percentBlue = 0;
	var values = lines[7].split('|').clean("\r");
	
	if (values && values.length == 2){
		var elementsRed = values[0].split(';');
		var elementsBlue = values[1].split(';');
		if (!isValg ){		
			if (elementsRed.length > 0)
				percentRed = parseFloat(elementsRed[0]).toFixed(0);
			if (elementsBlue.length > 0)
				percentBlue = parseFloat(elementsBlue[0]).toFixed(0);
		}else if (isValg == true ){
			if (elementsRed.length > 0 && elementsBlue.length > 0){
				percentRed = parseFloat(elementsRed[1]);
				percentBlue = parseFloat(elementsBlue[1]);
				var allMandates = percentRed + percentBlue;
				percentRed = parseFloat(percentRed * 100 / allMandates).toFixed(1);
				
				percentBlue = parseFloat(percentBlue * 100 / allMandates).toFixed(1);
			}
		}else {
			if (elementsRed.length > 0)
				percentRed = parseFloat(elementsRed[0]).toFixed(0);;
			if (elementsBlue.length > 0)
				percentBlue = parseFloat(elementsBlue[0]).toFixed(0);;	
		}
	}

		var pfeilPer = 110;
		var pfeilPos;
		var posB = 0;
		//  0 % = -90deg, 50% = 0 deg, 100% = +90deg, 
		//percentRed = 50;
		//percentBlue	= 50;
		
		/** damit need to keep the real percent */
		if (!isValg) {
			var redPer = percentRed;
			var bluePer = percentBlue;
			var newProzent = parseFloat(percentRed) + parseFloat(percentBlue);
			var andreP = 100 - newProzent;
			andreP = andreP.toFixed(1);
			percentRed = (newProzent * percentRed) / 100;
			percentBlue = 100 - percentRed;
		}else {
		var redPer = parseFloat(elementsRed[1]);
		var bluePer = parseFloat(elementsBlue[1]);
		
		}
		if (percentRed < 50){
			pfeilPer = -((50 - percentRed) * 1.8);
		}
		else if (percentRed == 50){
			pfeilPer = 0;
		}else {
			pfeilPer = +(( percentRed -50) * 1.8);
		}
		if ( percentRed == 'NaN' && percentBlue == 'NaN' ) {
			percentRed = 50;
			percentBlue = 50;
		}
	
		percentRed = percentRed * 1.8;
		percentBlue = percentBlue * 1.8;
		blueStart= percentRed + 270;
		
		innerHTML += "<div class='duellWrapperOuter'>";
		innerHTML += "<div class='duellWrapper'>";
		
		//innerHTML += "<div class='contentHeadline"+((!isValg) ?  ' maalingBtn' : '')+"'>&nbsp;</div>";
		
		//innerHTML += "<div class='bigGreyHL'>"+((redPer > bluePer) ? 'RØD-GRØNT' : 'BLÅTT')+" FLERTALL</div>";
		if (!isValg){
			//innerHTML += "<div class='duellInfo'>"+((redPer > bluePer) ? redPer : bluePer) +"% av velgerne vil ha "+ ((redPer > bluePer) ?'RØD-GRØNN' : 'BORGERLIG')+" regjering</div>";
		
			//innerHTML += "<div class='proInfo'><div class='tableRow leftRow'><div class='proRotHL notValg'>RØD-GRØNNE:</div><div class='proBlauHL notValg'>BORGERLIGE:</div></div><div class='tableRow rightRow'><div class='proRot notValg "+((redPer > bluePer) ? 'biggerParti' : '')+"'>"+redPer+"%</div><div class='proBlau notValg "+((redPer < bluePer) ? 'biggerParti' : '')+"'>"+bluePer+"%</div></div></div>";
			innerHTML += "<div class='fsRedPer'>"+redPer+"%</div>";
			innerHTML += "<div class='fsBluePer'>"+bluePer+"%</div>";
		}else{
			//innerHTML += "<div class='duellInfo'>"+((redPer > bluePer) ? redPer : bluePer) +" mandater vil "+ ((redPer > bluePer) ?'RØD-GRØNN' : 'BORGERLIG')+" regjering ha fra høsten.</div>";
			//innerHTML += "<div class='duellInfo'>&nbsp;</div>";
			//innerHTML += "<div class='proInfo'><div class='tableRow leftRow'><div class='proRotHL'>RØD-GRØNNE:</div><div class='proBlauHL'>BORGERLIGE:</div></div><div class='tableRow rightRow'><div class='proRot "+((redPer > bluePer) ? 'biggerParti' : '')+"'>"+redPer+"</div><div class='proBlau "+((redPer < bluePer) ? 'biggerParti' : '')+"'>"+bluePer+"</div></div></div>";
			innerHTML += "<div class='fsRedPer'>"+redPer+"</div>";
			innerHTML += "<div class='fsBluePer'>"+bluePer+"</div>";
		}
		innerHTML += "</div>";
		
		innerHTML += "<div class='duellContainer'><div id='duellRed' class='hold'><div class='pie' style='transform:rotate("+percentRed+"deg);-webkit-transform:rotate("+percentRed+"deg);-moz-transform:rotate("+percentRed+"deg);-o-transform:rotate("+percentRed+"deg);-ms-transform:rotate("+percentRed+"deg);'></div></div> <div id='duellBlue' style='transform:rotate("+blueStart+"deg);-webkit-transform:rotate("+blueStart+"deg);-moz-transform:rotate("+blueStart+"deg);-o-transform:rotate("+blueStart+"deg);-ms-transform:rotate("+blueStart+"deg);' class='hold'><div style='transform:rotate("+percentBlue+"deg);-webkit-transform:rotate("+percentBlue+"deg);-moz-transform:rotate("+percentBlue+"deg);-o-transform:rotate("+percentBlue+"deg);-ms-transform:rotate("+percentBlue+"deg);' class='pie'></div></div><div class='forBG'></div><div class='pfeil' style='transform:rotate("+pfeilPer+"deg);-webkit-transform:rotate("+pfeilPer+"deg);-moz-transform:rotate("+pfeilPer+"deg);-o-transform:rotate("+pfeilPer+"deg);-ms-transform:rotate("+pfeilPer+"deg);-sand-transform:rotate("+pfeilPer+"deg);'></div></div>"
		if(isValg) {
			//innerHTML += parseVotesCounted(lines, regionCode);
		}else {
			//innerHTML += "<div class='maalingInfo'>Gjennomsnitt 10 siste landsdekkende målinger</div>";
		}
		innerHTML += "</div></div>";
	
	
	return innerHTML;
	
	
}

function parseVotesCounted(lines, forComponent){
	//64.2;		
	var innerHTML = '';
	var lastUpdatedDate = lines[2]; //20070910 YYYYmmDD
	var lastUpdateTime = lines[3]; //11:50:29
	var percentCounted = lines[4];
	if (lastUpdateTime.length > 4){
		lastUpdateTime = lastUpdateTime.substring(0,lastUpdateTime.length -4);
	}
	if (lastUpdatedDate.length >= 8 && lastUpdatedDate != '19000101\r'){
	    lastUpdatedDate = lastUpdatedDate.substring(6, 8) + '.' + lastUpdatedDate.substring(4, 6) + ". kl. " + lastUpdateTime;
	}
    
	var widthFilled = parseFloat(percentCounted);
	    
	innerHTML += "<table class='votesCountedContainer" + (forComponent ? 'Small' : '') + "' border='0px' cellpadding='0' cellspacing='0'>";
		
		innerHTML += "<tr>";
        innerHTML += "<td colspan='2' class='votesCountedHeader'>Stemmer opptalt:  "+ widthFilled + "%</td>";
        innerHTML += "</tr>";
		
		innerHTML += "<tr>";
        innerHTML += "<td class='votesCountedEmpty'><div class='votesCountedFilled' style='width:" + widthFilled + "%;'>&nbsp;</div></td>";
        //innerHTML += "<td class='votesCountedPercent" + (forComponent ? 'Small' : '') + "'></td>";
        innerHTML += "</tr>";
		
        innerHTML += "<tr>";
        innerHTML += "<td colspan='2' class='votesCountedHeader" + (forComponent ? 'Small' : '') + "'>OPPDATERT" + (lastUpdatedDate == '19000101\r' ? '' : " - " + lastUpdatedDate) + "</td>";
        innerHTML += "</tr>";

        

        //innerHTML += "<tr>";
		
        //innerHTML += "<td colspan='2' class='votesCountedHeader" + (forComponent ? 'Small' : '') + "'>Basert på fylkestingsvalget</td>";
        //innerHTML += "</tr>";

        innerHTML += "</table>";
	
    
	return innerHTML;
}
function checkDate() {
	if (idag > valgStart) {
		isValg = true;
	}else{
		isValg = false;
	}
}