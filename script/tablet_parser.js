﻿var currentPollCollection;		
var SEATS_PR_COLUMN = 10;//6;
var SEATS_WIDTH = 33; //22 * 1.5 + space 4
var SEATS_HEIGHT =  22;// 25.5; //17 * 1.5 + space 4
var COLUMN_GAP = 1.5;//1.3;
var LABEL_GAP = 60;
var cur_Y = 30;
var cur_X = 0;
var total_width = 0;
var total_nr_of_columns = 0;
var componentHeight/* = 350*/;
var componentWidth = 460;
var currentSelectedPartyId = 1;
var currentSelectedPartyComponent = 2;
var partyDataSets = [[], [], [], [], [], [], [], []];
var partyAccuDataSets = {};
var mapDataBlokk = [];
var mapDataParti = [];
var mapDataM = [];
var countySpecialM = [];
var herausfordererMandat = [];
var herausforderer = [];
var historyDataArray = {};
var isFavorite;

function parseText(value) {
	time2 = $.now() ;
	
	
	mapDataBlokk = [];
	mapDataParti = [];
	mapDataM = [];

	currentSelectedPartyId = 1;
	currentSelectedPartyComponent = 2;
	componentHeight = $("#votes").height() - (window.innerWidth < 800 ? 150 :200);

    //fade out first so old menu disappears

	lastWeekPolls = '';
    var lines = value.split('\n');
	
	var lastUpdateTime = lines[3].split(' ').clean("\r");
	if(lastUpdateTime == '') {
		noResltsInfo();
	}else {
		$("#noResltInfo").attr('style','display:none;');
	}
	
    var votesCountedHTML;
	
	/*** TODO if kommune can be fav too have to find another way all all the state == fav aswell **/
	if (state == 'Fav' && isValg == true) {
		state = 'County';
	}else if (((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')) && isValg == false) {
		state = 'Polls';
	
	}else if (((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')) && isValg == true) {
		//state = 'Municipality';
	
	}else if (state == 'Fav' && isValg == false){
		state = 'countyPolls';
	}else if (state == 'Sok') {
		var innerHTML = createRepArray(lines);
	}
	
	var partyResults = "";
    var index = 0;

    if (state == 'Municipality' ||region == 'c11m1103' || region == 'c12m1201' || region == 'c16m1601') //district or municipality
        partyResults = lines[7].split('|').clean("\r");
    else if (state == 'Nation' || state == 'County' || state == 'Storting' || state == 'Duellen' || (state == 'Fav' && isValg==true)) //county or nation 
        partyResults = lines[8].split('|').clean("\r");
		
    else if (state == 'Polls' || state == 'countyPolls' || isValg == false) {
        if (region == 'n'){
            partyResults = lines[8].split('|').clean("\r");
		}
        else if (region.length == 3 || region == 'c00m0301'){ //county
			partyResults = lines[14].split('|').clean("\r");
		}
		else { //municipality
            partyResults = lines[13].split('|').clean("\r"); //if this region has poll first poll exists on line 13
            index = 0; //select first poll;
		}
		oldRegion = region;
    }
	
	
	
	if (stateCheck == 'Home'){
		isFavorite = false;
		if (state == 'countyPolls' || state == 'County' || state == 'Municipality') {
			parseFavorites(getRegionId(regionName), getRegionId(regionName), regionName,true);
		}
		if (state == 'Polls' || state == 'countyPolls'){
			currentPollCollection = parsePollCollectionForRegion(lines);
				if (currentPollCollection)
					parsePollCollectionResultsForRegion(lines);
		}
		
		var reloadable = document.getElementById('reloadableComponent');
		if (reloadable) {
			if (state == 'Polls') {
				if (homePage == 'votes'){
					saeuleArray = [];
					reloadable.innerHTML = parseColumns(partyResults, index);
					var myhero = saeuleArray;
				
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
				}if (homePage == 'mandates'){
					reloadable.innerHTML = parseMandatesWeb(lines);
				}
			}else if (state == 'countyPolls'){
				if (homePage == 'votes'){
					saeuleArray = [];
					reloadable.innerHTML = parseColumns(partyResults, index);
					var myhero = saeuleArray;
				
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
				}
			}else if (state == 'Duellen'){
				parseTextDuellen(lines);
			}else if (state == 'Nation') {
				reloadable.innerHTML = parseMandatesWeb(lines);
			}else if (state == 'Storting') {
				reloadable.innerHTML = parseMandates(lines);
			}else if (state == 'County') {
				saeuleArray = [];
				reloadable.innerHTML = parseColumns(partyResults);
				var myhero = saeuleArray;
				reloadable.innerHTML += "<div style='padding-top:17px;' />";
				reloadable.innerHTML += parseVotesCounted(lines, false);;
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
				
			}
			
		}
		
		
		$("#valgLeftBtn").attr("style","display: none;");
		$("#valgRightBtn").attr("style","display: none;");
		$("#storRightBtn").attr("style","display: none;");
		$("#storLeftBtn").attr("style","display: none;");
		
		fadeInMainPanel(reloadable);
	
	
	
	}else if (state == 'Nation'){
		parseTextValgNationCounty(lines);
	}else if(state == 'County'){
		
		parseTextValgNationCounty(lines);
	}else if ( state == 'Municipality'){
		parseTextValgMuni(lines);
	}else if (state == 'Duellen'){
		parseTextDuellen(lines);
	}else if (state == 'Polls'){
		 currentPollCollection = parsePollCollectionForRegion(lines);

        if (currentPollCollection)
            parsePollCollectionResultsForRegion(lines);
			
		parseTextPoll(lines);
	}else if(state == 'countyPolls'){
		 currentPollCollection = parsePollCollectionForRegion(lines);

        if (currentPollCollection)
            parsePollCollectionResultsForRegion(lines);
		
		parseTextCountyPoll(lines);		
	}else if (state == 'Storting'){
		parseTextStorting(lines);
	}
	
	if (state == 'countyPolls') {

	}
    else if ((state == 'Polls' /*|| state == 'countyPolls'*/ || (isValg==false && state != 'countyPolls')) && state != 'Duellen') {
		
        //votesCountedHTML = parseLastWeekPolls(lines);
    }
    else{
       //if(isValg) votesCountedHTML = parseVotesCounted(lines, false);
	   
	}
   
		
	$('#favHolderButton').html('');
 if (state == 'County' || state == 'countyPolls'){
	if (isFavorite == false){
		var myregion = region;
		while(myregion.charAt(0) == 'c')
			myregion = myregion.substr(1);
		
			$('#favHolderButton').html("<div id='isFavorite' onclick=\"saveFavouriteSelector('"+myregion+"','"+regionName.toUpperCase()+"','"+true+"')\"><img src='images/webFavOff.png' /></div>");
	}else {
		//saveFavouriteSelector('01','ØSTFOLD', 'true');
		
		$('#favHolderButton').html("<div id='isFavorite' onclick=\"removeFromFavourites2('"+regionName.toUpperCase()+"')\"><img src='images/webFavOn.png' /></div>");
	}
}else if (state == 'Municipality'){

	if (isFavorite == false){
		var myregion = getRegionId(regionName);
		
		$('#favHolderButton').html("<div id='isFavorite' onclick=\"saveFavouriteSelector('"+myregion+"','"+regionName.toUpperCase()+"','"+false+"')\"><img src='images/webFavOff.png' /></div>");
	}else {
		//saveFavouriteSelector('01','ØSTFOLD', 'true');
		
		$('#favHolderButton').html("<div id='isFavorite' onclick=\"removeFromFavourites2('"+regionName.toUpperCase()+"')\"><img src='images/webFavOn.png' /></div>");
	}
}
var db = new Date();
	
	if (vars != ''){
	
		var module = decodeURI(vars[2]).toUpperCase();
		var module2 = decodeURI(vars[1]).toUpperCase();
		var module3 = decodeURI(vars[3]).toUpperCase();
		var startingpage = decodeURI(vars[0]).toUpperCase();
		
			if ((state == 'County' || state == 'Nation') && isValg == true){
				if (module == 'STEMMER' || module2 == 'STEMMER'){
					showMainCircle('votes');
				}else if (module == 'MANDATER' || module2 == 'MANDATER'){
					showMainCircle('mandates');
					
				}else if (module == 'KART' || module2 == 'KART'){
					showMainCircle('kart');
				}else if (module == 'HIST'|| module2 =='HIST'){
					showMainCircle('history');
				}else if (module == 'PARTIER'|| module2 =='PARTIER'){
					showMainCircle('parties');
				}else if (module == 'TABELL' || module2 == 'TABELL'){
					showMainCircle('table');
				}
			}else if (state == 'Municipality'){
				if (module3 == 'STEMMER'|| module2 == 'STEMMER'){
					showMainCircle('votes');
					
				}else if (module3 == 'HIST' || module2 == 'HIST'){
					showMainCircle('history');
					
				}else if (module3 == 'TABELL' || module2 == 'TABELL'){
					showMainCircle('table');
				}else {
					showMainCircle('votes');
				}
			}else if (state == 'countyPolls'){
				if (module == 'STEMMER'|| module2 == 'STEMMER'){
					showMainCircle('votes');
					if (module3 != 'UNDEFINED'){
						selectPoll('ID-'+module3, ''  , 'votes', '');
						$("#pollSelect_votes").val('ID-'+module3);
					}
				}else if (module == 'MANDATER' || module2 == 'MANDATER'){
					showMainCircle('mandates');
					if (module3 != 'UNDEFINED'){
						selectPoll('ID-'+module3, ''  , 'mandates', '');
						$("#pollSelect_mandates").val('ID-'+module3);
					}
				}
			}else if (state == 'Polls') {
			
				if (startingpage == 'MAALINGER'){ //AP
					showMainCircle('votes');
				}else if (module == 'STEMMER' || module2 == 'STEMMER'){
					showMainCircle('votes');
					
					if (module != 'UNDEFINED'){
						selectPoll('ID-'+module, ''  , 'votes', '');
						$("#pollSelect_votes").val('ID-'+module);
					}
				}else if (module == 'MANDATER' || module2 == 'MANDATER'){
					showMainCircle('mandates');
					if (module != 'UNDEFINED'){
						selectPoll('ID-'+module, $("#pollSelect_mandates #ID-"+module).index()  , 'mandates', $("#pollSelect_mandates #ID-"+module).text());
						
					}
				}else if (module == 'KART' || module2 == 'KART'){
					showMainCircle('kart');
				}else if (module == 'HIST' || module2 =='HIST'){
					showMainCircle('history');
				}else if (module == 'PARTIER' || module2 =='PARTIER'){
					showMainCircle('parties');
				}
			}else if (state == 'Storting'){
				showMainCircle('mandatesBlock', true);
			}
		vars = '';
	}
}

function parseTextValgNationCounty (lines) {
var d = new Date();
	var partyResults = "";
    var index = 0;
	var myVotesCountet = '';
	myVotesCountet = parseVotesCounted(lines,true);
	if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')){
		partyResults = lines[7].split('|').clean("\r");
	}else
		partyResults = lines[8].split('|').clean("\r");
	/** add votes countet to nation **/
	if (state == 'Nation')
		$('#kartVotesCountet').html(myVotesCountet);
	else 
		$('#kartVotesCountet').html('');
	/** load Kart **/
	var kartBlokk = document.getElementById('kartBlokk');
	if (kartBlokk) {
		testSVG(lines);
	}
	/** load votes **/
	var votes = document.getElementById('votes');
    if (votes) {
        if (partyResults != "") {
			saeuleArray = [];
            votes.innerHTML = parseColumns(partyResults, index);
			votes.innerHTML += "<div style='padding-top:17px;' />";
			votes.innerHTML += myVotesCountet;
			var myhero = saeuleArray;
			for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
        }
    }
	/** load mandater **/
	if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')){
	
	}else {
	var mandates = document.getElementById('mandates');
    if (mandates) {
		if ( state == 'County'){
			var regionCode = 04;
			mandates.innerHTML = centerComponent(parseReps(partyResults, regionCode));
		
		}else{ 
			mandates.innerHTML = centerComponent(parseMandatesWeb(lines));
			mandates.innerHTML += "<div style='padding-top:180px;' />";
		}
		mandates.innerHTML += myVotesCountet;
    }
	}
	/** load table **/
	var table = document.getElementById('table');
    if (table) {
        if (partyResults != "") {
			table.innerHTML = parseTable(partyResults);
			table.innerHTML += myVotesCountet;
			var container=  $('div#tableInnerWrapper');
			container.scrollTop(0);
			checkScrollPosition2( $('div#tableInnerWrapper'));
		}
    }
    /** load Parti **/
	var parties = document.getElementById('parties');
    if (parties) {
        parties.innerHTML = parseParties(lines, partyResults);
        if (currentSelectedPartyId != 9)
            selectParty(currentSelectedPartyId);
        else 
            selectParty(currentSelectedPartyId, partyResults);
    }
	/** load Histo **/
	openHistory();
	if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')){
		$("#kart").attr("style", "left:0;right:0;");
		$("#votes").attr("style", "left:100%;right:-100%;");
		$("#mandates").attr("style", "left:1000;right:-1000%;").html('');
		$("#history").attr("style", "left:200%;right:-200%;");
		$("#parties").attr("style", "left:300%;right:-300%;");
		$("#table").attr("style", "left:400%;right:-400%;");
		$("#circleMandates").addClass("disabled");
	}else {
		$("#kart").attr("style", "left:0;right:0;");
		$("#votes").attr("style", "left:100%;right:-100%;");
		$("#mandates").attr("style", "left:200%;right:-200%;");
		$("#history").attr("style", "left:300%;right:-300%;");
		$("#parties").attr("style", "left:400%;right:-400%;");
		$("#table").attr("style", "left:500%;right:-500%;");
		
	}
	if (state == 'Nation'){
			$('#kartCounty').fadeOut('slow',function(){$('#kartCounty').attr('style','display:none;')});
			$('#kartBlokk').attr('style','display:block;');
			$('#kartBlokk').fadeIn('slow');
	}else{
			$('#kartBlokk').fadeOut('slow',function(){$('#kartBlokk').attr('style','display:none;')});
			$('#kartCounty').attr('style','display:block;');
			$('#kartCounty').fadeIn('slow');
	}
	
	$("#circleContainer").removeClass("disabled");
	
	var g = new Date();
	
	
	$('#blackOverlay').attr('style','display:none;');
	
}


function parseTextValgMuni(lines){
	
	regionCode = 03;
	var partyResults = "";
    var index = 0;
	partyResults = lines[7].split('|').clean("\r");
	
	/** load votes **/
	if (partyResults != "") {
		saeuleArray = [];
        votes.innerHTML = parseColumns(partyResults, index);
		
		votes.innerHTML += "<div style='padding-top:17px;' />";
        votes.innerHTML += parseVotesCounted(lines, true);
		var myhero = saeuleArray;
			for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
    }
	/** load Histo **/
	openHistory();
	/** load table **/
	 var table = document.getElementById('table');
    if (table && state != 'countyPolls') {
        if (partyResults != "") {
            var akkuArray = [0, 0];
            var akkuData;
            if (state == 'Nation' || state == 'Storting') 
                akkuData = lines[19];
            else if (state == 'County' || state == 'Fav')
                akkuData = lines[15];

            //1;68;57;116;50|2;31;-21;238;-307|3;218;-36;1282;-2006|4;40;-33;485;-1106|6;75;19;361;-405|5;67;25;419;-140|7;116;-20;668;-954|8;156;6;810;-812|154;7;7;9;9|205;3;3;3;3|207;1;-2;6;-29|216;0;0;26;20|230;2;-3;19;-28|224;0;0;2;2|239;1;1;0;0|305;0;0;0;0|240;0;-1;0;-10|1001;0;0;49;-635|
            if (akkuData) {
                var partyAkkuDataElements = akkuData.split('|').clean('\r');

                var mandatesObject = {};
                var mandatesChangeObject = { };
                
                for (var i = 0; i < partyAkkuDataElements.length; i++) {
                    var partyResult = partyAkkuDataElements[i];
                    var partyResultElements = partyResult.split(';');
                    
                    if (partyResultElements.length == 5) {
                        var id = parseInt(partyResultElements[0].toString());
                        var akkuMandates = parseFloat(partyResultElements[1].toString());
                        var akkuMandatesChange = parseFloat(partyResultElements[2].toString());
                        mandatesObject[id] = akkuMandates;
                        mandatesChangeObject[id] = akkuMandatesChange;                        
                    }
                }

                akkuArray[0] = mandatesObject;
                akkuArray[1] = mandatesChangeObject;
            }

            table.innerHTML = parseTable(partyResults, akkuArray);

            table.innerHTML += parseVotesCounted(lines, true);
			var container=  $('div#tableInnerWrapper');
			container.scrollTop(0);
			checkScrollPosition2( $('div#tableInnerWrapper'));
        }
    }
	$("#reloadableComponent").html('');
	$("#kart").attr("style", "left:1000%;right:-1000%;");
	$("#votes").attr("style", "left:0;right:0;");
	$("#mandates").attr("style", "left:1000;right:-1000%;").html('');
	$("#parties").attr("style", "left:1000;right:-1000%;").html('');
	if (region.length > 8){
		$("#history").attr("style","left:1000;right:-1000%;");
		$("#table").attr("style", "left:100%;right:-100%;");
	}else {
		$("#history").attr("style", "left:100%;right:-100%;");
		$("#table").attr("style", "left:200%;right:-200%;");
	}
	$("#circleContainer").removeClass("disabled");
	if (kartMeny == true){
		$("#circleKart").addClass("disabled");
		$("#circleMandates").addClass("disabled");
		$("#circleParties").addClass("disabled");
		showMainCircle('votes');
		kartMeny = false;
	}else {
	
	kartMeny = false;
	}
	if (region.length > 8){
		$("#circleHistory").addClass("disabled");
	}else {
		$("#circleHistory").removeClass("disabled");
	}
}

function parseTextDuellen(lines){
	var reloadable = document.getElementById('reloadableComponent');
    if (reloadable) {
		pfeilDeg = '';
        reloadable.innerHTML = parseMajority(lines);
		if (isValg == true)

			reloadable.innerHTML += parseVotesCounted(lines, true);

    }
	$("#valgLeftBtn").attr("style","display: none;");
	$("#valgRightBtn").attr("style","display: none;");
	$("#storRightBtn").attr("style","display: none;");
	$("#storLeftBtn").attr("style","display: none;");
	fadeInMainPanel(reloadable);
	
	$('.pfeil').attr('style', '');
	animateRotate( -90, pfeilDeg, 1500,'easeOutBounce');
}
function parseTextHome(lines) {
	
}
function parseTextStorting(lines){
var d = new Date();
	var partyResults = "";
    var index = 0;
	partyResults = lines[8].split('|').clean("\r");
	/** load saalen **/
	//mandates (salen or representatives)
    var mandates = document.getElementById('mandatesBlock');
    if (mandates) {
        mandates.innerHTML = parseMandates(lines);
		
        //mandates.innerHTML += parseVotesCounted(lines,true);
    }
	var e = new Date();
	/** load Parties **/
    var parties = document.getElementById('partier');
    if (parties) {
		
			parties.innerHTML = parseParties(lines, partyResults);
			if (currentSelectedPartyId != 9)
				selectParty(currentSelectedPartyId);
			else 
				selectParty(currentSelectedPartyId, partyResults);
		
    }
	var d = new Date();
	/** load Representanter **/
	var galleri = document.getElementById('gallerier');
	if (galleri) {
		inRepres = [];
		galleri.innerHTML = createRepArray(lines);
		checkScrollPosition($('div.repInnerWrapper'));
	}
var d = new Date();
	$("#circleContainer").removeClass("disabled");
	if (searchRep != '') {
		toggleState('Repres',false);
	}
	
	//$('#blackOverlay').attr('style','display:none;');
}

function parseTextPoll(lines) {
	var partyResults = "";
    var index = 0;
	 partyResults = lines[8].split('|').clean("\r");
	 
	/** load kart **/
	var kartBlokk = document.getElementById('kartBlokk');
	var kartHL = document.getElementById('kartVotesCountet');
	kartHL.innerHTML = '';
	if (kartBlokk) {
		testSVG(lines);
	}
	/** load votes **/
	 var votes = document.getElementById('votes');
    if (votes) {
        if (partyResults != "") {
			saeuleArray = [];
            votes.innerHTML = parseColumns(partyResults, index);
			
            votes.innerHTML += "<div style='padding-top:17px;' />";
			var myhero = saeuleArray;
				
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
        }
    }
	/** load mandates **/
	var mandates = document.getElementById('mandates');
    if (mandates) {
        mandates.innerHTML =  centerComponent(parseMandatesWeb(lines));
	}
	
	// TODO SOME HARDCODED PARTI INFO 
	/** load Parties **/
	var parties = document.getElementById('parties');
    if (parties) {
        parties.innerHTML = parseParties(lines, partyResults);
        if (currentSelectedPartyId != 9)
            selectParty(currentSelectedPartyId);
        else 
            selectParty(currentSelectedPartyId, partyResults);
		
    }
	$("#kart").attr("style", "left:0;right:0;");
	$("#votes").attr("style", "left:100%;right:-100%;");
	$("#mandates").attr("style", "left:200%;right:-200%;");
	$("#history").attr("style", "left:300%;right:-300%;");
	$("#parties").attr("style", "left:400%;right:-400%;");
	$("#table").attr("style", "left:500%;right:-500%;");
	$("#circleContainer").removeClass("disabled");
	$('#kartCounty').fadeOut('slow',function(){$('#kartCounty').attr('style','display:none;')});
		$('#kartBlokk').attr('style','display:block;');
		$('#kartBlokk').fadeIn('slow');
	/** load history **/
	openHistory();
	$('#blackOverlay').attr('style','display:none;');
	//$("#reloadableComponent").html('');
}

function parseTextCountyPoll(lines) {
	regionCode = 04;
	var partyResults = "";
    var index = 0;
	partyResults = lines[14].split('|').clean("\r");
	
	/** load votes **/
	var votes = document.getElementById('votes');
    if (votes) {
        if (partyResults != "") {
			saeuleArray = [];
            votes.innerHTML = parseColumns(partyResults, index);
			var myhero = saeuleArray;
				
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
		}
	}
	/** load mandates **/
	var mandates = document.getElementById('mandates');
    if (mandates) {
		if (state == 'Fav' && isValg == false){
			mandates.innerHTML = centerComponent(parseReps(partyResults, index));
		}else {
			mandates.innerHTML = centerComponent(parseReps(partyResults, index));
		}
	}
	$("#kart").attr("style", "left:1000%;right:-1000%;");
	$("#votes").attr("style", "left:0;right:0;");
	$("#mandates").attr("style",  "left:100%;right:-100%;");
	$("#history").attr("style", "left:1000%;right:-1000%;").html('');
	$("#parties").attr("style", "left:1000;right:-1000%;").html('');
	$("#table").attr("style", "left:1000%;right:-1000%;").html('');
	$("#circleContainer").removeClass("disabled");
	$('#blackOverlay').attr('style','display:none;');
	//$("#reloadableComponent").html('');
}

function parseNotFound(value) {

    //fade out first so old menu disappears
    fadeOutMainPanel();
	var innerHTML = '';
    var reloadable = document.getElementById('reloadableComponent');
    if (reloadable) {
      

        if (state == 'Polls' || state == 'countyPolls') {
			innerHTML = "<table id='majorityContainer' style='margin-top:40px;' border='0px' cellpadding='0' cellspacing='0'>";
            innerHTML += "<tr><td id='pollHeader'>MENINGSM&Aring;LING</td></tr>";
        

			innerHTML += "<tr><td id='regionHeader'>" + regionName.toUpperCase() + "</td></tr>";
			innerHTML += (state == 'Polls') ? "<tr><td id='pollHeader' style='color:#000000;padding-top:20px;'>Det foreligger ingen meningsmålinger for " + regionName + "</td></tr>" : "<tr><td id='pollHeader' style='color:#000000;padding-top:20px;'>Det foreligger ikke data for " + regionName + "</td></tr>";
			innerHTML += "</table>";
		}else {
			innerHTML = "<table id='majorityContainer' style='width:80%;margin:auto;margin-top:40px;' border='0px' cellpadding='0' cellspacing='0'>";
            innerHTML += "<tr><td id='pollHeader'></td></tr>";
        

			innerHTML += "<tr><td id='regionHeader'>" + regionName.toUpperCase() + "</td></tr>";
			innerHTML +=  "<tr><td id='pollHeader' style='color:#000000;padding-top:20px;'>Det foreligger ikke data for " + regionName + "</td></tr>";
			innerHTML +=  "<tr><td  style='color:#000000;padding-top:10px;'>Den første landsoversikten offentliggjøres kl. 2100.<br /> Opptellingsresultatene vil bli rapportert fortløpende.<br /> Hvis du ikke finner resultater fra din kommune er det fordi kommunen ennå ikke har innrapportert tallene til SSB.</td></tr>";

			innerHTML += "</table>"; 
		
		}
        reloadable.innerHTML = innerHTML;
		$('#blackOverlay').attr('style','display:none;');
    }

    if (state == 'Polls') {
        currentPollCollection = {}; //clears object
        currentPollCollectionResults = {}; //clears object
    }

    // clear columns
    var votes = document.getElementById('votes');
    if (votes) {
        votes.innerHTML = parseColumns('', -1);
    }

    // clear mandates
    var mandates = document.getElementById('mandates');
    if (mandates) {
        mandates.innerHTML = parseReps('', -1);
    }
	$('#blackOverlay').attr('style','display:none;');
    fadeInMainPanel(reloadable);
		$(".underMenu").attr("style","display: none;");
		document.getElementById('circleContainer').style.display='none';
		var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
}
function testSVG(lines){
	$('#kartModulWrapper').attr('style','display:block;');
	$('#kartModulWrapperAlt').attr('style','display:none;');
	$('#kartHeadLineAlt').attr('style','display:none;');
	$('#kartAlt').attr('style','display:block;');
		$('#kartNormal').attr('style','display:none;');
	var innerHTML = '';		
	var percentRed = 0;
	var percentBlue = 0;
	var values;
	var color= 'C8C5A8';
	var colorP = '#eeeeee';
	var colorM = '#eeeeee';
	var colorZ = '#eeeeee';
	var partiPerzent = 0;
	var majorityValues;
	if (state == "Polls" || state == "nationPolls"){
		majorityValues = lines[7].split('|').clean("\r");
	}
	if (state == 'Polls'|| (state == 'Fav' && isValg == false)){
		//if (region.length > 3) //municipality, special case for iPad version
			//values = lines[12].split('|').clean("\r"); //if  municipality has poll, majoritydata is at line 12			
		//else
		  values = lines[9].split('|').clean("\r");
		  if (region == 'n' && (state != 'Polls' && state != 'nationPolls')) {
			//$("#kartHeadLine").removeClass('pollkart');
			$("#kartHeadLine").innerHTML("LANDSOVERSIKTEN");
			
		  }else if (state == 'Polls' || state == 'nationPolls'){
			//$("#kartHeadLine").html("LANDS-<br/>OVERSIKTEN");
			$("#kartHeadLine").html("LANDSDEKKENDE<br/>MÅLINGER");
			//$("#kartHeadLine").addClass('pollkart');
		  }else {
			$("#kartHeadLineCounty").html(regionName);
		  }
	}
	else if (state == 'Nation'){
		values = lines[9].split('|').clean("\r");
		$("#kartHeadLine").html("LANDS-<br/>OVERSIKTEN");
	}else if (region == 'c11m1103'|| region == 'c12m1201' || region == 'c16m1601' ) {
		$("#kartHeadLineCounty").html(regionName.toUpperCase());
		values = lines[8].split('|').clean("\r");
	}else if (region == 'c00m0301') { //oslo bydeler
		$("#kartHeadLineCounty").html(regionName.toUpperCase());
		values = lines[16].split('|').clean("\r");
	}else if (state == 'County' || state == 'Storting' || (state == 'Fav' && isValg == true)){ 
	    values = lines[10].split('|').clean("\r");
		$("#kartHeadLineCounty").html(regionName.toUpperCase());
	}
    else if (state == 'Municipality') {
        //values = lines[6].split('|').clean("\r");
		
    }

	// blue 06_0091CC_100
	// red 06_EC1C24_80
	
	
	
	if (state == 'Nation' || state == 'Polls'){
		var myId = 'LANDSOVERSIKTEN';
		if (mapLoaded == false)
			$('#kartModulWrapper svg').attr("id", myId + 'SVG');
			var svgC = $('#kartModulWrapper').svg('get');
			$('#kartModulWrapperAlt svg').attr("id", myId + 'AltSVG');
			var svgC = $('#kartModulWrapperAlt').svg('get');
			$('#kartSwitch').attr('style','');
	}else {
	var myId = regionName.toUpperCase();
	myId = myId.replace(/ /g,"");
		$('#kartModulWrapperCounty svg').attr("id", myId + 'SVG');
		var svgC = $('#kartModulWrapperCounty').svg('get');
		$('#kartSwitch').attr('style','display:none;');
	}
	$('#kartHeadLineAlt').html("Mandater fordelt geografisk");
	$('#kartHeadLineAlt').attr('style','display:none;');
	if (values){
		
		//innerHTML += "<div class='kartModulWrapper'>";
		for (var i = 0; i < values.length ; i ++){	
			
			color = 'C8C5A8';
			var elements = values[i].split(';');
			
			var parseRegion = elements[0];
			
			$('#kartBlokkLink').removeClass('hidden');
			$('#ui-accordion-kartMenu-panel-0').removeClass('hidden');
			$('#kartPartiLink').removeClass('hidden');
			$('#ui-accordion-kartMenu-panel-1').removeClass('hidden');
			if(state != 'Polls' && state != "nationPolls"){
				$('#kartDeltaLink').removeClass('hidden');
				$('#ui-accordion-kartMenu-panel-2').removeClass('hidden');
			}else {
				$('#kartDeltaLink').addClass('hidden');
				$('#ui-accordion-kartMenu-panel-2').addClass('hidden');
			
			}
			$('#kartValgLink').addClass('hidden');
			$('#ui-accordion-kartMenu-panel-3').addClass('hidden');
			var prozent = elements[1].split('#');
			
			var prozentD = parseFloat(elements[4]);
			
			if (prozentD < 65)
				colorM = '#C8C5A8';
			else if (prozentD >= 65 && prozentD < 70)
				colorM = '#FFDF8F';
			else if (prozentD >= 70 && prozentD < 75)
				colorM = '#FAB613';
			else if (prozentD >= 75 && prozentD < 80)
				colorM = '#F28020';
			else if (prozentD >= 80)
				colorM = '#58595B';
				
			
			var elementsRed = prozent[0].split('$');
			
			var elementsBlue = prozent[1].split('$');
			if (elementsRed.length > 0)
				percentRed = parseFloat(elementsRed[0]);
			if (elementsBlue.length > 0)
				percentBlue = parseFloat(elementsBlue[0]);
				
			if (percentRed > percentBlue)
				color = '#EC1C24';
			else if (percentBlue > percentRed)
				color = '#0091CC';
			
			
			
				mapDataBlokk.push({reg:parseRegion,far:color});
			var partiInfo = elements[2].split('#');
			
			partiPerzent = 0;
			for(var k =0; k < partiInfo.length; k++){
				var partiGrid = partiInfo[k].split('$');
				var parseParti = partiGrid[0];
				//var prozentK = parseFloat(partiGrid[2]);
				if (state == 'Nation' || state == 'Polls')
					var prozentK = parseFloat(partiGrid[2]);
				else if (state =='County')	
					var prozentK = parseFloat(partiGrid[1]);
				if (prozentK > partiPerzent) {
				
					partiPerzent = prozentK;
					colorP = getPartyColor(parseParti);
					
					//while ( color.charAt(0) === '#')
						//color = color.substr(1);
					partiCode = getPartyName(parseParti);
				}
				//color= 'C8C5A8';
				//partiPerzent = 0;
			//}
			}
			mapDataParti.push({reg:parseRegion,far:colorP});
			//closeMapTooltip
			mapDataM.push({reg:parseRegion,far:colorM});
			
			if (state == 'Nation' || state == 'Polls'){
				$("#kartModulWrapper svg path[id^='"+parseRegion+"']").attr({ 'stroke': '#fff', 'fill': color});
				$("#kartModulWrapperAlt svg path[id^='f"+parseRegion+"']").attr({ 'stroke': '#fff', 'fill': color});
				if (mapLoaded == false){
					if(window.innerWidth >=850){
						$("#kartModulWrapper svg path[id^='"+parseRegion+"']").bind('click', svgOver);
						if ( isValg == true && state == "Polls" ) {
							
						}else {
							$("#kartModulWrapperAlt svg path[id^='f"+parseRegion+"']").bind('click', clickToReps);
							$("#kartModulWrapperAlt svg path[id^='c"+parseRegion+"']").bind('click', clickToReps);
						}
						$("#kartModulWrapper svg path[id^='e"+parseRegion+"']").bind('click', svgOver);
					}else{
					$("#kartModulWrapper svg path[id^='"+parseRegion+"']").bind('click',testonclick).bind('mouseover', svgOver).bind('mouseout',svgOut);
					if ( isValg == true && state == "Polls" ) {
						$("#kartModulWrapperAlt svg path[id^='f"+parseRegion+"']").bind('mouseover', svgOver).bind('mouseout',svgOut);
						$("#kartModulWrapperAlt svg path[id^='c"+parseRegion+"']").bind('mouseover', svgOver).bind('mouseout',svgOut);
					}else {
						$("#kartModulWrapperAlt svg path[id^='f"+parseRegion+"']").bind('click',clickToReps).bind('mouseover', svgOver).bind('mouseout',svgOut);
						$("#kartModulWrapperAlt svg path[id^='c"+parseRegion+"']").bind('click',clickToReps).bind('mouseover', svgOver).bind('mouseout',svgOut);
					}
					$("#kartModulWrapper svg path[id^='e"+parseRegion+"']").bind('click',testonclick).bind('mouseover', svgOver).bind('mouseout',svgOut);
					}
				}
			}else{
				if(window.innerWidth >=850){
				
					$("#kartModulWrapperCounty svg path[id^='"+parseRegion+"']").attr({ 'stroke': '#fff', 'fill': color }).attr('style','');
					$("#kartModulWrapperCounty svg path[id^='"+parseRegion+"']").bind('click',svgOver).bind('mouseover', svgOver);
					
					//'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')) 
					if (region == 'c00m0301'|| region == 'c12m1201'|| region == 'c16m1601' || region == 'c11m1103' ){
						//while (parseRegion.charAt(0) == 0)
						//	parseRegion = parseRegion.substr(1);
						
						$("#kartModulWrapperCounty svg g[id^='"+parseRegion+"']").attr({ 'stroke': '#fff', 'fill': color });
						$("#kartModulWrapperCounty svg g[id^='"+parseRegion+"']").bind('click',svgOver).bind('mouseover', svgOver);
					}

				
				}else{
					$("#kartModulWrapperCounty svg path[id^='"+parseRegion+"']").attr({ 'stroke': '#fff', 'fill': color }).attr('style','');
					$("#kartModulWrapperCounty svg path[id^='"+parseRegion+"']").bind('click',testonclick).bind('mouseover', svgOver).bind('mouseout',svgOut);
					
					//'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')) 
					if (region == 'c00m0301'|| region == 'c12m1201'|| region == 'c16m1601' || region == 'c11m1103' ){
						//while (parseRegion.charAt(0) == 0)
						//	parseRegion = parseRegion.substr(1);
						
						$("#kartModulWrapperCounty svg g[id^='"+parseRegion+"']").attr({ 'stroke': '#fff', 'fill': color });
						$("#kartModulWrapperCounty svg g[id^='"+parseRegion+"']").bind('click',testonclick).bind('mouseover', svgOver).bind('mouseout',svgOut);
					}
				}
			}
			//$("path[id^='"+parseRegion+"']").bind('click',testonclick).bind('mouseover', svgOver).bind('mouseout',svgOut);
			//3_1_101
			
				innerHTML += "<div class='map_"+parseRegion+"Tooltip mapTooltip' style='visibility:hidden;'>";
			

			var percentRedText = percentRed;
			var percentBlueText = percentBlue;
			if (percentBlue == 0)
				percentBlue = 50;
			if (percentRed == 0)
				percentRed = 50;
			//calculate %
			var gesamt = percentBlue + percentRed;
			percentBlue = 100 * percentBlue / gesamt;
			percentRed =  100 * percentRed / gesamt;
			
			percentBlue = (percentBlue - 50) < 0 ? 0 : percentBlue - 50;
			percentRed = (percentRed - 50) < 0 ? 0 : percentRed - 50;
			
			if (percentBlue != 0) percentBlue = percentBlue * 2;
			if (percentRed != 0) percentRed = percentRed * 2;
			
			if(state == "Polls" || state == "nationPolls") {
				var myHTMLPolls = "";
				if (majorityValues && majorityValues.length == 2) {
					var majorityelementsRed = majorityValues[0].split(';');
					var majorityelementsBlue = majorityValues[1].split(';');
					
					var majoritypercentRed = parseFloat(majorityelementsRed[0]).toFixed(1);
					
					var majoritypercentBlue = parseFloat(majorityelementsBlue[0]).toFixed(1);
					var majoritypercentRedText = parseFloat(majoritypercentRed).toFixed(1);
					var majoritypercentBlueText =parseFloat(majoritypercentBlue).toFixed(1);	
					var majoritypercentGesamt = parseFloat(majoritypercentRed) + parseFloat(majoritypercentBlue);
					majoritypercentRed = ( 100 * majoritypercentRed) / majoritypercentGesamt;
					majoritypercentBlue = ( 100 * majoritypercentBlue) / majoritypercentGesamt;
					
					//calculate %
					majoritypercentBlue = (majoritypercentBlue - 50) < 0 ? 0 : majoritypercentBlue - 50;
					majoritypercentRed = (majoritypercentRed - 50) < 0 ? 0 : majoritypercentRed - 50;
	
					if (majoritypercentBlue != 0) majoritypercentBlue = majoritypercentBlue * 2;
					if (majoritypercentRed != 0) majoritypercentRed = majoritypercentRed * 2;
						
					myHTMLPolls += "<table id='majorityContainer' border='0px' cellpadding='0' cellspacing='0'>";
					myHTMLPolls += "<tr><td colspan='4' class='myPollHeader'>Gjennomsnitt siste 10 målinger</td></tr>";
					myHTMLPolls += "<tr>";
					myHTMLPolls += "<td class='" +(majoritypercentRed > majoritypercentBlue ? 'makeBold' : '') +"' style='vertical-align:top;padding-right:5px;padding-top:5px;font-size:14px;'></td>";

					myHTMLPolls += "<td style='width:100%;'>";
					myHTMLPolls += "<table border='0px' style='margin-top:5px;border-collapse:collapse;width:100%;' cellpadding='0' cellspacing='0'>";
					myHTMLPolls += "<tr><td id='majorityComponentRed'><div id='red' style='width:100%;background-color:#FF0000;float:left;'><div style='width:" + majoritypercentBlue + "%;background-color:#00AFCC;float:right;'>&nbsp;</div></div></td>";
					myHTMLPolls += "<td id='majorityComponentBlue'><div id='blue' style='width:100%;background-color:#00AFCC;float:right'><div style='width:" + majoritypercentRed + "%;background-color:#FF0000;float:left;'>&nbsp;</div></div></td></tr>";
					myHTMLPolls += "<tr><td style='text-align:left;padding-left:5px;'>RØD-GRØNNE</td>";
					myHTMLPolls += "<td style='text-align:right;padding-right:5px;'>BORGERLIGE</td></tr>";
					myHTMLPolls += "<tr><td style='text-align:left;padding-left:5px;'>" + majoritypercentRedText + "%</td>";
					myHTMLPolls += "<td style='text-align:right;padding-right:5px;'>" + majoritypercentBlueText + "%</td></tr>";					
					myHTMLPolls += "</table>";    
					myHTMLPolls += "</td>";

					myHTMLPolls += "<td class='" +(majoritypercentBlue > majoritypercentRed ? 'makeBold' : '' )+"' style='vertical-align:top;padding-left:5px;padding-top:5px;font-size:14px;'></td>";
					myHTMLPolls += "</tr>";
					
					myHTMLPolls += "</table>";
			
				$('#kartVotesCountet').html(myHTMLPolls);
				}
			}
			
					innerHTML += "<table id='majorityContainer' border='0px' cellpadding='0' cellspacing='0'>";
			
			if (state == 'Polls' || state == 'countyPolls' || isValg == 'false'){
				innerHTML += "<tr><td id='pollHeader'>Gjennomsnitt siste 10 målinger</td></tr>";
			}
			////'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')) 
			if (region == 'c00m0301'){
				//while (parseRegion.charAt(0) == 0)
					//parseRegion = parseRegion.substr(1);
				//innerHTML += "<div class='map_3_1_"+parseRegion+"Tooltip mapTooltip' style='visibility:hidden;'>";
				innerHTML += "<tr><td id='regionHeader'>" + getRegionName('3_1_' + parseRegion).toUpperCase() + "</td></tr>";
			}else if(region == 'c11m1103'){
				while (parseRegion.charAt(0) == 0)
					parseRegion = parseRegion.substr(1);
					innerHTML += "<tr><td id='regionHeader'>" + getRegionName('11_3_' + parseRegion).toUpperCase() + "</td></tr>";
			}else if (region == 'c12m1201'){
				//while (parseRegion.charAt(0) == 0)
					//parseRegion = parseRegion.substr(1);
					innerHTML += "<tr><td id='regionHeader'>" + getRegionName('12_1_' + parseRegion).toUpperCase() + "</td></tr>";
			}else if(region == 'c16m1601'){
				while (parseRegion.charAt(0) == 0)
					parseRegion = parseRegion.substr(1);
					innerHTML += "<tr><td id='regionHeader'>" + getRegionName('16_1_' + parseRegion).toUpperCase() + "</td></tr>";
			}else 
				innerHTML += "<tr><td id='regionHeader' style='font-size:20px;'>" + getRegionName(parseRegion).toUpperCase() + "</td></tr>";
			
			innerHTML += "</table>";	

			
			innerHTML += "<table id='majorityContainer' width='100%' border='0px' cellpadding='0' cellspacing='0'>";
			innerHTML += "<tr>";
			innerHTML += "<td class='" +(percentRed > percentBlue ? 'makeBold' : '') +"' style='vertical-align:top;padding-right:5px;padding-top:5px;font-size:20px;'>" + percentRedText + "%</td>";

			innerHTML += "<td style='width:100%;'>";
			innerHTML += "<table border='0px' style='margin-top:5px;border-collapse:collapse;width:100%;' cellpadding='0' cellspacing='0'>";
			innerHTML += "<tr><td id='majorityComponentRed'><div id='red' style='width:100%;background-color:#FF0000;float:left;'><div style='width:" + percentBlue + "%;background-color:#00AFCC;float:right;'>&nbsp;</div></div></td>";
			innerHTML += "<td id='majorityComponentBlue'><div id='blue' style='width:100%;background-color:#00AFCC;float:right'><div style='width:" + percentRed + "%;background-color:#FF0000;float:left;'>&nbsp;</div></div></td></tr>";
			innerHTML += "<tr><td style='text-align:left;padding-left:5px;'>RØD-GRØNNE</td>";
			innerHTML += "<td style='text-align:right;padding-right:5px;'>BORGERLIGE</td></tr>";  
			innerHTML += "</table>";    
			innerHTML += "</td>";

			innerHTML += "<td class='" +(percentBlue > percentRed ? 'makeBold' : '' )+"' style='vertical-align:top;padding-left:5px;padding-top:5px;font-size:20px;'>" + percentBlueText + "%</td>";
			innerHTML += "</tr>";
			
			innerHTML += "</table>";
			if (window.innerWidth > 850){
			
				
				if (isValg == false)
					innerHTML += "<div onclick=\"testonpad('"+parseRegion+"')\" class='myResultLink'><img src='images/pad/gotofylke.png' /></div>";
				else
					innerHTML += "<div onclick=\"testonpad('"+parseRegion+"')\" class='myResultLink'><img src='images/pad/resultater.png' /></div>";
			}
			innerHTML += "</div>";
			var color= 'C8C5A8';
		colorP = '#eeeeee';
		colorM = '#eeeeee';
		colorZ = '#eeeeee';
		}
	}
	//}
	//document.getElementById('KartTTBox').innerHTML = '';
	if (state =='Nation' || state == 'Polls')
		document.getElementById('KartTTBox').innerHTML = innerHTML;
	else 
		document.getElementById('KartTTBoxCounty').innerHTML = innerHTML;
	mapLoaded = true;
}


function parseTextHistory(value){
	var lines = value.split('\n');
    var history = document.getElementById('history');
    if (history) {
        for (var i = 0; i < lines.length; i++) {
            var obj = lines[i];                       
            var elements = obj.split("|");

            if (elements.length > 2) {
                var regionIdStr = elements[0].toString();

                var type = elements[1].toString();
                if (type == '1') regionIdStr = 'Polls';
                
                historyDataArray[regionIdStr] = obj;
            }
        }
	}
}

//TODO:
function parseNotFoundHistory(value){

}

function parseMajority(lines){
	//47.5;85|52.5;84
	var innerHTML = '';		
	var percentRed = 0;
	var percentBlue = 0;
	var values;
	
	if (state == 'Polls' || state == 'countyPolls' || (state == 'Fav' && isValg == false)){
		if (region.length > 3) //municipality, special case for iPad version
			values = lines[12].split('|').clean("\r"); //if  municipality has poll, majoritydata is at line 12			
		else
		    values = lines[7].split('|').clean("\r"); 
	}
	else if (state == 'Nation' || state == 'County' || state == 'Storting' || state == 'Duellen' || (state == 'Fav' && isValg == true)){ 
	    values = lines[7].split('|').clean("\r");
	}
    else if (state == 'Municipality') {
        values = lines[6].split('|').clean("\r");
    }
			
	if (values && values.length == 2){
		var elementsRed = values[0].split(';');
		var elementsBlue = values[1].split(';');
		if (!isValg ){		
			if (elementsRed.length > 0)
				percentRed = parseFloat(elementsRed[0]).toFixed(1);
			if (elementsBlue.length > 0)
				percentBlue = parseFloat(elementsBlue[0]).toFixed(1);
		}else if (isValg == true && state == 'Duellen'){
			//var mandate
			if (elementsRed.length > 0 && elementsBlue.length > 0){
				percentRed = parseFloat(elementsRed[1]);
				percentBlue = parseFloat(elementsBlue[1]);
				var allMandates = percentRed + percentBlue;
				percentRed = parseFloat(percentRed * 100 / allMandates).toFixed(1);
				
				percentBlue = parseFloat(percentBlue * 100 / allMandates).toFixed(1);
			}
			/*if (elementsBlue.length > 0){
				percentBlue = parseFloat(elementsBlue[1]);
				percentBlue = parseFloat(percentBlue * 100 / 169).toFixed(1);
			}*/
		}else {
			if (elementsRed.length > 0)
				percentRed = parseFloat(elementsRed[0]).toFixed(1);;
			if (elementsBlue.length > 0)
				percentBlue = parseFloat(elementsBlue[0]).toFixed(1);;	
		}
	}

	if (state == 'Duellen'){

		var pfeilPer = 110;
		var pfeilPos;
		var posB = 0;
		//  0 % = -90deg, 50% = 0 deg, 100% = +90deg, 
		/*percentRed = 20;
		percentBlue	= 80;*/
		/** damit need to keep the real percent */
		if (!isValg) {
			var redPer = percentRed;
			var bluePer = percentBlue;
			var newProzent = parseFloat(percentRed) + parseFloat(percentBlue);
			var andreP = 100 - newProzent;
			andreP = andreP.toFixed(1);
			percentRed = (100 * percentRed) / newProzent;
			percentBlue = 100 - percentRed;
		}else {
			var redPer = parseFloat(elementsRed[1]);
			var bluePer = parseFloat(elementsBlue[1]);
		}
		
		
		if (percentRed < 50){
			pfeilPer = -((50 - percentRed) * 1.8);
		}
		else if (percentRed == 50){
			pfeilPer = 0;
		}else {
			pfeilPer = +(( percentRed -50) * 1.8);
		}
		
		if ( percentRed == 'NaN' && percentBlue == 'NaN' ) {
			percentRed = 50;
			percentBlue = 50;
		}
		pfeilDeg = pfeilPer;
		percentRed = percentRed * 1.8;
		percentBlue = percentBlue * 1.8;
		blueStart= percentRed + 270;
		
		innerHTML += "<div class='duellWrapperOuter'>";
		innerHTML += "<div class='duellWrapper'>";
		
		innerHTML += "<div class='contentHeadline"+((!isValg) ?  ' maalingBtn' : '')+"'>&nbsp;</div>";
		if(redPer > bluePer || bluePer > redPer)
			innerHTML += "<div class='bigGreyHL'>"+((redPer > bluePer) ? 'RØD-GRØNT' : 'BLÅTT')+" FLERTALL</div>";
		else 
			innerHTML += "<div class='bigGreyHL'>&nbsp;</div>";
		if (!isValg){
			innerHTML += "<div class='duellInfo'>"+((redPer > bluePer) ? redPer : bluePer) +"% av velgerne vil ha "+ ((redPer > bluePer) ?'RØD-GRØNN' : 'BORGERLIG')+" regjering</div>";
		
			innerHTML += "<div class='proInfo'><div class='tableRow leftRow'><div class='proRotHL notValg'>RØD-GRØNNE:</div><div class='proBlauHL notValg'>BORGERLIGE:</div></div><div class='tableRow rightRow'><div class='proRot notValg "+((redPer > bluePer) ? 'biggerParti' : '')+"'>"+redPer+"%</div><div class='proBlau notValg "+((redPer < bluePer) ? 'biggerParti' : '')+"'>"+bluePer+"%</div></div></div>";
		}else{
			//innerHTML += "<div class='duellInfo'>"+((redPer > bluePer) ? redPer : bluePer) +" mandater vil "+ ((redPer > bluePer) ?'RØD-GRØNN' : 'BORGERLIG')+" regjering ha fra høsten.</div>";
			innerHTML += "<div class='duellInfo'>&nbsp;</div>";
			innerHTML += "<div class='proInfo'><div class='tableRow leftRow'><div class='proRotHL'>RØD-GRØNNE:</div><div class='proBlauHL'>BORGERLIGE:</div></div><div class='tableRow rightRow'><div class='proRot "+((redPer > bluePer) ? 'biggerParti' : '')+"'>"+redPer+"</div><div class='proBlau "+((redPer < bluePer) ? 'biggerParti' : '')+"'>"+bluePer+"</div></div></div>";
		
		}
		innerHTML += "</div>";
		
		innerHTML += "<div class='duellContainer'><div id='duellRed' class='hold'><div class='pie' style='transform:rotate("+percentRed+"deg);-webkit-transform:rotate("+percentRed+"deg);-moz-transform:rotate("+percentRed+"deg);-o-transform:rotate("+percentRed+"deg);-ms-transform:rotate("+percentRed+"deg);'></div></div> <div id='duellBlue' style='transform:rotate("+blueStart+"deg);-webkit-transform:rotate("+blueStart+"deg);-moz-transform:rotate("+blueStart+"deg);-o-transform:rotate("+blueStart+"deg);-ms-transform:rotate("+blueStart+"deg);' class='hold'><div style='transform:rotate("+percentBlue+"deg);-webkit-transform:rotate("+percentBlue+"deg);-moz-transform:rotate("+percentBlue+"deg);-o-transform:rotate("+percentBlue+"deg);-ms-transform:rotate("+percentBlue+"deg);' class='pie'></div></div><div class='forBG'></div><div class='pfeil' style='transform:rotate("+pfeilPer+"deg);-webkit-transform:rotate("+pfeilPer+"deg);-moz-transform:rotate("+pfeilPer+"deg);-o-transform:rotate("+pfeilPer+"deg);-ms-transform:rotate("+pfeilPer+"deg);-sand-transform:rotate("+pfeilPer+"deg);'></div></div>"
		if(isValg) {
			//innerHTML += parseVotesCounted(lines, regionCode);
		}else {
			innerHTML += "<div class='maalingInfo'>Gjennomsnitt 10 siste landsdekkende målinger<br/>Andre "+ andreP +"%</div>";
		}
		innerHTML += "</div></div>";
		
		//innerHTML += "<div class='duellContainer'><div id='duellRed' class='hold'><div class='pie' style='transform:rotate("+percentRed+"deg);-webkit-transform:rotate("+percentRed+"deg);-moz-transform:rotate("+percentRed+"deg);-o-transform:rotate("+percentRed+"deg);-ms-transform:rotate("+percentRed+"deg);'></div></div> <div id='duellBlue' style='transform:rotate("+blueStart+"deg);-webkit-transform:rotate("+blueStart+"deg);-moz-transform:rotate("+blueStart+"deg);-o-transform:rotate("+blueStart+"deg);-ms-transform:rotate("+blueStart+"deg);' class='hold'><div style='transform:rotate("+percentBlue+"deg);-webkit-transform:rotate("+percentBlue+"deg);-moz-transform:rotate("+percentBlue+"deg);-o-transform:rotate("+percentBlue+"deg);-ms-transform:rotate("+percentBlue+"deg);' class='pie'></div></div></div>"

	}else{
	
	
	if (percentBlue == 0)
		percentBlue = 50;
	if (percentRed == 0)
		percentRed = 50;

	var percentRedText = percentRed;
	var percentBlueText = percentBlue;

	//calculate %
	percentBlue = (percentBlue - 50) < 0 ? 0 : percentBlue - 50;
	percentRed = (percentRed - 50) < 0 ? 0 : percentRed - 50;

	if (percentBlue != 0) percentBlue = percentBlue * 2;
	if (percentRed != 0) percentRed = percentRed * 2;

	innerHTML += "<table id='majorityContainer' border='0px' cellpadding='0' cellspacing='0'>";
	
	if (state == 'Polls' || state == 'countyPolls' || isValg == 'false'){
	    innerHTML += "<tr><td id='pollHeader'>MENINGSM&Aring;LING</td></tr>";
	}
	
	innerHTML += "<tr><td id='regionHeader'>" + regionName.toUpperCase() + "</td></tr>";
	innerHTML += "</table>";	

    
	innerHTML += "<table id='majorityContainer' width='100%' border='0px' cellpadding='0' cellspacing='0'>";
	innerHTML += "<tr>";
	innerHTML += "<td style='vertical-align:top;padding-right:5px;padding-top:5px;'>" + percentRedText + "%</td>";

	innerHTML += "<td style='width:100%;'>";
	innerHTML += "<table border='0px' style='border-collapse:collapse;width:100%;' cellpadding='0' cellspacing='0'>";
    innerHTML += "<tr><td id='majorityComponentRed'><div id='red' style='width:100%;background-color:#FF0000;float:left;'><div style='width:" + percentBlue + "%;background-color:#00AFCC;float:right;'>&nbsp;</div></div></td>";
    innerHTML += "<td id='majorityComponentBlue'><div id='blue' style='width:100%;background-color:#00AFCC;float:right'><div style='width:" + percentRed + "%;background-color:#FF0000;float:left;'>&nbsp;</div></div></td></tr>";
    innerHTML += "<tr><td style='text-align:left;padding-left:5px;'>RØD-GRØNNE</td>";
    innerHTML += "<td style='text-align:right;padding-right:5px;'>BORGERLIGE</td></tr>";  
    innerHTML += "</table>";    
    innerHTML += "</td>";

    innerHTML += "<td style='vertical-align:top;padding-left:5px;padding-top:5px;'>" + percentBlueText + "%</td>";
	innerHTML += "</tr>";
    
    innerHTML += "</table>";
	}
	return innerHTML;
	
	
}

function parseVotesCounted(lines, forComponent){
	//64.2;		
	var innerHTML = '';
	var lastUpdatedDate = lines[2]; //20070910 YYYYmmDD
	var lastUpdateTime = lines[3]; //11:50:29
	if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')){
		var percentCounted = lines[16];
	}else {
		var percentCounted = lines[4];
	}
	if (lastUpdateTime.length > 4){
		lastUpdateTime = lastUpdateTime.substring(0,lastUpdateTime.length -4);
	}
	if (lastUpdatedDate.length >= 8 && lastUpdatedDate != '19000101\r'){
	    lastUpdatedDate = lastUpdatedDate.substring(6, 8) + '.' + lastUpdatedDate.substring(4, 6) + ". kl. " + lastUpdateTime;
	}
    
	if (state == 'Municipality' /*|| (region != 'c11m1103') || (region != 'c12m1201') || (region != 'c03m0301') || (region != 'c16m1601')*/) { //district or municipality
		/*
		"Ingen stemmer opptalt" (0)
		"Kun forhåndsstemmer" (1,2,3,4,5)
		"Et lite antall stemmer gjenstår"  (6,7)
		"Alt opptalt" (8)			
		*/
		var codeText = '';
			
		if (percentCounted == 0)
			codeText = 'Ingen stemmer opptalt';
		else if (percentCounted == 1)
			codeText = "Kun forh&aring;ndsstemmer";
		else if (percentCounted == 2)
			codeText = "Et lite antall stemmer gjenst&aring;r";
		else if (percentCounted == 3)
			codeText = "Alt opptalt";

        if (lastUpdatedDate != '19000101\r')
	        codeText += "<br/>Oppdatert " + lastUpdatedDate;
	    
		innerHTML += "<table class='votesCountedContainer" + (forComponent ? 'Small' : '') + "' width='100%' border='0px' cellpadding='0' cellspacing='0' style='padding-top: 30px;'>";			
		innerHTML += "<tr>";
		innerHTML += "<td style='text-align:center;' class='votesCountedText" + (forComponent ? 'Small' : '') + "'>" + codeText + "</td>";
		innerHTML += "</tr>";
		innerHTML += "</table>";
	}
    else { //county or nation or city

        //if (state == 'Municipality') percentCounted = lines[4];

        var widthFilled = parseFloat(percentCounted);
	    
	   /* if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c03m0301') || (region == 'c16m1601')) {
	        var muniPercent = parseFloat(lines[16]);
	        widthFilled = muniPercent;
	    }*/

	    innerHTML += "<table class='votesCountedContainer" + (forComponent ? 'Small' : '') + "' border='0px' cellpadding='0' cellspacing='0'>";
		
		innerHTML += "<tr>";
        innerHTML += "<td colspan='2' class='votesCountedHeader'>Stemmer opptalt:  "+ widthFilled + "%</td>";
        innerHTML += "</tr>";
		
		innerHTML += "<tr>";
        innerHTML += "<td class='votesCountedEmpty'><div class='votesCountedFilled' style='width:" + widthFilled + "%;'>&nbsp;</div></td>";
        //innerHTML += "<td class='votesCountedPercent" + (forComponent ? 'Small' : '') + "'></td>";
        innerHTML += "</tr>";
		
        innerHTML += "<tr>";
        innerHTML += "<td colspan='2' class='votesCountedHeader" + (forComponent ? 'Small' : '') + "'>OPPDATERT" + (lastUpdatedDate == '19000101\r' ? '' : " - " + lastUpdatedDate) + "</td>";
        innerHTML += "</tr>";

        

        //innerHTML += "<tr>";
		
        //innerHTML += "<td colspan='2' class='votesCountedHeader" + (forComponent ? 'Small' : '') + "'>Basert på fylkestingsvalget</td>";
        //innerHTML += "</tr>";

        innerHTML += "</table>";
	}
    
	return innerHTML;
}

function parseColumns(partyResults, selectedPollIndex){
	
	var topParti ='';
	var topPer = 0;
    var innerHTML = '';
    var labelOffset = 15;
    var interval = 5;
    var maxPercent = 0;
	
    if (partyResults != "") {
        var columnWidth = 35;
        var columnSpace = 5;
        var MAX_NR_OF_COLUMNS = 13; //11 columns + 2 other partyresults        
        var showOther = (state == 'Nation' || state == 'County' || state == 'Polls' || state == 'countyPolls' || state == 'Storting' || state == 'Fav') ? true : ((partyResults.length > MAX_NR_OF_COLUMNS) ? true : false);
        var columnsLength = ((state == 'Municipality') ? ((partyResults.length > MAX_NR_OF_COLUMNS) ? 10 : partyResults.length) : 10);
        var regionCode = '';
        
        if (state == 'Nation' || state == 'Storting' )
            regionCode = '05';
        else if (state == 'County' || state == 'Fav' || state == 'countyPolls')
            regionCode = '04';
        else if (state == 'Municipality')
            regionCode = '02';
	
       // var startLeft = (460 / 2) -  ((( columnsLength == 10? (showOther ? 10 : columnsLength - 2) : columnsLength-2) * (columnWidth + columnSpace)) / 2) + 15;
		var startLeft = (460 / 2) -  ((10 * (columnWidth + columnSpace)) / 2) + 15;
        maxPercent = getColumnsMaxPercent(partyResults, columnsLength, regionCode, showOther);
		
        interval = (maxPercent <= 10) ? 2 : ((maxPercent < 40) ? 5 : 10);
	
        if (maxPercent > 0) {
            innerHTML += "<div id='columnsContainer' style='height:" + componentHeight + "px;'>";
			
            //innerHTML += getColumnsBackground(maxPercent, interval);
			var sortedPartyResults = [];
			var newPartyResults = partyResults.clean("\r");
			sortedPartyResults = newPartyResults.slice(0).sort(sortMyResults); 
			for (var i = 0; i < columnsLength; i++) {
				/*if( state == 'countyPolls') {
					var partyResult = partyResults[i];
					var partyElements = partyResult.split('$');
				}else {*/
					if ( i == 8){
						var partyResult = sortedPartyResults[7];
					}else if (i == 4) {
						
						var partyResult = sortedPartyResults[9];
						
					}else if (i == 5){
						var partyResult = sortedPartyResults[5];
					}else if (i == 6){
						var partyResult = sortedPartyResults[4];
					}else if (i == 7){
						var partyResult = sortedPartyResults[6];
					}else if ( i == 9) {
						
						var partyResult = sortedPartyResults[10];
					}else {
						var partyResult = sortedPartyResults[i];
					}
					var partyElements = partyResult.split(';');
				//}
				
				if (partyElements.length == 5){
					var partyId = partyElements[0]; 
					if (!showOther && partyId == 9)
                        continue;

                    if (partyId == 1000)
                        continue;
						
					var percent = roundNumber(partyElements[2],1);
					
					if (percent > topPer) {
						topParti = getPartyName(partyId);
						
						topParti = topParti.toUpperCase();
						topPer = percent;
					}
					var  percentDiff = partyElements[4];
					
                    var left = startLeft + ((columnWidth + columnSpace) * (partyId > 9 ? (i - 1) : i));
                    //var height = /*((percent * 100 /  maxPercent)*/ (60 * componentHeight) / 100;
					var height = percent  * componentHeight /60;
                    if (percentDiff == 0)
                        percentDiff = '';
                    if (percentDiff > 0)
                        percentDiff = '+' + percentDiff;
                    saeuleArray[i] = height;
                    //logo
                    //innerHTML += "<div style='margin-bottom:-50px;border-right:1px #000000 solid;position:absolute;bottom:0px;left:" + left + "px;'>";
					innerHTML += "<div style='border-right:1px #000000 solid;position:absolute;bottom:0px;left:" + left + "px;'>";
                    if (partyId <= 11)
                        innerHTML += "<img style='position:absolute;top:0;width:" + columnWidth + "px;' src='images/logo/" + partyId + "_480.png' alt='logo'/>";
                    else {

                        var shortName = getPartyShortName(partyId);

                        innerHTML += "<div style='font-size:10pt;overflow:auto;position:absolute;top:0;width:" + (columnWidth + 5) + "px;'>" + ((shortName.length > 3) ? shortName.substring(0,3) : shortName) + "</div>";                        
                    }

                    innerHTML += "</div>";
                    
                    
                    //column
                    innerHTML += "<div class='saeule saeule"+i+"' style='position:absolute;bottom:0px;left:" + left + "px; background-color:" + getPartyColor(partyId) + ";height:" + height + "px;width:" + columnWidth + "px;'></div>";
					//innerHTML += "<div class='saeule saeule"+i+"' style='position:absolute;bottom:0px;left:" + left + "px; background-color:" + getPartyColor(partyId) + ";width:" + columnWidth + "px;'></div>";
                    //value			
                    innerHTML += "<div class='callOutValue' style='bottom:" + (height + 20) + "px;left:" + left + "px;'>" + percent + "</div>";

                    //tip
                    //innerHTML += "<img style='z-index:1000;position:absolute;bottom:" + (height + 11) + "px;left:" + (left + 10) + "px;' src='images/pad/tip.png' alt='tip' />";
                    
                    //diff value
                    innerHTML += "<div class='callOutDiff' style='bottom:" + (height + 20 + 20) + "px;left:" + left + "px;'>" + percentDiff + "</div>";

                    //diff arrow
                    var imgSrc = (percentDiff > 0) ? 'images/up_black_480.png' : ((percentDiff < 0) ? 'images/down_black_480.png' : '');
                    
                    if (imgSrc != '')
                        innerHTML += "<img class='arrow' src='" + imgSrc + "' style='bottom:" + (height + 20 + 20 + 20) + "px;left:" + (left + (columnWidth / 2) - (15 / 2)) + "px;' />";

					
				}
                else if (partyElements.length > 5) {
                    var partyId = partyElements[0];

                    if (!showOther && partyId == 9)
                        continue;

                    if (partyId == 1000)
                        continue;

                    var percent = (regionCode == '03' || regionCode == '02' || region == 'c12m1201'|| region == 'c16m1601' || region == 'c11m1103' ) ? roundNumber(partyElements[1],1) : roundNumber(partyElements[3],1);
                     
					if (parseFloat(percent) > parseFloat(topPer)) {
						topPer = percent;
						if (partyId == 2 || partyId == 6 ){
							topParti = getPartyShortName(partyId);
						}else {
							topParti = getPartyName(partyId);
						}
						topParti = topParti.toUpperCase();
						
					}
					var percentDiff = 0;

                    if (regionCode == '05')
                        percentDiff = partyElements[4];
						
					else if (region == 'c12m1201'|| region == 'c16m1601' || region == 'c11m1103')
						 percentDiff = partyElements[2];
                    else if (regionCode == '04') {
                        percentDiff = partyElements[4];
                    }
                    else if (regionCode == '02'  || regionCode == '03' ){
                        percentDiff = partyElements[2];
                        //if (region.length > 8) percentDiff = partyElements[4];
                    }

                    if (regionCode == '' && state == 'Polls' /*|| state == 'countyPolls'*/)
                        percentDiff = partyElements[4];
                    
                    var left = startLeft + ((columnWidth + columnSpace) * i);// (partyId > 9 ? (i - 1) : i));
                    //var height = ((percent * 100 / maxPercent) * componentHeight) / 100;
					var height = percent  * componentHeight /60;
					 saeuleArray[i] = height;
                    if (percentDiff == 0)
                        percentDiff = '';
                    if (percentDiff > 0)
                        percentDiff = '+' + percentDiff;
                    
                    //logo
                    innerHTML += "<div style='border-right:1px #000000 solid;position:absolute;bottom:0px;left:" + left + "px;'>";

                    if (partyId <= 11)
                        innerHTML += "<img style='position:absolute;top:4px;left:7px;width:" + (columnWidth -10) + "px;' src='images/logo/" + partyId + "_480.png' alt='logo'/>";
                    else {

                        var shortName = getPartyShortName(partyId);

                        innerHTML += "<div style='font-size:10pt;overflow:auto;position:absolute;top:0;width:" + (columnWidth + 5) + "px;'>" + ((shortName.length > 3) ? shortName.substring(0,3) : shortName) + "</div>";                        
                    }

                    innerHTML += "</div>";
                    
                    
                    //column
                   innerHTML += "<div class='saeule saeule"+i+"'style='position:absolute;bottom:0px;left:" + left + "px; background-color:" + getPartyColor(partyId) + ";height:" + height + "px;width:" + columnWidth + "px;'></div>";
					 //innerHTML += "<div class='saeule saeule"+i+"'style='position:absolute;bottom:0px;left:" + left + "px; background-color:" + getPartyColor(partyId) + ";width:" + columnWidth + "px;'></div>";

                    //value			
                    innerHTML += "<div class='callOutValue' style='bottom:" + (height + 10) + "px;left:" + left + "px;'>" + percent + "</div>";

                    //tip
                    //innerHTML += "<img style='z-index:1000;position:absolute;bottom:" + (height + 11) + "px;left:" + (left + 10) + "px;' src='images/pad/tip.png' alt='tip' />";
                    
                    //diff value
                    innerHTML += "<div class='callOutDiff' style='bottom:" + (height + 20 + 10) + "px;left:" + left + "px;'>" + percentDiff + "</div>";

                    //diff arrow
                    var imgSrc = (percentDiff > 0) ? 'images/up_black_480.png' : ((percentDiff < 0) ? 'images/down_black_480.png' : '');
                    
                    if (imgSrc != '')
                        innerHTML += "<img class='arrow' src='" + imgSrc + "' style='bottom:" + (height + 20 + 20 + 10) + "px;left:" + (left + (columnWidth / 2) - (15 / 2)) + "px;' />";

                }
            }
			if (state == 'Polls'){
				innerHTML += "<div id='votesHeader'>LANDSDEKKENDE MÅLINGER</div>";
				innerHTML += "<div id='regionsHeader'></div>";
			}else if(state =='countyPolls'){
				innerHTML += "<div id='votesHeader'>MENINGSMÅLINGER</div>";
				innerHTML += "<div id='regionsHeader'>i "+ regionName +"</div>";
			}else{
			if(topParti != '')
				innerHTML += "<div id='votesHeader'>"+ topParti +" STØRST</div>";
			else 
				innerHTML += "<div id='votesHeader'>&nbsp;</div>";
				
				//innerHTML += "<div id='regionsHeader'>De "+ topParti +" har flest mandater i "+ regionName +"</div>";
				if (state == 'Nation')
					innerHTML += "<div id='regionsHeader'>LANDSOVERSIKT</div>";
				else
					innerHTML += "<div id='regionsHeader'>i "+ regionName +"</div>";
			}
			if (kartMeny == true  && state == 'Municipality'){
				if (oldRegion  == 'c12m1201'){
				//<div onclick="selectCounty('12_1','BERGEN');" class="boxLink">BERGEN</div>
					innerHTML += "<div onclick=\"toggleState('County',false);selectCounty('12_1', 'BERGEN');showMainCircle('kart');\" class='backToFylke'> <img style='width:75px;' src='images/kart/c12.png' /></div>";
				}else if(oldRegion  == 'c16m1601'){
					innerHTML += "<div onclick=\"toggleState('County',false);selectCounty('16_1', 'TRONDHEIM');showMainCircle('kart');\" class='backToFylke'> <img style='width:75px;' src='images/kart/c16.png' /></div>";
				}else if(oldRegion  == 'c11m1103'){
					innerHTML += "<div onclick=\"toggleState('County',false);selectCounty('11_3', 'STAVANGER');showMainCircle('kart');\" class='backToFylke'> <img style='width:75px;' src='images/kart/c11.png' /></div>";
				}else if(oldRegion  == 'c00m0301'){
					innerHTML += "<div onclick=\"toggleState('County',false);selectCounty('00m0301', 'OSLO');showMainCircle('kart');\" class='backToFylke'> <img style='width:75px;' src='images/kart/c00.png' /></div>";
				}else {
				
				
				var myOldRegion = region.substring(1,3);
				innerHTML += "<div onclick=\"toggleState('County',false);selectCounty('"+myOldRegion+"', '"+getRegionName(myOldRegion).toUpperCase()+"');showMainCircle('kart');\" class='backToFylke'> <img style='width:75px;' src='images/kart/c"+myOldRegion+".png' /></div>";
				//<div class="boxLink" onclick="selectCounty('14','SOGN OG FJORDANE');">SOGN OG FJORDANE</div>
				
				}
			}
            innerHTML += "</div>";
            if (state == 'Polls' || state == 'countyPolls' || state == 'nationPolls' )
                innerHTML += parsePollPanel(selectedPollIndex, 'votes'); 
        }

    }
		
	return innerHTML;
	
	
	
}

function getColumnsBackground(maxPercent, interval){
    var innerHTML = '';
    var numRows = 0;
    var height = 0;
    var color = '#E6E6E6';

    numRows = Math.ceil(maxPercent / interval);
    innerHTML += "<table id='columnsBackground' cellpadding='0' cellspacing='0' border='0px'>";

    for (var j = numRows; j > 0; j--) {
        if (color == '#FFFFFF')
            color = '#E6E6E6';
        else if (color == '#E6E6E6')
            color = '#FFFFFF';

        innerHTML += "<tr>";

        if ((j % 1) == 0 && (j > 1)) {
            innerHTML += "<td class='columnsBackgroundRow' style='background-color:" + color + "'>" + ((j - 1) * interval) + "%</td>";
            innerHTML += "<td class='columnsBackgroundRow' style='background-color:" + color + ";width:100%'>&nbsp;</td>";
        }
        else
            innerHTML += "<td class='columnsBackgroundRow' colspan='2' style='background-color:" + color + ";width:100%;'>&nbsp;</td>";

        innerHTML += "</tr>";
    }
    innerHTML += "</table>";

    return innerHTML;
}

function parseMandates(lines) {
	myHCMandater = [];
    var innerHTML = '';
	var innerHTML2 = '';
    var partyResults;

    if (state == 'Nation' || state == 'Storting' || state == 'Duellen' ) //nation Bianca add storting
        partyResults = lines[8].split('|');
    else if (state == 'Polls' || state == 'countyPolls')
        partyResults = lines[8].split('|');

    var seatIndex = 169//0;
    var scaleX = 162;
    var scaleY = 154.36 - 27;
	
	
	innerHTML += "<div class='storHeader contentHeadline'>MANDATFORDELINGEN</div>";
	
	innerHTML += "<div class='storBtnMenu' ><div id='mandatPartiBtn' onclick='toggleStor(this);'  class='button active storBtn'></div><div id='mandatBubbleBtn' onclick='toggleStor(this);'  class='button storBtn'></div></div>"
	
    //innerHTML2 += "<div id='mandaterBlock' style='display:inline;'>";

	innerHTML += "<div id='mandaterParti' style='display:inline;'>";
	innerHTML += parseMajorityMandates(lines);
	//innerHTML += parseMajorityMandates(lines,'parti');
	//innerHTML2 += "<div class='rotate'><div class='mirror'>"
	innerHTML += "<div class='rotate'><div class='mirror'>"
	
	var mCounterRed = 0;
	var mCounterBlue = 0;
	var sortedPartyResults = [];
	var newPartyResults = partyResults.clean("\r");
		sortedPartyResults = newPartyResults.slice(0).sort(sortMyResults);
    for (var i = 0; i < 10; i++) { //TODO: hardcoded first 9 parties (established + others)
        
		/*if ( i < 4 ) {
			var partyResult = partyResults[i];
		} else if ( i == 4 ) {
			var partyResult = partyResults[8];
		}else {
			var partyResult = partyResults[i - 1];
		}*/
		if ( i == 8){
			var partyResult = sortedPartyResults[7];
		}else if (i == 4) {
			var partyResult = sortedPartyResults[9];
		}else if (i == 5){
			var partyResult = sortedPartyResults[5];
		}else if (i == 6){
			var partyResult = sortedPartyResults[4];
		}else if (i == 7){
			var partyResult = sortedPartyResults[6];
		}else if ( i == 9) {
			var partyResult = sortedPartyResults[10];
		}else {
			var partyResult = sortedPartyResults[i];
		}
		
        var partyElements = partyResult.split(';');
		
        var partiId = partyElements[0];
		
        //seats
        var mandates;

        if (state == 'Nation' || state == 'Storting') 
            mandates = partyElements[1]; //??
        else if (state == 'Polls' || state == 'countyPolls')
            mandates = partyElements[1];
		
		/*** fill array for bubble ***/
		if (partiId == 10) {
			myHCMandater[partiId] = [getXAxisPos('9'),getYAxisPos('9'),mandates];
		}else {
			myHCMandater[partiId] = [getXAxisPos(partiId),getYAxisPos(partiId),mandates];
		}
		if(partiId <= 4) mCounterRed = mCounterRed + parseFloat(mandates);
		if(partiId > 4 &&  partiId != 10)	mCounterBlue = mCounterBlue + parseFloat(mandates);
	
		
        if (mandates > 0) {
            for (var mandate = 0; mandate < mandates; mandate++) {
                //seatIndex++;
				
                var x = getXCoordinateForMandateSeat(seatIndex - 1) - scaleX;
                var y = getYCoordinateForMandateSeat(seatIndex - 1) - scaleY;
				
                var rot = getRotationForSeat(seatIndex);
				seatIndex--;
				//innerHTML2 += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);-o-transform: rotate(" + rot + "deg);-ms-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg); width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='images/seats/" + ((partiId > 4) ? 7 : 3) + ".png' alt='seat' />";
				
                innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);-o-transform: rotate(" + rot + "deg);-ms-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='images/seats/" + ((partiId == 11) ? 9 : partiId )+ ".png' alt='seat' />";
				//innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='images/seats/" + partiId + ".png' alt='seat' />";
             
			}
        }else {
			if ( i == 1 ){
				for (var mandate = 0; mandate < 169; mandate++) {
					var x = getXCoordinateForMandateSeat(seatIndex - 1) - scaleX;
					var y = getYCoordinateForMandateSeat(seatIndex - 1) - scaleY;
				
					var rot = getRotationForSeat(seatIndex);
					seatIndex--;
				 innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);-o-transform: rotate(" + rot + "deg);-ms-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='images/seats/10000.png' alt='seat' />";
				}
			}
		}


        //logo and circle
		//var caller = 'phone'; // maybe change TODO BIANCA
		if (partiId == 11) {
		
			var partyLogoSrc = '';
		}else {
			var partyLogoSrc = getPartyLogoSrc(partiId, 'webMandat');
		}
         //var partyLogoSrc = getPartyLogoSrc(partiId, 'webMandat');
        if (partyLogoSrc != '') {

			var logoX = getLogoXCoordinate(partiId);
            var logoY = getLogoYCoordinate(partiId);

           /* //circle
            innerHTML += "<div class='mandatesLabel' style='bottom:" + (logoY - (31 - 8)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";

            //logo
            innerHTML += "<img class='logo' style='bottom:" + (logoY - ( (partiId == 1) ? 5 : 0)) + "px;left:" + (logoX - ( (partiId == 1) ? 10 : 0)) + "px;' src='" + partyLogoSrc + "' alt='logo'/>";
			*/
			
			if ((partiId > 6 && partiId < 9)  || partiId == 5) {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:left;bottom:" + (logoY - ( (partiId == 9) ? 0 : 0)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";
			}/*else if (partiId == 5){
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:center;height: 45px;width:26px;bottom:" + (logoY - ( (partiId == 9) ? 5 : 30)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";

			}*/else if (partiId < 4 || partiId == 5 ) {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:right;bottom:" + (logoY - ( (partiId == 1) ? 0 : 0)) + "px;left:" + (logoX - 25 ) + "px;'>" + mandates + "</div>";

			}else {
				innerHTML += "<div class='mandatesLabel mirrorY' style='text-align:center;width:26px;bottom:" + (logoY - ( (partiId == 10) ? 30 : 30) +50) + "px;left:" + (logoX - 10) + "px;'>" + mandates + "</div>";
			}
            //logo
			if (partiId != 11) {
            innerHTML += "<img class='logo mirrorY' style='width: 30px;bottom:" + (logoY - ( (partiId == 1) ? 0 : 0)) + "px;left:" + logoX  + "px;' src='" + partyLogoSrc + "' alt='logo'/>";
			}
        }
    }
	/*** fill array for bubbles ***/
		myHCMandater[0] = [getXAxisPos('3'),getYAxisPos('3'),mCounterRed];
		myHCMandater.splice(5,0, [getXAxisPos('7'),getYAxisPos('7'),mCounterBlue]);
		
		
	innerHTML +="</div></div>"
	//innerHTML2 +="</div></div>"
	//innerHTML2 += "<div class='storParteiImages tableRow'><div class='proRotPartier'><img src='images/logo/1_480_off.png' >&nbsp;&nbsp;<img src='images/logo/2_480_off.png' >&nbsp;&nbsp;<img src='images/logo/3_480_off.png' >&nbsp;&nbsp;<img src='images/logo/4_480_off.png' > </div><div class='proBlauPartier'><img src='images/logo/5_480_off.png' >&nbsp;&nbsp;<img src='images/logo/6_480_off.png' >&nbsp;&nbsp;<img src='images/logo/7_480_off.png' >&nbsp;&nbsp;<img src='images/logo/8_480_off.png' > </div></div>";
	
    //add label "169 mandater"
    //innerHTML += "<div class='mandatesDescription' style='bottom:70px;'>169</div>";
    //innerHTML += "<div class='mandatesDescription' style='bottom:50px;'>mandater</div>";
	innerHTML += "<div class='mandatesDescription' >169 mandater</div>";
	//innerHTML += ((window.innerWidth >= 681) ? "<div class='mandatesDescription' >169 mandater</div>" : "<div class='mandatesDescription' >169</div><div class='mandatesDescription line2' >mandater</div>");
    
    
	innerHTML += "</div>";
   
	//innerHTML2 += "</div>";
	if (!isValg){
		innerHTML += "<div class='maalingInfo stortinget'>Før valgkvelden er framstilling basert på siste tilgjengelige gjennomsnitt av meningsmålingene</div>";
		//innerHTML2 += "<div class='maalingInfo stortinget'>Før valgkvelden er framstilling basert på siste tilgjengelige gjennomsnitt av meningsmålingene</div>";
		
	}
	innerHTML += innerHTML2;
	
	innerHTML += "<div id='mandaterBubble' style='display:inline;'><div id='bubbleWrapper'></div></div>";
    return innerHTML;

}
function parseMandatesBlock(lines) {

    var innerHTML = '';
    var partyResults;

    if (state == 'Nation' || state == 'Storting' || state == 'Duellen' ) //nation Bianca add storting
        partyResults = lines[8].split('|');
    else if (state == 'Polls' || state == 'countyPolls')
        partyResults = lines[8].split('|');

    var seatIndex = 169//0;
    var scaleX = 162;
    var scaleY = 154.36 - 27;

    for (var i = 0; i < 9; i++) { //TODO: hardcoded first 9 parties (established + others)
        var partyResult = partyResults[i];
        var partyElements = partyResult.split(';');
		
        var partiId = partyElements[0];
		
        //seats
        var mandates;

        if (state == 'Nation' || state == 'Storting') 
            mandates = partyElements[1]; //??
        else if (state == 'Polls' || state == 'countyPolls')
            mandates = partyElements[1];

        if (mandates > 0) {
            for (var mandate = 0; mandate < mandates; mandate++) {
                seatIndex--;

                var x = getXCoordinateForMandateSeat(seatIndex - 1) - scaleX;
                var y = getYCoordinateForMandateSeat(seatIndex - 1) - scaleY;
                var rot = getRotationForSeat(seatIndex);
																																																								//partyId > 9 ? (i - 1) : i)
                innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);-o-transform: rotate(" + rot + "deg);-ms-transform: rotate(" + rot + "deg);transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='images/seats/" + ((partiId > 4) ? 7 : 3) + ".png' alt='seat' />";
				//innerHTML += "<img style='-moz-transform: rotate(" + rot + "deg);-webkit-transform: rotate(" + rot + "deg);width:22px;height:17px;position:absolute;bottom:" + y + "px;left:" + x + "px;' src='images/seats/" + partiId + ".png' alt='seat' />";
             
			}
        }

		
        //logo and circle
		//if (partiId == 9) {
		
		//	var partyLogoSrc = getPartyLogoSrc('107', 'webMandat');
		//}else {
			var partyLogoSrc = getPartyLogoSrc(partiId, 'webMandat');
		//}
        if (partyLogoSrc != '') {
            var logoX = getLogoXCoordinate(partiId);
            var logoY = getLogoYCoordinate(partiId);

            //circle
            innerHTML += "<div class='mandatesLabel' style='bottom:" + (logoY - (31 - 8)) + "px;left:" + (logoX - 3) + "px;'>" + mandates + "</div>";

            //logo
            innerHTML += "<img class='logo' style='bottom:" + (logoY - ( (partiId == 1) ? 5 : 0)) + "px;left:" + (logoX - ( (partiId == 1) ? 10 : 0)) + "px;' src='" + partyLogoSrc + "' alt='logo'/>";

        }
    }

    //add label "169 mandater"
    innerHTML += "<div class='mandatesDescription' >169</div><div class='mandatesDescription line2' >mandater</div>";
    //innerHTML += "<div class='mandatesDescription line2' >mandater</div>";

    innerHTML += parseMajorityMandates(lines);
    
    return innerHTML;

}
function parseMajorityMandates(lines) {
    //47.5;85|52.5;84
    var innerHTML = '';
    var mandatesRed = 0;
    var mandatesBlue = 0;
    var values = lines[7].split('|'); //7 county/nation, 6 municipality

    if (values.length == 2) {
        var elementsRed = values[0].split(';');
        var elementsBlue = values[1].split(';');

        if (elementsRed.length == 2)
            mandatesRed = parseInt(elementsRed[1]); //mandats
        if (elementsBlue.length == 2)
            mandatesBlue = parseInt(elementsBlue[1]); //mandates			
    }
	
	var mandatesGesamt = mandatesRed + mandatesBlue; 
	var mandatesRedFill = 169 * mandatesRed / mandatesGesamt;
	var mandatesBlueFill = 169 * mandatesBlue / mandatesGesamt;
	
    innerHTML += "<table class='mandatesMajority' border='0px' cellpadding='0' cellspacing='0'>";
    innerHTML += "<tr>";
    innerHTML += "<td style='padding-right:5px;width:35px ;'>" + mandatesRed + "</td>";

    innerHTML += "<td>";
    innerHTML += "<table style='width:169px;border-collapse:collapse;'>";
    innerHTML += "<td id='majorityComponentRedSalen' style='width:" + (mandatesRedFill ) + "px;'>&nbsp;</td>";
    innerHTML += "<td id='majorityComponentBlueSalen' style='width:" + (mandatesBlueFill ) + "px;'>&nbsp;</td>";
    innerHTML += "</table>";
    innerHTML += "</td>";

    innerHTML += "<td style='padding-left:5px;width:35px ;'>" + mandatesBlue + "</td>";
    innerHTML += "</tr>";
    innerHTML += "</table>";

    //create majority centerline	
    var centerX = 234;

    //if (window.orientation == 0 || window.orientation == 180)
    //centerX = 380;
	innerHTML += "<div id='centerLineMandates'>|</div>";
    //var centerLine = createLine(centerX, 320, centerX, 345, '#000000', 1, 'centerLineMandates');
    //innerHTML += centerLine.outerHTML;

    return innerHTML;
}
function parseMandatesWeb(lines, selectedPollIndex,headline ) {
    //1;0;0;1.5;-0.1;1.6;0;-0.3;120|2;2;-2;7;-4.3;11.3;4;-6;497|3;13;2;35.7;4;31.7;11;1.8;3167|4;4;1;11.3;4;7.3;3;1.8;868|5;1;1;2.3;0.5;1.8;0;1.2;293|6;2;-1;4.8;-2.9;7.7;3;1.9;910|7;5;0;15.2;0.8;14.4;5;-0.1;1359|8;7;-1;18.1;-2.7;20.8;8;-0.8;1892|9;1;0;4.1;0.6;3.5;1;0.4;361|21;0;0;0.2;-0.1;0.3;0;0;24|23;0;0;0.4;0.1;0.3;0;0;37|24;0;0;0.1;-0.2;0.3;0;-0.2;6|25;0;0;0.4;0.4;0;0;0.4;42|27;1;0;3.1;0.6;2.5;1;0.2;252|37;0;0;0;0;0;0;0;0|
    var innerHTML = '';
    var row1 = '<tr>';
    var row2 = '<tr>';
    var row3 = '<tr>';
    var row4 = '<tr>';
    var backgroundColor = '';//'#ECECEC';
    var partyResults;
	//document.getElementById('divCountyMunicipalitySelect').style.display='block';
	//document.getElementById('divCountySelect').style.display='block';
	
	if((selectedPollIndex || selectedPollIndex == '0') && headline) {
		
		partyResults = lines;
	}else if(state == 'Municipality'){
		//partyResults = lines[7].split('|').clean("\r"); ;
	}else{
		partyResults = lines[8].split('|').clean("\r"); ;
	}
	if (headline) {
		var headLine = headline;
		innerHTML += "<div class='headline'>"+ headLine +"</div>";
	}else if (pollText){
		//document.getElementById('divCountyMunicipalitySelect').style.display='none';
		//document.getElementById('divCountySelect').style.display='none';
		
		if(regionCode != '03'){
			innerHTML += "";//"<div class='headline'>what does this nr mean?TODO</div>";
		}else {innerHTML += "<div class='headline'>"+pollText+"+</div>";}
	}
    if (partyResults != null) {
        var MAX_NR_OF_COLUMNS = 13; //11 columns + 2 other partyresults
        //var showOther = (regionCode == '05' || regionCode == '04') ? true : ((partyResults.length > MAX_NR_OF_COLUMNS) ? true : false);
        var showOther = (state == 'Nation' || state == 'County' || state == 'Polls' || state == 'countyPolls' || state == 'Storting') ? true : ((partyResults.length > MAX_NR_OF_COLUMNS) ? true : false);
        
		var columnsLength =  ((state == 'Municipality') ? ((partyResults.length > MAX_NR_OF_COLUMNS) ? 10 : partyResults.length) : 10);
		
        innerHTML += "<table id='votesContainer' cellpadding='0' cellspacing='1' border='0px'>";
        var names = [];
        var sortedPartyResults = [];
		var newPartyResults = partyResults.clean("\r");
			sortedPartyResults = newPartyResults.slice(0).sort(sortMyResults); 
        for (var i = 0; i < columnsLength; i++) {
		
			//if( state == 'countyPolls') {
			//		var partyResult = partyResults[i];
			//		var partyElements = partyResult.split('$');
			//	}else {
					
			//	}
			if ( i == 8){
				var partyResult = sortedPartyResults[7];
			}else if (i == 4) {
				var partyResult = sortedPartyResults[9];
			}else if (i == 5){
				var partyResult = sortedPartyResults[5];
			}else if (i == 6){
				var partyResult = sortedPartyResults[4];
			}else if (i == 7){
				var partyResult = sortedPartyResults[6];
			}else if ( i == 9) {
				var partyResult = sortedPartyResults[10];
			}else {
				var partyResult = sortedPartyResults[i];
			}
			
			//var partyResult = partyResults[i];
			var partyElements = partyResult.split(';');
			
            if (partyElements.length > 7 || (state == 'countyPolls' && partyElements.length == 5 )) {
                var partyId = partyElements[0];

                if (!showOther && partyId == 9)
                    continue;

                if (partyId == 1000)
                    continue;

                var mandates = undefined;
                var diffMandates = undefined;

                if (state == 'Municipality') { //district or municipality
                    mandates = partyElements[6];
                    diffMandates = partyElements[7];
                }
                else if (state == 'Nation' || state == 'County' || state == 'Polls') { //county or nation
                    mandates = partyElements[1];
                    diffMandates = partyElements[2];
                }else if (state == 'countyPolls'){
					mandates = partyElements[1];
					
                    diffMandates = partyElements[3];
				}

                if (mandates && diffMandates) {
                    if (backgroundColor == 'hasTableBackground')
                        backgroundColor = '';
                    else
                        backgroundColor = 'hasTableBackground';

                    row1 += "<td class='votesLogo "+backgroundColor+"'>";
                        
                    if (partyId <= 11) {
                        row1 += "<img src='images/logo/" + partyId + "_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='logo' />";
                    }else if (partyId == 9) {
                        row1 += "<img src='images/logo/107_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='logo' />";
                    }
                    else {
                        var shortName = getPartyShortName(partyId); 

                        row1 += ((shortName.length > 3) ? shortName.substring(0, 3) : shortName);
                        names.push(((shortName.length > 3) ? shortName.substring(0, 3) : shortName) + " = " + getPartyName(partyId));
                    }

                    row1 += "</td>";
                    
                    row2 += "<td class='votesValue " + backgroundColor + "'>" + mandates + "</td>";
                    row3 += "<td class='votesDiff " + backgroundColor + "'>" + (diffMandates > 0 ? '+' + diffMandates : diffMandates) + "</td>";
                    row4 += "<td class='votesDirection " + backgroundColor + "'>";

                    if (diffMandates > 0)
                        row4 += "<img src='images/up_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='up' />";
                    else if (diffMandates < 0)
                        row4 += "<img src='images/down_" + ((window.innerWidth >= 480) ? '480' : '320') + ".png' alt='down' />";
                    else
                        row4 += "&nbsp;";

                    row4 += "</td>";   
                }            
            }
        }
    }

    innerHTML += row1 + "</tr>";
    innerHTML += row2 + "</tr>";
    innerHTML += row3 + "</tr>";
    innerHTML += row4 + "</tr>";
    innerHTML += "</table>";
	if (state == 'Polls'){
		innerHTML += "<div class='storHeader contentHeadline mandatesheadline'>MANDATFORDELINGEN</div>";
		innerHTML += "<div id='regionsHeader'>Landsdekkende målinger</div>";
		
	}else if (state == 'contyPolls'){
	
	}else {
		innerHTML += "<div class='storHeader contentHeadline mandatesheadline'>MANDATFORDELINGEN</div>";
		innerHTML += "<div id='regionsHeader'>"+ regionName +"</div>";
	}
    innerHTML += "</div>";

    innerHTML += "<div class='partyNames'>";
    for (var k = 0; k < names.length; k++) {
        innerHTML += names[k] + "<br/>";
    }
	
    innerHTML += "</div>";
	
    if (state == 'nationPolls' || state =='Polls' || state == 'countyPolls'){ 
		innerHTML += parsePollPanel(selectedPollIndex, 'mandates');
	}else {
		//innerHTML += parseVotesCountet();	
	}
    if (state == 'countyPolls') {
		innerHTML += "<div class='zusatzInfo'>Prognosen omfatter ikke evt. Utjevningsmandater</div>";
	}
    return innerHTML;
}
function getLogoXCoordinate(partyId) {
    switch (partyId) {
        case '10':				
            //return 124;
			return 217;
            break;
        case '8':
            //return 124;
			return 124;
            break;
        case '7':
            //return 124;
			return 124;
            break;
        case '6':
            //return 160;
			return 160;
            break;
        case '5':
            //return 217;
			return 124;
            break;
        case '4':
            return 274;
            break;
        case '3':
            return 310;
            break;
        case '2':
            return 310;
            break;
        case '1':
            return 310;
            break;
    }
}
function getLogoYCoordinate(partyId) {
    switch (partyId) {
        case '10':
            //return 5;
			return 170;
            break;
        case '8':
            //return 62;
			return 5;
            break;
        case '7':
            //return 120;
			return 62;
            break;
        case '6':
            return 170;
            break;
        case '5':
            //return 170;//200;
			return 120;
            break;
        case '4':
            return 170;
            break;
        case '3':
            return 120;
            break;
        case '2':
            return 62;
            break;
        case '1':
            return 5;
            break;
    }
}
function getXAxisPos(partiId){
	 switch (partiId) {
        case '1':
            return 130;
            break;
        case '2':
            return 230;
            break;
        case '3':
            return 130;
            break;
        case '4':
            return 130;
            break;
        case '5':
            return 300;
            break;
        case '6':
            return 390;
            break;
        case '7':
            return 390;
            break;
        case '8':
            return 440;
            break;
        case '9':
            return 300;
            break;
    }
}
function getYAxisPos(partiId) {
	 switch (partiId) {
        case '1':
            return 40;
            break;
        case '2':
            return 150;
            break;
        case '3':
            return 150;
            break;
        case '4':
            return 250;
            break;
        case '5':
            return 200;
            break;
        case '6':
            return 40;
            break;
        case '7':
            return 150;
            break;
        case '8':
            return 240;
            break;
        case '9':
            return 260;
            break;
    }
}
function getRotationForSeat(seatId) {
    var degrees = 0;

    // first group of rotation (seats 23 - 27) - 11,25'
    if ((seatId > 25) && (seatId <= 30))
        degrees = 11, 25;

    // second group of rotation (seats 28 - 37) - 22,5'
    if ((seatId > 30) && (seatId <= 40))
        degrees = 22, 5;

    // third group of rotation (seats 38 - 42) - 33,75'	
    if ((seatId > 40) && (seatId <= 45))
        degrees = 33, 75;

    // fourth group of rotation (seats 43 - 53) - 45'
    if ((seatId > 45) && (seatId <= 56))
        degrees = 45;

    // fifth group of rotation (seats 54 - 59) - 56,75'
    if ((seatId > 56) && (seatId <= 62))
        degrees = 56, 75;

    // sixth group of rotation (seats 60 - 71) - 67,5'
    if ((seatId > 62) && (seatId <= 74))
        degrees = 67, 5;

    // seventh group of rotation (seats 72 - 76) - 78,75'
    if ((seatId > 74) && (seatId <= 79))
        degrees = 78, 75;

    // eight group of rotation (seats 77 - 82) - 90'
    if ((seatId > 79) && (seatId <= 85))
        degrees = 90;

    if ((seatId > 85) && (seatId <= 91))
        degrees = 90;

    if ((seatId > 91) && (seatId <= 96))
        degrees = 90 + 11, 25;

    if ((seatId > 96) && (seatId <= 108))
        degrees = 90 + 22, 5;

    if ((seatId > 108) && (seatId <= 114))
        degrees = 90 + 33, 75;

    if ((seatId > 114) && (seatId <= 125))
        degrees = 90 + 45;

    if ((seatId > 125) && (seatId <= 129))
        degrees = 90 + 56, 75;

    if ((seatId > 129) && (seatId <= 139))
        degrees = 90 + 67, 5;

    if ((seatId > 139) && (seatId <= 144))
        degrees = 90 + 78, 75;

    if (seatId > 144)
        degrees = 90 + 90;

    return degrees;
}

function getXCoordinateForMandateSeat(index) {
    return seatXCoordinates[index];
}
function getYCoordinateForMandateSeat(index) {
    return seatYCoordinates[index];
}

function parseReps(partyResults, selectedPollIndex) {
    var innerHTML = '';
    //reset vars
    cur_X = 0;
    total_width = 0;
    total_nr_of_columns = 0;
	var topParti= '';
	var topPer = 0;
innerHTML += "<div class='contentDiv'>";
if (state == 'countyPolls') {
		innerHTML += "<div id='manHeader'>MENINGSMÅLINGER</div>";
		innerHTML += "<div id='regionsHeader'>Mandatfordelingen i "+ regionName +"</div>";
	}else {
		//innerHTML += "<div id='manHeader'>"+ topParti +" STØRST</div>";
		innerHTML += "<div id='manHeader'>MANDATFORDELINGEN</div>";
		innerHTML += "<div id='regionsHeader'>i "+ regionName +"</div>";
		//innerHTML += "<div id='regionsHeader'>De "+ topParti +" har flest mandater i "+ regionName +"</div>";
	}
innerHTML += "<div id='manWrapper'>";
	var sortedPartyResults = [];
	var newPartyResults = partyResults.clean("\r");
		sortedPartyResults = newPartyResults.slice(0).sort(sortMyResults);
    for (var i = 0; i < partyResults.length; i++) {
		/*if (i == 8){ // andre including mdg
			var partyResult = partyResults[10];	// andre uten mdg
		}else {
			var partyResult = partyResults[i];
		}*/
		
		
		if ( i == 8){ 
			var partyResult = partyResults[7];
		}else if (i == 4) {
			var partyResult = partyResults[9];
		}else if (i == 5){
			var partyResult = partyResults[4];
		}else if (i == 6){
			var partyResult = partyResults[5];
		}else if (i == 7){
			var partyResult = partyResults[6];
		}else if ( i == 9) {
			if (isValg && state != 'Polls' && state != 'countyPolls') {
				var partyResult = '';
			}else {
				var partyResult = partyResults[10];
			}
		}else if (i == 10){
			if (isValg && state != 'Polls' && state != 'countyPolls') {
				var partyResult = '';
			}else {
				var partyResult = '';
			}
		}else {
			var partyResult = partyResults[i];
		}
		
        if (partyResult) {
            var partyElements = partyResult.split(';');

            var partyId = partyElements[0];
			if (isValg == true)
				if (partyId == '9') continue;
            
            var newParty = true;
            var mandates = null;
            var mandatesChange = null;

            if (state == 'Polls' || state == 'countyPolls' || (state == 'Fav' && isValg == false)) {
                mandates = partyElements[1];
                mandatesChange = partyElements[2];
            }
            else if (state == 'Municipality') { //district or municipality
                if (partyElements.length > 6 &! countyElectionMode) { //not district
                    mandates = partyElements[6];
                    mandatesChange = partyElements[7];
                }
            }else if ( region == 'c12m1201'|| region == 'c16m1601' || region == 'c11m1103') {
					mandates = partyElements[6];
                    mandatesChange = partyElements[7];
			}
            else if (state == 'County' || state == 'Nation' || (state == 'Fav' && isValg == true)) { //county
                if (countyElectionMode)
                    mandates = partyElements[1];
                    mandatesChange = partyElements[2];
            }
            if (parseFloat(mandates) > parseFloat(topPer)) {
						topPer = mandates;
						if (partyId == 2 || partyId == 6 ){
							topParti = getPartyShortName(partyId);
						}else {
							topParti = getPartyName(partyId);
						}
						topParti = topParti.toUpperCase();
						
					}
            if (mandates && mandatesChange) {
                if (mandates > 0) {
					
                    for (var mandate = 1; mandate <= mandates; mandate++) {

                        var logoId = partyId//((partyId == 9) ? 107 : partyId);  /***HACK***/ ((partyId > 9) ? 9 : partyId);
                        var x = getXCoordinateForSeat(mandate, newParty);
                        var y = getYCoordinateForSeat(mandate);

                        //innerHTML += "<img style='-moz-transform: rotate(90deg);-webkit-transform: rotate(90deg);width:" + SEATS_WIDTH + "px;height:" + SEATS_HEIGHT + "px;position:absolute;top:" + y + "px;left:|" + x + "|px;' src='images/seats/" + logoId + ".png' alt='seat' />";
						innerHTML += "<img style='position:absolute;top:" + y + "px;left:|" + x + "|px;' src='images/seats/" + logoId + "M.png' alt='seat' />";
                        newParty = false;
                    }
					 //value			
                    innerHTML += "<div class='callOutValue' style='top:" + (y - 35) + "px;left:|" + x + "|px;'>" + mandates + "</div>";

                    //tip
                    //innerHTML += "<img style='z-index:1000;position:absolute;bottom:" + (height + 11) + "px;left:" + (left + 10) + "px;' src='images/pad/tip.png' alt='tip' />";
                    
                    //diff value
					 var mandatesChangeF =  (mandatesChange > 0) ? '+' + mandatesChange: mandatesChange ;
                    innerHTML += "<div class='callOutDiff' style='top:" + (y - 20 - 20 -10) + "px;left:|" + x + "|px;'>" + mandatesChangeF + "</div>";

                    //diff arrow
                    var imgSrc = (mandatesChange > 0) ? 'images/up_black_480.png' : ((mandatesChange < 0) ? 'images/down_black_480.png' : '');
                    
                    if (imgSrc != '')
                        innerHTML += "<img class='arrow' src='" + imgSrc + "' style='top:" + (y - 20 -20-20) + "px;left:|" + (x +8) /*(x + (SEATS_WIDTH / 2) - (15 / 2))*/ + "|px;' />";
					//logo
                    //innerHTML += "<div style='margin-bottom:-50px;border-right:1px #000000 solid;position:absolute;bottom:0px;left:" + left + "px;'>";
					innerHTML += "<div style='border-right:1px #000000 solid;position:absolute;top:"+(getYCoordinateForSeat(0)+1)+"px;left:|" + (x+5) + "|px;'>";
					if ( isValg == true) {
                    if (partyId <= 10)
                        innerHTML += "<img style='position:absolute;width:" + (SEATS_WIDTH  - 8)+ "px;' src='images/logo/" + partyId + "_480.png' alt='logo'/>";
                    else {
						
                        var shortName = getPartyShortName(partyId);

                        innerHTML += "<div style='font-size:10pt;overflow:auto;position:absolute;top:0;width:" + (SEATS_WIDTH + 5) + "px;'>" + ((shortName.length > 3) ? shortName.substring(0,3) : shortName) + "</div>";                        
                    }
					}else {
					if (partyId < 9)
                        innerHTML += "<img style='position:absolute;width:" + (SEATS_WIDTH  - 8)+ "px;' src='images/logo/" + partyId + "_480.png' alt='logo'/>";
                    else {
						if (partyId == 9){
							//partyId	= 107;
							}
                        var shortName = getPartyShortName(partyId);
						innerHTML += "<img style='position:absolute;width:" + (SEATS_WIDTH  - 8)+ "px;' src='images/logo/" + partyId + "_480.png' alt='logo'/>";
                        //innerHTML += "<div style='font-size:10pt;overflow:auto;position:absolute;top:0;width:" + (SEATS_WIDTH + 5) + "px;'>" + ((shortName.length > 3) ? shortName.substring(0,3) : shortName) + "</div>";                        
                    }
					
					
					
							
						
					
					}
                    innerHTML += "</div>";
                    //label
                   // innerHTML += addLabel(partyId, mandates, mandatesChange);
				   
                }
               else if (mandatesChange != 0) {

                    //manually update x
                    cur_X += ((SEATS_WIDTH - 5) * COLUMN_GAP);
                    total_width += ((SEATS_WIDTH - 5) * COLUMN_GAP);
					var x = cur_X ;//getXCoordinateForSeat(1, false);
					var y = getYCoordinateForSeat(0);
					
					
					 //value			
                    innerHTML += "<div class='callOutValue' style='top:" + (y - 35) + "px;left:|" + x + "|px;'>" + mandates + "</div>";

                    //tip
                    //innerHTML += "<img style='z-index:1000;position:absolute;bottom:" + (height + 11) + "px;left:" + (left + 10) + "px;' src='images/pad/tip.png' alt='tip' />";
                    var mandatesChangeF =  (mandatesChange > 0) ? '+' + mandatesChange: mandatesChange ;
                    //diff value
                    innerHTML += "<div class='callOutDiff' style='top:" + (y - 20 - 20 -10) + "px;left:|" + x + "|px;'>" +mandatesChangeF+ "</div>";

                    //diff arrow
                    var imgSrc = (mandatesChange > 0) ? 'images/up_black_480.png' : ((mandatesChange < 0) ? 'images/down_black_480.png' : '');
                    
                    if (imgSrc != '')
                        innerHTML += "<img class='arrow' src='" + imgSrc + "' style='top:" + (y - 20 -20-20) + "px;left:|" + (x +8) /*(x + (SEATS_WIDTH / 2) - (15 / 2))*/ + "|px;' />";
					//logo
                    //innerHTML += "<div style='margin-bottom:-50px;border-right:1px #000000 solid;position:absolute;bottom:0px;left:" + left + "px;'>";
					innerHTML += "<div style='border-right:1px #000000 solid;position:absolute;top:"+(getYCoordinateForSeat(0)+1) +"px;left:|" + (x+5) + "|px;'>";
                    if (partyId <= 10)
                        innerHTML += "<img style='position:absolute;width:" + (SEATS_WIDTH -8)+ "px;' src='images/logo/" + partyId + "_480.png' alt='logo'/>";
                    //else if (partyId == 9){
						//innerHTML += "<img style='position:absolute;width:" + (SEATS_WIDTH -8)+ "px;' src='images/logo/107_480.png' alt='logo'/>";
					//}
					else {

                        var shortName = getPartyShortName(partyId);

                        innerHTML += "<div style='font-size:10pt;overflow:auto;position:absolute;top:0;width:" + (SEATS_WIDTH + 5) + "px;'>" + ((shortName.length > 3) ? shortName.substring(0,3) : shortName) + "</div>";                        
                    }

                    innerHTML += "</div>";
					
					
					
					

                    //label
                    //innerHTML += addLabel(partyId, mandates, mandatesChange);
                }                
            }
        }
    }
	/*if (state == 'countyPolls') {
		innerHTML += "<div id='manHeader'>MENINGSMÅLINGER</div>";
		innerHTML += "<div id='regionsHeader'>Mandatfordelingen i "+ regionName +"</div>";
	}else {
		//innerHTML += "<div id='manHeader'>"+ topParti +" STØRST</div>";
		innerHTML += "<div id='manHeader'>MANDATFORDELINGEN</div>";
		innerHTML += "<div id='regionsHeader'>i "+ regionName +"</div>";
		//innerHTML += "<div id='regionsHeader'>De "+ topParti +" har flest mandater i "+ regionName +"</div>";
	}*/
   innerHTML += "</div></div>";
    if (state == 'Polls' || state == 'countyPolls' || (state == 'Fav' && isValg == false))
        innerHTML += parsePollPanel(selectedPollIndex, 'mandates');

    if (changeRegions[region] != null) {
        var changeRegion = changeRegions[region];
        if (changeRegion.length == 3) {
            innerHTML += "<div class='boardChangeLabel'>Kommunestyret endres fra " + changeRegion[0] + " til " + changeRegion[1] + " representanter</div>";
        }
    }
    
 
    return innerHTML;
}

function addLabel(partyId, mandates, mandatesChange) {
    var innerHTML = '';
    var y = cur_Y + (SEATS_HEIGHT * SEATS_PR_COLUMN) + LABEL_GAP;
    var x = getXCoordinateForLabel(mandates);

    //seats
    innerHTML += "<div class='seatsRepsLabel' style='top:" + y + "px;left:|" + x + "|px;width:" + SEATS_WIDTH + "px;'>" + mandates + "</div>";

    //seats change
    var text = '';

    if (mandatesChange > 0)
        text = "+" + mandatesChange;
    else if (mandatesChange < 0)
        text = mandatesChange;

    innerHTML += "<div class='seatsRepsLabel' style='font-size:10pt;font-style:italic;top:" + (y -200/*+ 25*/) + "px;left:|" + x + "|px;width:" + SEATS_WIDTH + "px;'>" + text + "</div>";

    //label
    if (partyId > 9) {
        var party = parties[partyId];
        if (party) {
            innerHTML += "<div class='seatsRepsLabel' style='font-size:10pt;top:" + (y + 50) + "px;left:|" + x + "|px;width:" + SEATS_WIDTH + "px;'>" + ((party.shortName.length > 3) ? party.shortName.substring(0,3) : party.shortName) + "</div>";
        }
    }
    else {
        var partyLogoSrc = getPartyLogoSrc(partyId);
        if (partyLogoSrc != '') {
            var add = ((partyId == 9 || partyId == 7) ? 60 : 50);
            innerHTML += "<img class='seatsRepsLogo' style='left:|" + x + "|px;top:" + (y + add) + "px;' src='" + partyLogoSrc + "' alt='logo'/>";
        }           
    }

    return innerHTML;
}

function getXCoordinateForLabel(mandates) {
    var last_column_x = cur_X;
    var nr_of_columns_pr_party = Math.ceil(mandates / SEATS_PR_COLUMN);

    if (nr_of_columns_pr_party > 1) {
        var first_column_x = last_column_x - (SEATS_WIDTH * (nr_of_columns_pr_party - 1));
        var xPos = (last_column_x + first_column_x) / 2;
        last_column_x = xPos;
    }

    return last_column_x;
}

function getXCoordinateForSeat(seatId, newParty) {

    if ((seatId % SEATS_PR_COLUMN) == 1) {
        if (newParty) {
            cur_X += ((SEATS_WIDTH - 5) * COLUMN_GAP);
            total_width += ((SEATS_WIDTH - 5) * COLUMN_GAP);
        }
        else {
            cur_X += (SEATS_WIDTH - 5);
            total_width += (SEATS_WIDTH - 5);
        }

        total_nr_of_columns++;
    }

    return cur_X;
}

function getYCoordinateForSeat(seatId) {
    var factor = seatId;

    if (seatId > SEATS_PR_COLUMN) {
        factor = seatId % SEATS_PR_COLUMN;
        if (factor == 0)
            factor = SEATS_PR_COLUMN;
    }

    return cur_Y + ((SEATS_HEIGHT /*+ 10*/) * (SEATS_PR_COLUMN - factor));
}

function centerComponent(innerHTML) {
    var totalWidth = 460;
    var seatsWidth = total_width;

    var addX = (totalWidth / 2) - (seatsWidth / 2);
    addX -= SEATS_WIDTH;

    var patt = /\|([0-9]+\.?[0-9]+?)\|/;

    //loop all columns up to total_nr_of_columns
    for (var columnNr = 0; columnNr <= total_nr_of_columns; columnNr++) {
        //loop 5 seats for each column
        for (var seat = 0; seat < (SEATS_PR_COLUMN + 3); seat++) { //add 3 to ensure we also loop all logos, mandates and mandateChanges
            var match = patt.exec(innerHTML);

            if (match && (match.length == 2)) {
                //regular expression match gives full expression in index 0, and number only in index 1
                var oldNumber = Number(match[1]);
                var newNumber = oldNumber + addX;


                innerHTML = innerHTML.replace(match[0], newNumber);

            }
        }
    }

    return innerHTML;
}

function parseTable(partyResults, akkuArray){
	var innerHTML = '';
	var backgroundColor = '#FFFFFF';
	var switchy = false;
	innerHTML += "<div id='regionsHeader'>"+ regionName +"</div>";
	innerHTML += "<div id='tableWrapper'><div id='tableInnerWrapper'><div class='upButton' onclick=\"scrollTable('up');\"><img src='images/button/upBtn.png' /></div><div class='downButton' onclick=\"scrollTable('down');\"><img src='images/button/downBtn.png' /></div>";
	innerHTML += "<table id='tableContainer' border='0px' cellpadding='1' cellspacing='0'>";
	innerHTML += "<tr>";
	innerHTML += "<td class='tableHeader hasBackground' style='padding-left:2px;border-left:0;width:30%;'>PARTI</td>";
	innerHTML += "<td class='tableHeader hasBackground' >STEMMER</td>";
	innerHTML += "<td class='tableHeader hasBackground' >%</td>";
	innerHTML += "<td class='tableHeader hasBackground' >ENDRING - 11</td>";
	innerHTML += "<td class='tableHeader hasBackground' >ENDRING - 09</td>";
	 if (state == 'Municipality' ||region  == 'c12m1201'||region  == 'c16m1601' || region  == 'c11m1103') {
	 
	 }else {
	innerHTML += "<td class='tableHeader hasBackground' >MANDATER</td>";
	innerHTML += "<td class='tableHeader hasBackground' style='border-right:1px #E4E4E4 solid;'>ENDRING</td>";
	}
	innerHTML += "</tr>";
        
	for (var i = 0;i < partyResults.length; i++){
		var partyResult = partyResults[i];			
		var partyElements = partyResult.split(';');

		if (partyElements.length >= 6) {
		    var partyId = partyElements[0];
	        
		    if (partyId == '9') continue;
			//if (partyId == '10') continue;
			if (partyId == '11') continue;
			if (partyId == '1000') continue;
		    var party = parties[partyId];
		    
		    if (party) {
		        var partyName = party.name;
		        var votes = '0';
		        var percent = '0';
		        var percentChange09 = '0';//11
		        var percentChange07 = '0';//9
		        var mandates = '0';
		        var mandatesChange = '0';

		        if (state == 'Municipality'  ||region  == 'c12m1201'||region  == 'c16m1601' || region  == 'c11m1103') { //district or municipality
		            
		            if (region.length > 8) {
		                votes = partyElements[5];
		                percent = partyElements[1];
		                percentChange09 = partyElements[4];//11
		                percentChange07 = partyElements[2];//09
		            }
		            else {
		                votes = partyElements[5];
		                percent = partyElements[1];
		                percentChange09 = partyElements[4];
		                percentChange07 = partyElements[2];
		                //mandates = partyElements[6];
		                //mandatesChange = partyElements[7];		                
		            }
		        }
		        else if (state == 'Nation' || state == 'County' ) { //county or nation
		            votes = partyElements[8];
		            percent = partyElements[3];
		            percentChange09 = (state == 'County') ? partyElements[7] : partyElements[7];
		            percentChange07 = (state == 'County') ? partyElements[4] : partyElements[4];
		            mandates = partyElements[1];
		            mandatesChange = partyElements[2];
					
		            /*if (akkuArray.length == 2) {
		                mandates = (akkuArray[0][partyId] ? akkuArray[0][partyId] : '0');
		                mandatesChange = (akkuArray[1][partyId] ? akkuArray[1][partyId] : '0');
		            }*/
		        }

		        if (backgroundColor == '#FFFFFF'){
		            backgroundColor = '#19ACC8';
					switchy = false;
					
		        }else if (backgroundColor == '#19ACC8'){
		            backgroundColor = '#FFFFFF';
					switchy = true;
				}
		        innerHTML += "<tr class='"+(switchy == true ? '' : 'hasBackgroundLight')+"'>";
		        innerHTML += "<td style='padding-left:5px;border-bottom:1px #E4E4E4 solid;'>" + partyName + "</td>";
		        innerHTML += "<td class='tableData'>" + addThousandSeparator(votes, ' ') + "</td>";
		        innerHTML += "<td class='tableData'>" + roundNumber(percent, 1) + "%</td>";
		        innerHTML += "<td class='tableData'>" + (percentChange09 > 0 ? '+' : '') + roundNumber(percentChange09, 1) + "%</td>"; //change -09
		        innerHTML += "<td class='tableData'>" + (percentChange07 > 0 ? '+' : '') + roundNumber(percentChange07, 1) + "%</td>"; //change -07
		        if (state == 'Municipality' ||region  == 'c12m1201'||region  == 'c16m1601' ||region  == 'c11m1103') {
	 
				}else {
				innerHTML += "<td class='tableData'>" + mandates + "</td>"; //mandates
		        innerHTML += "<td class='tableData' style='border-right:1px #E4E4E4 solid;'>" + (mandatesChange > 0 ? '+' : '') + mandatesChange + "</td>"; 
				}
				//change mandates
		        innerHTML += "</tr>";		           
		    }
	    }
	}

	innerHTML += "</table></div></div>";
	
    innerHTML += "<div class='tableInfo'>Frem til endelig resultat foreligger er mandater basert på prognose</div>";

	return innerHTML;
	
}

function openHistory(){
	var innerHTML = "";
    var historyData = null;
	$('#partier').html('');
    if (historyDataArray) {

        if (state == 'Polls' || state == 'countyPolls' || state == 'nationPolls')
            historyData = historyDataArray['Polls'];//historyData = historyDataArray[state];
        else
            historyData = historyDataArray[region];
    }
   if (historyData){
		var history = historyData.split("|");
		if (state == 'Polls') {
			innerHTML += "<div class='storHeader contentHeadline'>LANDSDEKKENDE MÅLINGER</div>";
			innerHTML += "<div id='regionsHeader'>Gjennomsnittsberegning</div>";
		}else if (state == 'countyPolls'){
		
		}else if (state == 'Nation'){
			innerHTML += "<div class='storHeader contentHeadline'>OPPSLUTNING "+ regionName +"</div>";
		}else {
			innerHTML += "<div class='storHeader contentHeadline'>OPPSLUTNING "+ regionName +"</div>";
		}
		innerHTML += "<div id='historyWrapper'>"; 
	
		if (history.length > 2){	
			var regionIDStr = history[0].toString();
			var pollMode = (history[1].toString() == "1");
			var maxPercent = 0;

			maxPercent = getHistoryMaxPercent(history);

			if (maxPercent > 0) {
			    var startPos = 20;
			    var x = 0 + startPos;
			    var innerWidth = 360;
			    var xIncrement = innerWidth / (history.length - 3); //40; //space between points
			    var pointsWidth = innerWidth + 12; //add width of last point 12px
			    var prevPoints = [];
				
			    innerHTML += getHistoryBackground(maxPercent, pointsWidth, xIncrement, startPos);
				
				for( var j = 2; j < history.length; j++ ) {						
					var pointInTime = history[j].split(";"); //aug.-10;1#1.8;2#5.6;3#26.6;4#5.0;5#4.6;6#5.3;7#27.1;8#22.8;9#1.1
							
					// check that there are not too few entries
					if( pointInTime.length < 10 ) { //name + 9 parties (includes other)
						return '';
					}
					
					myHCHistoryLabel[j -2] = pointInTime[0].toString();
					
		            var label = pointInTime[0].toString();
					label = label.split('-')[0];
				if (state != 'Polls' && state != 'Municipality'){
		            if (!historyShowStortingResults) {
						
		                if (stortingsValg.indexOf(label) == -1) // != -1)
		                    continue;
		            }
				}   
					//innerHTML += "<div class='historyLabel' style='bottom:-10px;left:" + (x-5) + "px;'>" + label + "</div>";
					innerHTML += "<div class='historyLabel' style='left:" + (x-5) + "px;'>" + label + "</div>";
					//iterate points for 1 point in time
					 
					for (var k = 1; k < 10/*pointInTime.length*/; k++){
						if ( isValg && k == 9 ) {
							var pointData = pointInTime[pointInTime.length-1];
						}else {
							var pointData = pointInTime[k];	//3#26.6	
						}
						
								
						var pointElements = pointData.split("#");
						
						if (pointElements.length < 2){
							return '';
						}
						
						var partyId = pointElements[0];
						var percent = pointElements[1];
						//myHCHistory[pointElements[0]].push(parseFloat(pointElements[1]));
						var partyColor = getPartyColor(partyId);	
						if (window.innerWidth > 850 )
							var y = 320 - ((320 * (percent / maxPercent))) ;
						else
							var y = componentHeight - ((componentHeight * (percent / maxPercent))) ;

					    y = y - 6;
					    var lineX = x + 6;
					    var lineY = y+6;
						
						if (prevPoints.length == 9){
							var prevPoint = prevPoints[k - 1];
							
							if (prevPoint.length == 2){
								var line = createLine(prevPoint[0], prevPoint[1], lineX, lineY, partyColor, 3);
								
								if (historyExceptions[partyId - 1] == 1)
									innerHTML += line.outerHTML;												
							}
						}
						
						//updateprevpoints before iteration
						prevPoints[k - 1] = [lineX, lineY];
						
						if (historyExceptions[partyId - 1] == 1){
							//innerHTML += "<div ontouchend='hideTooltip()' ontouchstart=\"showTooltip(event," + partyId + "," + percent + ",'" + label + "')\" class='historyPoint' style='left:" + x + "px;top:" + y + "px;background-color:" + partyColor + ";'></div>";							
							innerHTML += "<div onmouseout='hideTooltip();' onmouseover=\"showTooltip2(event," + partyId + "," + percent + ",'" + label + "', " + x + ", " + y + ")\" onclick=\"showTooltip(event," + partyId + "," + percent + ",'" + label + "', " + x + ", " + y + ")\" class='historyPoint' style='left:" + x + "px;top:" + y + "px;background-color:" + partyColor + ";'></div>";			
						}
					}
					//console.log(x+ " " + j + " " + xIncrement + " " +startPos);
					
					if (!historyShowStortingResults && (state != 'Municipality' && region != 'c12m1201' && region  != 'c16m1601' && region  != 'c11m1103' && state != 'Polls')) {
						x = ((j) * xIncrement) + startPos;
					}else{
						x = ((j -1) * xIncrement) + startPos;
					}
				}	
			
				//add tooltipcontainer
				innerHTML += "<div id='historyTooltip' class='historyTooltip'></div>";
			
				//add legends
				var legendsLeft = pointsWidth + 25 + startPos ;//+20;
				var legendsIncrementTop = 20;
				innerHTML += "<a href='#' class='historyLegendHeader' onclick='selectAllHistoryLegend();' style='width:80px;left:" + legendsLeft + "px;top:"+(window.innerWidth < 800 ? -5 : 40 ) +"px;color:#7D7B7E;'>VIS ALLE</a>";
			
				var color = "#FFFFFF";
			
				//TODO: hardcoded 9 parties
				for (var partyId = 1; partyId < 10; partyId++){
					var top = legendsIncrementTop * partyId -20;
					var partyName = parties[partyId].shortName;
					var partyColor = getPartyColor(partyId);
					
					if (partyId == 9 || partyId == 1) partyName = parties[partyId].name;
					
					if (color == "#CECECE")
						color = "#FFFFFF";
					else if (color == "#FFFFFF")
						color = "#CECECE";
					
					innerHTML += "<div class='historyLegendBackground' style='left:" + legendsLeft + "px;top:" +((window.innerWidth < 800 ? 25 : 70 ) + top) + "px;background-color:" + color + ";'>";
					
					innerHTML += "<input id='" + partyId + "' class='historyLegendCheckBox' onchange='checkHistoryLegend(this);' type='checkbox' id='" + partyId + "' value='" + partyId + "' " + ((historyExceptions[partyId - 1] == 1)?'checked':'') + " />";
					
					innerHTML += "<div class='historyLegendLabel'>" + partyName + "</div>";
					
					//var partyLine = createLine(65,11,85,11,partyColor,4);
					//innerHTML += partyLine.outerHTML;
					
					innerHTML += "</div>";
				}				
				
				innerHTML += "<div class='historyLegendHeader' onclick='deSelectAllHistoryLegend();' style='width:80px;left:" + legendsLeft + "px;top:" + (legendsIncrementTop * 10 +(window.innerWidth < 800 ? 15 : 60 )) + "px;color:#7D7B7E;'>SKJUL ALLE</div>";

				innerHTML += "<div style='position:absolute;bottom:10px;left:160px;display:" + ( (state == 'Nation' || (state == 'County' && (region != 'c12m1201' && region  != 'c16m1601' && region  != 'c11m1103'))) ? '' : 'none') + ";'><input id='checkElection' class='historyLegendCheckBox' style='left:-25px;' onchange='checkChangeElection(this);' type='checkbox' " + ((historyShowStortingResults) ? 'checked' : '') + " /> VIS FYLKESTINGVALG</div";
            }
		}
		
		innerHTML += "</div>";
	}	
	
    var historyElement = document.getElementById('history');
    if (historyElement)
        historyElement.innerHTML = innerHTML;
  
 
}

function getHistoryBackground(maxPercent, width, verticalIncrement, startPos){
	var innerHTML = '';
	
	var interval = (maxPercent <= 10)? 2 : ((maxPercent == 100)? 20 : 10);
	var nrOfRows = maxPercent / interval;
	var rowHeight = (window.innerWidth > 820)? 320 / nrOfRows : componentHeight / nrOfRows;	
	var extraPx = (window.innerWidth > 820)? 320 - (Math.floor(rowHeight) * nrOfRows) : componentHeight - (Math.floor(rowHeight) * nrOfRows);
	var background = '';

	for (var i=1;i <= nrOfRows; i++){
		var label = ((nrOfRows) * interval) - ((i-1) * interval);
			
		innerHTML += "<div style='border-bottom:2px solid #DEF2F6;left:" + startPos + "px;position:relative;width:" + width + "px;";
		
		if (i == nrOfRows)
			innerHTML += "border-bottom:1px #000000 solid;height:" + (rowHeight + extraPx) + "px;";
		else
			innerHTML += "height:" + (rowHeight) + "px;";
		if(label == 10 || label == '10'){		
			innerHTML += "'><p class='axisStyle' style='left:-" + (startPos + 4) + "px;'>" + label + "</p>";
		
			innerHTML += "<p class='axisStyle' style='bottom:-5px;top:40px;left:-" + (startPos + 4) + "px;'>%</p></div>";
		}else {
			innerHTML += "'><p class='axisStyle' style='left:-" + (startPos + 4) + "px;'>" + label + "</p></div>";
		}
	}
	
	//add vertical strokes
	for (var inc = startPos + 6 + (verticalIncrement / 2); inc < width; inc += verticalIncrement){
	    var vertLine = createLine(
	        (inc),
	        0,
	        (inc), 
	        ((window.innerWidth > 820)? 320 : componentHeight), "#AAAAAA", 1); //adding 6 is half of the points width
		
	    innerHTML += vertLine.outerHTML;	
	}
	

	return innerHTML;
}

function createLine(x1, y1, x2, y2, color, weight, id){
	if (x2 < x1){
		var temp = x1;
		x1 = x2;
		x2 = temp;
		temp = y1;
		y1 = y2;
		y2 = temp;
	}
	
	var line = document.createElement("div");
	line.className = "line";
	
	if (id)
		line.id = id;
	
	var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
	line.style.width = length + "px";
	line.style.borderColor = color;
	line.style.borderWidth = weight + "px 0px 0px 0px";


	//if (isIE){
	//	line.style.top = (y2 > y1) ? y1 + "px" : y2 + "px";
	//	line.style.left = x1 + "px";
	//	var nCos = (x2-x1)/length;
	//	var nSin = (y2-y1)/length;
	//	line.style.filter = "progid:DXImageTransform.Microsoft.Matrix(sizingMethod='auto expand', M11=" + nCos + ", M12=" + -1*nSin + ", M21=" + nSin + ", M22=" + nCos + ")";
	//}
	//else{
		var angle = Math.atan((y2-y1)/(x2-x1));
		line.style.top = y1 + 0.5*length*Math.sin(angle) + "px";
		line.style.left = x1 - 0.5*length*(1 - Math.cos(angle)) + "px";
		line.style.MozTransform = line.style.WebkitTransform = line.style.OTransform = line.style.msTransform = "rotate(" + angle + "rad)";
	//}
	
	return line;
}

function parseParties(lines, partyResults) {
var d = new Date();
	var innerHTML = '';
	var innerHTML2 = '';
	var innerHTML3 = '';
	var innerHTML4 = '';
	var innerHTML5 = '';
	var innerHTML6 = '';
	var innerHTML7 = '';
	var innerHTML8 = '';
	if (state == 'Nation' || state == 'County' || state == 'Storting' || state == 'Duellen') { 
		
        var subRegionsRaw = (state == 'Nation' || state == 'Storting' || state == 'Duellen') ? lines[10] : (region == 'c12m1201' || region == 'c16m1601' || region =='c11m1103')? lines[10] : lines[11];
		if (region == 'c00m0301'){
			subRegionsRaw = lines[17];
		}
		//1;0101$27.6#0127$17.2#0121$12.2#0128$8.3#0118$7.8;0122$1.2#0106$4.0#0118$7.8#0128$8.3#0121$12.2;0101$29.4#0127$17.9#0121$12.5#0128$8.8#0118$8.2;0122$1.9#0106$5.2#0118$8.2#0128$8.8#0121$12.5|2;0127$9.3#
        //var akkuRaw = (state == 'Nation' || state == 'Storting') ? lines[19] : lines[15]; //"1;4;4;19;17|2;1;-1;3;-15|3;8;-5
        
        // 1 array like this pr party
        //var dataArray = "[" +
        //"['Finnmark;34', 'Østfold;23', 'Hedmark;-23', 'Oppland;-10.5', 'Sogn og Fjordane;-29.7']," +
        //"['Møre og Romsdal;30', 'Sør-Trøndelag;23', 'Hedmark;-23', 'Oppland;-10.5', 'Sogn og fjordane;-29.7']," +
        //max,
        //min,
        //"['Finnmark;34', 'Østfold;23', 'Hedmark;-23', 'Oppland;-10.5', 'Sogn og fjordane;-29.7']," +
        //"['Finnmark;34', 'Østfold;23', 'Hedmark;-23', 'Oppland;-10.5', 'Sogn og fjordane;-29.7']," +
        //max,
        //min    
        //"]";
		
        var partySubRegionResults = subRegionsRaw.split('|').clean("\r");
        for (var i = 0; i <  partySubRegionResults.length/*((partySubRegionResults.length < 8) ? partySubRegionResults.length : 8)*/; i++) {
            //1;
            //0101$27.6#0127$17.2#0121$12.2#0128$8.3#0118$7.8;
            //0122$1.2#0106$4.0#0118$7.8#0128$8.3#0121$12.2;
            //0101$29.4#0127$17.9#0121$12.5#0128$8.8#0118$8.2;
            //0122$1.9#0106$5.2#0118$8.2#0128$8.8#0121$12.5        

            var partySubRegionResult = partySubRegionResults[i].split(";")
			if (partySubRegionResult.length >= 5) {
                var dataArray = [];
				
                // grab party ID
                var partyId = parseInt(partySubRegionResult[0].toString());
				// results for party
                var maxValue = 0;
                var minValue = 0;
                for (var k = 1; k < 5; k++) {
                    var partySubRegions = partySubRegionResult[k].split("#");
                    var typeArray = [];

                    for (var l = 0; l < partySubRegions.length; l++) {
                        var partySubRegion = partySubRegions[l].split("$");
						
                        if (partySubRegion.length == 2) {
                            var regionId = partySubRegion[0].toString();
                            //var regionName = getRegionName( (state == 'Nation' || state == 'Storting') ? ('c' + regionId) : (parseRegionId(regionId)));
							var regionName = regionId.toString();
							
                            var value = parseFloat(partySubRegion[1].toString());

                            if (value > 0 && value > maxValue)
                                maxValue = value;

                            if (value < 0 && value < minValue)
                                minValue = value;

                            var arrayValue = regionName + ";" + value;
                            typeArray.push(arrayValue);
                        }
                    }
					dataArray.push(typeArray);

                    if (k % 2 == 0) {
                        dataArray.push(maxValue);
                        dataArray.push(minValue);
                        maxValue = 0;
                        minValue = 0;
                    }
                }

                partyDataSets[partyId - 1] = dataArray;
				
            }
        }
var g = new Date();
	var muniMax = 0;
        var countyMax = 0;
  		
		innerHTML += "<div class='storHeader'>PARTIENE</div>";
		innerHTML += "<div id='partiWrapper'>";
		innerHTML += "<div id='partiInnerWrapper'>";
		
		/*** LINE ONE ***/
		innerHTML += "<div id='partiLineOne'>";
		
        innerHTML += "<div class='imageWrapper'>";
        innerHTML += "<div class='parti1Logo inlineElement'>";
        innerHTML += "<div class='pLogoWrapper pImg1' onclick=\"selectParty(1);openPartiInfoTT('1');\" ><img  src='images/logo/parti1_big.png' /></div></div>";
		//TODO FIX LINKS
		innerHTML += "<div class='parti2Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg2' onclick=\"selectParty(2);openPartiInfoTT('2');\" ><img  src='images/logo/parti2_big.png' /></div></div>";
		innerHTML += "<div class='parti3Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg3' onclick=\"selectParty(3);openPartiInfoTT('3');\" ><img  src='images/logo/parti3_big.png' /></div></div></div>";
		
		
		innerHTML += generatePartiBox(1);
		
		innerHTML += generatePartiBox(2);
		innerHTML += generatePartiBox(3);
		
		
		innerHTML += "</div>";
		/*** end line one ***/
		
		/*** line 2 ***/
		
		innerHTML += "<div id='partiLineTwo'>";
		
        innerHTML += "<div class='imageWrapper'>";
        innerHTML += "<div class='parti4Logo inlineElement'>";
        innerHTML += "<div class='pLogoWrapper pImg4' onclick=\"selectParty(4);openPartiInfoTT('4');\" ><img  src='images/logo/parti4_big.png' /></div></div>";
		innerHTML += "<div class='parti5Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg5' onclick=\"selectParty(5);openPartiInfoTT('5');\" ><img  src='images/logo/parti5_big.png' /></div></div>";
		innerHTML += "<div class='parti6Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg6' onclick=\"selectParty(6);openPartiInfoTT('6');\" ><img  src='images/logo/parti6_big.png' /></div></div></div>";
		
		
		innerHTML += generatePartiBox(4);
		innerHTML += generatePartiBox(5);
		innerHTML += generatePartiBox(6);
		
		
		innerHTML += "</div>";
		/*** end line 2 ***/
		
		/*** line 3 ***/
		
		
		innerHTML += "<div id='partiLinethree'>";
		
        innerHTML += "<div class='imageWrapper'>";
        //innerHTML += "<div class='parti7Logo inlineElement'>";
        //innerHTML += "<div class='pLogoWrapper pImg7' onclick=\"selectParty(7);openPartiInfoTT('7');\" ><img  src='images/logo/parti7_big.png' /></div></div>";
		innerHTML += "<div class='parti7Logo inlineElement'>";
        innerHTML += "<div class='pLogoWrapper pImg7' onclick=\"selectParty(7);openPartiInfoTT('7');\" ><img  src='images/logo/parti7_big.png' /></div></div>";
		innerHTML += "<div class='parti8Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg8' onclick=\"selectParty(8);openPartiInfoTT('8');\" ><img  src='images/logo/parti8_big.png' /></div></div>";
		innerHTML += "<div class='parti9Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg9' onclick=\"selectParty(9,'"+partyResults+"');openPartiInfoTT('9');\" ><img  src='images/logo/parti9_big.png' /></div></div></div>";
		
		innerHTML += generatePartiBox(7);
		innerHTML += generatePartiBox(8);
		innerHTML += generatePartiBox(9);
		
		innerHTML += "</div>";
		/*** end line 3 ***/
       
    } else {
		
		
		innerHTML += "<div class='storHeader'>PARTIENE</div>";
		innerHTML += "<div id='partiWrapper'>";
		innerHTML += "<div id='partiInnerWrapper'>";
		
		/*** LINE ONE ***/
		innerHTML += "<div id='partiLineOne'>";
		
        innerHTML += "<div class='imageWrapper'>";
        innerHTML += "<div class='parti1Logo inlineElement'>";
        innerHTML += "<div class='pLogoWrapper pImg1' onclick=\"selectParty(1);openPartiInfoTT('1');\" ><img  src='images/logo/parti1_big.png' /></div></div>";
		//TODO FIX LINKS
		innerHTML += "<div class='parti2Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg2' onclick=\"selectParty(2);openPartiInfoTT('2');\" ><img  src='images/logo/parti2_big.png' /></div></div>";
		innerHTML += "<div class='parti3Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg3' onclick=\"selectParty(3);openPartiInfoTT('3');\" ><img  src='images/logo/parti3_big.png' /></div></div></div>";
		
		
		innerHTML += generatePartiBox(1);
		
		innerHTML += generatePartiBox(2);
		innerHTML += generatePartiBox(3);
		
		
		innerHTML += "</div>";
		/*** end line one ***/
		
		/*** line 2 ***/
		
		innerHTML += "<div id='partiLineTwo'>";
		
        innerHTML += "<div class='imageWrapper'>";
        innerHTML += "<div class='parti4Logo inlineElement'>";
        innerHTML += "<div class='pLogoWrapper pImg4' onclick=\"selectParty(4);openPartiInfoTT('4');\" ><img  src='images/logo/parti4_big.png' /></div></div>";
		innerHTML += "<div class='parti5Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg5' onclick=\"selectParty(5);openPartiInfoTT('5');\" ><img  src='images/logo/parti5_big.png' /></div></div>";
		innerHTML += "<div class='parti6Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg6' onclick=\"selectParty(6);openPartiInfoTT('6');\" ><img  src='images/logo/parti6_big.png' /></div></div></div>";
		
		
		innerHTML += generatePartiBox(4);
		innerHTML += generatePartiBox(5);
		innerHTML += generatePartiBox(6);
		
		
		innerHTML += "</div>";
		/*** end line 2 ***/
		
		/*** line 3 ***/
		
		
		innerHTML += "<div id='partiLinethree'>";
		
        innerHTML += "<div class='imageWrapper'>";
		innerHTML += "<div class='parti7Logo inlineElement'>";
        innerHTML += "<div class='pLogoWrapper pImg7' onclick=\"selectParty(7);openPartiInfoTT('7');\" ><img  src='images/logo/parti7_big.png' /></div></div>";
		innerHTML += "<div class='parti8Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg8' onclick=\"selectParty(8);openPartiInfoTT('8');\" ><img  src='images/logo/parti8_big.png' /></div></div>";
		innerHTML += "<div class='parti9Logo inlineElement'>";
		innerHTML += "<div class='pLogoWrapper pImg9' onclick=\"selectParty(9,'"+partyResults+"');openPartiInfoTT('9');\" ><img  src='images/logo/parti9_big.png' /></div></div></div>";
		
		innerHTML += generatePartiBox(7);
		innerHTML += generatePartiBox(8);
		innerHTML += generatePartiBox(9);
		
		innerHTML += "</div>";
		
		
		
		
		
		
		
		
		
		/*
        var backgroundColor = '#FFFFFF';

        innerHTML += "<div style='width:100%;text-align:center;margin-bottom:-30px;color:#5A5A5A;'>Disse partiene har liste</div>";
        
        innerHTML += "<div style='border-top: 2px #C0C0C0 solid;width:100%;margin-top:50px;text-align:center'><table id='tableContainer' style='width:80%;margin-left:auto;margin-right:auto;' border='0px' cellpadding='0' cellspacing='0'>";
        innerHTML += "<tr>";
        innerHTML += "<td class='tableHeader' style='padding-left:4px;border-left:0;'>Parti</td>";
        innerHTML += "<td class='tableHeader' style='padding-left:4px;'>Oppslutning</td>";
        innerHTML += "<td class='tableHeader' style='padding-left:4px;'>Endring</td>";
        innerHTML += "<td class='tableHeader' style='padding-left:4px;'>Mandater</td>";
        innerHTML += "</tr>";


        for (var i = 0; i < partyResults.length; i++) {
            var partyResult = partyResults[i];
            var partyElements = partyResult.split(';');

            if (partyElements.length >= 6) {
                var partyId = partyElements[0];

                if (partyId == '9') continue;
                var party = parties[partyId];

                if (party) {
                    var partyName = party.name;
                    var percent = '0';
                    var percentChange07 = '0';
                    var mandates = '0';

                    if (state == 'Municipality') { //district or municipality
                        
                        if (region.length > 8) {
                            percent = partyElements[1];
                            percentChange07 = partyElements[4];
                            mandates = 0;                            
                        }
                        else {
                            percent = partyElements[1];
                            percentChange07 = partyElements[2];
                            mandates = partyElements[6];                            
                        }
                    }

                    if (backgroundColor == '#FFFFFF')
                        backgroundColor = '#D8E9F2';
                    else if (backgroundColor == '#D8E9F2')
                        backgroundColor = '#FFFFFF';

                    innerHTML += "<tr style='background-color:" + backgroundColor + "'>";
                    innerHTML += "<td style='text-align:left;padding-left:4px;border-bottom:1px #E4E4E4 solid;'>" + partyName + "</td>";
                    innerHTML += "<td class='tableData' style='padding-bottom:0;'>" + roundNumber(percent, 1) + "%</td>";
                    innerHTML += "<td class='tableData' style='padding-bottom:0;'>" + (percentChange07 > 0 ? '+' : '') + roundNumber(percentChange07, 1) + "%</td>"; //change -07
                    innerHTML += "<td class='tableData' style='padding-bottom:0;'>" + mandates + "</td>"; //mandates
                    innerHTML += "</tr>";
                }
            }
        }

        innerHTML += "</table></div>";
		*/
		
    }
	var f = new Date();
	return innerHTML;    
}
function generatePartiBox(parti){
var innerHTML ='';
	if (isValg == true && state != 'Polls'){

		innerHTML += "<div class='parti"+parti+"Info pTT'>";
		innerHTML += "<div class='selectedPartyHeader'></div>";
		innerHTML += "<div class='partyMenuElection partyContainer'>";
		innerHTML += "<div class='closeBtn' onclick='closePartiInfoTT();'><img src='images/closeMe.png' /></div>";
		innerHTML += "<div class='partiMenuWrapper'>";
        innerHTML += "<div class='partyElectionMenuElement2 partyElectionMenuElement' onclick='selectPartyComponent(2)' style='padding-right:5px;'>OPPSLUTNING</div>";
        innerHTML += "<div class='partyElectionMenuElement3 partyElectionMenuElement' onclick='selectPartyComponent(3)' style='padding-right:5px;'>ENDRING</div>";
        innerHTML += "<div class='partyElectionMenuElement4 partyElectionMenuElement' onclick='selectPartyComponent(4)' style='padding-right:5px;'>FAKTA</div>";
		innerHTML += "</div>";
		
		innerHTML += "<table class='reloadableComponentTable partyContainer' border='0px' cellpadding='0' cellspacing='0' style='height:100%;'><tbody><tr><td>";
        innerHTML += "<div class='reloadablePartyComponent' />";
        innerHTML += "</td></tr></tbody></table>"; 
		innerHTML += "</div></div>";
		
	}else {
		innerHTML += "<div class='parti"+parti+"Info pTT' >";
		innerHTML += "<div class='selectedPartyHeader'></div>";
		innerHTML += "<div class='partyMenuElection partyContainer'>";
		innerHTML += "<div class='closeBtn' onclick='closePartiInfoTT();'><img src='images/closeMe.png' /></div>";
		
       	innerHTML += "<table class='reloadableComponentTable partyContainer' border='0px' cellpadding='0' cellspacing='0' style='height:100%;'><tbody><tr><td>";
        innerHTML += "<div class='reloadablePartyComponent' />";
        innerHTML += "</td></tr></tbody></table>"; 
		innerHTML += "</div></div>";
		currentSelectedPartyComponent = 4;
	}
	return innerHTML;
}
function parseRegionId(regionId) {
//16_1_1
    if (regionId.length != 4){
        return '';
	}else if (region == 'c12m1201' || region == 'c16m1601' || region =='c11m1103' || region == 'c00m0301'){
		
		return region +'d'+regionId;
	}{
    var countyId = regionId.toString().substring(0, 2);
    return 'c' + countyId + 'm' + regionId; 
	}
}
function parseFavorites( countyId,muniiId,countyName,isFavCounty){
	
	
	var favourites = prefs.load();
	var name = countyName.toUpperCase();
	var id = countyId;

	var muniId = muniiId;
	var isCounty = isFavCounty;
	var cookieValue = '';
	

	if ( isCounty == false ) { //bianca

		var muniElements = id.split('_');
        if (muniElements.length == 2){
        	id = muniElements[0];
			
			 if ($.isArray(favourites)){
			var newFav = {name: name , countyId: id, muniId: id};
			 if (!eksistsInArrayWeb(newFav, favourites))
				isFavorite = false;
			else 
				isFavorite = true;
				
				
		}
			
        }else if (muniElements.length == 3){
		
		
			 if ($.isArray(favourites)){
			var newFav = {name: name , countyId: id, muniId: muniiId};
			 if (!eksistsInArrayWeb(newFav, favourites))
				isFavorite = false;
			else 
				isFavorite = true;
				
				
		}
		
		}
		
        if ($.isArray(favourites)){
			var newFav = {name: name , countyId: id, muniId: id};
			 if (!eksistsInArrayWeb(newFav, favourites))
				isFavorite = false;
			else 
				isFavorite = true;
				
				
		}
	} else if (isCounty == true) {
		
		if (name != 'Oslo' && name != 'OSLO'){
        	if ($.isArray(favourites)){
			var newFav = {name: name , countyId: id, muniId: id};
			if(!eksistsInArrayWeb(newFav, favourites))
					isFavorite = false;
				else 
					isFavorite = true;
			}
        } else { //Oslo special treatment
			
        	id = '00m0301';
        	if ($.isArray(favourites)){
			
			var newFav = {name: name, countyId: id , muniId:id};
			if(!eksistsInArrayWeb(newFav, favourites))
				isFavorite = false;
			else 
				isFavorite = true;
			}
			
        }        
    }
	
	
} 
function parseFavouritesWeb() {
	//document.getElementById('mainCircle').style.display='none';
	//$(".displayPanel").attr("style","display: none;");
	//$("#mainPanel").attr("style","display: none;");
	$("#favPanel").attr("style","display: block;");
	var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');
	var innerHTML ='';
	var favourites = prefs.load();
	var color = '#F0F0F0';
	innerHTML += "<div class='favouriteContainer'>";
	
	//goToWidgetRegion()
	
	//if ( region && region.length <=8) {
	
            //innerHTML += "<tr onclick='openFavouriteSelector();'>";
			 //innerHTML += "<div>";
          //  innerHTML += "<td class='favouriteCell' style='padding-top: 8px;background-color:#F0F0F0;border-bottom:1px #C0C0C0 solid;text-align:center;'><img src='images/favAdd.png' /></td>";
            //innerHTML += "<td colspan='3' class='favouriteCell' style='color:#FFFFFF;text-align:center;'>LEGG TIL DINE FAVORITT STEDER</td>";
            //innerHTML += "</tr>";      
			innerHTML += "<div class='favouriteCellHL'>LEGG TIL DINE FAVORITTSTEDER</div>";
        //}
		
	if (favourites){
        for (var i = 0; i < favourites.length; i++) {
			var mod = i % 2;
			
			if ( mod > 0) {
				 innerHTML += "<div class='favRow' style='color: #FFFFFF' >";
			}
			else {
				innerHTML += "<div class='favRow whiteBG' style='color: #FFFFFF'>";
			}
            /*if (color == '#F0F0F0')
                color = '#FFFFFF';
            else if (color == '#FFFFFF')
                color = '#F0F0F0';
			*/
            var muni = favourites[i];

           
            innerHTML += "<div onclick=\"goToWidgetRegion('" + muni.name + "|" + muni.countyId + "|" + muni.muniId+"');\" class='favouriteCell favStar' ><img src='images/favStarWhite.png' /></div>";
            innerHTML += "<div onclick=\"goToWidgetRegion('" + muni.name + "|" + muni.countyId + "|" + muni.muniId+"');\" class='favouriteCell favText'>" + muni.name + "</div>";
            innerHTML += "<div onclick=\"removeFromFavourites('" + i + "');\" class='favouriteCell favClose' ><img src='images/favXPink.png' /></div>";
            innerHTML += "</div>";
        }
	}
    innerHTML += "</div>";
	
	innerHTML += "<div class='favWrapper'>"
	
	innerHTML += "<div class='headLine' style='cursor:pointer;' onclick='refreshCounties(false, true);'> VELG FYLKE: </div>"
	innerHTML += "<div class='headLine' style='cursor:pointer;' onclick='refreshCounties(true, true);'> VELG KOMMUNE: </div>"
    innerHTML += "<div id='favSelectorCounty' class='blockMenu gradientBG'>"
	
	innerHTML += "</div>"
	
	
	
    innerHTML += "<div id='favSelectorMuni' class='blockMenu gradientBG'>"
	
	innerHTML += "</div>"
	
	innerHTML += "</div>"
    var container = document.getElementById('favPanel');
    if (container) container.innerHTML = innerHTML;
	
	
}			
function parseMyReps(){
	
}
function createRepArray(lines) {
	var f = new Date();
	var mandatcounter = 0;
	var values;
	var parseRegioner2;
	/*var kartLine;*/
	countySpecialM = [];
	herausforderer = [];
	var tempArray = [];
	herausfordererMandat = [];
	//inRepres
	if (state == 'Polls' || state == 'Nation' || state == 'Storting' || state == 'Duellen' || state == 'Sok'){
		parseRegioner2 = lines[9].split('|').clean("\r"); 
	}
	/*if (state == 'Nation' || state == 'Storting' || state == 'Duellen'){
		kartLine = parseRegioner2 = lines[10].split('|').clean("\r"); 
		
		for (var k = 0; k < kartLine.length ; k++){
			var countyData = kartLine[k].split('#');
			
		}
	}*/
	if (parseRegioner2){
		
		for (var i = 0; i < parseRegioner2.length ; i ++){		
			
			var elements = parseRegioner2[i].split(';');
			
			var parseRegion = elements[0];
			/*if (parseRegion == '01' ||parseRegion == '02'||parseRegion == '03'||parseRegion == '04'||parseRegion == '05'||parseRegion == '06'||parseRegion == '07'||parseRegion == '08'||parseRegion == '09'){*/
				while ( parseRegion.charAt(0) === '0')
					parseRegion = parseRegion.substr(1);
			//}
			if (parseRegion == '301') {
				parseRegion = '3';
			}

			
			var elementParti = elements[2].split('#').clean("\r"); 
			if (isValg){
				var umandatParti 	= elements[elements.length -5]; //parti in fylkewho gets special mandat
				var mistMandat 		= elements[elements.length -4]; //parti whos about to loose
				var vinneMandat 	= elements[elements.length -3]; //parti whos abot to win 
				var mistStemmer 	= elements[elements.length -2];
				var vinneStemmer 	= elements[elements.length -1];
				var mAdded = false;
				var loseAddad = false;
				var vinneMAdded = false;
			}
			
			for ( var k = 0; k < elementParti.length ; k++) { // changed 9 to elementParti.length
				
				var fylkePartiData 		= elementParti[k].split('$');
				var fylkesPartiId 		= fylkePartiData[0]; //parti id
				/** MDG HACK **/
				/*if (fylkesPartiId == 9   ) { // mdg is parti id 107 but in the json from trond its 10
					fylkesPartiId = 1000000000;
				}*/
				if (fylkesPartiId == 106) {
					fylkesPartiId = 11;
				}
				var fylkesPartiMandat 	= parseInt(fylkePartiData[1]);	// parti antall mandater
				var fylkesPartiPercent	= fylkePartiData[2];	// %
				var myPartiCode =  parseInt(fylkesPartiId);
				//console.log(' fylke :'+ parseRegion+' party: ' + fylkesPartiId + ' mandater: '+fylkesPartiMandat);
				
				if(isValg && fylkesPartiId != 9){
					if ((umandatParti == fylkesPartiId && mAdded == false) || (mistMandat == fylkesPartiId && loseAddad == false)) {
						if (umandatParti == fylkesPartiId && mAdded == false) {
							mAdded = true;
							countySpecialM.push({countyId:elements[0], umandatParti:umandatParti,mistMandat:'',vinneMandat:'',mistStemmer:'',vinneStemmer:'', normalMandatforParti: fylkesPartiMandat });
						}else if (mistMandat == fylkesPartiId && loseAddad == false) {
							loseAddad = true;
							countySpecialM.push({countyId:elements[0], umandatParti:'',mistMandat:mistMandat,vinneMandat:'',mistStemmer:mistStemmer,vinneStemmer:vinneStemmer, normalMandatforParti: fylkesPartiMandat });
						}
					}
					
					if (vinneMandat == fylkesPartiId && vinneMAdded == false) {
						
						herausfordererMandat.push({countyId:elements[0], vinneMandat:vinneMandat,vinneStemmer:vinneStemmer, normalMandatforParti: fylkesPartiMandat });
						vinneMAdded = true;
					}
				}
				if(fylkesPartiMandat > 0 && fylkesPartiId != 9){
					mandatcounter =mandatcounter +parseInt(fylkesPartiMandat);
					tempArray.push({parseRegion:parseRegion,myPartiCode:myPartiCode,fylkesPartiMandat:fylkesPartiMandat });
				}
				
			}
		}	
	}
	//console.log (mandatcounter);
	/*$.each(countySpecialM, function(){
		console.log(this);
	});*/
	if (inRepres.length == 0) {
	$.each(allRepres , function() {
		
		for ( var l =0 ;l < tempArray.length;l++) {
			if (this.CountyId == tempArray[l].parseRegion && parseInt(this.PartiOrganizationId) == tempArray[l].myPartiCode && this.CountyPosition <= tempArray[l].fylkesPartiMandat) {
				inRepres.push(this);
			}
		}
		for (var h = 0; h < herausfordererMandat.length; h++){
		
			if (this.CountyId == (herausfordererMandat[h].countyId == 301 ? 3 : herausfordererMandat[h].countyId ) &&  parseFloat(this.CountyPosition) == (parseFloat(herausfordererMandat[h].normalMandatforParti) +1) && this.PartiOrganizationId == herausfordererMandat[h].vinneMandat ) {
				herausforderer.push(this);
			}
		}
	});
	//console.log(inRepres.length + ' inrepres length '+ herausforderer.length);
	
	}
	var d = new Date();
	
	if (state == 'Sok'){
		return '';
	}else {
		return displayRepPartiSmall(tempArray, 'small');
	}
	
}	
function displayRepPartiSmall(tempArray, size) {
	var k = new Date();
	
	if (size == 'small'){
		var itemsPerLine = (window.innerWidth <= 680 ? 15 : 18);
	}else {
		var itemsPerLine = 4;
	}
	
	var dk = new Date();
	
	var innerHTML = '';var innerHTML1 = '';var innerHTML2 = '';var innerHTML3 = '';var innerHTML4 = '';var innerHTML5 = '';var innerHTML6 = '';var innerHTML7 = '';var innerHTML8 = '';var innerHTML9 = '';
	var parti1Counter = 0;var parti2Counter = 0;var parti3Counter = 0;var parti4Counter = 0;var parti5Counter = 0;var parti6Counter = 0;var parti7Counter = 0;var parti8Counter = 0;var parti9Counter = 0; var itemsInlineCounter = 0;
	inRepres.sort(sortByParti); 
		//open the holder for each parti
		if (size == 'small') {
			innerHTML += "<div id='repBtnMenu'><div id='velgBig' class='button' onclick=\"toggleRepres('string', 'big','parti');\"><img src='images/button/repBigBtn.png' /></div><div id='velgFylke' class='button' onclick=\"toggleRepres('string', 'small','fylke');\"><img src='images/button/velgFylke.png' /></div> </div>";
			innerHTML += "<div class='galleriHL'>DET NYE STORTINGET</div><div id='repWrapper'><div class='repInnerWrapper'><div class='upButton' onclick=\"scrollRepWindow('up');\"><img src='images/button/upBtn.png' /></div><div class='downButton' onclick=\"scrollRepWindow('down');\"><img src='images/button/downBtn.png' /></div><div class='pusher'>";
		}else {
			innerHTML += "<div id='repBtnMenu'><div id='velgSmall' class='button' onclick=\"toggleRepres('string', 'small','parti');\"><img src='images/button/repSmallBtn.png' /></div><div id='velgFylke' class='button' onclick=\"toggleRepres('string', 'big','fylke');\"><img src='images/button/velgFylke.png' /></div> </div>";
			innerHTML += "<div class='galleriHL'>DET NYE STORTINGET</div><div id='repWrapperBig'><div class='repInnerWrapperBig'><div class='upButton' onclick=\"scrollRepWindow('upBig');\"><img src='images/button/upBtn.png' /></div><div class='downButton' onclick=\"scrollRepWindow('downBig');\"><img src='images/button/downBtn.png' /></div><div class='pusher'>";
		}
		
	var hk = new Date();
	
	//parse 	
	// after every 15th or 18th or 4th item has to come a infobox
	// before ever first partirep has to come logo
	$.each(inRepres , function() {
		
		if ( this.PartiOrganizationId == 1 ){
			if(parti1Counter == 0) {
				if (size == 'small'){
					innerHTML1 += "<div class='parti1holder'><img src='images/logo/parti1.png' onclick='openAllParti(1, this);' /></div>";
				}else {
					//innerHTML1 += "<div class='parti1holderBig'><img src='images/logo/parti1_big.png' onclick='openAllParti(1);' /></div>";
					innerHTML1 += "<div class='parti1holderBig'><img src='images/logo/parti1_big.png' /></div>";
				}
				parti1Counter ++;
				itemsInlineCounter++;
			}
			if ( itemsInlineCounter == itemsPerLine ) { 
				innerHTML1 += createBox(size);
				innerHTML1 += createBigBox(size);
				
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML1 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML1 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML1 += createBox(size);
				innerHTML1 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 2 ){
			if(parti2Counter == 0) {
				if (size == 'small'){
					innerHTML2 += "<div class='parti1holder'><img src='images/logo/parti2.png' onclick='openAllParti(2, this);' /></div>";
				}else {
					//innerHTML2 += "<div class='parti1holderBig'><img src='images/logo/parti2_big.png' onclick='openAllParti(2);' /></div>";
					innerHTML2 += "<div class='parti1holderBig'><img src='images/logo/parti2_big.png' /></div>";
				}
				parti2Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML2 += createBox(size);
				innerHTML2 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML2 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML2 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML2 += createBox(size);
				innerHTML2 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 3){
			if(parti3Counter == 0) {
				if (size == 'small'){
					innerHTML3 += "<div class='parti1holder'><img src='images/logo/parti3.png' onclick='openAllParti(3, this);' /></div>";
				}else {
					//innerHTML3 += "<div class='parti1holderBig'><img src='images/logo/parti3_big.png' onclick='openAllParti(3);' /></div>";
					innerHTML3 += "<div class='parti1holderBig'><img src='images/logo/parti3_big.png' /></div>";
				}
				parti3Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML3 += createBox(size);
				innerHTML3 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML3 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML3 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML3 += createBox(size);
				innerHTML3 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 4){
			if(parti4Counter == 0) {
				if (size == 'small'){
					innerHTML4 += "<div class='parti1holder'><img src='images/logo/parti4.png' onclick='openAllParti(4, this);' /></div>";
				}else {
					//innerHTML4 += "<div class='parti1holderBig'><img src='images/logo/parti4_big.png' onclick='openAllParti(4);' /></div>";
					innerHTML4 += "<div class='parti1holderBig'><img src='images/logo/parti4_big.png' /></div>";
				}
				parti4Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML4 += createBox(size);
				innerHTML4 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML4 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML4 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML4 += createBox(size);
				innerHTML4 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 5){
			if(parti5Counter == 0) {
				if (size == 'small'){
					innerHTML5 += "<div class='parti1holder'><img src='images/logo/parti5.png' onclick='openAllParti(5, this);' /></div>";
				}else {
					//innerHTML5 += "<div class='parti1holderBig'><img src='images/logo/parti5_big.png' onclick='openAllParti(5);' /></div>";
					innerHTML5 += "<div class='parti1holderBig'><img src='images/logo/parti5_big.png'  /></div>";
				}
				parti5Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML5 +=createBox(size);
				innerHTML5 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML5 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML5 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML5 += createBox(size);
				innerHTML5 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 6){
			if(parti6Counter == 0) {
				if (size == 'small'){
					innerHTML6 += "<div class='parti1holder'><img src='images/logo/parti6.png' onclick='openAllParti(6, this);' /></div>";
				}else {
					//innerHTML6 += "<div class='parti1holderBig'><img src='images/logo/parti6_big.png' onclick='openAllParti(6);' /></div>";
					innerHTML6 += "<div class='parti1holderBig'><img src='images/logo/parti6_big.png'  /></div>";
				}
				parti6Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML6 +=createBox(size);
				innerHTML6 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML6 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML6 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML6 +=createBox(size);
				innerHTML6 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 7){
			if(parti7Counter == 0) {
				if (size == 'small'){
					innerHTML7 += "<div class='parti1holder'><img src='images/logo/parti7.png' onclick='openAllParti(7, this);' /></div>";
				}else {
					//innerHTML7 += "<div class='parti1holderBig'><img src='images/logo/parti7_big.png' onclick='openAllParti(7);' /></div>";
					innerHTML7 += "<div class='parti1holderBig'><img src='images/logo/parti7_big.png' /></div>";
				}
				parti7Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML7 +=createBox(size);
				innerHTML7 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML7 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML7 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML7 +=createBox(size);
				innerHTML7 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.PartiOrganizationId  == 8){
			if(parti8Counter == 0) {
				if (size == 'small'){
					innerHTML8 += "<div class='parti1holder'><img src='images/logo/parti8.png' onclick='openAllParti(8, this);' /></div>";
				}else {
					//innerHTML8 += "<div class='parti1holderBig'><img src='images/logo/parti8_big.png' onclick='openAllParti(8);' /></div>";
					innerHTML8 += "<div class='parti1holderBig'><img src='images/logo/parti8_big.png'  /></div>";
				}
				parti8Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML8 += createBox(size);
				innerHTML8 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML8 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML8 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML8 += createBox(size);
				innerHTML8 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else {
			if(parti9Counter == 0) {
				if (size == 'small'){
					innerHTML9 += "<div class='parti1holder'><img src='images/logo/parti10.png' onclick='openAllParti(10, this);' /></div>";
				}else {
					//innerHTML9 += "<div class='parti1holderBig'><img src='images/logo/parti9_big.png' onclick='openAllParti(9);' /></div>";
					innerHTML9 += "<div class='parti1holderBig'><img src='images/logo/parti10_big.png'/></div>";
				}
				parti9Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML9 += createBox(size);
				innerHTML9 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML9 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML9 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML9 += createBox(size);
				innerHTML9 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}
		
	});
	var xk = new Date();
	innerHTML += innerHTML1 += innerHTML2 +=innerHTML3 +=innerHTML4 +=innerHTML5+=innerHTML6+=innerHTML7+=innerHTML8+=innerHTML9 ;
	innerHTML += createBox(size) + createBigBox(size) +"</div>";
	innerHTML += "</div></div></div>";
	$('#blackOverlay').attr('style','display:none;');
	return innerHTML;
	
}


function displayRepFylkeSmall(tempArray, size) {
	
	if (size == 'small'){
		var itemsPerLine = (window.innerWidth <= 680 ? 15 : 18);
	}else {
		var itemsPerLine = 4;
	}
	
	/*if (inRepres.length == 0) {
	$.each(allRepres , function() {
		
		for ( var l =0 ;l < tempArray.length;l++) {
			if (this.CountyId == tempArray[l].parseRegion && String(this.PartiOrganizationId).toLowerCase() == tempArray[l].myPartiCode.toLowerCase() && this.CountyPosition < tempArray[l].fylkesPartiMandat) {
				inRepres.push(this);
			}
		}		
	});
	}*/
	var innerHTML = '';var innerHTML1 = '';var innerHTML2 = '';var innerHTML3 = '';var innerHTML4 = '';var innerHTML5 = '';var innerHTML6 = '';var innerHTML7 = '';var innerHTML8 = '';var innerHTML9 = '';var innerHTML10 = '';var innerHTML11 = '';var innerHTML12 = '';var innerHTML13 = '';var innerHTML14 = '';var innerHTML15 = '';var innerHTML16 = '';var innerHTML17 = '';var innerHTML18 = '';var innerHTML19 = '';var innerHTML20 = '';
	var parti1Counter = 0;var parti2Counter = 0;var parti3Counter = 0;var parti4Counter = 0;var parti5Counter = 0;var parti6Counter = 0;var parti7Counter = 0;var parti8Counter = 0;var parti9Counter = 0;var parti10Counter = 0;var parti11Counter = 0;var parti12Counter = 0;var parti20Counter = 0;var parti14Counter = 0;var parti15Counter = 0;var parti16Counter = 0;var parti17Counter = 0;var parti18Counter = 0;var parti19Counter = 0; var itemsInlineCounter = 0;
	inRepres.sort(sortByFylke);
		//open the holder for each parti
		if (size == 'small') {
			innerHTML += "<div id='repBtnMenu'><div id='velgBig' class='button' onclick=\"toggleRepres('string', 'big','Fylke');\"><img src='images/button/repBigBtn.png' /></div><div id='velgParti' class='button' onclick=\"toggleRepres('string', 'small','parti');\"><img src='images/button/velgParti.png' /></div> </div>";
			innerHTML += "<div class='galleriHL'>DET NYE STORTINGET</div><div id='repWrapper'><div class='repInnerWrapper'><div class='upButton' onclick=\"scrollRepWindow('up');\"><img src='images/button/upBtn.png' /></div><div class='downButton' onclick=\"scrollRepWindow('down');\"><img src='images/button/downBtn.png' /></div><div class='pusher'>";
		}else {
			innerHTML += "<div id='repBtnMenu'><div id='velgSmall' class='button' onclick=\"toggleRepres('string', 'small','fylke');\"><img src='images/button/repSmallBtn.png' /></div><div id='velgParti' class='button' onclick=\"toggleRepres('string', 'big','parti');\"><img src='images/button/velgParti.png' /></div> </div>";
			innerHTML += "<div class='galleriHL'>DET NYE STORTINGET</div><div id='repWrapperBig'><div class='repInnerWrapperBig'><div class='upButton' onclick=\"scrollRepWindow('upBig');\"><img src='images/button/upBtn.png' /></div><div class='downButton' onclick=\"scrollRepWindow('downBig');\"><img src='images/button/downBtn.png' /></div><div class='pusher'>";
		}
		

	//parse 	
	// after every 15th or 18th or 4th item has to come a infobox
	// before ever first partirep has to come logo
	$.each(inRepres , function() {
		if ( this.CountyId  == 01 || this.CountyId   == '01' ){
			if(parti1Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML1 += "<div class='parti1holder repParty1'><img src='images/fylke/1_small.png'  onclick='openAllFylke(1, this);' /></div>";
					} else
					innerHTML1 += "<div class='parti1holder repParty1'><img src='images/fylke/1_small.png' onmouseover='displayFylkeTT(event,1);' onclick='openAllFylke(1, this);' /></div>";
				}else {
				if (window.innerWidth > 850){
						innerHTML1 += "<div class='parti1holderBig'><img src='images/fylke/1.png'  onclick='openAllFylke(1, this);' /></div>";
					} else
					innerHTML1 += "<div class='parti1holderBig'><img src='images/fylke/1.png' onmouseover='displayFylkeTT(event,1);' onclick='openAllFylke(1, this);' /></div>";
				}
				parti1Counter ++;
				itemsInlineCounter++;
			}
			if ( itemsInlineCounter == itemsPerLine ) { 
				innerHTML1 += createBox(size);
				innerHTML1 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML1 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML1 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML1 += createBox(size);
				innerHTML1 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 02  || this.CountyId  == '02'){
			if(parti2Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML2 += "<div class='parti1holder repParty2'><img src='images/fylke/2_small.png'  onclick='openAllFylke(2, this);' /></div>";
					} else
					innerHTML2 += "<div class='parti1holder repParty2'><img src='images/fylke/2_small.png' onmouseover='displayFylkeTT(event,2);' onclick='openAllFylke(2, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML2 += "<div class='parti1holderBig repParty2'><img src='images/fylke/2.png'  onclick='openAllFylke(2, this);' /></div>";
					} else
					innerHTML2 += "<div class='parti1holderBig  repParty2'><img src='images/fylke/2.png' onmouseover='displayFylkeTT(event,2);' onclick='openAllFylke(2, this);' /></div>";
				}
				parti2Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML2 += createBox(size);
				innerHTML2 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML2 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML2 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML2 += createBox(size);
				innerHTML2 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 03  || this.CountyId  == '03' || this.CountyId   == 301  || this.CountyId  == '301'){
			if(parti3Counter == 0) {
				
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML3 += "<div class='parti1holder repParty3'><img src='images/fylke/3_1_small.png'  onclick='openAllFylke(3, this);' /></div>";
					} else
					innerHTML3 += "<div class='parti1holder repParty3'><img src='images/fylke/3_1_small.png' onmouseover='displayFylkeTT(event,301);' onclick='openAllFylke(3, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML3 += "<div class='parti1holderBig repParty3'><img src='images/fylke/3_1.png'  onclick='openAllFylke(3, this);' /></div>";
					} else
					innerHTML3 += "<div class='parti1holderBig repParty3'><img src='images/fylke/3_1.png' onmouseover='displayFylkeTT(event,301);' onclick='openAllFylke(3, this);' /></div>";
				}
				parti3Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML3 += createBox(size);
				innerHTML3 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML3 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML3 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML3 += createBox(size);
				innerHTML3 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 04  || this.CountyId  == '04'){
			if(parti4Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML4 += "<div class='parti1holder repParty4'><img src='images/fylke/4_small.png'  onclick='openAllFylke(4, this);' /></div>";
					} else
					innerHTML4 += "<div class='parti1holder repParty4'><img src='images/fylke/4_small.png' onmouseover='displayFylkeTT(event,4);' onclick='openAllFylke(4, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML4 += "<div class='parti1holderBig repParty4'><img src='images/fylke/4.png'  onclick='openAllFylke(4, this);' /></div>";
					} else
					innerHTML4 += "<div class='parti1holderBig repParty4'><img src='images/fylke/4.png' onmouseover='displayFylkeTT(event,4);' onclick='openAllFylke(4, this);' /></div>";
				}
				parti4Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML4 += createBox(size);
				innerHTML4 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML4 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML4 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML4 += createBox(size);
				innerHTML4 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 05  || this.CountyId  == '05'){
			if(parti5Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML5 += "<div class='parti1holder repParty5'><img src='images/fylke/5_small.png'  onclick='openAllFylke(5, this);' /></div>";
					} else
					innerHTML5 += "<div class='parti1holder repParty5'><img src='images/fylke/5_small.png' onmouseover='displayFylkeTT(event,5);' onclick='openAllFylke(5, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML5 += "<div class='parti1holderBig repParty5'><img src='images/fylke/5.png'  onclick='openAllFylke(5, this);' /></div>";
					} else
					innerHTML5 += "<div class='parti1holderBig repParty5'><img src='images/fylke/5.png' onmouseover='displayFylkeTT(event,5);' onclick='openAllFylke(5, this);' /></div>";
				}
				parti5Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML5 +=createBox(size);
				innerHTML5 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML5 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML5 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML5 += createBox(size);
				innerHTML5 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 06  || this.CountyId  == '06'){
			if(parti6Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML6 += "<div class='parti1holder repParty6'><img src='images/fylke/6_small.png'  onclick='openAllFylke(6, this);' /></div>";
					} else
					innerHTML6 += "<div class='parti1holder repParty6'><img src='images/fylke/6_small.png' onmouseover='displayFylkeTT(event,6);' onclick='openAllFylke(6, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML6 += "<div class='parti1holderBig repParty6'><img src='images/fylke/6.png'  onclick='openAllFylke(6, this);' /></div>";
					} else
					innerHTML6 += "<div class='parti1holderBig repParty6'><img src='images/fylke/6.png' onmouseover='displayFylkeTT(event,6);' onclick='openAllFylke(6, this);' /></div>";
				}
				parti6Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML6 +=createBox(size);
				innerHTML6 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML6 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML6 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML6 +=createBox(size);
				innerHTML6 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 07  || this.CountyId  == '07'){
			if(parti7Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML7 += "<div class='parti1holder repParty7'><img src='images/fylke/7_small.png'  onclick='openAllFylke(7, this);' /></div>";
					} else
					innerHTML7 += "<div class='parti1holder repParty7'><img src='images/fylke/7_small.png' onmouseover='displayFylkeTT(event,7);' onclick='openAllFylke(7, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML7 += "<div class='parti1holderBig repParty7'><img src='images/fylke/7.png'  onclick='openAllFylke(7, this);' /></div>";
					} else
					innerHTML7 += "<div class='parti1holderBig repParty7'><img src='images/fylke/7.png' onmouseover='displayFylkeTT(event,7);' onclick='openAllFylke(7, this);' /></div>";
				}
				parti7Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML7 +=createBox(size);
				innerHTML7 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML7 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML7 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML7 +=createBox(size);
				innerHTML7 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if(this.CountyId   == 08  || this.CountyId  == '08'){
			if(parti8Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML8 += "<div class='parti1holder repParty8'><img src='images/fylke/8_small.png'  onclick='openAllFylke(8, this);' /></div>";
					} else
					innerHTML8 += "<div class='parti1holder repParty8'><img src='images/fylke/8_small.png' onmouseover='displayFylkeTT(event,8);' onclick='openAllFylke(8, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML8 += "<div class='parti1holderBig repParty8'><img src='images/fylke/8.png'  onclick='openAllFylke(8, this);' /></div>";
					} else
					innerHTML8 += "<div class='parti1holderBig repParty8'><img src='images/fylke/8.png' onmouseover='displayFylkeTT(event,8);' onclick='openAllFylke(8, this);' /></div>";
				}
				parti8Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML8 += createBox(size);
				innerHTML8 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML8 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML8 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML8 += createBox(size);
				innerHTML8 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 09  || this.CountyId  == '09'){
			if(parti9Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML9 += "<div class='parti1holder repParty9'><img src='images/fylke/9_small.png'  onclick='openAllFylke(9, this);' /></div>";
					} else
					innerHTML9 += "<div class='parti1holder repParty9'><img src='images/fylke/9_small.png' onmouseover='displayFylkeTT(event,9);' onclick='openAllFylke(9, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML9 += "<div class='parti1holderBig repParty9'><img src='images/fylke/9.png'  onclick='openAllFylke(9, this);' /></div>";
					} else
					innerHTML9 += "<div class='parti1holderBig repParty9'><img src='images/fylke/9.png' onmouseover='displayFylkeTT(event,9);' onclick='openAllFylke(9, this);' /></div>";
				}
				parti9Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML9 += createBox(size);
				innerHTML9 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML9 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML9 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML9 += createBox(size);
				innerHTML9 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 10  || this.CountyId  == '10'){
			if(parti10Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML10 += "<div class='parti1holder repParty10'><img src='images/fylke/10_small.png'  onclick='openAllFylke(10, this);' /></div>";
					} else
					innerHTML10 += "<div class='parti1holder repParty10'><img src='images/fylke/10_small.png' onmouseover='displayFylkeTT(event,10);' onclick='openAllFylke(10, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML10 += "<div class='parti1holderBig repParty10'><img src='images/fylke/10.png' onclick='openAllFylke(10, this);' /></div>";
					} else
					innerHTML10 += "<div class='parti1holderBig repParty10'><img src='images/fylke/10.png' onmouseover='displayFylkeTT(event,10);' onclick='openAllFylke(10, this);' /></div>";
				}
				parti10Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML10 += createBox(size);
				innerHTML10 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML10 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML10 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML10 += createBox(size);
				innerHTML10 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 11  || this.CountyId  == '11'){
			if(parti11Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML11 += "<div class='parti1holder repParty11'><img src='images/fylke/11_small.png'  onclick='openAllFylke(11, this);' /></div>";
					} else
					innerHTML11 += "<div class='parti1holder repParty11'><img src='images/fylke/11_small.png' onmouseover='displayFylkeTT(event,11);' onclick='openAllFylke(11, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML11 += "<div class='parti1holderBig repParty11'><img src='images/fylke/11.png' onclick='openAllFylke(11, this);' /></div>";
					} else
					innerHTML11 += "<div class='parti1holderBig repParty11'><img src='images/fylke/11.png' onmouseover='displayFylkeTT(event,11);' onclick='openAllFylke(11, this);' /></div>";
				}
				parti11Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML11 += createBox(size);
				innerHTML11 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML11 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML11 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML11 += createBox(size);
				innerHTML11 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 12  || this.CountyId  == '12'){
			if(parti12Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML12 += "<div class='parti1holder repParty12'><img src='images/fylke/12_small.png'  onclick='openAllFylke(12, this);' /></div>";
					} else
					innerHTML12 += "<div class='parti1holder repParty12'><img src='images/fylke/12_small.png' onmouseover='displayFylkeTT(event,12);' onclick='openAllFylke(12, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML12 += "<div class='parti1holderBig repParty12'><img src='images/fylke/12.png' onclick='openAllFylke(12, this);' /></div>";
					} else
					innerHTML12 += "<div class='parti1holderBig repParty12'><img src='images/fylke/12.png' onmouseover='displayFylkeTT(event,12);' onclick='openAllFylke(12, this);' /></div>";
				}
				parti12Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML12 += createBox(size);
				innerHTML12 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML12 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML12 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML12 += createBox(size);
				innerHTML12 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 14  || this.CountyId  == '14'){
			if(parti14Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML14 += "<div class='parti1holder repParty14'><img src='images/fylke/14_small.png'  onclick='openAllFylke(14, this);' /></div>";
					} else
						innerHTML14 += "<div class='parti1holder repParty14'><img src='images/fylke/14_small.png' onmouseover='displayFylkeTT(event,14);' onclick='openAllFylke(14, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML14 += "<div class='parti1holderBig repParty14'><img src='images/fylke/14.png' onclick='openAllFylke(14, this);' /></div>";
					} else
						innerHTML14 += "<div class='parti1holderBig repParty14'><img src='images/fylke/14.png' onmouseover='displayFylkeTT(event,14);' onclick='openAllFylke(14, this);' /></div>";
				}
				parti14Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML14 += createBox(size);
				innerHTML14 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML14 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML14 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML14 += createBox(size);
				innerHTML14 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 15  || this.CountyId  == '15'){
			if(parti15Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML15 += "<div class='parti1holder repParty151'><img src='images/fylke/15_small.png' onclick='openAllFylke(15, this);' /></div>";
					} else
						innerHTML15 += "<div class='parti1holder repParty15'><img src='images/fylke/15_small.png' onmouseover='displayFylkeTT(event,15);' onclick='openAllFylke(15, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML15 += "<div class='parti1holderBig repParty15'><img src='images/fylke/15.png' onclick='openAllFylke(15, this);' /></div>";
					} else
						innerHTML15 += "<div class='parti1holderBig repParty15'><img src='images/fylke/15.png' onmouseover='displayFylkeTT(event,15);' onclick='openAllFylke(15, this);' /></div>";
				}
				parti15Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML15 += createBox(size);
				innerHTML15 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML15 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML15 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML15 += createBox(size);
				innerHTML15 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 16  || this.CountyId  == '16'){
			if(parti16Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML16 += "<div class='parti1holder repParty16'><img src='images/fylke/16_small.png' onclick='openAllFylke(16, this);' /></div>";
					} else
						innerHTML16 += "<div class='parti1holder repParty16'><img src='images/fylke/16_small.png' onmouseover='displayFylkeTT(event,16);' onclick='openAllFylke(16, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML16 += "<div class='parti1holderBig repParty16'><img src='images/fylke/16.png' onclick='openAllFylke(16, this);' /></div>";
					} else
						innerHTML16 += "<div class='parti1holderBig repParty16'><img src='images/fylke/16.png' onmouseover='displayFylkeTT(event,16);' onclick='openAllFylke(16, this);' /></div>";
				}
				parti16Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML16 += createBox(size);
				innerHTML16 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML16 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML16 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML16 += createBox(size);
				innerHTML16 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 17  || this.CountyId  == '17'){
			if(parti17Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML17 += "<div class='parti1holder repParty17'><img src='images/fylke/17_small.png' onclick='openAllFylke(17, this);' /></div>";
					} else
					innerHTML17 += "<div class='parti1holder repParty17'><img src='images/fylke/17_small.png' onmouseover='displayFylkeTT(event,17);' onclick='openAllFylke(17, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML17 += "<div class='parti1holderBig repParty17'><img src='images/fylke/17.png' onclick='openAllFylke(17, this);' /></div>";
					} else
					innerHTML17 += "<div class='parti1holderBig repParty17'><img src='images/fylke/17.png' onmouseover='displayFylkeTT(event,17);' onclick='openAllFylke(17, this);' /></div>";
				}
				parti17Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML17 += createBox(size);
				innerHTML17 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML17 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML17 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML17 += createBox(size);
				innerHTML17 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 18  || this.CountyId  == '18'){
			if(parti18Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML18 += "<div class='parti1holder repParty18'><img src='images/fylke/18_small.png' onclick='openAllFylke(18, this);' /></div>";
					} else
					innerHTML18 += "<div class='parti1holder repParty18'><img src='images/fylke/18_small.png' onmouseover='displayFylkeTT(event,18);' onclick='openAllFylke(18, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML18 += "<div class='parti1holderBig repParty18'><img src='images/fylke/18.png'  onclick='openAllFylke(18, this);' /></div>";
					} else
					innerHTML18 += "<div class='parti1holderBig repParty18'><img src='images/fylke/18.png' onmouseover='displayFylkeTT(event,18);' onclick='openAllFylke(18, this);' /></div>";
				}
				parti18Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML18 += createBox(size);
				innerHTML18 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML18 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML18 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML18 += createBox(size);
				innerHTML18 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 19  || this.CountyId  == '19'){
			if(parti19Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML19 += "<div class='parti1holder repParty19'><img src='images/fylke/19_small.png' onclick='openAllFylke(19, this);' /></div>";
					} else
					innerHTML19 += "<div class='parti1holder repParty19'><img src='images/fylke/19_small.png' onmouseover='displayFylkeTT(event,19);' onclick='openAllFylke(19, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML19 += "<div class='parti1holderBig repParty19'><img src='images/fylke/19.png' onclick='openAllFylke(19, this);' /></div>";
					} else
					innerHTML19 += "<div class='parti1holderBig repParty19'><img src='images/fylke/19.png' onmouseover='displayFylkeTT(event,19);' onclick='openAllFylke(19, this);' /></div>";
				}
				parti19Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML19 += createBox(size);
				innerHTML19 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				
				innerHTML19 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML19 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML19 += createBox(size);
				innerHTML19 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}else if (this.CountyId   == 20  || this.CountyId  == '20'){
			if(parti20Counter == 0) {
				if (size == 'small'){
					if (window.innerWidth > 850){
						innerHTML20 += "<div class='parti1holder repParty20'><img src='images/fylke/20_small.png' onclick='openAllFylke(20, this);' /></div>";
					} else
					innerHTML20 += "<div class='parti1holder repParty20'><img src='images/fylke/20_small.png' onmouseover='displayFylkeTT(event,20);' onclick='openAllFylke(20, this);' /></div>";
				}else {
					if (window.innerWidth > 850){
						innerHTML20 += "<div class='parti1holderBig repParty20'><img src='images/fylke/20.png' onclick='openAllFylke(20, this);' /></div>";
					} else
					innerHTML20 += "<div class='parti1holderBig repParty20'><img src='images/fylke/20.png' onmouseover='displayFylkeTT(event,20);' onclick='openAllFylke(20, this);' /></div>";
				}
				parti20Counter ++;
				itemsInlineCounter++;
			}
			if(itemsInlineCounter == itemsPerLine) { 
				innerHTML20 += createBox(size);
				innerHTML20 += createBigBox(size);
				itemsInlineCounter= 0;
			}
			if (size == 'small'){
				innerHTML20 += generateRepresentanterSmall(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}else {
				innerHTML20 += generateRepresentanter(this.PartiOrganizationId,this.Picture,this.CandidateId,this.CountyId,this.CountyPosition,(this.ParliamentPeriods == ''));
			}
			itemsInlineCounter++;
			if(itemsInlineCounter == itemsPerLine) {
				innerHTML20 += createBox(size);
				innerHTML20 += createBigBox(size);
				itemsInlineCounter= 0;
			}
		}
		
	});
	
	innerHTML += innerHTML1 += innerHTML2 +=innerHTML3 +=innerHTML4 +=innerHTML5+=innerHTML6+=innerHTML7+=innerHTML8+=innerHTML9 +=innerHTML10+=innerHTML11+=innerHTML12+=innerHTML14+=innerHTML15+=innerHTML16+=innerHTML17+=innerHTML18+=innerHTML19+=innerHTML20;
	innerHTML += createBox(size) +createBigBox()+"</div>";
	//innerHTML += "<div id='fylkeTT'></div>";
	innerHTML += "</div></div></div>";
	$('#blackOverlay').attr('style','display:none;');
	return innerHTML;
	
}
/*
countySpecialM.push({countyId:elements[0], umandatParti:umandatParti,mistMandat:mistMandat,vinneMandat:vinneMandat,mistStemmer:mistStemmer,vinneStemmer:vinneStemmer, normalMandatforParti: (parseFloat(fylkesPartiMandat) - 1) });
*/

function generateRepresentanter(PartiKode,Picture,CandidateId,CountyId, repPos, isNew){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
		if(isNew == true){
		
			extraMHTML += "<img src='images/rep/grueneEckeBig.png' />";
		}
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == CountyId){ // county fit
				if ( repPos == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( PartiKode  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
					}
					if ( PartiKode  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='images/rep/uManBig.png' />";
					}	
				}
			}
		}
		
	}
	extraMHTML +="</div>";
	var myURL = "images/rep/big/"+(Picture != '' ? (Picture+'_org200.png') : 'dummy_big.jpg' );
	innerHTML = "<div class='repholderBig parti_"+PartiKode+" county_"+CountyId+"' id='candidate_"+CandidateId+"' ><div class='posRel'><div class='repImgBG' style='background-image:url("+myURL+");'><div class='repImgBlend'></div><div class=' parti_"+PartiKode+" county_"+CountyId+"'  ></div></div>"+extraMHTML+"</div></div>";
	return innerHTML;
}
function generateRepresentanterBildeFylke(PartiKode,CountyId,CandidateId, repPos){
	var innerHTML = '';
	innerHTML += "<div class='repholder' id='Fcandidate_"+CandidateId+"' ><div class='repImgBG' style='background-image:url(../images/rep/"+((this.Picture != null) ? this.Picture : 'dummy.jpg' )+");'><div class='repImgBlend'></div><div class=' parti_"+PartiKode+" county_"+CountyId+"'  ></div></div></div>";
	return innerHTML;
}

function generateRepresentanterSmall(PartiKode,Picture,CandidateId,CountyId, repPos, isNew){
	var innerHTML = '';
		var extraMHTML = "<div class='specialMandat'>";
		if(isNew == true){
		
			extraMHTML += "<img src='images/rep/grueneEcke.png' />";
		}
	if (isValg  ){
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == CountyId){ // county fit
			
			//todao heres soemthing wrong
				if ( repPos+'' == countySpecialM[i].normalMandatforParti+''   ){	// representanter position is as big as  then the normal amount of mandater + 1
					
					 if ( PartiKode  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='images/rep/roteEcke_.png' />";
						
					}
					if ( PartiKode  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='images/rep/uMan.png' />";
						
					}					
				}
				
			}
		}
		
	}
	extraMHTML +="</div>";
	var myURL = "images/rep/small/"+(Picture != '' ? (Picture+'_org60.png') : 'dummy.jpg' );
	innerHTML = "<div class='repholder parti_"+PartiKode+" county_"+CountyId+"' id='candidate_"+CandidateId+"' ><div class='posRel' style='background-image:url("+myURL+");'>"+extraMHTML+"</div></div>";
	return innerHTML;
}

function createBox(size){
	var innerHTML = "<div class='repTT TT"+size+"'><div class='TT"+size+"'><div class='newlogoBox'></div><div class='repBox'><div class='repHeadline'><div class='repPartiLogo'></div><div class='repName'></div></div><div class='repContent'><div class='repBild'></div><div class='repInfo'></div></div></div><div class='herrausforderer'></div></div></div>";
	return innerHTML;
}
function createBigBox(size) {
	var innerHTML = "<div class='repGroupTT TT"+size+"'></div>";
	return innerHTML;
}
function openPartiPanel(nr){
var containerHeight =  $('.parti'+nr+'holder').height();
	if (containerHeight > 0) {
		$('.parti'+nr+'holder').attr('style','display:none;');
		//$('#Parti'+nr).attr('style',' ');
		$('.parti'+nr+'holder').animate({height:"0px"});
	}else {
		$('.parti'+nr+'holder').attr('style','display:block;');
		$('.parti'+nr+'holder').animate({height:"1%"});
	}
}
function openFylkePanel(nr){
var containerHeight =  $('.fylke'+nr+'holder').height();
	if (containerHeight > 0) {
		$('.fylke'+nr+'holder').attr('style','display:none;');
		//$('#Fylke'+nr).attr('style',' ');
		$('.fylke'+nr+'holder').animate({height:"0px"});
	}else {
		$('.fylke'+nr+'holder').attr('style','display:block;');
		$('.fylke'+nr+'holder').animate({height:"1%"});
	}
}