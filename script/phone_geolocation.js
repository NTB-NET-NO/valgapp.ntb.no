﻿function  setLocation() {
    var header = document.getElementById('myDataHeader');
    header.innerHTML = '';
    
    // check browser can support geolocation, if so get the current position
    if (navigator.geolocation) {
        writeData('Henter din kommune...vennligst vent');
        navigator.geolocation.getCurrentPosition(showPosition, errorHandler);
    }
    else {
        writeData('Beklager, din enhet støtter ikke geolokasjon');
    }
}

// show the position on the page and make a google maps link
function showPosition(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;

    //lat = 59.932556;
    //lon = 10.6643353;
    
    makeGoogleCall(lat, lon);  
}

function makeGoogleCall(lat, lon) {
    var initialLocation = new google.maps.LatLng(lat, lon);
    var geocoder = new google.maps.Geocoder();
    
    if (geocoder) {
        geocoder.geocode(
            { 'latLng': initialLocation }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results.length > 0) {
                        var loc = null;

                        for (var i = 0; i < results.length; i++) {
                            var result = results[i];

                            if (result.address_components) {
                                for (var j = 0; j < result.address_components.length; j++) {
                                    var component = result.address_components[j];
                                    if (component.types) {
                                        for (var k = 0; k < component.types.length; k++) {
                                            var type = component.types[k];
                                            if (type == 'administrative_area_level_2') {
                                                loc = component.long_name;
                                                break;
                                            }
                                            else if (type == 'administrative_area_level_1' && component.long_name == 'Oslo') {
                                                loc = component.long_name;
                                                break;
                                            }
                                        }
                                    }

                                    if (loc != null)
                                        break;
                                }
                            }

                            if (loc != null)
                                break;
                        }

                        if (loc != null) {
                            handleMuni(loc, lat, lon);
                        }
                        else {
                            writeData('Din posisjon kunne ikke hentes, feilmelding:<br/>' + status);
                            var headerDiv = document.getElementById('myDataHeader');
                            if (headerDiv) {
                                var img = document.createElement('img');
                                img.src = "http://valgflash.ntb.no/prod/geo/w.gif?lat=" + lat + "&lng=" + lon + "&Loc=none";
                                headerDiv.appendChild(img);
                            }
                        }
                    }
                    else {
                        writeData('Din posisjon kunne ikke hentes, feilmelding:<br/>' + status);
                    }
                }
                else {
                    writeData('Din posisjon kunne ikke hentes, feilmelding:<br/>' + status);
                }
            }
        );
    }
}

/*Fredrikstad;2;1_6*/
function handleMuni(muniName, lat, lon) {
    var subRegionsArray = subRegions.split('|');
        for (var i = 0; i < subRegionsArray.length; i++) {
        var elements = subRegionsArray[i].split(';');
        if (elements.length == 3) {
            var name = elements[0];
            var type = elements[1];
            var id = elements[2];

            if (name.toLowerCase() == muniName.toLowerCase()) {
                var headerDiv = document.getElementById('myDataHeader');
                if (headerDiv) {
                    headerDiv.innerHTML = name;
                    selectMunicipality(id);
                    
                    var img = document.createElement('img');
                    img.src = "http://valgflash.ntb.no/prod/geo/w.gif?lat=" + lat + "&lng=" + lon + "&Loc=" + muniName;
                    headerDiv.appendChild(img); 
                }                    
            }
        }        
    }
}



function writeData(data) {
    var myData = document.getElementById('divMyData');

    if (myData) {
        myData.innerHTML = '';
        myData.innerHTML = data;
    }
}


// report errors to user
function errorHandler(error) {
    var myData = document.getElementById('divMyData');

    if (myData) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                myData.innerHTML = "Kan ikke hente posisjon fordi stedstjenester ikke er slått på";
                break;
            case error.POSITION_UNAVAILABLE:
                myData.innerHTML = "Informasjon om din posisjon er ikke tilgjengelig";
                break;
            case error.TIMEOUT:
                myData.innerHTML = "Forsøk på å hente informasjon brukte for lang tid, prøv igjen";
                break;
            default:
                myData.innerHTML = "En feil oppstod. Kode: " + error.code + " Feilmelding: " + error.message;
                break;
        }         
    }
}

