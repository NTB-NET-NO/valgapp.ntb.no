﻿var subRegions = 'Østfold;4;1|Akershus;4;2|Hedmark;4;4|Oppland;4;5|Buskerud;4;6|Vestfold;4;7|Telemark;4;8|Aust-Agder;4;9|Vest-Agder;4;10|Rogaland;4;11|Hordaland;4;12|Sogn og Fjordane;4;14|Møre og Romsdal;4;15|Sør-Trøndelag;4;16|Nord-Trøndelag;4;17|Nordland;4;18|Troms;4;19|Finnmark;4;20|Agdenes;2;16_22|Alstahaug;2;18_20|Alta;2;20_12|Alvdal;2;4_38|Andebu;2;7_19|Andøy;2;18_71|Aremark;2;1_18|Arendal;2;9_6|Asker;2;2_20|Askim;2;1_24|Askvoll;2;14_28|Askøy;2;12_47|Audnedal;2;10_27|Aukra;2;15_47|Aure;2;15_76|Aurland;2;14_21|Aurskog-Høland;2;2_21|Austevoll;2;12_44|Austrheim;2;12_64|Averøy;2;15_54|Balestrand;2;14_18|Ballangen;2;18_54|Balsfjord;2;19_33|Bamble;2;8_14|Bardu;2;19_22|Beiarn;2;18_39|Berg;2;19_29|Bergen;2;12_1|Berlevåg;2;20_24|Bindal;2;18_11|Birkenes;2;9_28|Bjarkøy;2;19_15|Bjerkreim;2;11_14|Bjugn;2;16_27|Bodø;2;18_4|Bokn;2;11_45|Bremanger;2;14_38|Brønnøy;2;18_13|Bygland;2;9_38|Bykle;2;9_41|Bærum;2;2_19|Bø;2;18_67|Bø;2;8_21|Bømlo;2;12_19|Båtsfjord;2;20_28|Deanu gielda / Tana;2;20_25|Dovre;2;5_11|Drammen;2;6_2|Drangedal;2;8_17|Dyrøy;2;19_26|Dønna;2;18_27|Eid;2;14_43|Eide;2;15_51|Eidfjord;2;12_32|Eidsberg;2;1_25|Eidskog;2;4_20|Eidsvoll;2;2_37|Eigersund;2;11_1|Elverum;2;4_27|Enebakk;2;2_29|Engerdal;2;4_34|Etne;2;12_11|Etnedal;2;5_41|Evenes;2;18_53|Evje Og Hornnes;2;9_37|Farsund;2;10_3|Fauske;2;18_41|Fedje;2;12_65|Fet;2;2_27|Finnøy;2;11_41|Fitjar;2;12_22|Fjaler;2;14_29|Fjell;2;12_46|Flakstad;2;18_59|Flatanger;2;17_49|Flekkefjord;2;10_4|Flesberg;2;6_31|Flora;2;14_1|Flå;2;6_15|Folldal;2;4_39|Forsand;2;11_29|Fosnes;2;17_48|Fredrikstad;2;1_6|Frogn;2;2_15|Froland;2;9_19|Frosta;2;17_17|Fræna;2;15_48|Frøya;2;16_20|Fusa;2;12_41|Fyresdal;2;8_31|Førde;2;14_32|Gaivuona suohkan / Kåfjord;2;19_40|Gamvik;2;20_23|Gaular;2;14_30|Gausdal;2;5_22|Gildeskål;2;18_38|Giske;2;15_32|Gjemnes;2;15_57|Gjerdrum;2;2_34|Gjerstad;2;9_11|Gjesdal;2;11_22|Gjøvik;2;5_2|Gloppen;2;14_45|Gol;2;6_17|Gran;2;5_34|Grane;2;18_25|Granvin;2;12_34|Gratangen;2;19_19|Grimstad;2;9_4|Grong;2;17_42|Grue;2;4_23|Gulen;2;14_11|Guovdageainnu suohkan / Kautokeino;2;20_11|Hadsel;2;18_66|Halden;2;1_1|Halsa;2;15_71|Hamar;2;4_3|Hamarøy;2;18_49|Hammerfest;2;20_4|Haram;2;15_34|Hareid;2;15_17|Hasvik;2;20_15|Hattfjelldal;2;18_26|Haugesund;2;11_6|Hemne;2;16_12|Hemnes;2;18_32|Hemsedal;2;6_18|Herøy;2;15_15|Herøy;2;18_18|Hitra;2;16_17|Hjartdal;2;8_27|Hjelmeland;2;11_33|Hobøl;2;1_38|Hof;2;7_14|Hol;2;6_20|Hole;2;6_12|Holmestrand;2;7_2|Holtålen;2;16_44|Hornindal;2;14_44|Horten;2;7_1|Hurdal;2;2_39|Hurum;2;6_28|Hvaler;2;1_11|Hyllestad;2;14_13|Hægebostad;2;10_34|Høyanger;2;14_16|Høylandet;2;17_43|Hå;2;11_19|Ibestad;2;19_17|Inderøy;2;17_56|Iveland;2;9_35|Jevnaker;2;5_32|Jondal;2;12_27|Jølster;2;14_31|Karasjoga gielda / Karasjok;2;20_21|Karlsøy;2;19_36|Karmøy;2;11_49|Klepp;2;11_20|Klæbu;2;16_62|Kongsberg;2;6_4|Kongsvinger;2;4_2|Kragerø;2;8_15|Kristiansand;2;10_1|Kristiansund;2;15_5|Krødsherad;2;6_22|Kvalsund;2;20_17|Kvam;2;12_38|Kvinesdal;2;10_37|Kvinnherad;2;12_24|Kviteseid;2;8_29|Kvitsøy;2;11_44|Kvæfjord;2;19_11|Kvænangen;2;19_43|Lardal;2;7_28|Larvik;2;7_9|Lavangen;2;19_20|Lebesby;2;20_22|Leikanger;2;14_19|Leirfjord;2;18_22|Leka;2;17_55|Leksvik;2;17_18|Lenvik;2;19_31|Lesja;2;5_12|Levanger;2;17_19|Lier;2;6_26|Lierne;2;17_38|Lillehammer;2;5_1|Lillesand;2;9_26|Lindesnes;2;10_29|Lindås;2;12_63|Lom;2;5_14|Loppa;2;20_14|Lund;2;11_12|Lunner;2;5_33|Lurøy;2;18_34|Luster;2;14_26|Lyngdal;2;10_32|Lyngen;2;19_38|Lærdal;2;14_22|Lødingen;2;18_51|Lørenskog;2;2_30|Løten;2;4_15|Malvik;2;16_63|Mandal;2;10_2|Marker;2;1_19|Marnardal;2;10_21|Masfjorden;2;12_66|Meland;2;12_56|Meldal;2;16_36|Melhus;2;16_53|Meløy;2;18_37|Meråker;2;17_11|Midsund;2;15_45|Midtre Gauldal;2;16_48|Modalen;2;12_52|Modum;2;6_23|Molde;2;15_2|Moskenes;2;18_74|Moss;2;1_4|Målselv;2;19_24|Måsøy;2;20_18|Namdalseid;2;17_25|Namsos;2;17_3|Namsskogan;2;17_40|Nannestad;2;2_38|Narvik;2;18_5|Naustdal;2;14_33|Nedre Eiker;2;6_25|Nes;2;2_36|Nes;2;6_16|Nesna;2;18_28|Nesodden;2;2_16|Nesset;2;15_43|Nissedal;2;8_30|Nittedal;2;2_33|Nome;2;8_19|Nord-Aurdal;2;5_42|Norddal;2;15_24|Nord-Fron;2;5_16|Nordkapp;2;20_19|Nord-Odal;2;4_18|Nordre Land;2;5_38|Nordreisa;2;19_42|Nore Og Uvdal;2;6_33|Notodden;2;8_7|Nærøy;2;17_51|Nøtterøy;2;7_22|Odda;2;12_28|Oppdal;2;16_34|Oppegård;2;2_17|Orkdal;2;16_38|Os;2;12_43|Os;2;4_41|Osen;2;16_33|Oslo;2;3_1|Osterøy;2;12_53|Overhalla;2;17_44|Porsanger;2;20_20|Porsgrunn;2;8_5|Radøy;2;12_60|Rakkestad;2;1_28|Rana;2;18_33|Randaberg;2;11_27|Rauma;2;15_39|Re;2;7_16|Rendalen;2;4_32|Rennebu;2;16_35|Rennesøy;2;11_42|Rindal;2;15_67|Ringebu;2;5_20|Ringerike;2;6_5|Ringsaker;2;4_12|Rissa;2;16_24|Risør;2;9_1|Roan;2;16_32|Rollag;2;6_32|Rygge;2;1_36|Rælingen;2;2_28|Rødøy;2;18_36|Rømskog;2;1_21|Røros;2;16_40|Røst;2;18_56|Røyken;2;6_27|Røyrvik;2;17_39|Råde;2;1_35|Salangen;2;19_23|Saltdal;2;18_40|Samnanger;2;12_42|Sande;2;7_13|Sande;2;15_14|Sandefjord;2;7_6|Sandnes;2;11_2|Sandøy;2;15_46|Sarpsborg;2;1_5|Sauda;2;11_35|Sauherad;2;8_22|Sel;2;5_17|Selbu;2;16_64|Selje;2;14_41|Seljord;2;8_28|Sigdal;2;6_21|Siljan;2;8_11|Sirdal;2;10_46|Skaun;2;16_57|Skedsmo;2;2_31|Ski;2;2_13|Skien;2;8_6|Skiptvet;2;1_27|Skjervøy;2;19_41|Skjåk;2;5_13|Skodje;2;15_29|Skånland;2;19_13|Smøla;2;15_73|Snillfjord;2;16_13|Snåsa;2;17_36|Sogndal;2;14_20|Sokndal;2;11_11|Sola;2;11_24|Solund;2;14_12|Songdalen;2;10_17|Sortland;2;18_70|Spydeberg;2;1_23|Stange;2;4_17|Stavanger;2;11_3|Steigen;2;18_48|Steinkjer;2;17_2|Stjørdal;2;17_14|Stokke;2;7_20|Stord;2;12_21|Stordal;2;15_26|Stor-Elvdal;2;4_30|Storfjord;2;19_39|Strand;2;11_30|Stranda;2;15_25|Stryn;2;14_49|Sula;2;15_31|Suldal;2;11_34|Sund;2;12_45|Sunndal;2;15_63|Surnadal;2;15_66|Sveio;2;12_16|Svelvik;2;7_11|Sykkylven;2;15_28|Søgne;2;10_18|Sømna;2;18_12|Søndre Land;2;5_36|Sør-Aurdal;2;5_40|Sørfold;2;18_45|Sør-Fron;2;5_19|Sør-Odal;2;4_19|Sørreisa;2;19_25|Sørum;2;2_26|Sør-Varanger;2;20_30|Time;2;11_21|Tingvoll;2;15_60|Tinn;2;8_26|Tjeldsund;2;18_52|Tjøme;2;7_23|Tokke;2;8_33|Tolga;2;4_36|Torsken;2;19_28|Tranøy;2;19_27|Tromsø;2;19_2|Trondheim;2;16_1|Trysil;2;4_28|Træna;2;18_35|Trøgstad;2;1_22|Tvedestrand;2;9_14|Tydal;2;16_65|Tynset;2;4_37|Tysfjord;2;18_50|Tysnes;2;12_23|Tysvær;2;11_46|Tønsberg;2;7_4|Ullensaker;2;2_35|Ullensvang Herad;2;12_31|Ulstein;2;15_16|Ulvik;2;12_33|Unjargga gielda / Nesseby;2;20_27|Utsira;2;11_51|Vadsø;2;20_3|Vaksdal;2;12_51|Valle;2;9_40|Vang;2;5_45|Vanylven;2;15_11|Vardø;2;20_2|Vefsn;2;18_24|Vega;2;18_15|Vegårshei;2;9_12|Vennesla;2;10_14|Verdal;2;17_21|Verran;2;17_24|Vestby;2;2_11|Vestnes;2;15_35|Vestre Slidre;2;5_43|Vestre Toten;2;5_29|Vestvågøy;2;18_60|Vevelstad;2;18_16|Vik;2;14_17|Vikna;2;17_50|Vindafjord;2;11_60|Vinje;2;8_34|Volda;2;15_19|Voss;2;12_35|Værøy;2;18_57|Vågan;2;18_65|Vågsøy;2;14_39|Vågå;2;5_15|Våler;2;4_26|Våler;2;1_37|Øksnes;2;18_68|Ørland;2;16_21|Ørskog;2;15_23|Ørsta;2;15_20|Østre Toten;2;5_28|Øvre Eiker;2;6_24|Øyer;2;5_21|Øygarden;2;12_59|Øystre Slidre;2;5_44|Åfjord;2;16_30|Ål;2;6_19|Ålesund;2;15_4|Åmli;2;9_29|Åmot;2;4_29|Årdal;2;14_24|Ås;2;2_14|Åseral;2;10_26|Åsnes;2;4_25|Gamle Oslo;3;3_1_01|Grünerløkka;3;3_1_02|Sagene;3;3_1_03|St. Hanshaugen;3;3_1_04|Frogner;3;3_1_05|Ullern;3;3_1_06|Vestre Aker;3;3_1_07|Nordre Aker;3;3_1_08|Bjerke;3;3_1_09|Grorud;3;3_1_10|Stovner;3;3_1_11|Alna;3;3_1_12|Østensjø;3;3_1_13|Nordstrand;3;3_1_14|Søndre Nordstrand;3;3_1_15|Austre Åmøy;3;11_3_11|Buøy/Hundvåg;3;11_3_1|Eiganes;3;11_3_8|Gausel;3;11_3_16|Hafrsfjord;3;11_3_23|Hinna;3;11_3_17|Indre Tasta;3;11_3_26|Kampen;3;11_3_9|Kvaleberg;3;11_3_19|Kvernevik;3;11_3_24|Madlamark;3;11_3_22|Roaldsøy;3;11_3_13|Stokka;3;11_3_21|Storhaug;3;11_3_5|Sunde;3;11_3_25|Tjensvoll;3;11_3_20|Ullandhaug;3;11_3_7|Varden;3;11_3_4|Vassøy;3;11_3_14|Vaulen;3;11_3_18|Våland;3;11_3_6|Ytre Tasta;3;11_3_27|Nesttun;3;12_1_0049|Alvøen;3;12_1_0070|Sædalen;3;12_1_0077|Bergen rådhus;3;12_1_0001|Bjørndalsskogen;3;12_1_0071|Bønes;3;12_1_0009|Damsgård;3;12_1_0074|Eidsvåg;3;12_1_0022|Garnes;3;12_1_0037|Gimle;3;12_1_0015|Haukedalen;3;12_1_0028|Haukeland;3;12_1_0004|Haukås;3;12_1_0030|Hellen;3;12_1_0018|Hjellestad;3;12_1_0056|Hop;3;12_1_0051|Hordvik;3;12_1_0029|Indre Arna;3;12_1_0036|Kaland;3;12_1_0044|Kirkevoll;3;12_1_0045|Kringlebotn;3;12_1_0040|Landås;3;12_1_0013|Liland;3;12_1_0058|Lone;3;12_1_0039|Lynghaug;3;12_1_0062|Lyshovden;3;12_1_0061|Mathopen;3;12_1_0069|Mjølkeråen;3;12_1_0024|Møhlenpris;3;12_1_0002|Nordnes;3;12_1_0003|Ny Krohnborg;3;12_1_0007|Nygårdslien;3;12_1_0076|Olsvik;3;12_1_0067|Ortun;3;12_1_0063|Paradis;3;12_1_0052|Rolland;3;12_1_0032|Rothaugen;3;12_1_0019|Rå;3;12_1_0055|Salhus;3;12_1_0025|Sandsli;3;12_1_0054|Skeie;3;12_1_0057|Skjold;3;12_1_0048|Slettebakken;3;12_1_0014|Søreide;3;12_1_0059|Tertnes;3;12_1_0023|Vadmyra;3;12_1_0068|Ytre Arna;3;12_1_0035|Nyborg;3;12_1_0031|Blussuvoll;3;16_1_14|Bratsberg;3;16_1_19|Breidablikk;3;16_1_24|Brundalen;3;16_1_36|Byåsen;3;16_1_22|Charlottenlund;3;16_1_12|Eberg;3;16_1_37|Elgeseter/Øya;3;16_1_4|Flatåsen;3;16_1_27|Hallset;3;16_1_21|Hoeggen;3;16_1_20|Ila;3;16_1_2|Midtbyen;3;16_1_3|Kolstad;3;16_1_26|Lade;3;16_1_9|Lademoen;3;16_1_7|Møllenberg;3;16_1_38|Nardo;3;16_1_16|Nidarvoll;3;16_1_15|Nypvang;3;16_1_28|Ranheim;3;16_1_11|Romolslia;3;16_1_23|Rosenborg;3;16_1_6|Rye;3;16_1_32|Singsaker;3;16_1_5|Sjetne;3;16_1_25|Solbakken;3;16_1_18|Spongdal;3;16_1_30|Stabbursmoen;3;16_1_35|Strindheim;3;16_1_13|Sverresborg;3;16_1_1|Tonstad;3;16_1_8|Trolla;3;16_1_10|Ugla;3;16_1_31|Utleira;3;16_1_34|Vikåsen;3;16_1_39|Åsheim;3;16_1_29|Åsvang;3;16_1_17|Åsveien;3;16_1_33|Uoppgitt/Forhåndsstemmer;3;3_1_00|Forhånd ikke kretsfordelt;3;11_3_VIRT|Forhånd ikke kretsfordelt;3;12_1_VIRT|Forhånd ikke kretsfordelt;3;16_1_VIRT';


//|Sædalen;3;12_1_377 this 3 are not in the map
//|Alvøen;3;12_1_570
//|Nesttun;3;12_1_0049

//|Harstad;2;19_1 taken out


//|Holen;3;12_1_575
//|Midtun;3;12_1_349
//|Minde;3;12_1_710
//|Ulsmåg;3;12_1_341
//|Varden;3;12_1_464

// EC1C24    0091CC    FFDD2E    DD5E74    009A4C    005E96    C8C5A8    B01116    98B94E
// C8C5A8    FFDF8F    FAB613    F28020    58595B
//rot 	E21C30
//blau	0FABC9
/* regionid : [reps2007, reps2011, change]*/
var changeRegions = {
    'c01m0105': [49, 43, -6],
    'c04m0429': [17, 23, 6],
    'c04m0432': [19, 17, -2],
    'c05m0528': [37, 29, -8],
    'c06m0616': [41, 35, -6],
    'c06m0620': [25, 21, -4],
    'c06m0623': [43, 35, -8],
    'c06m0624': [41, 37, -4],
    'c06m0625': [41, 35, -6],
    'c06m0627': [35, 27, -8],
    'c08m0814': [27, 33, 6],
    'c08m0828': [25, 21, -4],
    'c08m0831': [21, 13, -8],
    'c09m0901': [31, 29, -2],
    'c09m0919': [17, 19, 2],
    'c09m0926': [29, 27, -2],
    'c09m0928': [27, 21, -6],
    'c09m0940': [21, 15, -6],
    'c11m1101': [35, 31, -4],
    'c11m1122': [25, 27, 2],
    'c11m1134': [25, 19, -6],
    'c12m1241': [25, 21, -4],
    'c12m1265': [17, 15, -2],
    'c14m1426': [29, 25, -4],
    'c14m1428': [23, 21, -2],
    'c15m1534': [37, 31, -6],
    'c15m1535': [27, 23, -4],
    'c15m1576': [23, 21, -2],
    'c16m1601': [85, 67, -18],
    'c16m1624': [27, 23, -4],
    'c17m1717': [21, 17, -4],
    'c17m1729': [27, 31, 4],
    'c17m1749': [17, 15, -2],
    'c18m1820': [31, 27, -4],
    'c18m1837': [27, 25, -2],
    'c18m1851': [21, 17, -4],
    'c18m1870': [35, 27, -8],
    'c19m1901': [43, 35, -8],
    'c19m1923': [15, 19, 4],
    'c19m1924': [29, 25, -4],
    'c19m1925': [21, 15, -6],
    'c20m2019': [25, 19, -6],
    'c20m2025': [17, 23, 6]
};

// 5 coordinates per line
var seatXCoordinates = [

147.14,171.48,195.81,220.15,244.48,
147.14,171.48,195.81,220.15,244.48,
147.14,171.48,195.81,220.15,244.48,
147.14,171.48,195.81,220.15,244.48,
147.14,171.48,195.81,220.15,244.48,			
			
149.79,173.65,199.99,152.95,176.81,

161.11,183.61,206.12,228.62,251.12,
167.25,189.75,212.72,234.76,257.26,

180.66,200.96,228.08,189.55,209.85,

209.3,226.48,243.67,260.85,278.04,
203.7,220.73,237.91,255.1,272.28,289.46,

231.16,244.17,257.61,
278.63,257.61,271.06,

274.55,284.45,293.64,302.82,312.01,321.19,
289.49,299.4,308.58,317.76,326.95,336.13,

//75-79
328.58,333.14,345.2,344.43,348.99,

//midten
374.82,374.82,374.58,374.34,374.09,373.85,
390.96,390.96,390.71,390.47,390.23,389.99,

//92-96
424.41,419.41,420.56,441.86,436.81,

476.37,466.35,456.72,447.09,437.46,427.83,
491.19,481.17,471.54,461.91,452.28,442.64,

508.05,494.2,486.19,
535.71,521.32,507.47,

562.11,544.81,527.29,509.77,492.24,474.72,
556.01,538.48,520.96,503.44,485.91,

575.19,558.36,537.85,583.88,

598.00,575.31,552.62,529.93,507.24,
603.83,581.14,558.45,535.76,513.07,

609.33,586.33,564.94,612.33,589.33,

616.01,592.67,568.34,544.01,520.67,
616.01,592.67,568.34,544.01,520.67,
616.01,592.67,568.34,544.01,520.67,
616.01,592.67,568.34,544.01,520.67,
616.01,592.67,568.34,544.01,520.67,
];

var seatYCoordinates = [

154.36,154.36,154.36,154.36,154.36,			
185.42,185.42,185.42,185.42,185.42,
201.55,201.55,201.55,201.55,201.55,
231.11,231.11,231.11,231.11,231.11,
247.25,247.25,247.25,247.25,247.25,
								
277.3,272.53,275.19,293.12,288.35,

321.25,311.99,302.73,293.47,284.2,
336.17,326.91,318.99,308.39,299.12,

362.21,348.81,343.44,375.68,362.27,
		
400.42,383.18,365.95,348.72,331.49,			
429.14,411.81,394.58,377.34,360.11,342.88,

451.52,431.39,411.11,
396.34,440.31,420.03,

478.42,454.89,432.36,409.82,387.28,364.75,
484.51,460.98,438.45,415.91,393.37,370.84,

//75-79
469.98,446.08,423.96,473.01,449.1,

//midten
500.53,476.08,451.75,427.41,403.07,378.74,			
500.53,476.08,451.75,427.41,403.07,378.74,

//92-96
473.01,449.1,423.96,469.98,446.08,

484.9,460.68,438.33,415.98,393.63,371.29,
478.51,454.29,431.94,409.59,387.24,364.9,
		
439.78,419.78,395.85,
452.09,430.6,410.59,
		
429.11,411.08,394.2,377.31,360.42,343.54,
399.47,382.58,365.69,348.81,331.92,

375.7,357.43,344.34,362.1,

335.01,326.21,317.42,308.62,299.82,
319.97,311.17,302.37,293.57,284.78,

293.12,288.35,275.19,277.3,272.53,

247.25,247.25,247.25,247.25,247.25,
231.11,231.11,231.11,231.11,231.11,			
201.55,201.55,201.55,201.55,201.55,
185.42,185.42,185.42,185.42,185.42,
154.36,154.36,154.36,154.36,154.36,

88.96,88.96,88.96,88.96,88.96,			
72.82,72.82,72.82,72.82,72.82
];					


function party(id, shortName, name, color){
    this.id = id;
    this.shortName = shortName;
    this.name = name;
    this.color = color;
}

var partiesBlock = {
	//1: new party(3, 'RV', 'Rødt', '#B01116'),
	1: new party(3, 'RØDT', 'Rødt', '#B01116'),
	2: new party(3, 'Sv', 'Sosialistisk Venstreparti', '#EC1C24'),
	3: new party(3, 'Ap', 'Arbeiderpartiet', '#DD5E74'),
	4: new party(3, 'Sp', 'Senterpartiet', '#009A4C'),
	5: new party(7, 'V', 'Venstre', '#98B94E'),
	6: new party(7, 'KrF', 'Kristelig Folkeparti', '#FFDD2E'),
	7: new party(7, 'H', 'Høyre', '#0091CC'),
	8: new party(7, 'FrP', 'Fremskrittspartiet', '#005E96'),
	9: new party(7, 'A', 'Andre', '#C8C5A8')
}
var parties = {
    //1: new party(1, 'RV', 'Rødt', '#B01116'),
	1: new party(1, 'RØDT', 'Rødt', '#B01116'),
	2: new party(2, 'Sv', 'Sosialistisk Venstreparti', '#EC1C24'),
	3: new party(3, 'Ap', 'Arbeiderpartiet', '#DD5E74'),
	4: new party(4, 'Sp', 'Senterpartiet', '#009A4C'),
	5: new party(5, 'V', 'Venstre', '#98B94E'),
	6: new party(6, 'KrF', 'Kristelig Folkeparti', '#FFDD2E'),
	7: new party(7, 'H', 'Høyre', '#0091CC'),
	8: new party(8, 'FrP', 'Fremskrittspartiet', '#005E96'),
	9: new party(9, 'A', 'Andre', '#C8C5A8'), //andre med mdg
	10: new party(10,'MDG','Miljøpartiet De Grønne', '#18ff00'),
	11: new party(9, 'A', 'Andre', '#C8C5A8'), //andre uten mdg
	100: new party(100, 'AKSJOLJE', 'Folkeliste mot oljeboring i Lofoten, Vesterålen og Senja', '#C8C5A8'),
	101: new party(101,'DEMN','Demokratene i Norge', '#C8C5A8'),
	102: new party(102,'DLF','Det Liberale Folkepartiet', '#C8C5A8'),
	103: new party(103,'FOLKEM','Folkemakten', '#C8C5A8'),
	104: new party(104,'KRISTNE','De Kristne', '#C8C5A8'),
	105: new party(105,'KSP','Kristent Samlingsparti', '#C8C5A8'),
	106: new party(106,'KYST','Kystpartiet', '#C8C5A8'),
	//107: new party(107,'MDG','Miljøpartiet De Grønne', '#18ff00'),
	107: new party(107,'NKP','Norges Kommunistiske Parti', '#C8C5A8'),
	108: new party(108,'PIR','Piratpartiet', '#C8C5A8'),
	109: new party(109,'PP','Pensjonistpartiet', '#C8C5A8'),
	110: new party(110,'SFP','Samfunnspartiet', '#C8C5A8'),
	111: new party(111,'SJALTA','Sykehus til Alta', '#C8C5A8'),
	1000: new party(1000, 'A', 'Andre', '#C8C5A8'), //andre fra SSB
   /* 1000: new party(1000, 'Andre', 'Andre', '#C8C5A8'),
    1001: new party(1001, 'Lokale', 'Lokale lister', '#C8C5A8')*/
};

/*
AD = Adresseavisen 
AP = Aftenposten 
BT = Bergens Tidende 
DO = dittoslo.no 
DT = Drammens Tidende 
FB = Fredriksstad Blad 
FV = Fædrelandsvennen 
HAM = Hamar Arbeiderblad 
HAU = Haugesunds Avis 
HE = hegnar.no
KB = Kragerø Blad 
LP = Laagendalsposten 
MA = Moss Avis 
NA = Nationen 
OB = Østlandets Blad 
RH = Røyken og Hurum 
SA = Stavanger Aftenblad 
SB = Sandefjords Blad 
TB = Tønsbergs Blad 
VA = Varden 
VG = VG 
OS = Østlendingen
VL = Vårt land
SMP = Sunnmørsposten
DA = Dagsavisen
GG = Gjengangeren
ABC = ABC Nyheter
NDLA = NDLA
VIS = vision norge
*/

/*var logoCodes = {
   // AD: 'logo_adresseavisen.png',
    AP: 'aftenposten.png',
    BT: 'btnoLogo.png',
   // DO: 'dittoslo-osloavisene.png',
   // DT: 'dt.png',
   // FB: 'fb.png',
    FV: 'fvnno_logo_176x44.png',
    HAM: 'HA logo NY 2003.png',
   // HAU: 'ha.png',
  //  HE: 'HegnarOnline.png',
  //  KB: 'kv.png',
  //  LP: 'lp.png',
  //  MA: 'ma.png',
    NA: 'nationen_logo.png',
  //  OB: 'ob.png',
 //   RH: 'rha.png',
    SA: 'Aftenbladet.png',
 //   SB: 'sb.png',
 //   TB: 'tb.png',
 //   VA: 'va.png',
    VG: 'vg.png',
  //  OS: 'os.png',
    VL: 'Vartland-masthead.png',
    SMP: 'smp-no.jpg',
 //   GG: 'gg.png',
 //   DA: 'ntb.jpg',
 //   ABC: 'ntb.jpg'
 // 
};*/
var logoCodes = {
	AD: 'ADRESSA_mini.png,ADRESSA_150.png,ADRESSA_125.png',
    AP: 'aftenposten.png,aftenposten.png,aftenposten.png',
    BT: 'BT_logo_forenklet_symbol_150x20.png,BT_logo_forenklet_symbol_150x70.png,BT_logo_forenklet_symbol_125x70.png',
    FV: 'fvn_symbol_150_20.png,fvn_symbol_150_70.png,fvn_symbol_125_70.png',
    HAM: 'HA_logo_150x20.png,HA_logo_150x70.png,HA_logo_125x70.png',
    NA: 'NA_20.png,NA_150.png,NA_125.png',
    SA: 'saNTB_150x20_white.png,saNTB_150x70_white.png,saNTB_125x70_white.png',
    VG: 'vg_mini.png,vg_vanlig.png,vg_phone.png',
    VL: 'vl_20.png,vl_150.png,vl_125.png',
    SMP: 'Sunnmorsposten_S_20px.png,Sunnmorsposten_S_70px.png,Sunnmorsposten_S_70px.png',
	ABC: 'ABCMini_150x20.png,ABCVanlig_150x70.png,ABCphone_125x70.png',
	DA: 'Dagsavisen_Mini.png,Dagsavisen_Vanlig.png,Dagsavisen_Phone.png',
	NDLA : 'ndla_20.png,ndla_150.png,ndla_125.png',
	VIS: 'ntb_.jpg,ntb_.jpg,ntb_.jpg',
    PROD: 'ntb.png,ntb.png,ntb.png'
};
var homeCodes = {
	AD: '',
	AP: '',//'countyPolls,OSLO,c00m0301,votes',
    BT: 'Storting, ,n',
    FV: '',
    HAM: 'countyPolls,HEDMARK,c04,votes',
    NA: '',
    SA: '',
    VG: '',
    VL: '',
    SMP: '',
	ABC: '',
	DA: 'Polls,,n,mandates',
	NDLA : '',
	PROD: ''//'Storting,OSLO,n,votes'//'countyPolls,OSLO,c00m0301,votes'
}
var homeCodesValg = {
	AD: '',
	AP: '',
    BT: 'Storting, ,n',
    FV: '',
    HAM: 'County,HEDMARK,c04,votes',
    NA: '',
    SA: '',
    VG: '',
    VL: '',
    SMP: '',
	ABC: '',
	DA: 'Nation,,n,mandates',
	NDLA : '',
	PROD: ''//'Storting,OSLO,n,votes'//'countyPolls,OSLO,c00m0301,votes'
}
function getPartiId(partiShort) {
	var id;
	$.each(parties, function() {
	
		if(this.shortName.toLowerCase() == partiShort.toLowerCase()){
			id = this.id;
			
			
		}
		});
		return id;
}

function getPartyShortName(id) {
    var partyShortName = '';    
    var party = parties[id];

    if (party != undefined)
        partyShortName = party.shortName;
	
    return partyShortName;        
}
function getPartyName(id) {
    var partyShortName = '';
    var party = parties[id];

    if (party != undefined)
        partyShortName = party.name;

    return partyShortName;
}
function getPartyNameBySC(sc){
	var partyName = '';
	$.each(parties, function() {
	
		if(this.shortName.toLowerCase() == sc.toLowerCase()){
			partyName = this.name;
			
			
		}
		});
		return partyName;
}
function getPartyLogoSrc(id, caller){
	var logoSrc = '';
	var path = 'images/logo/';
	var party = parties[id];
	var name = '';
	var caller_ = caller;
		
	if (party != undefined){
		//name = party.id + '_480.png';
		if (caller_ == 'phone'){
			path = '../images/logo/';
			name = party.id + '.png';
		}else if(caller_ == 'webMandat'){
			name = party.id + '.png';
		}
		else 
			name = party.id + '_480.png';
		logoSrc = path + name;
	}
	
	return logoSrc;
}

function getPartyColor(id){
	var color = '';//'#C8C5A8'; //return Andre color if no color is found
    var party = parties[id];

	if (party != undefined)
		color = party.color;
	
	return color;
}
function getRegionId(name){
	var subRegionsArray = subRegions.split('|');
    for (var i = 0; i < subRegionsArray.length; i++) {
		
        var elements = subRegionsArray[i].split(';');
		var namea = elements[0];
        var type = elements[1];
        var id = elements[2];
		
		if (name.toUpperCase() == namea.toUpperCase()) {
			if (id == '3_1') id = '03';
			
			return id;
		}
	}
}
// get regionname from regioncode
// c01m0106
// c14
// c301
function getRegionName(region) {
region = region+'';

	if (region == null || region == 'null' || region == ''){
		return region;
	}
	if(region == '01' || region == '02' || region == '03'|| region == '04' || region == '05' || region == '06' ||region == '07' || region == '08' || region == '09'  ){
		while ( region.charAt(0) === '0')
						region = region.substr(1);
		
	}	
	
	if (region == '301' || region == 'c03' || region == 3)
		region = 'c301';
    var subRegionsArray = subRegions.split('|');
    for (var i = 0; i < subRegionsArray.length; i++) {
		
        var elements = subRegionsArray[i].split(';');
		
        if (elements.length == 3) {
            var name = elements[0];
            var type = elements[1];
            var id = elements[2];
			
			if (region == id) return name;
            // id = 1_5  
            var idElements = id.split('_');

            var id;
            if (idElements.length == 2) {
                //Oslo 3_1 = c301
                if (idElements[0] == '3' && idElements[1] == '1' && (region == 'c301'  ))
                    return name;
                //Ammerud skole;3;3_1_1003
				
                var countyId = idElements[0];
                var municipalityId = idElements[1];

                if (countyId.length == 1)
                    countyId = '0' + countyId;

                if (municipalityId.length == 1)
                    municipalityId = '0' + municipalityId;

                id = countyId + municipalityId;
				
				if (id.toLowerCase() == region.toLowerCase())
                    return name;
                id = 'c' + countyId + 'm' + id;
			
                if (id.toLowerCase() == region.toLowerCase())
                    return name;
            }
			else if (idElements.length == 3){
				//oslo
				
				if (idElements[0] == '3' && idElements[1] == '1'){
					var distId = idElements[2];
					/*if (distId.length == 1)
						distId = '0' + distId;
					if(distId.length == 2)
						distId = '0' + distId;
					if (distId.length == 3)
						distId = '0' + distId;*/
					var id = 'c00m0301d'+distId;
					
					if (id.toLowerCase() == region.toLowerCase())
                    return name;
				}else {
				//Bergen Rådhus;3;12_1_201
					var countyId = idElements[0];
					if (countyId.length == 1)
						countyId = '0' + countyId;
					
					var municipalityId = idElements[1];
					if (municipalityId.length == 1)
						municipalityId = '0' + municipalityId;
					
					id = countyId + municipalityId; // 1201 
					countyId = 'c' + countyId; 	 	//c12	
					id = countyId +"m"+ id;				//c12m1201
					var distId = idElements[2];
					if (distId.length == 1)
						distId = '0' + distId;
					if(distId.length == 2)
						distId = '0' + distId;
					if (distId.length == 3)
						distId = '0' + distId;
						
					id = id +"d"+distId				//c121201d0201
					
					if (id.toLowerCase() == region.toLowerCase())
                    return name;
				}
			}
            else if (idElements.length == 1) {
                var countyId = idElements[0];
				
                if (countyId.length == 1)
                    countyId = '0' + countyId;
				if (id.toLowerCase() == region.toLowerCase())
                    return name;
                id = 'c' + countyId;
                if (id.toLowerCase() == region.toLowerCase())
                    return name;
            }
        }
    }

    return '';
}

function getColumnsMaxPercent(partyResults, maxColumns, regionCode, includeOther) {
    var maxPercent = 0;
	
	if(state == 'countyPolls') {}
	
	
    for (var i = 0; i < maxColumns; i++) {        
        var partyResult = partyResults[i];
		
        if(state == 'countyPolls') {
			var partyElements = partyResult.split('$').clean("\r");;
		}else{
			var partyElements = partyResult.split(';').clean("\r");;
		}
        if (partyElements.length > 3) {
		
			
			var partyId = parseInt(partyElements[0]);
            
            if (!includeOther && partyId == 9)
                continue;
            
			if(state == 'countyPolls') {
				var percent = parseFloat(partyElements[2]);
			}else{
            var percent = ((regionCode == '03' || regionCode == '02') /**** here is the last shit ***/ && ( isPollParse == false )) ? parseFloat(partyElements[1]) : parseFloat(partyElements[3]);
			}
			
            if (percent > maxPercent)
                maxPercent = percent;
        }
    }
	
    var interval = (maxPercent <= 10) ? 2 : ((maxPercent < 30) ? 5 : 10);
    var maxValue = (maxPercent == 0) ? 100 : (Math.ceil(maxPercent / interval) * interval) + interval;
	
    return maxValue;

}

function getHistoryMaxPercent(history){
	var maxPercent = 0;
		
	for (var i = 2; i < history.length; i++){ //don't bother with first 2 elements (n|1)
		var pointInTime = history[i].split(";"); //aug.-10;1#1.8;2#5.6;3#26.6;4#5.0;5#4.6;6#5.3;7#27.1;8#22.8;9#1.1
		
		if (pointInTime){
			for (var k = 1; k < pointInTime.length; k++){ //don't bother with first element (aug.-10)
				var pointData = pointInTime[k];	//3#26.6			
				var pointElements = pointData.split("#");
						
				if (pointElements.length == 2){
					var partyId = parseInt(pointElements[0],10);
					var percent = parseFloat(pointElements[1]);
					
					if (historyExceptions[partyId - 1] == 1)
						if (percent > maxPercent)
							maxPercent = percent;
				}
			}
		}		
	}
	
	var interval = (maxPercent <= 10)? 2 : 10;
	var maxValue = (maxPercent == 0)? 100 : (Math.ceil(maxPercent/interval) * interval) + interval;
	
	return maxValue;			
}
	
function loadCounties() {
	//Parser fylkesnavn
	var countyId = 1;
	var counties = 'Østfold|Akershus|Oslo|Hedmark|Oppland|Buskerud|Vestfold|Telemark|Aust-Agder|Vest-Agder|Rogaland|Hordaland|Undef|Sogn og Fjordane|Møre og Romsdal|Sør-Trøndelag|Nord-Trøndelag|Nordland|Troms|Finnmark|';
	var countyArray = [];
 
	while (counties.indexOf("|") > -1){
		var pos = counties.indexOf("|");
		countyArray[countyId] = counties.substring(0, pos);
		counties = counties.substring(pos + 1, counties.length);
		countyId++;
  	}
  		
  	return countyArray;
}

function roundNumber(num, dec) {
	//var result = //Math.round( Math.round( num * Math.pow( 10, dec + 1 ) ) / Math.pow( 10, 1 ) ) / Math.pow(10,dec);

    var result = String(Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec));
    
    if (result.indexOf('.') < 0) { result += '.'; }
        while (result.length - result.indexOf('.') <= dec) { result += '0'; }
    
    return result;
}

Array.prototype.clean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
         if (this[i] == deleteValue) {
              this.splice(i, 1); i--;
         }
    } 
    return this; 
};

function serialize(obj) {
    var returnVal;
    if (obj != undefined) {
        switch (obj.constructor) {
            case Array:
                var vArr = "[";
                for (var i = 0; i < obj.length; i++) {
                    if (i > 0) vArr += ",";
                    vArr += serialize(obj[i]);
                }
                vArr += "]";
                return vArr;
            case String:
                returnVal = "'" + obj + "'";
                return returnVal;
            case Number:
                returnVal = isFinite(obj) ? obj.toString() : null;
                return returnVal;
            case Date:
                returnVal = "#" + obj + "#";
                return returnVal;
            default:
                if (typeof obj == "object") {
                    var vobj = [];
                    for (attr in obj) {
                        if (typeof obj[attr] != "function") {
                            vobj.push('"' + attr + '":' + serialize(obj[attr]));
                        }
                    }
                    if (vobj.length > 0)
                        return "{" + vobj.join(",") + "}";
                    else
                        return "{}";
                }
                else {
                    return obj.toString();
                }
        }
    }
    return null;
}


var prefs = {
    data: {},
    load: function () {
        var cookie = getCookie('valg2013Fav');
        if (cookie) {
            this.data = eval(unescape(cookie));
        }
        return this.data;
    },
    save: function (name, expires, path) {
        var d = expires || new Date(2020, 02, 02);
        var p = path || '/';
        document.cookie = name + "=" + escape(this.data)
            + ';path=' + p
                + ';expires=' + d.toGMTString();
    }
};

function getCookie(check_name) {
    // first we'll split this cookie up into name/value pairs
    // note: document.cookie only returns name=value, not the other components
    var a_all_cookies = document.cookie.split(';');
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false; // set boolean t/f default f

    for (var i = 0; i < a_all_cookies.length; i++) {
        // now we'll split apart each name=value pair
        a_temp_cookie = a_all_cookies[i].split('=');


        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed check_name
        if (cookie_name == check_name) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if (!b_cookie_found) {
        return null;
    }
}

function getPollIndex(obj, pollArray) {
	var id;
	$.each(pollArray, function() {
		/*if(this.shortName.toLowerCase() == partiShort.toLowerCase()){
			id = this.id;
			
			
		}*/
		});
		return id;

}
function eksistsInArray(obj, array) {
    for (var i = 0; i < array.length; i++ ) {
        var arrObj = array[i];
        if (arrObj.id && obj.id && (arrObj.id == obj.id))
            return true;
    }

    return false;
}
function eksistsInArrayWeb(obj, array) {
	
    for (var i = 0; i < array.length; i++ ) {
		
        var arrObj = array[i];
        if (arrObj.name && obj.name && (arrObj.name == obj.name)){
			return true;
		}
    }

    return false;
}

function addThousandSeparator(n, sep) {
    var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})'),
    sValue = n + '';

    if (sep === undefined) {sep = ',';}

    while(sRegExp.test(sValue)) {
        sValue = sValue.replace(sRegExp, '$1' + sep + '$2');
    }

    return sValue;
}

function cookies_are_enabled() {
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;

    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
        document.cookie = "testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }
    return (cookieEnabled);
}

function querySt() {

    //http://valgflash.ntb.no/fv/ipad

    var url = window.location.toString();
	
    var urlElements = url.split("/");
    if (urlElements.length > 3)
        return urlElements[3];

    return "";
    
    /*var elements = q.split("&");
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i].split("=");
        if (element[0] == query) {
            return element[1];
        }
    }*/
}

function readAdd() {
	//var hashes = window.location.href.slice(window.location.href.indexOf('?')+1).split('&');

	var vars = [],hash;
	var t = window.top.location;//.toString().split('?')[1];
	var t = document.referrer.toString().split('?')[1];

	if (t != undefined){
		t = t.split(',');
		return t;
	}else {
		t = location.search.split('?')[1];
		if (t != undefined){
			t = t.split(',');
			return t;
		}else 
			return "";
		
		
		
		
	}
	return "";
	
}
function sortByParti (x,y) {
	
	return parseInt(x.PartiOrganizationId) - parseInt(y.PartiOrganizationId);
	//return (parseInt(x.PartiOrganizationId) < parseInt(y.PartiOrganizationId)) ? -1 : (parseInt(x.PartiOrganizationId) > parseInt(y.PartiOrganizationId)) ? 1 : 0;
}
function sortByFylke (x,y){
	
	return parseInt(x.CountyId) - parseInt(y.CountyId);
	//return (parseInt(x.CountyId) < parseInt(y.CountyId)) ? -1 : (parseInt(x.CountyId) > parseInt(y.CountyId)) ? 1 : 0;

}
function sortMyResults (x,y) {
	return parseInt(x.split(';')[0]) - parseInt(y.split(';')[0]);
}
