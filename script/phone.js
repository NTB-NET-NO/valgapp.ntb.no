﻿	var state; //global var for selected state - nation, county, municipality or search
	var region; //global var for selected region
	var regionName;
	var componentIdNation;
	var componentIdCounty;
	var componentIdMunicipality;
	var componentIdDistrict;
	var componentIdMy;
	
	var regionCodeNation = '06';
	var regionCodeNationPoll = '05';
	var regionCodeCounty = '04';
	var regionCodeDistrict = '03';
	var regionCodeMunicipality = '02';
	
	var isValg = false;
	var itsNine = false;
	//var valgStart =  new Date(2013,08,04,12,50,00);
	var valgStart =  new Date(2013,08,09,20,50,00); // 9.9.2013 20.50
	var infoSwitch =  new Date(2013,08,09,21,01,00); // 21.01 from land info to kommune info
	
	//var valgStart =  new Date(2013,08,09,19,20,00); 
	//var infoSwitch =  new Date(2013,08,09,19,01,00); 
	var idag = new Date();
	
	var searchRepParty = 0;
	var countyElectionMode = true;
	var currentPollCollectionResults = {};
	var currentPollCollectionResultsM = {};
	var lastWeekPolls;
	var pollText = '';
	var isPollParse = false;
	var gotoSelectLastWeek = false;
	var mainButton = 'empty'; // to define mainbutton states
	var isFav = false; //to define if choosen kommune or county is added to fav

	var inRepres = [];
	var	countySpecialM = [];
	
	var herausfordererMandat = [];
	//var dataDir = 'data/';	
	var dataDir = 'http://valgapp.ntb.no/prod/data/';


	var mySelectedPoll = 0;
	var mySelectedPollHL = '';
	var mySelectedPollID = '';
	
	var vars = [], value; /*** TEST VG ***/
	
	function loadPage() {
		closeNoResltsInfo();
		checkDate();
		vars = readAdd();
	    //set logo
	    var code = querySt();
		//code = 'VG';
		$('body').addClass(code.toUpperCase());
	    if (logoCodes[code.toUpperCase()]) {
	        var logoFileName = logoCodes[code.toUpperCase()];
			if (logoFileName){
				var logoArray = logoFileName.split(',');
				logoFileName = logoArray[2];
			}
	        if (!logoFileName)
	            logoFileName = "ntb.png";

	        var logoimg = document.getElementById('logo');
            if (logoimg)
                logoimg.src = '../logo/' + logoFileName;
        }else {
			logoFileName = "ntb.png";
			var logoimg = document.getElementById('logo');
			if (logoimg)
				logoimg.src = '../logo/' + logoFileName;
		}
		if(!isValg) {
			
			$('#valgBtnText').text('MÅLING');
			$('#valgBtn').addClass('notValg');
			$('#buttonNation').attr('onclick', 'toggleState("nationPolls")');
			$('#buttonConty').attr('onclick', 'toggleState("countyPolls")');
			$('#buttonMunicipality').attr('style', 'display:none');
		}
		
		
	$(".mMBtn").click(function() {
		$(".mMBtn").removeClass('activeMMBtn');
		if (mainButton == this.id){
			mainButton = 'empty';
			$(".mMBtnText").attr("style","display:table-row;");
			$(".contentDiv").attr("style","display:none;");
			$("#stemmerMeny").attr("style","display:none;");
			if(this.id == ''){
			
			}
		
		}else {
			$("#stemmerMeny").attr("style","display:none;");
			$(".contentDiv").attr("style","display:none;");
			mainButton = this.id;
			$(this).addClass('activeMMBtn');
			if(this.id == 'stortingBtn'){
				$("#mainCircle").attr("style","display:block;");
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'valgBtn'){
				$("#stemmerMeny").attr("style","display:table;");
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'buttonSearch'){
				$("#divSearch").attr("style","display:block;");
				$("#bottomBtnText").attr("style","display:none;");
				$("#upperBtnText").attr("style","display:table-row;");
			}
			
			if(this.id == 'favBtn'){
				$("#divFav").attr("style","display:block;");
				$("#bottomBtnText").attr("style","display:none;");
				$("#upperBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'duellenBtn'){
				$("#divDuell").attr("style","display:block;");
				$("#bottomBtnText").attr("style","display:none;");
				$("#upperBtnText").attr("style","display:table-row;");
			}
			if(this.id == 'reprBtn'){
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				$("#divRepres").attr("style","display:block;");
			}
		}
		
	});
	$(".valgBtn").click(function() {
		$(".valgBtn").removeClass('active');
		$(this).addClass('active');
	});
	$("#mandatBlockBtn").click(function(){
		$("#mandatPartiBtn").removeClass('active');
		$("#mandatBlockBtn").addClass('active');
		$("#mandaterBlock").attr("style","display:block;");
		$("#mandaterParti").attr("style","display:none;");
	});
	$("#mandatPartiBtn").click(function() {
		$("#mandatBlockBtn").removeClass('active');
		$("#mandatPartiBtn").addClass('active');
		$("#mandaterParti").attr("style","display:block;");
		$("#mandaterBlock").attr("style","display:none;");
	});
	$("#divRepres").on("click",".repholder",function(e){
		if($(this).hasClass('activeRep')){
			$(".repholder").removeClass('activeRep');
			$(".repTT").animate({height:"0px"});
		}else{
			$(".repholder").removeClass('activeRep');
			$(this).addClass('activeRep');
			openRepBox(this.id, this);
		}
	});
	$("#divRepres").on("click",".listHeader",function(e){
		if($(this).parent().hasClass('activeRep')){
			$(".listRepContent").animate({height:"0px"});
			$(".listRep").removeClass('activeRep');
		}else{
			$(".listRepContent").attr('style','height:0px;');
			$(".listRep").removeClass('activeRep');
			$(this).parent().addClass('activeRep');
			openRepBoxList(this.parentNode.id, this);
		}
	});
	$("#divRepres").on("click",".repTT",function(e){
		$(".repholder").removeClass('activeRep');
		$(this).animate({height:"0px"});
	});
	$("#divRepres").on("click",".listRepContent",function(e){
		$(this).animate({height:"0px"});
		$(".listRep").removeClass('activeRep');
	});
	$("#blackOverlay").attr("style","display:none;");
		setTimeout(function(){window.scrollTo(0,1);},500);
		
	if (vars != '' && isValg == true) {
		var t = vars;
		if(decodeURI(t[0]).toUpperCase() == 'FYLKE'){
			$("#stemmerMeny").attr("style","display:table;");
			$("#upperBtnText").attr("style","display:none;");
			$("#bottomBtnText").attr("style","display:table-row;");
			$("#valgBtn").addClass("activeMMBtn");
			$("#buttonCounty").addClass("active");
			mainButton = 'valgBtn';
			toggleState('County',false);
			
			var rName = decodeURI(t[1]).toUpperCase();
			if(rName != undefined){
				var id = getRegionId(rName);
				if (id.length == 1)
					id = '0'+id;
				id = 'c'+id;
					selectCounty(id, rName); 
			}
			selectComponent(2);
		}else if(decodeURI(t[0]).toUpperCase() == 'KOMMUNE'){
			$("#stemmerMeny").attr("style","display:table;");
			$("#upperBtnText").attr("style","display:none;");
			$("#bottomBtnText").attr("style","display:table-row;");
			$("#valgBtn").addClass("activeMMBtn");
			$("#buttonMunicipality").addClass("active");
			mainButton = 'valgBtn';
			if (isValg == true){
				
				toggleState('Municipality',false);
				$('#topmenuButtonValg').addClass('activeMMBtn');
				var rFylke = decodeURI(t[1]).toLowerCase();
				rFylke = rFylke.replace(/_/g," "); 
				var rFylkeID = getRegionId(rFylke);
				var rName = decodeURI(t[2]).toLowerCase();
				rName = rName.replace(/_/g," "); 
				var regionsArray = subRegions.split('|');
				
				
				for (var i = 0; i < regionsArray.length;i++) {
					var region = regionsArray[i];
					var regionElements = region.split(';');
					var regionArray = regionElements[2].split('_');
					var searchFylke = regionArray[0];
					if (rFylkeID == 03) rFylkeID = '3_1';
					if (rFylkeID.split('_').length == 2){ searchFylke = searchFylke +'_'+regionArray[1];}
					
					if (regionElements.length == 3 && (rFylkeID == searchFylke ) && (regionElements[0].toLowerCase()== rName)){
						//match
						
						var regionType = region.charAt(region.indexOf(';') + 1);
						var regionIdRaw = region.substring(region.lastIndexOf(';') + 1);
						if (regionType == 2 || regionType == 3) {
							var regionIdElements = regionIdRaw.split('_');						
					
							if (regionIdElements.length == 2){
								var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];
						
								//if (countyId == '03') countyId = '00';
								
								//toggleState('Municipality',false);
								regionName = regionElements[0]
								selectMunicipality(regionElements[2]);//selectComponent(2);
						
							}else if(regionIdElements.length == 3){
								regionName = regionElements[0]
								
								selectMunicipality(regionElements[2]);selectComponent(2);
							}
							
			
						}
						
					}
				}	
				
			}
		}else {
			$("#stemmerMeny").attr("style","display:table;");
			$("#upperBtnText").attr("style","display:none;");
			$("#bottomBtnText").attr("style","display:table-row;");
			$("#valgBtn").addClass("activeMMBtn");
			$("#buttonNation").addClass("active");
			mainButton = 'valgBtn';
				toggleState('Nation',false);selectComponent(2);
			}	
	}
}
function checkDate() {
	if (idag > valgStart) {
		isValg = true;
		$('#valgBtnText').text('RESULTAT');
			$('#valgBtn').removeClass('notValg');
			$('#buttonNation').attr('onclick', 'toggleState("Nation")');
			$('#buttonConty').attr('onclick', 'toggleState("County")');
			$('#buttonMunicipality').attr('style', '');
	}else{
		isValg = false;
		
			$('#valgBtnText').text('MÅLING');
			$('#valgBtn').addClass('notValg');
			$('#buttonNation').attr('onclick', 'toggleState("nationPolls")');
			$('#buttonConty').attr('onclick', 'toggleState("countyPolls")');
			$('#buttonMunicipality').attr('style', 'display:none');
		
	}
	if (idag > infoSwitch) {
		itsNine = true;
	}else {
		itsNine = false;
	}
}
function noResltsInfo() {
	$("#noResltInfo").html('');
	var infoHTML = '';
	if (itsNine == true)
		infoHTML = '<div class="closeButton" onclick="closeNoResltsInfo();"></div>Hvis du ikke finner resultater fra din kommune er det fordi kommunen ennå ikke har innrapportert tallene til SSB.';
	else 
		infoHTML = '<div class="closeButton" onclick="closeNoResltsInfo();"></div><span class="infoHeader">INGEN RESULTATER FØR KL 21:00</span><br /><br />Den første landsoversikten offentliggjøres kl. 21:00.<br />Opptellingsresultatene vil bli rapportert fortløpende.<br /> Hvis du ikke finner resultater fra din kommune er det fordi kommunen ennå ikke har innrapportert tallene til SSB.';
	
	$("#noResltInfo").html(infoHTML);
	$("#noResltInfo").attr('style','');
}
function closeNoResltsInfo() {
	$("#noResltInfo").attr('style','display:none;');
}
function loadRepres(){
	var myTempRep = {};
	var innerHTML= '';
	$.getJSON('../repData/candidates-min.json',function(data){
		allRepres = data;

		$("#blackOverlay").attr("style","display:none;");
		setTimeout(function(){window.scrollTo(0,1);},500);
		//return data;
	});
	}
function openRepBox(repId,athis) {
	
	
	var innerHTML = '';
	var obj = athis;
	var myrepid = repId;
	$(".repTT").attr('style','');
	var newObj = $('#'+repId).closest('div').nextAll('.repTT')[0];
	while ( repId.charAt(0) === 'F')
		repId = repId.substr(1);
	
	if ($(newObj).hasClass('TTsmall')){
		var container = $('div#repWrapper');
	}else {
		var container = $('div#repWrapper');
	}
	container.scrollTop(0);
	//$('.'+$(newObj).attr('class')  +' .newLogoBox').removeClass('isNewImage');
	//$('.'+$(newObj).attr('class')  +' .herrausforderer').html(' ');
	$.getJSON('../repData/'+repId+'_metainfo.json', function(data){
		innerHTML += "<div class='newlogoBox'>"; // DIV OPEN
		
		if (isValg  ){	 // valgmode
		
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							innerHTML += "<img src='../images/rep/usikker.png' />";
						}
						if ( data.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
							//extraMHTML += "<img src='images/rep/uManBig.png' />";
						}	
					}
				}
			}
		}
		
		if(data.ParliamentPeriods == ''){
			innerHTML += "<img class='isNewImage' src='../images/rep/isNew.png' />";
			//$('.'+$(newObj).attr('class')  +' .newlogoBox').addClass('isNewImage');
		}
		innerHTML += "</div>"; //CLOSE OPEN DIV
		innerHTML += "<div class='repBox'>";
		innerHTML += "<div class='repHeadline'>";
		innerHTML += "<div class='repPartiLogo partiLogo1'><img src='../images/logo/parti"+data.PartiOrganizationId +".png' /></div>"
		innerHTML += "<div class='repName'>"+data.FirstName.toUpperCase() + " " + data.LastName.toUpperCase() +"<br/>" +getPartyName(data.PartiOrganizationId) +" I " + data.CountyName +"</div>";
		innerHTML += "</div>";
		innerHTML += "<div class='repContent'>";
		
		/*if ($(newObj).hasClass('TTsmall')){
			innerHTML += "<div class='repBild'><img style='width:71px;' src='../images/rep/big/"+((data.Picute != null) ? data.Picture : 'dummy_big.jpg' )+"' /></div>"; // TODO ADD REAL BILDE
		}else {}*/
		
		innerHTML += "<div class='repInfo'>";
		if (data.DateOfBirth != ''){
			var birthDate = data.DateOfBirth.split('T');
			birthDate = birthDate[0].split('-');
			innerHTML += '<div class="lineWrapper"><div class="repLeft">FØDT:</div><div class="repRight"> '+ birthDate[2] + '.'+ birthDate[1] +'.'+ birthDate[0] +'</div></div>';
			//innerHTML += '<div class="lineWrapper"><div class="repLeft">FØDT:</div><div class="repRight"> '+ birthDate +'</div></div>';
		}
		if (data.Occupation != ''){
			innerHTML += '<div class="lineWrapper"> <div class="repLeft">STILLING:</div><div class="repRight"> '+ data.Occupation +'</div> </div>';
		}
		if (data.Municipality != ''){
			innerHTML += '<div class="lineWrapper"><div class="repLeft">BOSTED:</div><div class="repRight"> '+ data.Municipality +'</div></div>';
		}
		innerHTML += "<br/>";
		if (data.Education != ''){
			innerHTML += '<div class="blockWrapper"><div class="repFL"><span class="bold">Utdanning/Yrkesbakgrunn:</span> '+ data.Education +'</div>';
		}
		if (data.Description != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.Description +'</div>';
		}
		if (data.SocialConnections != '' && data.SocialConnections != null && data.SocialConnections != 'null'){
			/*for (var i=0; i < data.SocialConnections.length;i++){
				innerHTML += '<div class="repFL"><span class="bold">'+data.SocialConnections[i].Name+': </span> '+ data.SocialConnections[i].UserName +'</div>';
			}*/
			for (var i=0; i < data.SocialConnections.length;i++){
				//
				if (data.SocialConnections[i].Name.toUpperCase() == 'TWITTER') {
					if ( data.SocialConnections[i].UserName.charAt(0) === '@')
						var userName = data.SocialConnections[i].UserName.substr(1);
					
					innerHTML += '<br /><a href="https://twitter.com/'+ userName +'" class="twitter-follow-button" data-show-count="false" target="_blank" >Følg @'+ userName +'</a>';
				//	<a href="https://twitter.com/twitter" class="twitter-follow-button" data-show-count="false">Follow @twitter</a>
					

				}else {
					innerHTML += '<div class="repFL"><span class="bold">'+data.SocialConnections[i].Name+': </span> '+ data.SocialConnections[i].UserName +'</div>';
				}
				
				//data.SocialConnections[i].Name
			}
		}
		/*if (data.ParliamentPeriods != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.ParliamentPeriods +'</div>';
		}*/
		/*if (data.Email != ''){
			innerHTML += '<div class="repFL"><span class="bold">E-Post:</span> '+ data.Email +'</div>';
		}*/
		innerHTML += "</div>";
		innerHTML += "</div>";
		innerHTML += "</div>";
		
		if (isValg  ){	 // valgmode
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							for (var l = 0; l < herausforderer.length; l++) {
								
									if ( (herausforderer[l].CountyId== 301 ? 3 :herausforderer[l].CountyId)== data.CountyId) {
									innerHTML += "<div class='herrausforderer'>";
									innerHTML += "<div class='herrausfordererleft'>";
									innerHTML += "<img class='herrausfordererimg' src='../images/rep/gruenKleinerAls.png' />";
									innerHTML += "<div class='herrausforderertext'>Mangler "+countySpecialM[i].vinneStemmer +
									" stemmer</div>";
									innerHTML += "</div>";
									innerHTML += "<div class='herrausfordererright'>";
									innerHTML += "<div class='herrausfordererrighttext'>UTFORDRER</div>";
									innerHTML += "<img class='herrausfordererrightimg' style='width:71px;' src='../images/rep/big/"+herausforderer[l].Picture+"_org200.png' />";
									innerHTML += "</div>";
									innerHTML += "<div class='herrausfordererbottom'>";
									innerHTML += "<img class='herrausfordererpartyimg' src='../images/logo/parti"+herausforderer[l].PartiOrganizationId+".png' />";
									innerHTML += "<div class='herrausfordererbottomtext'>"+herausforderer[l].FirstName+", "+herausforderer[l].LastName+"</div>";
									//<div>"+herausforderer[l].FirstName+", "+herausforderer[l].LastName+", "+getPartyName(herausforderer[l].PartiOrganizationId)+", "+countySpecialM[i].vinneStemmer+"</div>
									innerHTML += "</div>";
									innerHTML += "</div>";
										
									}
								}
							
						}
					}
				}
			}
		}
		//innerHTML += "<div class='herrausforderer'></div>";
		innerHTML += "</div>";
		innerHTML += "</div>";
	
			
			$(newObj).html(innerHTML);
			$(newObj).attr('style','display:block;');
			twttr.widgets.load();
	
	
	if (isValg == false)
				$(newObj).animate({height:"320px"},"fast",function(){
					setTimeout(function(){scrollToButton(document.getElementById(myrepid))},200);
				});
			else if ( isValg == true && $(newObj).children().hasClass('herrausforderer'))
				$(newObj).animate({height:"500px"},"fast",function(){
					setTimeout(function(){scrollToButton(document.getElementById(myrepid))},200);
				});
			else
				$(newObj).animate({height:"320px"},"fast",function(){
					setTimeout(function(){scrollToButton(document.getElementById(myrepid))},200);
				});
	
	
	//}
	})
	.done(function(){})
	.fail(function(){})
	.always(function(){});

}
function openRepBoxList(repIds, athis){
	var innerHTML = '';
	var obj = athis;
	$(".repTT").attr('style','');
	var repId = repIds.slice(4);
	var newObj = $('#'+repIds+ ' .listRepContent');
	if ($(newObj).hasClass('TTsmall')){
		var container = $('div#repListWrapper');
	}else {
		var container = $('div#repListWrapper');
	}
	container.scrollTop(0);

	$.getJSON('../repData/'+repId+'_metainfo.json', function(data){
		innerHTML += "<div class='newlogoBox'>"; // DIV OPEN
		
		if (isValg  ){	 // valgmode
		
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							innerHTML += "<img src='../images/rep/usikker.png' />";
						}
						if ( data.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
							//extraMHTML += "<img src='images/rep/uManBig.png' />";
						}	
					}
				}
			}
		}
		if(data.ParliamentPeriods == ''){
			innerHTML += "<img class='isNewImage' src='../images/rep/isNew.png' />";
			//$('.'+$(newObj).attr('class')  +' .newlogoBox').addClass('isNewImage');
		}
		innerHTML += "</div>"; //CLOSE OPEN DIV
		innerHTML += "<div class='repBox'>";
		innerHTML += "<div class='repHeadline'>";
		innerHTML += "<div class='repPartiLogo partiLogo1'><img src='../images/logo/parti"+data.PartiOrganizationId +".png' /></div>"
		innerHTML += "<div class='repName'>"+data.FirstName.toUpperCase() + " " + data.LastName.toUpperCase() +"<br/>" +getPartyName(data.PartiOrganizationId) +" I " + data.CountyName +"</div>";
		innerHTML += "</div>";
		innerHTML += "<div class='repContent'>";
		
		//if ($(newObj).hasClass('TTsmall')){
			var myURL = "../images/rep/big/"+(data.Picture != '' ? (data.Picture+'_org200.png') : 'dummy.jpg' );
			innerHTML += "<div class='repBild'><img style='width:71px;' src='"+myURL+"' /></div>"; // TODO ADD REAL BILDE
		//}else {}
		
		innerHTML += "<div class='repInfo'>";
		if (data.DateOfBirth != ''){
			var birthDate = data.DateOfBirth.split('T');
			birthDate = birthDate[0].split('-');
			innerHTML += '<div class="lineWrapper"><div class="repLeft">FØDT:</div><div class="repRight"> '+ birthDate[2] + '.'+ birthDate[1] +'.'+ birthDate[0] +'</div></div>';
		}
		if (data.Occupation != ''){
			innerHTML += '<div class="lineWrapper"> <div class="repLeft">STILLING:</div><div class="repRight"> '+ data.Occupation +'</div> </div>';
		}
		if (data.Municipality != ''){
			innerHTML += '<div class="lineWrapper"><div class="repLeft">BOSTED:</div><div class="repRight"> '+ data.Municipality +'</div></div>';
		}
		innerHTML += "<br/>";
		if (data.Education != ''){
			innerHTML += '<div class="blockWrapper"><div class="repFL"><span class="bold">Utdanning/Yrkesbakgrunn:</span> '+ data.Education +'</div>';
		}
		if (data.Description != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.Description +'</div>';
		}
		if (data.SocialConnections != '' && data.SocialConnections != null && data.SocialConnections != 'null'){
			/*for (var i=0; i < data.SocialConnections.length;i++){
				innerHTML += '<div class="repFL"><span class="bold">'+data.SocialConnections[i].Name+': </span> '+ data.SocialConnections[i].UserName +'</div>';
			}*/
			for (var i=0; i < data.SocialConnections.length;i++){
				//
				if (data.SocialConnections[i].Name.toUpperCase() == 'TWITTER') {
					if ( data.SocialConnections[i].UserName.charAt(0) === '@')
						var userName = data.SocialConnections[i].UserName.substr(1);
					
					innerHTML += '<br /><a href="https://twitter.com/'+ userName +'" class="twitter-follow-button" data-show-count="false" target="_blank" >Følg @'+ userName +'</a>';
				//	<a href="https://twitter.com/twitter" class="twitter-follow-button" data-show-count="false">Follow @twitter</a>
					

				}else {
					innerHTML += '<div class="repFL"><span class="bold">'+data.SocialConnections[i].Name+': </span> '+ data.SocialConnections[i].UserName +'</div>';
				}
				
				//data.SocialConnections[i].Name
			}
		}
		/*if (data.ParliamentPeriods != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.ParliamentPeriods +'</div>';
		}*/
		/*if (data.Email != ''){
			innerHTML += '<div class="repFL"><span class="bold">E-Post:</span> '+ data.Email +'</div>';
		}*/
		innerHTML += "</div>";
		innerHTML += "</div>";
		innerHTML += "</div>";
		
		
		if (isValg  ){	 // valgmode
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							for (var l = 0; l < herausforderer.length; l++) {
								
									if ((herausforderer[l].CountyId== 301 ? 3 :herausforderer[l].CountyId) == data.CountyId) {
									innerHTML += "<div class='herrausforderer'>";
									innerHTML += "<div class='herrausfordererleft'>";
									innerHTML += "<img class='herrausfordererimg' src='../images/rep/gruenKleinerAls.png' />";
									innerHTML += "<div class='herrausforderertext'>Mangler "+countySpecialM[i].vinneStemmer +
									" stemmer</div>";
									innerHTML += "</div>";
									innerHTML += "<div class='herrausfordererright'>";
									innerHTML += "<div class='herrausfordererrighttext'>UTFORDRER</div>";
									innerHTML += "<img class='herrausfordererrightimg' style='width:71px;' src='../images/rep/big/"+herausforderer[l].Picture+"_org200.png' />";
									innerHTML += "</div>";
									innerHTML += "<div class='herrausfordererbottom'>";
									innerHTML += "<img class='herrausfordererpartyimg' src='../images/logo/parti"+herausforderer[l].PartiOrganizationId+".png' />";
									innerHTML += "<div class='herrausfordererbottomtext'>"+herausforderer[l].FirstName+", "+herausforderer[l].LastName+"</div>";
									//<div>"+herausforderer[l].FirstName+", "+herausforderer[l].LastName+", "+getPartyName(herausforderer[l].PartiOrganizationId)+", "+countySpecialM[i].vinneStemmer+"</div>
									innerHTML += "</div>";
									innerHTML += "</div>";
										
									}
								}
							
						}
					}
				}
			}
		}
		
		innerHTML += "</div>";
		innerHTML += "</div>";
	
			
			$(newObj).html(innerHTML);
			$(newObj).attr('style','display:block;');
			twttr.widgets.load();
			
			if (isValg == false)
				$(newObj).animate({height:"400px"},"fast",function(){
					setTimeout(function(){scrollToButton(document.getElementById(repIds))},200);
				});
			else if ( isValg == true && $(newObj).children().hasClass('herrausforderer'))
				$(newObj).animate({height:"500px"},"fast",function(){
					setTimeout(function(){scrollToButton(document.getElementById(repIds))},200);
				});
			else
				$(newObj).animate({height:"400px"},"fast",function(){
					setTimeout(function(){scrollToButton(document.getElementById(repIds))},200);
				});

	})
	.done(function(){})
	.fail(function(){})
	.always(function(){});
}
function toggleState(stateId, trueorfalse,polltext){
	idag = new Date();
	mySelectedPoll = 0;
	mySelectedPollHL = '';
	mySelectedPollID = '';
	checkDate();
	if(polltext){	
		pollText = polltext;
		
	}else {
		pollText = '';
	}
		if (stateId == 'Mandater' || stateId == 'Duell'  ) {
		
			state = stateId;
		}else {
			removeCloseButtons();    
	    
			if (state != stateId)
	       // setCloseButton(stateId); BIANCA
		
			if (state == stateId)
				state = undefined;
			else
				state = stateId; /* 	Possible states: Nation, County, Municipality, Search, My */

			//var menuDiv = document.getElementById('stemmerMeny');
			//selectComponent( ((componentIdMunicipality)? componentIdMunicipality : 2) );
			var div = document.getElementById('div' + ((stateId == 'Polls' || stateId == 'nationPolls') ? 'Nation' : stateId));
			$('.contentDiv').attr('style','display:none');
			if (div)
			{
				if (div.style.display == 'block')
					div.style.display = 'none';
				else
					div.style.display = 'block';						
			}
			//menuDiv.style.display = 'none';
		}
		if (stateId == 'Nation') {
		    region = 'n';
			regionName = 'LANDSOVERSIKT';
			$('#buttonBarNation').attr('style','display:block;');
		    selectComponent(1); //select majority before refresh
		}
		else if (stateId == 'nationPolls'){
			state = 'Polls';
			selectNationPolls('true');
			$('#buttonBarNation').attr('style','display:block;');
		}
		else if (stateId == 'countyPolls'){
			
		}
		else if (state == 'Municipality') {
		$("#buttonBarMunicipality").attr('style','');
		$(".contentHeadline").html("VELG FYLKE");
		$('ul.municipalitySelect').empty();
		document.getElementById('divCountyMunicipalitySelect').style.display='block';
			if (mainButton == 'buttonSearch'){
				mainButton = 'valgBtn';
				$("#buttonSearch").removeClass('activeMMBtn');
				$("#divSearch").attr("style","display:none;");
				$("#valgBtn").addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
		    /*if (cookies_are_enabled())
		        //parseFavourites();	*/	    
		}
		else if (state == 'County') {
		$("#buttonBarCounty").attr('style','');
		$(".contentHeadline").html("VELG FYLKE");
		document.getElementById('divCountySelect').style.display='block';
			if (mainButton == 'buttonSearch'){
				mainButton = 'valgBtn';
				$("#buttonSearch").removeClass('activeMMBtn');
				$("#divSearch").attr("style","display:none;");
				$("#valgBtn").addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
			}
		    /*if (cookies_are_enabled())
		        //parseFavourites();	*/	    
		}
		else if (state == 'Search'){
			region = 'n';
			reloadContent();
			$("#divSearch .contentHeadline").html("SØK");
		    resetSearch();
		}else if (state == 'My') {
		    setLocation();
		    if (cookies_are_enabled())
		        parseFavourites();		    
		}
		else if (stateId == 'Mandater') {
			region = 'n';
			regionName = 'LANDSOVERSIKT';
			selectComponent(9);
		}
		else if (stateId == 'Duell') {
			region = 'n';
			regionName = 'LANDSOVERSIKT';
		    selectComponent(8); //select majority before refresh
		}
	    else if (stateId == 'Fav') {
		// scroll down to buttondiv
			//selectComponent(10);
			parseFavourites();
			//<div id="addToFav" onclick="addToFavourites();">add</div>
		}else if (stateId == 'Repres') {
			 region = 'n';
			selectComponent(10);
		}
		if (stateId == 'Mandater'){
			
			setTimeout(function () {scrollToButton(document.getElementById('mainCircle'))}, 100);;
		}else {
			setTimeout(function () {scrollToButton(document.getElementById('div' + state ))}, 100);;
		}
		//scrollToButton(document.getElementById( 'bottomMainMeny'));
	}
	
	function removeCloseButtons() {

	    var btnChild1 = document.getElementById('buttonMy');
	    if (btnChild1) {
	        var div1 = document.getElementById('closeDivMy');
	        if (div1) btnChild1.removeChild(div1);
	    }

	    var btnChild2 = document.getElementById('buttonNation');
	    if (btnChild2) {
	        var div2 = document.getElementById('closeDivNation');
	        if (div2) btnChild2.removeChild(div2);
	    }

	    var btnChild3 = document.getElementById('buttonCounty');
	    if (btnChild3) {
	        var div3 = document.getElementById('closeDivCounty');
	        if (div3) btnChild3.removeChild(div3);
	    }

	    var btnChild4 = document.getElementById('buttonMunicipality');
	    if (btnChild4) {
	        var div4 = document.getElementById('closeDivMunicipality');
	        if (div4) btnChild4.removeChild(div4);
	    }
	    
	    var btnChild5 = document.getElementById('buttonSearch');
	    if (btnChild5) {
	        var div5 = document.getElementById('closeDivSearch');
	        if (div5) btnChild5.removeChild(div5);
	    }
	    
	}

	function setCloseButton(state) {
	    var button = document.getElementById('button' + state);
		if (button == null) {
			
		}else {
	    //                <div style="float:right;padding-top:9px;padding-right:5px;"><img src="../images/kryss_480.png" /></div> 

	    var child = document.createElement('div');
	    child.id = "closeDiv" + state;
	    child.style.float = "right";
	    child.style.paddingTop = ((window.innerWidth > 320) ? '9' : '6') + "px";
	    child.style.paddingRight = "5px";
	    
	    var imgchild = document.createElement('img');
	    imgchild.src = "../images/kryss_" + ((window.innerWidth > 320) ? '480' : '320') + ".png";

	    child.appendChild(imgchild);
	    button.appendChild(child);
		}
	}
	
	function scrollToButton(obj) {		
	    if (obj) {
	        var y = obj.offsetTop;
			
	        while (obj = obj.offsetParent){
	            y += obj.offsetTop;
				
			}
			
	        setTimeout(function () { window.scrollTo(0, y); }, 200);	    
			
	    }
	}

	function resetSearch(){
		var searchBox = document.getElementById('searchBox');
		if (searchBox){
			searchBox.value = '';
			
			var searchResults= document.getElementById('searchResults');
			if (searchResults)
				searchResults.innerHTML = '';
			
			searchBox.focus();    	
		}	
	}
	
	function doSearch(){
		var searchResults= document.getElementById('searchResults');
		if (searchResults){
			searchResults.innerHTML = '';
			
			var searchBox = document.getElementById('searchBox');
			if (searchBox){				
				//everything is ok, perform search
				searchName(searchBox.value);
			}
		}	
	}
	
	
	function searchName(text) {
 		
 		var innerHTML = '';
 		var results = '';
 		var criteria = text.toLowerCase();
		var hitcount = 0;
		var regionsArray = subRegions.split('|');
		//var repsArray = countyLeaders; //TODO temporary dummy from 2011
		var counties = loadCounties(); 		
		if (criteria == ''){
			innerHTML += "<h3>Du må angi minst et søkeord.</h3><br/>";
  			innerHTML += 'Du kan søke på fylkesnavn, kommunenavn og kretsnavn i Oslo, Bergen, Stavanger og Trondheim. <br/>';
		}
		else{	
			innerHTML += "<div id='searchResSted'><div id='stedHL'>STED</div>";
			if ( isValg == false) {
				innerHTML += "<div class='noData'>Ingen data på kommunenivå tilgjengelig før opptellingen begynner 9. september</div>";
			}
			for (var i = 0; i < regionsArray.length; i++){			
  				//format: Name;type;id //Aust-Agder;4;9 //Fredrikstad;2;1_6 //Hersleb skole;3;3_1_101 //Rosenborg;3;16_1_6
  				var region = regionsArray[i];
  				var regionElements = region.split(';');
  			
				if (regionElements.length == 3 && (regionElements[0].toLowerCase().indexOf(criteria) > -1)){
			
					var regionType = region.charAt(region.indexOf(';') + 1)
					var regionIdRaw = region.substring(region.lastIndexOf(';') + 1);				
					var url = '';
			
					if (regionType == 2 || regionType == 3){
						//regionIdRaw = 12_65	or 1_6
						var regionIdElements = regionIdRaw.split('_');						
						
						if (regionIdElements.length == 2){
							if (regionIdElements[0] == '3' && isValg == false){ //OSLO
								var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];
								url = "toggleState('County');selectCounty('c" + countyId + "','OSLO');";
							}else {
							var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];
							if (isValg == false)
								url = "";
							else
								url = "toggleState('Municipality');refreshSubRegions('c" + countyId + "');selectMunicipality('" + regionIdRaw + "');";	
							}		
						}
					}
					else if (regionType == 4){												
						//regionIdRaw = 1 or 12
						var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;					
						url = "toggleState('County');selectCounty('c" + countyId + "');";
					}
					
  					results += "<div onclick=\"" + url + "\" class='searchResult'>" + regionElements[0];
  					
					/* switch regiontype
					4 = county
					2 = municipality
					3 = district
					*/	
					switch(regionType) {
	 					case('2'): //municipality
	 					{
	 						var countyId = region.substring(region.indexOf(';') + 3, region.indexOf("_"))
 	 						if (countyId == 3){ //only Oslo
	 							results += ' fylke/kommune';
	 						} 
	 						else{
	 							results += ' kommune i ' + counties[countyId];
	 						}
     						break;
     					}
     					case ('3'): //district
     					{
     						//switch municipalityid
     						switch(region.substring(region.indexOf(';') + 3, region.indexOf("_"))){
     							case ('3'):
     								results += ' (krets i Oslo)';
     								break;
     							case ('11'):
     								results += ' (krets i Stavanger)';
     								break;
     							case ('12'):
     								results += ' (krets i Bergen)';
     								break;
     							case ('16'):
     								results += ' (krets i Trondheim)';
     								break; 
     						} 
     						break;
     					}
     					case ('4'): //county
     					{
     						results +=' fylke'
     						break;
     					}
 					}   
		  				
  					results += "</div>";				  				
  					hitcount++;
  				}
 			}
			/**** representanter search ****/
			
 		
 			//innerHTML += '<h3>' + hitcount + ' treff på søk etter "' + text + '":</h3>';

 			//any hits?
 			if (hitcount == 0){
 				innerHTML += 'ingen treff';
  				//innerHTML += 'Du kan søke på fylkesnavn, kommunenavn og kretsnavn i Oslo, Bergen, Stavanger og Trondheim. <br/>';
 			}
 			else if (hitcount == 1){
 				//navigate directly to region?
 				innerHTML += results;
				innerHTML += "</div>";
 			}
 			else{
 				innerHTML += results;
				innerHTML +="</div>";
 			}
		
		}
		innerHTML += "<div id='searchResRep'><div id='repHL'>PERSON</div>";
	//TODO Results for PERSON
	inRepres.sort(sortByParti);
	var repHitCount = 0;
	$.each(inRepres, function(){
		var lastnOne = '';
		var lastnTwo = '';
		var lastfOne = '';
		var lastfTwo = '';
		if (this.FirstName && this.LastName){
			var firstandlast = this.FirstName.toLowerCase() + " " + this.LastName.toLowerCase();
			if (this.LastName.indexOf(" ") != -1) {
				lastnOne = this.LastName.split( " " )[0];
				lastnTwo = this.LastName.split( " " )[1];
			}
			if (this.FirstName.indexOf(" ") != -1) {
				lastfOne = this.FirstName.split( " " )[0];
				lastfTwo = this.FirstName.split( " " )[1];
			}
			if(this.FirstName.toLowerCase().match('^'+criteria) || this.LastName.toLowerCase().match('^'+criteria) || firstandlast.match('^'+criteria) || (lastnOne.toLowerCase().match('^'+criteria) || lastnTwo.toLowerCase().match('^'+criteria) )   ||    (lastfOne.toLowerCase().match('^'+criteria) || lastfTwo.toLowerCase().match('^'+criteria) )) {
				innerHTML += "<div class='searchResult' onclick='openRepresentanter("+this.CandidateId+","+this.PartiOrganizationId+");'>"+ this.FirstName+" "+ this.LastName+" " +this.PoliticalPartyCode+" "+getRegionName(this.CountyId)+"</div>";
				repHitCount++;
			}
		}
	});
	if (repHitCount == 0){

			innerHTML += "Ingen treff i det nye Stortinget";
 		}
	innerHTML += "</div>";		
		document.getElementById("searchResults").innerHTML = innerHTML; 
 
		return false;
	}
function openRepresentanter(id, party) {
	searchRep = id;
	searchRepParty = party;
	toggleState('Repres',false);
}
	function loadCounties() {
 		//Parser fylkesnavn
 		var countyId = 1;
 		var counties = '&Oslash;stfold|Akershus|Oslo|Hedmark|Oppland|Buskerud|Vestfold|Telemark|Aust-Agder|Vest-Agder|Rogaland|Hordaland|Undef|Sogn og Fjordane|M&oslash;re og Romsdal|S&oslash;r-Tr&oslash;ndelag|Nord-Tr&oslash;ndelag|Nordland|Troms|Finnmark';
 		var countyArray = [];
 
 		while (counties.indexOf("|") > -1){
  			var pos = counties.indexOf("|");
  			countyArray[countyId] = counties.substring(0, pos);
  			counties = counties.substring(pos + 1, counties.length);
  			countyId++;
  		}
  		
  		return countyArray;
	}
	
	function closeAllOtherMenuItems(id){
		var divNation = document.getElementById('divNation');
		if (divNation && (divNation.id != id))
			divNation.style.display = 'none';
		
		var divCounty = document.getElementById('divCounty');
		if (divCounty && (divCounty.id != id))
			divCounty.style.display = 'none';
		if (divCounty && (divCounty.id == id))
			divCounty.style.display = 'block';

		var divMunicipality = document.getElementById('divMunicipality');
		if (divMunicipality && (divMunicipality.id != id))
			divMunicipality.style.display = 'none';
		if (divMunicipality && (divMunicipality.id == id))
			divMunicipality.style.display = 'block';

		var divSearch = document.getElementById('divSearch');
		if (divSearch && (divSearch.id != id))
		    divSearch.style.display = 'none';

		var divMy = document.getElementById('divMy');
		if (divMy && (divMy.id != id))
		    divMy.style.display = 'none';
			
		var divFav = document.getElementById('divFav');
		if (divFav && (divFav.id != id))
		    divFav.style.display = 'none';
	}

function selectCounty(value, countyName, fillMuniDropDown,fillMuniId){
	region = value;
		
	countyName = countyName;
		
		//if called from searchresult
	if(fillMuniDropDown){
			//fillMunicipalityDropDown(region, fillMuniId);
			if (state == 'County'){
				//if (document.getElementById('countySelect').value != value)
			//document.getElementById('countySelect').value = value;
			countyElectionMode = true; //BIANCA ADD ountyelectionmodestuff
			region = 'c' +region;
			document.getElementById('divCountySelect').style.display='none';
			selectComponent(1);
			}else{
			
			selectMunicipality(region);
			}
	}else {
			//if (document.getElementById('countySelect').value != value)
			//document.getElementById('countySelect').value = value;
			countyElectionMode = true; //BIANCA ADD ountyelectionmodestuff
			document.getElementById('divCountySelect').style.display='none';
			selectComponent(1);
			$("#buttonBarCounty").attr('style','display:block;');
		}
		
	}
	
function refreshSubRegions(value, obj){
	var selectedCountyId = value.substring(1,3);
	var regions = subRegions.split('|');
	$(".contentHeadline").html("VELG KOMMUNE:");
	$('ul.municipalitySelect').empty();
	var regionsDiv = $(obj).next('.municipalitySelect');
	//if called from searchresult
	if (document.getElementById('countyMunicipalitySelect').value != value)
		document.getElementById('countyMunicipalitySelect').value = value;		
		
	if (regionsDiv){
		var addedSep = false;
		for(var i=0;i < regions.length;i++){
			if (value == 'c12m1201' || value == 'c16m1601' || value == 'c11m1103' ) {
				if (value == 'c12m1201' ) selectedCountyId = '12_1';
				if (value == 'c16m1601' ) selectedCountyId = '16_1';
				if (value == 'c11m1103' ) selectedCountyId = '11_3';
				var region = regions[i].split(';');
				if (region.length == 3){
					var municipalityId = region[2];
					var regIds = municipalityId.split('_');				
					var type = region[1];
					if ( (regIds.length == 3 && regIds[0]+"_"+regIds[1] == selectedCountyId)|| municipalityId == selectedCountyId ) {
						if (type == 3 & !addedSep) {
							//innerHTML = innerHTML.substring(0, innerHTML.length - 2);
							//innerHTML += "<br/>";
							addedSep = true;
						}
						
						var elOptNew = document.createElement('li');
							elOptNew.innerHTML = "- " +region[0].toUpperCase();
							elOptNew.setAttribute('onclick','selectMunicipality("'+region[2]+'");');
						try {
				      		//regionsDiv.add(elOptNew); // standards compliant; doesn't work in IE
							regionsDiv.append(elOptNew);
				    	}
				    	catch(ex) {
				      		//elSel.add(elOptNew, elSel.selectedIndex); // IE only
				    	}
						//innerHTML += "<div style='font-size:8pt;' class='regionSelectLink boxLink' onclick=\"selectMunicipality('" + region[2] + "','" + region[0].toUpperCase() + "');\">" + region[0].toUpperCase() + "</div>, ";
					}
				}
			}else {
				var region = regions[i].split(';');
				if (region.length == 3){
					var countyId = region[2].substring(0,2);					
					var type = region[1];
					
					//remove '_' and add leading zero
					if (countyId.substring(1) == '_')
						countyId = '0' + countyId.substring(0,1);			
								
					if (countyId == selectedCountyId && (type != 4)){									
						//var elOptNew = document.createElement('option');
						if (region[0] != 'Oslo' ) {
						
							
							if(type == 3 && (selectedCountyId == 3 || selectedCountyId == '03') ){
								var elOptNew = document.createElement('li');
								elOptNew.innerHTML = "- " +region[0].toUpperCase();
								elOptNew.setAttribute('onclick','selectMunicipality("'+region[2]+'");');
							}else if (type != 3){
								var elOptNew = document.createElement('li');
								 elOptNew.innerHTML = region[0].toUpperCase();
								 elOptNew.setAttribute('onclick','selectMunicipality("'+region[2]+'");');
							}
						
				    	//elOptNew.value = region[2];
				    	try {
				      		//regionsDiv.add(elOptNew); // standards compliant; doesn't work in IE
							regionsDiv.append(elOptNew);
				    	}
				    	catch(ex) {
				      		//elSel.add(elOptNew, elSel.selectedIndex); // IE only
				    	}
						}						
					}				
				}		
			}
		}
	}
}
	
	function selectMunicipality(value){
		// value = 1_5  
		var idElements = value.split('_');
		//if called from searchresult
		if (document.getElementById('municipalitySelect').value != value)
			document.getElementById('municipalitySelect').value = value;		
		
		$('#divCountyMunicipalitySelect').attr('style','display:none;');
		if (idElements.length >= 2){
			var countyId = idElements[0];
			var municipalityId = idElements[1];
			var id;

			if (countyId.length == 1) {
			    countyId = '0' + countyId;
			}
		
			if (municipalityId.length == 1)
				municipalityId = '0' + municipalityId;
		
			id = countyId + municipalityId;

			if (countyId == '03' && idElements.length > 2)
			    countyId = '00';
		    
			id = 'c' + countyId + 'm' + id;

			var districtId = '';
		    if (idElements.length > 2) { //district
		        districtId = idElements[2];
				if (countyId != '00') {
		        for (var i = districtId.length; i < 4; i++)
		            districtId = "0" + districtId;
				}
		    }

		    region = id + ((districtId == '') ? '' : 'd' + districtId);
			
			//regionName = municipalityName;
			countyElectionMode = false;
			
			selectComponent( ((componentIdMunicipality)? componentIdMunicipality : 2) );	
				$("#buttonBarMunicipality").attr('style','display:block;');
		}		
	}
	
	function selectComponent(id){

	if (id != 9 && id != 8 && id != 10){ 
		if ((id == 2 || id == 1) && (state == 'Municipality' || state == 'My')) {
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' + state).className.replace( /(?:^|\s)active(?!\S)/g , '' );
			document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + state).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		
		}else {
		document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' +(state == 'Polls' ? 'Nation' : state)).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className.replace( /(?:^|\s)active(?!\S)/g , '' );
		}
	}
	    if (id == 1) {
		if (state == 'Municipality' || state == 'My') {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
			
		//        document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmerbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";
		//        document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/mandaterbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		    }
		    else {
		//    document.getElementById('buttonBarButton1_' + state).style.backgroundImage = "url('../images/blokk_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
			document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';	
			document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>MANDAT</span>';	
		//    document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmer_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//    document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/repr_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";	        
		}}
		else if (id == 2) {
		    if (state == 'Municipality' || state == 'My') {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
				
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';
		   }
		    else {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';	
				document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>MANDAT</span>';	
		    }
		}
		else if (id == 3) {
		    if (state == 'Municipality' || state == 'My') {
		        
		        if (region.length < 9) {
				document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
				document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';
		//            document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmerbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//            document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/mandaterbig_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";		            
		        }
		    }
		    else{
			document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className = document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).className + ' active';
			document.getElementById('buttonBarButton1_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>BLOKK</span>';	
			  document.getElementById('buttonBarButton2_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>STEMMER</span>';	
			  document.getElementById('buttonBarButton3_' + (state == 'Polls' ? 'Nation' : state)).innerHTML = '<span>MANDAT</span>';	
		//        document.getElementById('buttonBarButton1_' + state).style.backgroundImage = "url('../images/blokk_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//        document.getElementById('buttonBarButton2_' + state).style.backgroundImage = "url('../images/stemmer_" + ((window.innerWidth >= 480) ? '480' : '320') + "_off.png')";
		//        document.getElementById('buttonBarButton3_' + state).style.backgroundImage = "url('../images/repr_" + ((window.innerWidth >= 480) ? '480' : '320') + "_on.png')";		        
		    }
		}	    
	   
		if (state == 'Nation' || state == 'nationPolls'||state == 'Polls' ){
			componentIdNation = id;
			
			reloadContent('divNationData');
		}
		else if (state == 'County'){
			componentIdCounty = id;
			reloadContent('divCountyData');

        }
        else if (state == 'Municipality') {	
	    
		    if (!(id == 3 && region.length > 8)) {
		        componentIdMunicipality = id;
		        reloadContent('divMunicipalityData');		        
		    }else {
				componentIdMunicipality = id;
		        reloadContent('divMunicipalityData');
			}
        }
	    else if (state == 'My') {
		    componentIdMy = id;
		    reloadContent('divMyData');
		}
		else if (state == 'Mandater'){
		
			reloadContent('mainCircle');
		}
		else if (state == 'Duell') {
		
			reloadContent('divDuellData');
		}
		else if (state == 'Fav') {
			
		}else if (state == 'Repres'){
			reloadContent('divRepres');
			
		}	
	}	

function reloadContent(divId){
	var filePrefix = null;
	var componentId = null;
	var regionCode = null;
		////BIANCA ADD POLL
		/*if (!isValg){
			if (region == 'n')
				filePrefix = 'P05';
			else if (region.length == 3) //county
				filePrefix = 'P04';
			else { //municipality
				if (region == 'c00m0301') //Oslo
					filePrefix = 'P04';
				else
					filePrefix = 'P02';
			}
		}
		else {*/
		if (state == 'Nation' || state == 'nationPolls' || state == 'Polls'){
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = (isValg) ? regionCodeNation :  regionCodeNationPoll;;
			componentId = componentIdNation;
		}
		else if (state == 'County'){
			filePrefix = (!isValg) ? 'P'  + regionCodeCounty : (countyElectionMode) ? 'ST' + regionCodeCounty : 'ST'+ regionCodeCounty;
			regionCode = regionCodeCounty;
			
			componentId = componentIdCounty;
			if (isValg == true && region == "c03")
				region = 'c00m0301';
		}
		else if (state == 'Mandater'){
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = regionCodeNation;
			componentId = 9;
		}
		else if (state == 'Duell') {
			filePrefix = (!isValg) ? 'P' :'ST';
			filePrefix += (!isValg) ?regionCodeNationPoll :regionCodeNation;
			regionCode = (!isValg) ?regionCodeNationPoll :regionCodeNation;
			componentId = 8;
		}
		else if (state == 'Municipality'){
			filePrefix =	(!isValg) ? 'P' :'ST';
			filePrefix += ( (region.length > 8) ? regionCodeDistrict : regionCodeMunicipality);
			if (region.substring(0, 9) == "c00m0301d") {
				filePrefix = 'ST05';
			}
			regionCode = ((region.length > 8) ? regionCodeDistrict : regionCodeMunicipality);
			componentId = componentIdMunicipality;
		}
		else if (state == 'District'){
			filePrefix = 'ST' + regionCodeDistrict;
			regionCode = regionCodeDistrict;
			componentId = componentIdDistrict;
		}else if (state == 'Repres' ){
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = (isValg) ? regionCodeNation :  regionCodeNationPoll;
			componentId = 10;
		}else if (state == 'Search'){
		
			filePrefix = (isValg) ? 'ST' + regionCodeNation : 'P' + regionCodeNationPoll;
			regionCode = (isValg) ? regionCodeNation :  regionCodeNationPoll;
			componentId = 111;
		}

		if (filePrefix && componentId && regionCode){
		    var dataFile = dataDir + filePrefix + '-' + region + '.txt?m=' + Math.random();

			var xmlhttp;
			if (window.XMLHttpRequest){
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else{
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

		    xmlhttp.onreadystatechange = function() {

		        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					
		            parseText(xmlhttp.responseText, divId, componentId, regionCode);
		        }
		        else if (xmlhttp.status == 404) {
		            //not found		 		
		            document.getElementById(divId).innerHTML = "<div style='width:320px;text-align:center;'>Beklager ingen data funnet</div>";
		        }
		    };

		    xmlhttp.open("GET", dataFile, true);


		    try {
		        xmlhttp.send();	
		        
		    } catch(e) {
		        document.getElementById(divId).innerHTML = "<div style='width:100%;text-align:center;'>Det oppsto en feil, browseren din støtter ikke henting av datafiler fra annet domene<br/>" + e.toString() + "</div>";
		    } 
		}
	}

	function addToFavourites() {
	    var favourites = prefs.load();
		var regiona = region;
        if (regiona == 'c00m0301') {
			regiona = 'c03'
		}		
	    //finne ut om ligger der fra før!
	    if ($.isArray(favourites)) {
	        var newFav = { name: getRegionName(regiona), id: regiona };
	        if (!eksistsInArray(newFav, favourites))
	            favourites.unshift(newFav);
	    }
	    else {
	        favourites = [{ name: getRegionName(regiona), id: regiona}];
	    }

	    prefs.data = serialize(favourites);
	    prefs.save('valg2013Fav');

	    //parseFavourites(); BIANCA REMOVE INSTEAD CHANGE BUTTON
		
		$('#isNotFavBtn').attr('onclick','removeFromFavourites("'+regiona+'")');
		document.getElementById('isNotFavBtn').id = 'isFavBtn';
		
	}

	function removeFromFavourites(id) {
	    var favourites = prefs.load();

	    if ($.isArray(favourites)) {

	        for (var i = 0; i < favourites.length; i++) {
	            var fav = favourites[i];
	            if (fav.id == id) {
	                favourites.splice(i, 1);
	                break;
	            }
	        }

	        prefs.data = serialize(favourites);
	        prefs.save('valg2013Fav');
	        parseFavourites();
	    }
		
		$('#isFavBtn').attr('onclick','addToFavourites()');
		document.getElementById('isFavBtn').id = 'isNotFavBtn';
	}

	function goToFavourite(id, name) {
	    region = id;
		mainButton = '';
		$(".mMBtn").removeClass('activeMMBtn');
		
	    if (state == 'My') {
	        document.getElementById('myDataHeader').innerHTML = name;
	        selectComponent(componentIdMy);	        
	    }
	    else if (state == 'Municipality'  || state == 'Fav') {
	        var countyid = region.substring(0, 3);
	        var muniid = region.substring(4);
	        //refreshSubRegions(countyid);
			
	        if (muniid.length == 0) {
				state = 'County';
				selectCounty(countyid);
				$('#valgBtn').addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				mainButton = 'valgBtn';
				closeAllOtherMenuItems('div' + state);
				
			}
			
	        else if (muniid.length == 4) {
				state = 'Municipality';
	            var subCountyId = muniid.substring(0, 2);
	            var subMuniId = muniid.substring(2);

	            if (subCountyId.length == 2 && subCountyId.substring(0, 1) == '0')
	                subCountyId = subCountyId.substring(1);

	            if (subMuniId.length == 2 && subMuniId.substring(0, 1) == '0')
	                subMuniId = subMuniId.substring(1);

	            muniid = subCountyId + "_" + subMuniId;
	            selectMunicipality(muniid);
				$('#valgBtn').addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				mainButton = 'valgBtn';
				closeAllOtherMenuItems('div' + state);
	        }
	        else {
				state = 'Municipality';
	            var subCountyId = muniid.substring(0, 2);
	            var subMuniId = muniid.substring(2,4);
				var subdisId = muniid.substring(5);
	            if (subCountyId.length == 2 && subCountyId.substring(0, 1) == '0')
	                subCountyId = subCountyId.substring(1);

	            if (subMuniId.length == 2 && subMuniId.substring(0, 1) == '0')
	                subMuniId = subMuniId.substring(1);
					
				if (subdisId.length == 4 && subdisId.substring(0, 1) == '0')
	                subdisId = subdisId.substring(1);
					
				muniid = subCountyId + "_" + subMuniId +"_"+subdisId;
				selectMunicipality(muniid);
				$('#valgBtn').addClass('activeMMBtn');
				$("#upperBtnText").attr("style","display:none;");
				$("#bottomBtnText").attr("style","display:table-row;");
				mainButton = 'valgBtn';
				closeAllOtherMenuItems('div' + state);
			}
	       //selectComponent(componentIdMunicipality);	        
	    }

	    // scroll down to buttondiv
	    scrollToButton(document.getElementById('div' + state));
	}

var _id;
function showMainCircle(id){
	_id = id;
	//document.getElementById('blackOverlay').style.display='block';
	document.getElementById('mainCircle').style.display='block';


    var circleCurrent = document.getElementById('circleCurrent');
	if (circleCurrent){
		var text = document.getElementById('circleCurrentText');
		var background = '';
		if (_id == 'votes'){
			background = "#F5F5F5 url('../images/pad/votes.png') no-repeat 37.5px 30px";
			if (text)
				text.innerHTML = "SISTE M&Aring;LING";
		}
		else if (_id == 'mandates'){
		//just for testing
		region = 'n';
			if (text){
				if (region == 'n'){			
					background = "#F5F5F5 url('../images/pad/mandates.png') no-repeat 33.5px 30px";
					text.innerHTML = "MANDATER";
				}
				else{
					background = "#F5F5F5 url('../images/pad/representatives.png') no-repeat 40px 16px";					
					text.innerHTML = "REPRESENTANTER";
				}
			}
		}
		else if (_id == 'history'){
			background = "#F5F5F5 url('../images/pad/history.png') no-repeat 27px 30px";					
			if (text)
			    text.innerHTML = "HISTORIKK";

		    openHistory();
		}
		if (_id == 'table') {
		    background = "#F5F5F5 url('../images/pad/table.png') no-repeat 23px 30px";
		    if (text)
		        text.innerHTML = "TABELL";
		}
		if (_id == 'parties') {
		    background = "#F5F5F5 url('../images/pad/parties.png') no-repeat 38.5px 30px";
		    if (text)
		        text.innerHTML = "PARTIER";
		}   	    
	    
		circleCurrent.style.background = background;	
		circleCurrent.style.display='block';
	}
	
	toggleHeaders();
	fadeInMainCircle();
}

function fadeInMainCircle(){
	$("#mainCircle").animate(
	{
		width: '678px', height: '678px', 'margin-left': '-339px', 'margin-top': '-339px'
	}, 500, finishedFadeInMainCircle);
}

function finishedFadeInMainCircle(){
	$("#" + _id).animate({opacity:1}, 400);
	$("#mainCircleHeaderText").animate({opacity:1}, 400);
	$("#mainCircleRegionText").animate({opacity:1}, 400);
	$("#mainCircleDescriptionText").animate({opacity:1}, 400);

	var pollPanelSelect = document.getElementById("pollSelect_" + _id);	
	if (pollPanelSelect){
		
		if (_id == 'votes' || _id == 'mandates')
			pollPanelSelect.style.display = "block";
		else
			pollPanelSelect.style.display = "none";			
	}
	
	$("#circleCurrent").animate({opacity:1}, 400);
	$("#divCloseButton").animate({opacity:0.8}, 400, animateMainComponent);
	
}

function hideMainCircle(){	
	$("#" + _id).animate({opacity:0}, 400);
	$("#mainCircleHeaderText").animate({opacity:0}, 400);
	$("#mainCircleDescriptionText").animate({opacity:0}, 400);
	$("#mainCircleRegionText").animate({opacity:0}, 400);
	$("#circleCurrent").animate({opacity:0}, 400);	
	$("#divCloseButton").animate({opacity:0}, 400, fadeOutMainCircle);
}

function fadeOutMainCircle(){
	$("#mainCircle").animate(
	{
		width: '0px', height: '0px', 'margin-left': '0px', 'margin-top': '0px'
	}, 500, function (){document.getElementById('blackOverlay').style.display='none';document.getElementById('mainCircle').style.display='none';document.getElementById('circleCurrent').style.display='none'});	
	
}
function fadeOutMainPanel(){
	$("#mainPanel").animate({opacity: 0}, 1, function(){toggleCircleButtons();});
}
function fadeInMainPanel(){

	$("#mainPanel").animate({opacity: 1}, 1000);
}


/* -------- Animation for components --------*/
function animateMainComponent(){

	switch (_id){
		case 'votes':
			animateVotesComponent();
			break;
		case 'mandates':
			animateMandatesComponent();
			break;
		case 'history':
			break;
		case 'parties':
			break;
		case 'table':
			break;
	}
}

function animateVotesComponent(){

}
function animateMandatesComponent(){

}

/* --------Animation for votes counted component --------*/
var t;
var maxValue = 0;
var startValue = 0;
var nrOfLoops = 10;
var increment = 0;
function animateVotesCounted(lines){

	var percentCounted = parseFloat(lines[4]);
	var widthEmpty = (100 - percentCounted);	
	
	maxValue = percentCounted;
	startValue = 0;
	increment = maxValue / nrOfLoops;
	//alert(nrOfLoops + ' - ' + increment + ' - ' + maxValue + ' - ' + startValue );
	
	clearInterval(t);
	//t = setInterval(animate, 20);
}
function animate() {
    startValue += increment;
	if(startValue < maxValue) {
	    var fillDiv = document.getElementById('votesCountedFilled');
	    if (fillDiv) {
	        fillDiv.style.width = startValue.toString() + "%";
		}
	} 
	else {
		clearInterval(t); 
	}	
}
function toggleHeaders(){

	var mainCircleRegionText = document.getElementById("mainCircleRegionText");
	var mainCircleDescriptionText = document.getElementById("mainCircleDescriptionText");
    var mainCircleHeaderText = document.getElementById('mainCircleHeaderText');
    
	if (mainCircleRegionText && mainCircleDescriptionText && mainCircleHeaderText){
	    mainCircleHeaderText.innerHTML = '';
	    mainCircleRegionText.innerHTML = '';
		mainCircleDescriptionText.innerHTML = '';

		if (state == 'Polls') {
		    mainCircleHeaderText.innerHTML = 'MENINGSM&Aring;LING';

			if (region == 'n'){
				if (_id == 'votes'){
					mainCircleDescriptionText.innerHTML = 'Partienes oppslutning basert p&aring; landsdekkende m&aring;linger';
				}
				else if (_id == 'mandates'){
					mainCircleDescriptionText.innerHTML = 'Tenkt Storting basert p&aring; siste ti landsdekkende m&aring;linger';
				}
				else if (_id == 'history'){
					mainCircleDescriptionText.innerHTML = 'Gjennomsnitt siste 10 landsdekkende m&aring;linger';
				}
			}
			else{
				if (_id == 'votes'){
					mainCircleDescriptionText.innerHTML = 'Partienes oppslutning basert p&aring; lokale m&aring;linger';
					mainCircleRegionText.innerHTML = regionName.toUpperCase();
				}
				else if (_id == 'mandates'){
					mainCircleDescriptionText.innerHTML = 'Mandataprognose basert p&aring; lokale m&aring;linger';
					mainCircleRegionText.innerHTML = regionName.toUpperCase();
				}
			}
        }
	    else {
		    if (_id == 'votes') {
                if (state == 'Nation') {
                    mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
                    mainCircleDescriptionText.innerHTML = 'Basert p&aring; fylkestingsvalget';
                    }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
                }
		        
		    }
		    else if (_id == 'mandates') {
		        if (state == 'Nation') {
					
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		            mainCircleDescriptionText.innerHTML = 'Tenkt Storting basert p&aring; fylkestingsvalget';
		        }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		           //TODO mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		        }
		    }
		    else if (_id == 'history') {
		        if (state == 'Nation') {
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		            mainCircleDescriptionText.innerHTML = 'Historikk: Partienes oppslutning i fylkestingsvalg';
		        }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		            mainCircleDescriptionText.innerHTML = 'Historikk: Partienes oppslutning i ' + ((countyElectionMode) ? 'fylkestingsvalg' : 'kommunevalg');
		        }
		        
		    }
		    else if (_id == 'parties') {
                if (state == 'Nation')
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		        else
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		    }
		    else if (_id == 'table') {
		        if (state == 'Nation') {
		            mainCircleRegionText.innerHTML = "LANDSOVERSIKT";
		            mainCircleDescriptionText.innerHTML = 'Partienes oppslutning i fylkestingsvalg';
		        }
		        else {
		            mainCircleHeaderText.innerHTML = (countyElectionMode) ? 'FYLKESTINGSVALGET' : 'KOMMUNEVALGET';
		            mainCircleRegionText.innerHTML = regionName.toString().toUpperCase();
		        }		        
		    }
		}
	}
}
function gotoSelectLastWeekPoll(value, pollText){
	gotoSelectLastWeek = true;
	//value = ID-1200;20110629;kommune;5_28
	//pollText = Kommunestyrevalg i Østre Toten (Sentio for Oppland Arbeiderblad)
	var hL = pollText;
	var pollElements = value.split(";");
	if (pollElements.length == 4){
		var pollId = pollElements[0];
		var pollDateRaw = pollElements[1];
		var pollType = pollElements[2];
		var regionIdRaw = pollElements[3];
		//isPollParse = true;		
		//fix url
		switch (pollType){
			case 'kommune':
				//regionIdRaw = 12_65	or 1_6
				//regionIdRaw = 0 special case - spurt om kommunevalg i hele landet
				if (regionIdRaw == '0'){
					pollMenuMunicipalities.selectedIndex = 0;
					pollMenuCounties.selectedIndex = 0;
					selectNationPolls(false);					
				}
				else{
					var regionIdElements = regionIdRaw.split('_');											
					if (regionIdElements.length == 2){
						var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];							
						if (countyId == '03') 
							countyId = '00';						
						toggleState('Municipality', false, hL);
						pollMenuCounties.selectedIndex = ((regionIdElements[0] > 12)? regionIdElements[0] - 1:regionIdElements[0]);
						//state = 'Municipality';
						document.getElementById('divNation').style.display='none';
						selectCounty(regionIdRaw , '', true);
						
						var index = getIndexFromValue(regionIdElements[0], regionIdElements[1]);
						pollMenuMunicipalities.selectedIndex = index;
						//selectPollRegion();						
					}					
				}				
				break;
			case 'fylke':
				//regionIdRaw = 1 or 12
				var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;					
				toggleState('County', false, hL);
				pollMenuMunicipalities.selectedIndex = 0;
				pollMenuCounties.selectedIndex = ((regionIdRaw > 12)? regionIdRaw - 1:regionIdRaw);
				document.getElementById('divNation').style.display='none';
				selectCounty(countyId , '', true);
				//selectPollRegion();				
				break;
			case 'riks':
				pollMenuMunicipalities.selectedIndex = 0;
				pollMenuCounties.selectedIndex = 0;
				selectNationPolls(false);					
				break;
		}
	}
}
function getIndexFromValue(countyValue, value){
	var index = 0;
	var regionsDropDown = document.getElementById('pollMenuMunicipalities');
	if (regionsDropDown){
		for (var i = 0; i < regionsDropDown.options.length; i++){
			var option = regionsDropDown.options[i];
			
			if (option.value == countyValue + '_' + value){
				index = i;
				break;
			}
		}	
	}	
	
	return index;
}
function selectPoll(id, index, componentId,headline) {

	isPollParse = true;
	mySelectedPoll = index;
	mySelectedPollHL = headline;
	mySelectedPollID = id;
    var partyResultsRaw = currentPollCollectionResults[id];
	var partyResultsRawM = currentPollCollectionResultsM[id];
    var partyResults = "";
	var partyResultsM = "";
    
    if (partyResultsRaw){
    	partyResults = partyResultsRaw.split('|').clean("\r");;
		
    
	}
    else{
    	
    	index = -1;
    }
	
	var component ;
    if (state == 'Nation' || state == 'Storting' || state == 'Polls'){
            regionCode = '05';
			component = document.getElementById('divNationData');
		}
        else if (state == 'County'){
            regionCode = '04';
			component = document.getElementById('divCountyData');
		}
        else if (state == 'Municipality'){
			component = document.getElementById('divMunicipalityData');
            regionCode = '02';
			}
    //TODO
   // var component = document.getElementById(componentId);
	//component = document.getElementById('divMunicipalityData');
	
    if (component){    
       	switch (componentId){
       		case 'votes':	
				
       			component.innerHTML = parseColumns(partyResults,regionCode, index,headline);
       			break;
       		case 'blokk': //only used for seatsrepresentatives
       			component.innerHTML = parseMajority(partyResultsRawM, index,headline);
       			break;
			case 'mandates': //only used for seatsrepresentatives
       			component.innerHTML = parseMandates(partyResults,regionCode, index,headline);
       			break;
       	}
       	
       	var select = document.getElementById("pollSelect_" + componentId);
        if (select) select.style.display = "block";
	}
}
function selectPollRegion() {
	
    if (pollMenuMunicipalities.selectedIndex != 0) {
        selectMunicipality(pollMenuMunicipalities.value, pollMenuMunicipalities.options[pollMenuMunicipalities.selectedIndex].text);        
    }
    else if (pollMenuCounties.selectedIndex != 0) {
        selectCounty(pollMenuCounties.value, pollMenuCounties.options[pollMenuCounties.selectedIndex].text, false);        
    }

    //closePollMenu(); // BIANCA FIX
	
	
}
function selectNationPolls(toggle){

	region = 'n';
	regionName = 'LANDSOVERSIKT';
	//reloadContent('divNationData');
	selectComponent(1);
	//if (toggle)
	//    toggleState('Polls', false); //will close/open menu
}
function toggleRepDisplay(obj,nr){
	if (nr == 1){
		$('.listRep').attr('style','display:none;');
		$('.repholder').attr('style','display:inline-block;');		
	}else {

		$('.repholder').attr('style','display:none;');
		$('.listRep').attr('style','display:block;');
	}
}
function toggleRepres(obj) {
	$(".storBtn").removeClass('active');
	if(obj.id == 'represFylkeBtn'){
		$("#represFylkeBtn").addClass('active');
		$('#repWrapper').attr('style','display:none;');
		$('#repListWrapper').attr('style','display:block;');
	}else {
		$("#represPartiBtn").addClass('active');
		$('#repWrapper').attr('style','display:block;');
		$('#repListWrapper').attr('style','display:none;');
	}
}