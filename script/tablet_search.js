﻿function doSearch(){
	var searchResults= document.getElementById('searchResults');
	
	if (searchResults){
		searchResults.innerHTML = '';
			
		var searchBox = document.getElementById('searchBox');
		if (searchBox){				
			//everything is ok, perform search
			searchName(searchBox.value);
			showSearchResults();
		}
	}	
}
		
function searchName(text) {
 	var innerHTML = '';
 	var results = '';
 	var criteria = text.toLowerCase();
	var hitcount = 0;
	var regionsArray = subRegions.split('|');
	var counties = loadCounties(); 		
	innerHTML += "<div id='myScrollWindow'>";
	innerHTML += "<table  class='whiteFont' width='100%' cellpadding='5' cellspacing='0'>";
	innerHTML += "<tr class='borderTop' ><td colspan='3' >STED</td></tr>";
	if ( isValg == false) {
		innerHTML += "<tr ><td colspan='3' class='noData'>Ingen data på kommunenivå tilgjengelig før opptellingen begynner 9. september</td></tr>";
	}
	if (criteria == ''){
		
		innerHTML += "<tr>";
		innerHTML += "<td >&nbsp;</td>";
		innerHTML += "<td ><br/>Du m&aring; angi minst et s&oslash;keord.<br/><br/>";
  		innerHTML += "Du kan s&oslash;ke p&aring; fylkesnavn, kommunenavn og kretsnavn i Oslo, Bergen, Stavanger og Trondheim.</td>";
  		innerHTML += "</tr>";
  		
  		innerHTML += "<tr>";
  		innerHTML += "<td>&nbsp;</td>";
  		//innerHTML += "<td style='padding-bottom:20px;vertical-align:bottom;text-align:center;'><input onclick='closeSearchResults();' type='button' id='closeSearch' value='Lukk' style='font-size:14pt;' /></td>";
  		innerHTML += "</tr>";
	}
	else{		
		for (var i = 0; i < regionsArray.length; i++){			
  			//format: Name;type;id //Aust-Agder;4;9 //Fredrikstad;2;1_6 //Hersleb skole;3;3_1_101 //Rosenborg;3;16_1_6
  			var region = regionsArray[i];
  			var regionElements = region.split(';');
  		
			if (regionElements.length == 3 && (regionElements[0].toLowerCase().indexOf(criteria) > -1)){
				//match
			    var regionType = region.charAt(region.indexOf(';') + 1);
				var regionIdRaw = region.substring(region.lastIndexOf(';') + 1);				
				var row = ''; 
				var url = '';
			
				if (regionType == 2 || regionType == 3){
					//regionIdRaw = 12_65	or 1_6
					var regionIdElements = regionIdRaw.split('_');						
					
					if (regionIdElements.length == 2){
						var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];
						
						if (countyId == '03') countyId = '00';
						
					    if (isValg == false)
					        url = "";
					    else
						    url = "toggleState('Municipality', false);selectMunicipality('" + regionIdRaw + "','" + regionElements[0]+ "');";//closeSearchResults(false)";
					}
				    else if (regionIdElements.length == 3) {
				        var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];

				        if (countyId == '03') countyId = '00';

				        if (isValg == false)
				            url = "";
				        else
				            url = "toggleState('Municipality', false);selectMunicipality('" + regionIdRaw + "','" + regionElements[0] + "');";//closeSearchResults(false)";
				    }
				}
				else if (regionType == 4){												
					//regionIdRaw = 1 or 12
					var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;					
					
				    if (isValg == false)
				        url = "toggleState('countyPolls', false);selectCounty('" + countyId + "', '" + regionElements[0].toUpperCase() + "', false);selectPollRegion();";//closeSearchResults(false)";    //selectPollRegion();closeSearchResults(false)";
                    else
				        url = "toggleState('County', false);selectCounty('" + countyId + "','" + regionElements[0] + "');";//closeSearchResults(false)";
	            }
				
				//TODO: enable districts for results
				row += "<div onclick=\"" + url + "\" class='searchResult'>" + regionElements[0].toUpperCase();

				/* switch regiontype
				4 = county
				2 = municipality
				3 = district
				*/
				switch (regionType) {
				    case ('2'): //municipality
				        {
				            var countyId = region.substring(region.indexOf(';') + 3, region.indexOf("_"));
				            if (countyId == 3) { //only Oslo
				                row += ' FYLKE/KOMMUNE';
				            }
				            else {
				                row += ' KOMMUNE I ' + counties[countyId].toUpperCase();
				            }
				            break;
				        }
				    case ('3'): //district
				        {
				            //switch municipalityid
				            switch (region.substring(region.indexOf(';') + 3, region.indexOf("_"))) {
				                case ('3'):
				                    row += ' (KRETS I OSLO)';
				                    break;
				                case ('11'):
				                    row += ' (KRETS I STAVANGER)';
				                    break;
				                case ('12'):
				                    row += ' (KRETS I BERGEN)';
				                    break;
				                case ('16'):
				                    row += ' (KRETS I TRONDHEIM)';
				                    break;
				            }
				            break;
				        }
				    case ('4'): //county
				        {
				            row += ' FYLKE';
				            break;
				        }
				}

				row += "</div>";
				hitcount++;

				results += "<tr>";
				results += "<td ";

				if (hitcount > 1)
				    results += "";

				results += ">&nbsp;</td>";
				results += "<td ";

				//if (hitcount > 1)
				//    results += "border-top:1px #FFFFFF solid;";

				results += ">" + row + "</td>";
				results += "</tr>";

			    
  			}
 		}
 		
 		//any hits?
 		if (hitcount == 0){
 			innerHTML += "<tr>";
			innerHTML += "<td >&nbsp;</td>";
			innerHTML += "<td >Ingen treff p&aring; s&oslash;k etter \"" + text + "\" ";
  			//innerHTML += "Du kan s&oslash;ke p&aring; fylkesnavn, kommunenavn og kretsnavn i Oslo, Bergen, Stavanger og Trondheim.</td>"; //TODO: change for results
  			innerHTML += "</td>";  			
  			innerHTML += "</tr>";
 		}
 		else if (hitcount == 1){
 			//navigate directly to region?
 			innerHTML += results;
 		}
 		else{
 			innerHTML += results;
 		}	
 		
 		innerHTML += "<tr>";
  		innerHTML += "<td >&nbsp;</td>";
  		//innerHTML += "<td style='padding-bottom:20px;vertical-align:bottom;text-align:center;border-top:1px #FFFFFF solid;'><input onclick='closeSearchResults();' type='button' id='closeSearch' value='Lukk' style='font-size:14pt;' /></td>";
  		innerHTML += "</tr>"; 		
	}
	
	innerHTML += "</table>";
	innerHTML += "<table width='100%' class='whiteFont'>";
	//TODO Results for PERSON
	innerHTML += "<tr class='borderTop'><td colspan='3'>PERSON</td></tr>";
	inRepres.sort(sortByParti); 
	var repHitCount = 0;
	$.each(inRepres, function(){
		var lastnOne = '';
		var lastnTwo = '';
		var lastfOne = '';
		var lastfTwo = '';
		if (this.FirstName && this.LastName){
			var firstandlast = this.FirstName.toLowerCase() + " " + this.LastName.toLowerCase();
			if (this.LastName.indexOf(" ") != -1) {
				lastnOne = this.LastName.split( " " )[0];
				lastnTwo = this.LastName.split( " " )[1];
			}
			if (this.FirstName.indexOf(" ") != -1) {
				lastfOne = this.FirstName.split( " " )[0];
				lastfTwo = this.FirstName.split( " " )[1];
			}
			if(this.FirstName.toLowerCase().match('^'+criteria) || this.LastName.toLowerCase().match('^'+criteria) || firstandlast.match('^'+criteria) || (lastnOne.toLowerCase().match('^'+criteria) || lastnTwo.toLowerCase().match('^'+criteria) )   ||    (lastfOne.toLowerCase().match('^'+criteria) || lastfTwo.toLowerCase().match('^'+criteria) )) {
				innerHTML += "<tr style='cursor:pointer;' onclick='openRepresentanter("+this.CandidateId+");'><td></td><td>"+ this.FirstName.toUpperCase()+" "+ this.LastName.toUpperCase()+" (" +this.PoliticalPartyCode.toUpperCase()+") - "+getRegionName(this.CountyId).toUpperCase()+"</td></tr>";
				repHitCount++;
			}
		}
	});
	if (repHitCount == 0){
 			innerHTML += "<tr>";
			innerHTML += "<td >&nbsp;</td>";
			innerHTML += "<td >Ingen treff i det nye Stortinget";
  			innerHTML += "</td>";  			
  			innerHTML += "</tr>";
 		}
	innerHTML += "</table></div>";
	
	document.getElementById("searchResults").innerHTML = innerHTML; 
	//$('#myScrollWindow').sbscroller();
	var container = $('div#searchResults');
	container.scrollTop(0);
	return false;
}


function openRepresentanter(id) {
	searchRep = id;
	toggleState('Storting',false);
}
function showSearchResults(){
	//document.getElementById('blackOverlay').style.display='block';
	document.getElementById('searchResults').style.display = 'block';	
}
function closeSearchResults(withFocus){
	//document.getElementById('blackOverlay').style.display='none';
	document.getElementById('searchResults').style.display = 'none';
	
	if (withFocus)
		document.getElementById('searchBox').focus();
}

