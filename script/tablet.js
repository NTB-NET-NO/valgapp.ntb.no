var state; 
var region;
var regionName;
var countyElectionMode = true;
var myMap;
var myHCMandater = [];
var myHCHistory = [[], [], [], [], [], [], [], [], [], [], []];
var myHCHistoryLabel = [];
var saeuleArray = [];
var paper;
var mapLoaded = false;
var kartMeny = false;
var oldRegion = '';
var pfeilDeg;
//var dataDir = 'data/';	
var dataDir = 'http://valgapp.ntb.no/prod/data/';

var imageDataDir = 'images/';
var jsonURL = 'repData/';
//vars are the param from web to link into the app
//start = nation, storting,valg, poll or rep
//fylke = any fylke 
//komune = any kommune
//rep = party 
var vars = [], value;
var isPollParse;
var isValg = false;
var itsNine = false;
//var valgStart =  new Date(2013,08,04,12,50,00);
var valgStart =  new Date(2013,08,09,20,50,00); // VALGSTART
var infoSwitch =  new Date(2013,08,09,21,01,00); // infoSWITCH

//var valgStart =  new Date(2013,08,09,19,20,00); 
//var infoSwitch =  new Date(2013,08,09,19,01,00); 

var idag = new Date();
var pollText = '';
var currentPollCollectionResults = {};
var currentPollCollectionResultsM = {};
var lastWeekPolls;
var historyExceptions = [1,1,1,1,1,1,1,1,1]; // index = (partyId - 1), 1 indicates no exception, 0 indicates exception
var historyShowStortingResults = false;
var stortingsValg = ["2009","2005","2001","1997","1993","1989","1985","1981","1977","1973","1969","1965","1961"];
var searchRep = '';
var time1;
var time2;
var time3;
var inRepres = [];

function loadPage(){
closeNoResltsInfo();
	//testValgIds();  
try {
var circle = new Sonic({
	width:50,
	height: 50,
	padding:50,
	
	strokeColor: '#000',
	pointDistance: .01,
	stepsPerFrame: 3,
	trailLength: .7,
	
	step: 'fader',
	
	setup: function() {
		this._.lineWidth = 5;
	},
	path: [
		['arc',25,25,25,0,360]
	]
	
});
circle.play();
document.getElementById('blackOverlay').appendChild(circle.canvas);
}catch(er){}
	checkDate();
	vars = readAdd();
    //set logo
	var code = querySt();
	//code = 'VG';
	
	$('body').addClass(code.toUpperCase());
	
    if (logoCodes[code.toUpperCase()]) {
        var logoFileName = logoCodes[code.toUpperCase()];
		if (logoFileName){
			var logoArray = logoFileName.split(',');
			if (window.innerWidth <= 700)
				logoFileName = logoArray[0];
			else
				logoFileName = logoArray[1];
		}
        if (!logoFileName)
            logoFileName = "ntb.png";

        var logoimg = document.getElementById('logoImg');
        if (logoimg)
            logoimg.src = 'logo/' + logoFileName;
    }else {
		logoFileName = "ntb.png";
		 var logoimg = document.getElementById('logoImg');
        if (logoimg)
            logoimg.src = 'logo/' + logoFileName;
	}
		
	
	if (window.innerWidth <= 801){
		//$('#kartModulWrapper').svg({loadURL: 'images/kart/norge_small.svg',onLoad: mySvgLoadedStart});
		$('#kartModulWrapper').svg({loadURL: 'images/kart/map3b.svg',onLoad: mySvgLoadedStart});
	}else {
		//$('#kartModulWrapper').svg({loadURL: 'images/kart/norge_bigger.svg',onLoad: mySvgLoadedStart});
		$('#kartModulWrapper').svg({loadURL: 'images/kart/map3bPad.svg',onLoad: mySvgLoadedStart});
	}
	
	loadHistory();
	setWidgetRegion();
	$(".button").click(function() {
		$('.button').removeClass('active');
		$(this).addClass('active');
	});
	$("#gallerier").on("click",".repholder",function(e){
		
		$(".repholder").removeClass('activeRep');
		$(this).addClass('activeRep');
		openRepBox(this.id,this);
	});
	$("#gallerier").on("click",".repholderBig",function(e){
		if ($(this).hasClass('activeRep')){
			$(".repholderBig").removeClass('activeRep');
			$('.repTT').animate({height:"0px"});
		}else{
		$(".repholderBig").removeClass('activeRep');
		$(this).addClass('activeRep');
		openRepBox(this.id,this);
		}
	});
  	var data = cleanRegionArray(subRegions.split("|"));	
	/*$("#gallerier").on("click",".repTT",function(e){
		$(this).animate({height:"0px"},function(){$(this).attr('style','');});
		
		$(".repholderBig").removeClass('activeRep');
		$(".repholder").removeClass('activeRep');
	});*/
	
	$("#mandates").on(
		{
		mouseenter: function(){
			var t = parseInt($(this).index())+1;
			$('#votesContainer td:nth-child(' + t +')').addClass('highlighted');
		},
		mouseleave: function(){
			var t = parseInt($(this).index())+1;
			$('#votesContainer td:nth-child(' + t +')').removeClass('highlighted');
		}
		},'td');
	var d = new Date();
	
	$('#blackOverlay').attr('style','display:none;');

	var slideOne = $('#valgMaalingWrapper');
	slideOne
	.on('swipeleft', function() {
		changeValgWrapper('right');
	})
	.on('swiperight', function(){
		changeValgWrapper('left');
	});
	
	var slideTwo = $('#stortingWrapper');
	slideTwo 
	.on('swipeleft', function() {
		changeStorWrapper('right');
	})
	.on('swiperight', function(){
		changeStorWrapper('left');
	})
	.on('swipeup', function(){
		scrollRepWindow('downBig');
		scrollRepWindow('down');
	})
	.on('swipedown', function(){
		scrollRepWindow('upBig');
		scrollRepWindow('up');
	});
}
function notYetInfo(){
var innerHTML = "<div class='regionWrapper'><div class='notYetInfo'>Ingen data på kommunenivå tilgjengelig før opptellingen begynner 9. september</div></div>"
	$('#favSelectorCounty').html(innerHTML);
}
function noResltsInfo() {
	$("#noResltInfo").html('');
	var infoHTML = '';
	if (itsNine == true)
		infoHTML = '<div class="closeButton" onclick="closeNoResltsInfo();"></div>Hvis du ikke finner resultater fra din kommune er det fordi kommunen ennå ikke har innrapportert tallene til SSB.';
	else 
		infoHTML = '<div class="closeButton" onclick="closeNoResltsInfo();"></div><span class="infoHeader">INGEN RESULTATER FØR KL 21:00</span><br /><br />Den første landsoversikten offentliggjøres kl. 21:00.<br />Opptellingsresultatene vil bli rapportert fortløpende.<br /> Hvis du ikke finner resultater fra din kommune er det fordi kommunen ennå ikke har innrapportert tallene til SSB.';
	
	$("#noResltInfo").html(infoHTML);
	$("#noResltInfo").attr('style','');
}
function closeNoResltsInfo() {
	$("#noResltInfo").attr('style','display:none;');
}
function testonpad(blub) {
	
	$('.button').removeClass('active');
	if (state == 'Nation' || state == 'Polls'){
		
		if (state == 'Nation'){
			state = 'County';
			$("#topmenuButtonCounty").addClass('active');
		}else {
			state ='countyPolls';
			$("#topmenuButtonCountyPoll").addClass('active');
			$("#circleTable").addClass("disabled");
			$("#circleParties").addClass("disabled");
			$("#circleHistory").addClass("disabled");
			$("#circleKart").addClass("disabled");
		}
		if(blub == 301 || blub == '301' || blub == 'c301' || blub == 'f301' || blub == 'e301'){
			selectCounty('00m0301','OSLO');
		}else {
			var id = ''+blub;
				if(id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
					while ( id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
						id = id.substring(1);
					}
					selectCounty(id, getRegionName(id));	
				}else {
					selectCounty(blub, getRegionName(blub));
				}
		}
	}else {
	//selectMunicipality('11_3_1','BUØY/HUNDVÅG');
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonMunicipality").addClass('active');
		state = 'Municipality';
		oldRegion = region;
		
		if (region  == 'c12m1201'){
			var id = ''+blub;
			/*while (id.charAt(0) == 0)
				id = id.substring(1);*/
			
			id = '12_1_'+id;
			selectMunicipality(id, getRegionName(id), true);		
		}else if(region  == 'c16m1601'){
			var id =''+blub;
			while (id.charAt(0) == 0)
				id = id.substring(1);
			
			id = '16_1_'+id;
			selectMunicipality(id, getRegionName(id), true);	
		}else if(region  == 'c11m1103'){
			var id = ''+blub;
			while (id.charAt(0) == 0)
				id = id.substring(1);
			
			id = '11_3_'+id;
			selectMunicipality(id, getRegionName(id), true);
		}else if(region  == 'c00m0301'){
			var id = ''+blub;
			while (id.charAt(0) == 0)
				id = id.substring(1);
			
			id = '3_1_'+id;
			selectMunicipality(id, getRegionName(id), true);
		}else {
			var id__ = ''+blub;
			id__ = id__.substring(0,2) + "_" + id__.substring(2);
			selectMunicipality(id__, getRegionName(blub), true);
		}
	}
	var headerHeight = $("#header").height();
		var menyHeight = 0 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'0px'}, {duration:300,queue:false});
		});
}
function  clickToReps() {
	$("#kartMenu").accordion('destroy').accordion({heightStyle: "content"});
	$('.button').removeClass('active');
	toggleState('Storting',false);
	//toggleState('Repres',false);
	showMainCircle('gallerier', true);
	var id = this.id;
	if(id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'|| id.charAt(0) == '0'|| id.charAt(0) == 0){
		while ( id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'|| id.charAt(0) == '0'|| id.charAt(0) == 0){
			id = id.substring(1);
		}
			
	}
	
	
	//$(blub).addClass('activeRep');
	//openRepBox('candidate_'+searchRep,$(blub));
	
	if (id == 301 || id == '301') id = '3';
	setTimeout(function () {toggleRepres('string', 'small','fylke');  }, 200);
	setTimeout(function () {openAllFylke(id, $('.repParty'+id+' img'));  }, 500);
	
}
function testonclick(blub) {
	$("#kartMenu").accordion('destroy').accordion({heightStyle: "content"});
	/*** KART NAVIGATION ***/
	var windowwidth = window.innerWidth;
	if (windowwidth > 850 ) {
	
	}else {
	$('.button').removeClass('active');
	
	if (state == 'Nation' || state == 'Polls'){
		
		if (state == 'Nation'){
			state = 'County';
			$("#topmenuButtonCounty").addClass('active');
		}else {
			
			state ='countyPolls';
			$("#circleTable").addClass("disabled");
			$("#circleParties").addClass("disabled");
			$("#circleHistory").addClass("disabled");
			$("#circleKart").addClass("disabled");
			$("#topmenuButtonCountyPoll").addClass('active');
		}
		if(this.id == 301 || this.id == '301' || this.id == 'c301' || this.id == 'f301' || this.id == 'e301'){
			
			selectCounty('00m0301','OSLO');
		}else {
			var id = this.id;
				if(id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
					while ( id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
						id = id.substring(1);
					}
					selectCounty(id, getRegionName(id));	
				}else {
					selectCounty(this.id, getRegionName(this.id));
				}
		}
	}else {
	//selectMunicipality('11_3_1','BUØY/HUNDVÅG');
		$("#topmenuButtonCountyLink").html("FYLKEOVERSIKT");
		$("#topmenuButtonMunicipality").addClass('active');
		state = 'Municipality';
		oldRegion = region;
		
		if (region  == 'c12m1201'){
			var id = this.id;
			/*while (id.charAt(0) == 0)
				id = id.substring(1);*/
			
			id = '12_1_'+id;
			selectMunicipality(id, getRegionName(id), true);		
		}else if(region  == 'c16m1601'){
			var id = this.id;
			while (id.charAt(0) == 0)
				id = id.substring(1);
			
			id = '16_1_'+id;
			selectMunicipality(id, getRegionName(id), true);	
		}else if(region  == 'c11m1103'){
			var id = this.id;
			while (id.charAt(0) == 0)
				id = id.substring(1);
			
			id = '11_3_'+id;
			selectMunicipality(id, getRegionName(id), true);
		}else if(region  == 'c00m0301'){
			var id = this.id;
			//while (id.charAt(0) == 0)
				//id = id.substring(1);
			
			id = '3_1_'+id;
			selectMunicipality(id, getRegionName(id), true);
		}else {
			var id__ = this.id.substring(0,2) + "_" + this.id.substring(2);
			selectMunicipality(id__, getRegionName(this.id), true);
		}
	}
	}
	var headerHeight = $("#header").height();
		var menyHeight = 0 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'0px'}, {duration:300,queue:false});
		});
}

function svgOver(e){
	closeMapTooltip();
	var id = this.id;
	if(id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
		while ( id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
			id = id.substring(1);
		}
		
		$('#'+id).attr('style','z-index:5;opacity: 0.5;');	
		$('#f'+id).attr('style','z-index:5;opacity: 0.5;');	
	} else {
		$(this).attr('style','z-index:5;opacity: 0.5;');
	}
	var offset = $(this).offset();

	if (id.charAt(id.length - 1) != '0'  && id.charAt(id.length - 1) != '1' && id.charAt(id.length - 1) != '2' && id.charAt(id.length - 1) != '3' && id.charAt(id.length - 1) != '4' && id.charAt(id.length - 1) !='5' && id.charAt(id.length - 1) != '6' && id.charAt(id.length - 1) != '7' && id.charAt(id.length - 1) != '8' && id.charAt(id.length - 1) != '9'){
		
		id = id.substr( 0,id.length -1);
	}
	var windowheight = window.innerHeight;
	var tipHeight = $('.map_'+id+'Tooltip').height();
	
	if ( (e.clientY + tipHeight +50) > windowheight){
		var yposition = e.clientY - (tipHeight*2);
		$('.map_'+id+'Tooltip').attr("style","visibility:visible;top:"+ yposition +"px;left:"+( e.clientX  - ($('.map_'+id+'Tooltip').width()/2)) +"px;");
	}else {	
		$('.map_'+id+'Tooltip').attr("style","visibility:visible;top:"+  e.clientY  +"px;left:"+(e.clientX - ($('.map_'+id+'Tooltip').width()/2)) +"px;");
	}
	setTimeout(function(){$('.map_'+id+'Tooltip').attr("style", "visibility:hidden;");}, 5000);
}
function svgOut(){
	var id = this.id;
	if(id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
		while ( id.charAt(0) == 'f' || id.charAt(0) == 'c' || id.charAt(0) == 'e'){
			id = id.substring(1);
		}
		$('#f'+id).attr('style','');
		$('#'+id).attr('style','');		
	}else {
		$(this).attr('style',' ');
	}
	$(this).attr('style',' ');
	$(".mapTooltip").attr("style", "visibility:hidden;");
}
function mySvgLoadedStart(svg){

	$('#kartModulWrapper svg').attr('width','100%');
	$('#kartModulWrapper svg').attr('height','100%');
	$('#kartModulWrapperAlt').svg({loadURL: 'images/kart/norge_alternatea.svg',onLoad: mySvgLoadedAlt});


	if (vars != '') {
		var t = vars;
		if(decodeURI(t[0]).toUpperCase() == 'FYLKE'){
			if (isValg == true){
				toggleState('County',false);
			}else {
				toggleState('countyPolls',false);
			}
			var rName = decodeURI(t[1]).toUpperCase();
			if(rName != undefined){
				var id = getRegionId(rName);
				if (id.length == 1)
					id = '0'+id;
					if (rName == 'OSLO' && isValg == true)
						id = '00m0301';
					selectCounty(id, rName); 
			}
		}else if(decodeURI(t[0]).toUpperCase() == 'KOMMUNE'){
			if (isValg == true){
				
				toggleState('Municipality',false);
				$('#topmenuButtonValg').addClass('activeMMBtn');
				var rFylke = decodeURI(t[1]).toLowerCase();
				rFylke = rFylke.replace(/_/g," "); 
				var rFylkeID = getRegionId(rFylke);
				var rName = decodeURI(t[2]).toLowerCase();
				rName = rName.replace(/_/g," "); 
				var regionsArray = subRegions.split('|');
				
				
				for (var i = 0; i < regionsArray.length;i++) {
					var region = regionsArray[i];
					var regionElements = region.split(';');
					var regionArray = regionElements[2].split('_');
					var searchFylke = regionArray[0];
					if (rFylkeID == 03) rFylkeID = '3_1';
					if (rFylkeID.split('_').length == 2){ searchFylke = searchFylke +'_'+regionArray[1];}
					if (regionElements.length == 3 && (rFylkeID == searchFylke ) && (regionElements[0].toLowerCase()== rName)){
						//match
						var regionType = region.charAt(region.indexOf(';') + 1);
						var regionIdRaw = region.substring(region.lastIndexOf(';') + 1);
						if (regionType == 2 || regionType == 3) {
							var regionIdElements = regionIdRaw.split('_');						
					
							if (regionIdElements.length == 2){
								var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];
						
								if (countyId == '03') countyId = '00';
								
								//toggleState('Municipality',false);
								
								selectMunicipality(regionIdRaw, regionElements[0]);
						
							}else if(regionIdElements.length == 3){
								selectMunicipality(regionIdRaw, regionElements[0]);
							}
							
			
						}
						
					}
				}	
				
			}else {
				toggleState('Duellen',false);
			}
			/*var rName = decodeURI(t[1]).toUpperCase();
			if(rName != undefined){
				var id = getRegionId(rName);
				if (id.length == 1)
					id = '0'+id;
					if (rName == 'OSLO' && isValg == true)
						id = '00m0301';
					selectMunicipality(id, rName); 
			}*/
		}else if(decodeURI(t[0]).toUpperCase() == 'LAND'){
			if (isValg == true){
				toggleState('Nation',false);
			}else {
				toggleState('Polls',false);
			}
		}else if(decodeURI(t[0]).toUpperCase() == 'DUELLEN'){
			
				toggleState('Duellen',false);
			
		}else if(decodeURI(t[0]).toUpperCase() == 'STORTING'){
			
				toggleState('Storting',false);
			
		}else if(decodeURI(t[0]).toUpperCase() == 'POLLS'){
			
				toggleState('Polls',false);
			
		}else if(decodeURI(t[0]).toUpperCase() == 'FYLKEPOLLS'){
			
				toggleState('countyPolls',false);
				var rName = decodeURI(t[1]).toUpperCase();
				if(rName != undefined){
					var id = getRegionId(rName);
					if (id.length == 1)
						id = '0'+id;
					selectCounty(id, rName); 
				}
		}else if(decodeURI(t[0]).toUpperCase() == 'MAALINGER'){
			
				toggleState('Polls',false);
			
		}else {
			toggleState('Home',false);
		}
	}else {
		toggleState('Home', false);
	}
}
function mySvgLoadedAlt(svg){
	$('#kartModulWrapperAlt svg').attr('width','100%');
	$('#kartModulWrapperAlt svg').attr('height','100%');
}

function mySvgLoaded(svg){
$('#kartModulWrapperCounty svg').attr('width','100%');
$('#kartModulWrapperCounty svg').attr('height','100%');
if (state == 'Polls')
	selectNationPolls('true');
else
	reloadContent();
}
function checkDate() {
	if (idag > valgStart) {
		isValg = true;
		$("#topmenuButtonValg").removeClass("disabled").attr("onclick", "toggleState('Valg', false);");
	}else{
		isValg = false;
		$('#topmenuButtonValg').addClass('disabled').attr('onclick', '');
	}
	if (idag > infoSwitch) {
		itsNine = true;
	}else {
		itsNine = false;
	}
}
function loadRepres(){
	//allRepres;
	var myTempRep = {};
	var innerHTML= '';
	$.getJSON('repData/candidates-min.json?1234',function(data){
		allRepres = data;
	});
}
function readVars() {
	var regions = subRegions.split('|');
	if (vars['start'] == 'polls' || vars['start'] == 'Polls' ){
		
	}
		selectMunicipality('3_1','OSLO');
	if(vars['fylke'] == 'Oslo'){
		state= 'Municipality';
	}else if(vars['fylke']){
		for(var i = 0; i < 18; i++){
			var region = regions[i].split(';');
			if (region.length == 3){
				var countyId = region[2];					
				var type = region[1];

				if (type == 4){
				
					//add leading zero if necessary
					if (countyId.length == 1)
						countyId = '0' + countyId;		

					if(vars['fylke'] == region[0]){
						
						selectCounty(countyId , region[0].toUpperCase())
						state = 'County';
					
					}
					
				}				
			}		
		}
	}
	
}

function cleanRegionArray(array){
	var newArray = [];

	for (var i=0;i < array.length;i++){
		var regionArray =  array[i].split(';');
		
		if (regionArray.length == 3){
			var regionType = regionArray[1];
			if (state == 'Polls'){
				if (regionType != '3')
					newArray.push(regionArray[0]);		
			}
			else
				newArray.push(regionArray[0]);					
		}
	}
	return newArray;
}

function toggleHeaders(){
	var mainCircleRegionText = document.getElementById("mainCircleRegionText");
	var mainCircleDescriptionText = document.getElementById("mainCircleDescriptionText");
    var mainCircleHeaderText = document.getElementById('mainCircleHeaderText');
   
	if (mainCircleRegionText && mainCircleDescriptionText && mainCircleHeaderText){
	    mainCircleHeaderText.innerHTML = '';
	    mainCircleRegionText.innerHTML = '';
		mainCircleDescriptionText.innerHTML = '';

		
	}
}

function refreshCounties(municipalityElection,isFav){
	if (municipalityElection && isValg == false){
		notYetInfo();
		
		var headerHeight = $("#header").height();
		var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
		var ContentTop =  $("#header").height() + menyHeight;
		$(function(){
			$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
		});
	}else{
	var innerHTML = '';
	var regions = subRegions.split('|');
	
	if(isFav){
		
		var regionsDiv = document.getElementById('favSelectorCounty');
	}else {
		var regionsDiv = document.getElementById('selectCounty');
	}		
	
	if (regionsDiv){			
		//regionsDiv.innerHTML = ''; //clear countyselect div
		innerHTML += "<div class='regionWrapper'> ";
		//loop 18 (not Oslo) first elements, this is counties (type = 4), e.g. �stfold;4;1|M�re og Romsdal;4;15
		//if regionsDiv is used in Municipality election, include Oslo
		for(var i = 0; i < 18; i++){
			var region = regions[i].split(';');
			if (region.length == 3){
				var countyId = region[2];					
				var type = region[1];

				if (type == 4){
				
					//add leading zero if necessary
					if (countyId.length == 1)
						countyId = '0' + countyId;		
				
					var action;
					if (municipalityElection){
						if (isFav){
							if (isValg == false ) {
								action = "onclick='notYetInfo();'";
							}else {
								action = "onclick=\"refreshMunicipalities('" + countyId + "', true);\"";
							}
						}else{
							action = "onclick=\"refreshMunicipalities('" + countyId + "');\"";
							
						}
					}else{
						if (isFav){
							//saveFavouriteSelector('00m0301','Oslo','true');
							action = "onclick=\"saveFavouriteSelector('" + countyId + "','" + region[0].toUpperCase() + "', 'true');\"";
						}else
							action = "onclick=\"selectCounty('" + countyId + "','" + region[0].toUpperCase() + "');\"";
						}
					
					innerHTML += "<div " + action + " class='boxLink'>" + region[0].toUpperCase() + "</div>";

					if (i < 17)
						innerHTML += ", ";
					
					if (municipalityElection && i == 1){
						if (isFav){
							innerHTML += "<div class='boxLink' onclick='refreshMunicipalities(3, true);'>OSLO</div>, ";
						}else
							innerHTML += "<div class='boxLink' onclick='refreshMunicipalities(3);'>OSLO</div>, ";

					}else if (municipalityElection && i == 17) {
						innerHTML += "<br />";
						if (isFav){
							innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"refreshMunicipalities('12_1', true);\">BERGEN</div>, ";
							innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"refreshMunicipalities('16_1', true);\">TRONDHEIM</div>, ";
							innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"refreshMunicipalities('11_3', true);\">STAVANGER</div> ";
						}else{
						innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"refreshMunicipalities('12_1');\">BERGEN</div>, ";
						innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"refreshMunicipalities('16_1');\">TRONDHEIM</div>, ";
						innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"refreshMunicipalities('11_3');\">STAVANGER</div> ";
						}
					}else if (i == 1){
						if (isFav){
							innerHTML += "<div class='boxLink' onclick=\"saveFavouriteSelector('00m0301','OSLO','true');\">OSLO</div>, ";
						}else
							innerHTML += "<div class='boxLink' onclick=\"selectCounty('00m0301','OSLO');\">OSLO</div>, ";

					}else if( i == 17 ) {
						if (isFav){
							
						}else if (!municipalityElection) {
							innerHTML += "<br />";
							innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"selectCounty('12_1','BERGEN');\">BERGEN</div>, ";
							innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"selectCounty('16_1','TRONDHEIM');\">TRONDHEIM</div>, ";
							innerHTML += "<div class='boxLink' style='font-style: italic;' onclick=\"selectCounty('11_3','STAVANGER');\">STAVANGER</div> ";
						}
					}
				}				
			}		
		}
	
	}
	/*var headerHeight = $("#header").height();
		var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
		$("#mainCircle").attr('style','top:' + menyHeight +'px;');*/
		
		
		innerHTML += "</div>";	
		regionsDiv.innerHTML = innerHTML;
		
		var headerHeight = $("#header").height();
		var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
		var ContentTop =  $("#header").height() + menyHeight;
		$(function(){
			$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
		});
	}
}

function refreshMunicipalities(selectedCountyId, isFav){
	var regions = subRegions.split('|');
	var innerHTML = '';
	
	if (isFav)
		var regionsDiv = document.getElementById('favSelectorMuni');
	else
		var regionsDiv = document.getElementById('selectMunicipality');
			
	if (regionsDiv){			
		//regionsDiv.innerHTML = ''; //clear countyselect div
		innerHTML += "<div class='regionWrapper'>";	
	    var addedSep = false;
		//Agdenes;2;16_22|Fredrikstad;2;1_6			
		for(var i = 0; i < regions.length; i++){
			
			if (selectedCountyId == '12_1' || selectedCountyId == '16_1' || selectedCountyId == '11_3' ) {
				var region = regions[i].split(';');
				
				if (region.length == 3){
					
					var municipalityId = region[2];
					var regIds = municipalityId.split('_');				
					var type = region[1];
					if ( (regIds.length == 3 && regIds[0]+"_"+regIds[1] == selectedCountyId)|| municipalityId == selectedCountyId ) {
						if (type == 3 & !addedSep) {
							innerHTML = innerHTML.substring(0, innerHTML.length - 2);
							innerHTML += "<br/>";
							addedSep = true;
						}
						
						if (isFav)
						
							innerHTML += "<div style='font-size:8pt;' class='regionSelectLink boxLink' onclick=\"saveFavouriteSelector('" + region[2] + "','" + region[0].toUpperCase() + "', 'false');\">" + region[0].toUpperCase() + "</div>, "
						else
						innerHTML += "<div style='font-size:8pt;' class='regionSelectLink boxLink' onclick=\"selectMunicipality('" + region[2] + "','" + region[0].toUpperCase() + "');\">" + region[0].toUpperCase() + "</div>, ";
					}
					
				}
			}else {
			
				var region = regions[i].split(';');
				
				if (region.length == 3){
					var municipalityId = region[2].substring(0,2);					
					var type = region[1];
						
					//remove '_' and add leading zero
					
					if (municipalityId.substring(1) == '_')
						municipalityId = '0' + municipalityId.substring(0,1);

					if (municipalityId == selectedCountyId && (type != 4)) {
						
						if (type == 3 & !addedSep ) {
							innerHTML = innerHTML.substring(0, innerHTML.length - 2);
							innerHTML += "<br/>";
							addedSep = true;
						}
						var favminiid = region[2];	
						if (favminiid.charAt(1) == '_'){
							var favRegion = '0' + favminiid;
						}else {
							var favRegion = region[2];
						
						}
						if (isFav){
							if ((type == 3 && selectedCountyId == 3 ) || type != 3)
								innerHTML += "<div style='font-size:" + (selectedCountyId == 3 ? '8pt' : '10.5pt') + ";' class='regionSelectLink boxLink' onclick=\"saveFavouriteSelector('" + favRegion + "','" + region[0].toUpperCase() + "', 'false');\">" + region[0].toUpperCase() + "</div>, "
						}else{
							if ((type == 3 && selectedCountyId == 3 ) || type != 3)
								innerHTML += "<div style='font-size:" + (selectedCountyId == 3 ? '8pt' : '10.5pt') + ";' class='regionSelectLink boxLink' onclick=\"selectMunicipality('" + region[2] + "','" + region[0].toUpperCase() + "');\">" + region[0].toUpperCase() + "</div>, ";	
						}
					}				
				}	
			}
		}
		
		//remove last comma and whitespace
		innerHTML = innerHTML.substring(0, innerHTML.length - 2);
		innerHTML += "</div>";
		regionsDiv.innerHTML = innerHTML;
		openMenu(regionsDiv.id);
	}
	if (isFav) {
	var headerHeight = $("#header").height();
	var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
	var ContentTop =  $("#header").height() + menyHeight;
	$(function(){
		$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
		$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
	});
	}else {
	var headerHeight = $("#header").height();
		var menyHeight = 375 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'375px'}, {duration:300,queue:false});
		});
		}
}

function selectCounty(countyId, countyName, fillMuniDropDown, fillMuniId){
	$('#topmenuButtonCountyLink').removeClass('isMyFav');
	$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
	$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
	
	var headerHeight = $("#header").height();
		var menyHeight = 134 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'134px'}, {duration:300,queue:false});
		});
	
	
	isFavorite = false;
	parseFavorites(countyId, countyId, countyName,true);
	$('#kartModulWrapperCounty').svg('destroy');
	if (countyId == '12_1'){
		countyId = '12m1201';
		region = 'c12m1201';
	}else if (countyId == '16_1'){
		countyId = '16m1601';
		region = 'c16m1601';
	}else if (countyId == '11_3') {
		countyId = '11m1103';
		region = 'c11m1103';
	}else {
		region = 'c' + countyId;
	}
	regionName = countyName;
	var favhtml ='';
	if (state == 'countyPolls'){
		if (isFavorite == true){
			$("#topmenuButtonCountyPollLink").html(regionName).addClass('isMyFav');
		}
		else
			$("#topmenuButtonCountyPollLink").html(regionName).removeClass('isMyFav');
	}else{
		if (isFavorite == true){
			$("#topmenuButtonCountyLink").html(regionName).addClass('isMyFav');
		}
		else
			$("#topmenuButtonCountyLink").html(regionName).removeClass('isMyFav');
	}
	
	if (fillMuniDropDown)
		
	    fillMunicipalityDropDown(region, fillMuniId);
	else {
		$('#blackOverlay').attr('style','display:block;');
	    countyElectionMode = true;
		var tempLink = 'images/kart/'+countyId+'_small.svg';

		if (isValg == true) {
			$('#kartModulWrapperCounty').svg({loadURL: tempLink,onLoad: mySvgLoaded});
		}else {
			reloadContent();
		}
		if (state == 'County')
			closeMenu('selectCounty');
		else
			closeMenu('pollMenuCounty');
	}
}

function selectMunicipality(municipalityAndCountyId, municipalityName, isKartMeny){
	$('#topmenuButtonCountyLink').removeClass('isMyFav');
	$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
	$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
	isFavorite = false;
	parseFavorites(countyId, countyId, municipalityName,true);
	$('#kartModulWrapperCounty').svg('destroy');
	if (isKartMeny){
		kartMeny = isKartMeny;
		
	}
	var headerHeight = $("#header").height();
		var menyHeight = 134 + headerHeight;
		$(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:'134px'}, {duration:300,queue:false});
		});
	
	//state = Municipality
	//municipalityAndCountyId format 1_6 or 16_22
	var idElements = municipalityAndCountyId.split('_');
	if (isFavorite == true){
			$("#topmenuButtonMunicipalityLink").html(municipalityName).addClass('isMyFav');
		}
		else
			$("#topmenuButtonMunicipalityLink").html(municipalityName).removeClass('isMyFav');
	if (idElements.length >= 2){
		var countyId = idElements[0];
		var municipalityId = idElements[1];
		var id;

		if (countyId.length == 1)
			countyId = '0' + countyId;
		
		if (municipalityId.length == 1)
			municipalityId = '0' + municipalityId;
		
		id = countyId + municipalityId;

		if (countyId == '03' && idElements.length > 2)
		    countyId = '00';

	    id = 'c' + countyId + 'm' + id;

	    var districtId = '';
	    if (idElements.length > 2) { //district
	        districtId = idElements[2];
			if (countyId != '00'){
				for (var i = districtId.length; i < 4; i++)
					districtId = "0" + districtId;
			}
	    }

	    region = id + ((districtId == '') ? '' : 'd' + districtId);

		//region = id;
		regionName = municipalityName;
		countyElectionMode = false;

		reloadContent();
	    
    }
	
	//ensure both countyselect and municipalityselect menus are closed
	closeMenu('selectCounty');
	closeMenu('selectMunicipality');
	
}



function selectPollRegion() {
	if (pollMenuMunicipalities.selectedIndex != 0) {
        selectMunicipality(pollMenuMunicipalities.value, pollMenuMunicipalities.options[pollMenuMunicipalities.selectedIndex].text);        
    }
    else if (pollMenuCounties.selectedIndex != 0) {
        selectCounty(pollMenuCounties.value, pollMenuCounties.options[pollMenuCounties.selectedIndex].text, false);        
    }
    //closePollMenu(); // BIANCA FIX
}


function fillMunicipalityDropDown(selectedCountyId, fillMuniId){
	selectedCountyId = selectedCountyId.substring(1); //remove 'c';
	
    var regions = subRegions.split('|');
    var dropDownId = (fillMuniId)? fillMuniId : 'pollMenuMunicipalities';
	var regionsDropDown = document.getElementById(dropDownId);
				
	if (regionsDropDown){			
		regionsDropDown.options.length = 0;
		
		//add prompt
		var optn = document.createElement("OPTION");
		optn.text = 'Velg kommune';
		optn.value = 0;
		regionsDropDown.options.add(optn);

		//Agdenes;2;16_22|Fredrikstad;2;1_6	
		for(var i = 0; i < regions.length; i++){
			var region = regions[i].split(';');
			if (region.length == 3){
				var currentCountyId = region[2].substring(0,2);					
				var type = region[1];
					
				//remove '_' and add leading zero
				if (currentCountyId.substring(1) == '_')
					currentCountyId = '0' + currentCountyId.substring(0,1);			
								
				if (currentCountyId == selectedCountyId && (type != 4)){
					var optn = document.createElement("OPTION");
					optn.text = region[0];
					optn.value = region[2];
					regionsDropDown.options.add(optn);
				}				
			}		
		}
	}
}

function getIndexFromValue(countyValue, value){
	var index = 0;
	var regionsDropDown = document.getElementById('pollMenuMunicipalities');
	if (regionsDropDown){
		for (var i = 0; i < regionsDropDown.options.length; i++){
			var option = regionsDropDown.options[i];
			
			if (option.value == countyValue + '_' + value){
				index = i;
				break;
			}
		}	
	}	
	
	return index;
}

function selectPoll(id, index, componentId,headline) {
	saeuleArray = [];
    var partyResultsRaw = currentPollCollectionResults[id];
	var partyResultsRawM = currentPollCollectionResultsM[id];
    var partyResults = "";
	var partyResultsM = "";
    
    if (partyResultsRaw){
    	partyResults = partyResultsRaw.split('|');
    }
    else{
    	
    	index = -1;
    }
    
    if (stateCheck == 'Home'){
		
		var component = document.getElementById('reloadableComponent');
		if (component){    
       	switch (homePage){
       		case 'votes':	
				
				component.innerHTML = parseColumns(partyResults, index, headline);
				var myhero = saeuleArray;
				
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
				//$('.saeule').animate({height:'1%'});
       			break;
       		case 'mandates': //only used for seatsrepresentatives
				if (state == 'Polls')
					component.innerHTML = centerComponent(parseMandatesWeb(partyResults, index,headline));
				else 
					component.innerHTML = centerComponent(parseReps(partyResults, index,headline));
       			break;
       	}
       	
       	var select = document.getElementById("pollSelect_" + componentId);
        if (select) select.style.display = "block";
	}
	}else {
		var component = document.getElementById(componentId);
		if (component){    
       	switch (component.id){
       		case 'votes':	
			
				component.innerHTML = parseColumns(partyResults, index, headline);
				var myhero = saeuleArray;
				
				for ( var i = 0 ; i < myhero.length; i++) {
					$('.saeule'+ i).height(0);
					$('.saeule'+ i).animate({height: ""+myhero[i]+"px"});
				}
       			break;
       		case 'mandates': //only used for seatsrepresentatives
				if (state == 'Polls')
					component.innerHTML = centerComponent(parseMandatesWeb(partyResults, index,headline));
				else 
					component.innerHTML = centerComponent(parseReps(partyResults, index,headline));
       			break;
       	}
       	
       	var select = document.getElementById("pollSelect_" + componentId);
        if (select) select.style.display = "block";
	}
	}
    /*if (component){    
       	switch (component.id){
       		case 'votes':	
				component.innerHTML = parseColumns(partyResults, index, headline);
				
       			break;
       		case 'mandates': //only used for seatsrepresentatives
				if (state == 'Polls')
					component.innerHTML = centerComponent(parseMandatesWeb(partyResults, index,headline));
				else 
					component.innerHTML = centerComponent(parseReps(partyResults, index,headline));
       			break;
       	}
       	
       	var select = document.getElementById("pollSelect_" + componentId);
        if (select) select.style.display = "block";
	}*/
}

function checkHistoryLegend(checkBox){

	if (checkBox.checked){
		historyExceptions[checkBox.id - 1] = 1;
	}
	else{
		historyExceptions[checkBox.id - 1] = 0;	
	}

    openHistory();
}

function checkChangeElection(checkBox) {
    historyShowStortingResults = checkBox.checked;
    openHistory();
}

function selectAllHistoryLegend(){
	for (var index=0;index < historyExceptions.length;index++)
		historyExceptions[index] = 1;
	
	openHistory();
}

function deSelectAllHistoryLegend(){
	for (var index=0;index < historyExceptions.length;index++)
		historyExceptions[index] = 0;
	
	openHistory();
}


function reloadContent(){
time1 = $.now();

	var filePrefix = null;

	if (state == 'Polls' || state == 'countyPolls' ||  isValg == false){
	    if (region == 'n')
	        filePrefix = 'P05';
	    else if (region.length == 3) //county
	        filePrefix = 'P04';
	    else { //municipality
	        if (region == 'c00m0301') //Oslo
	            filePrefix = 'P04';
            else
                filePrefix = 'P02';
	    }
	}
	else if (state == 'Nation'  ){ 
		//filePrefix = 'F05';
		filePrefix = 'ST06';
	}else if (state == 'Storting'){
		
		filePrefix = ((isValg) ? 'ST06' : 'P05');
		region = 'n';
	}
	else if (state == 'County'){
		if (region == 'c12m1201' || region == 'c16m1601'   || region == 'c11m1103'){
			 filePrefix = (countyElectionMode) ? 'ST02' : 'ST02';
		}else  {
			
			filePrefix = (countyElectionMode) ? 'ST04' : 'ST04';
		}
	}
	else if (state == 'Municipality'){
	    filePrefix = (countyElectionMode) ? 'ST02' : ((region.length > 8) ? 'ST03' : 'ST02');
		if (region.substring(0, 9) == "c00m0301d") {
			filePrefix = 'ST05';
		}
	}
    else if (state == 'Duellen') {
	
			filePrefix = ((isValg) ? 'ST06' : 'P05');
			region = 'n';
	}
	 else if (state == 'Sok') {
	
			filePrefix = ((isValg) ? 'ST06' : 'P05');
			region = 'n';
	}
	if (filePrefix){
		
		if (dataDir.substring(dataDir.length - 1) != '/')
				dataDir += "/";
		//if (state == 'Polls' || state == 'countyPolls' ||  isValg == false){
		//	var dataFile = dataDir + filePrefix + '-n.txt?p=' + Math.random();
		//}else {
			if (region == 'c00m0301' && state == 'countyPolls') //Oslo
				var dataFile = dataDir + filePrefix + '-c03.txt?p=' + Math.random();
			else
				var dataFile = dataDir + filePrefix + '-' + region + '.txt?p=' + Math.random();
		//}
		var xmlhttp;
		if (window.XMLHttpRequest){
			// code for IE7+, Firefox, Chrome, Opera, Safari
			if($.browser.msie && parseInt($.browser.version) == 9){
				if(window.XDomainRequest){
			var xdr = new XDomainRequest();
		
			if (xdr) {
				xdr.onload = function() {
					parseText(xdr.responseText);
				};
				xdr.onerror = function () {
					parseNotFound(xdr.responseText);
				};
				xdr.onprogress = function() {};
				xdr.open("GET",dataFile);
				xdr.send();
			}
		}
			}else{
				xmlhttp=new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {

	        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	            parseText(xmlhttp.responseText);
	        }
	        else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
	            parseNotFound(xmlhttp.responseText);
	        }
			};
			xmlhttp.open("GET",dataFile,true);
			xmlhttp.send()
			}
		}
		else{
			// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");	
			xmlhttp.onreadystatechange = function() {

	        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	            parseText(xmlhttp.responseText);
	        }
	        else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
	            parseNotFound(xmlhttp.responseText);
	        }
			};
			xmlhttp.open("GET",dataFile,true);
			xmlhttp.send();
		}

	   /* xmlhttp.onreadystatechange = function() {

	        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	            parseText(xmlhttp.responseText);
	        }
	        else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
	            parseNotFound(xmlhttp.responseText);
	        }
	    };
  		
		//xmlhttp.open("GET",dataFile,true);
		//xmlhttp.send();
	try {
		xmlhttp.open("GET",dataFile,true);
		xmlhttp.send();	
	}catch(ex) {
		if(window.XDomainRequest){
			var xdr = new XDomainRequest();
		
			if (xdr) {
				xdr.onload = function() {
					parseText(xdr.responseText);
				};
				xdr.onerror = function () {
					parseNotFound(xdr.responseText);
				};
				xdr.open("GET",dataFile);
				xdr.send();
			}
		}
	}*/
	
		//writeDebugInfo('state=' + state + ' : region=' + region + ' : countyElectionMode=' + countyElectionMode + ' : datafile=' + dataFile);
	}
}

function loadHistory(){

	if (dataDir.substring(dataDir.length - 1) != '/')
		dataDir += "/";

	var dataFile = dataDir + 'HIST.txt';
		
	var xmlhttp;
	if (window.XMLHttpRequest){
		// code for IE7+, Firefox, Chrome, Opera, Safari
		if($.browser.msie && parseInt($.browser.version) == 9){
			if(window.XDomainRequest){
				var xdr = new XDomainRequest();
				if (xdr) {
					xdr.onload = function() {
						parseTextHistory(xdr.responseText);
					};
					xdr.onerror = function () {
						parseNotFoundHistory(xdr.responseText);
					};
					xdr.onprogress = function() {};
					xdr.open("GET",dataFile);
					xdr.send();
				}
			}
		}else{
			xmlhttp=new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					parseTextHistory(xmlhttp.responseText);
				}
				else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
					parseNotFoundHistory(xmlhttp.responseText);
				}
			};
			xmlhttp.open("GET",dataFile,true);
			xmlhttp.send()
		}
	}
	else{
		// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");	
		xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            parseTextHistory(xmlhttp.responseText);
        }
        else if (xmlhttp.readyState == 4 && xmlhttp.status == 404) {
            parseNotFoundHistory(xmlhttp.responseText);
				
        }
		
		};
		xmlhttp.open("GET",dataFile,true);
		xmlhttp.send();
	}
}

var tooltipTimeout;
function showTooltip(e, partyId, percent, label, x, y){	

	if (tooltipTimeout)
		clearTimeout(tooltipTimeout);
	
	var partyColor = getPartyColor(partyId);
	var partyName = (parties[partyId] != undefined) ? parties[partyId].name : '';
	var partyShortName = (parties[partyId] != undefined) ? parties[partyId].shortName : '';
	
	var div = document.getElementById("historyTooltip");
	if (div){
		div.style.visibility = "visible";
		//div.style.opacity = 1;
		div.style.left = x + "px";
		div.style.top = y + "px";	
		div.style.border = "2px " + partyColor + " solid";
		if (state == 'Polls'){
			div.innerHTML = "<b>" + partyName + "</b>" + " (" + partyShortName  + ")<BR/>Gj.snitt landsdekkende målinger " + label + " : <b>" + percent + "%</b>";
		}else {
			div.innerHTML = "<b>" + partyName + "</b>" + " (" + partyShortName  + ")<BR/>" + label + ": <b>" + percent + "%</b>";
		}
	}	
	
	tooltipTimeout = setTimeout("hideTooltip()",2000);	
}
function showTooltip2(e, partyId, percent, label, x, y){	

	var partyColor = getPartyColor(partyId);
	var partyName = (parties[partyId] != undefined) ? parties[partyId].name : '';
	var partyShortName = (parties[partyId] != undefined) ? parties[partyId].shortName : '';
	
	var div = document.getElementById("historyTooltip");
	if (div){
		div.style.visibility = "visible";
		//div.style.opacity = 1;
		div.style.left = (x +10) + "px";
		div.style.top = y + "px";	
		div.style.border = "2px " + partyColor + " solid";
		if (state == 'Polls'){
			div.innerHTML = "<b>" + partyName + "</b>" + " (" + partyShortName  + ")<BR/>Gj.snitt landsdekkende målinger " + label + " : <b>" + percent + "%</b>";
		}else {
			div.innerHTML = "<b>" + partyName + "</b>" + " (" + partyShortName  + ")<BR/>" + label + ": <b>" + percent + "%</b>";
		}
	}	
}

function hideTooltip(){
	var div = document.getElementById("historyTooltip");
	
	if (div && div.style.visibility == "visible"){
		div.style.visibility = "hidden";
	}
}
function hideTooltip2(){
	var div = document.getElementById("fylkeTT");
	
	if (div && div.style.visibility == "visible"){
		//$("#" + div.id).animate({opacity:0}, 800);
		div.style.visibility = "hidden";
	}
}
function openFavPanel() {
	parseFavouritesWeb();
}
function closeAllPanels(butOpenMe){
	$('.displayPanel').attr('style', 'display:none');
	var displayMe = butOpenMe;
	$('#'+displayMe).attr('style', 'display:block');
	
}
function openSearchPanel() {
	$("#searchPanel").attr("style","display: block;");
	/*var headerHeight = $("#header").height();
	var menyHeight = $("#wrapperUnderMenu").height() +headerHeight;
		//$("#mainCircle").animate({top: menyHeight});
	$("#mainCircle").attr('style','top:' + menyHeight +'px;');	*/
	
	
	var headerHeight = $("#header").height();
	var menyHeight = $('#searchPanel').height()  + 50;
	var ContentTop =  $("#header").height() + menyHeight;
	$(function(){
		$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
		$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
	});
}


function goToWidgetRegion(string){
$(".underMenu").attr("style","display: none;");
	var string = string;
	//var cookie = getCookie('widget');
	$('.mMBtn').removeClass('activeMMBtn');
	if (string){
		var cookieElements = string.split("|");//cookie.split("|");
		if (cookieElements.length == 3){
			var name = cookieElements[0];
			var countyIdRaw = cookieElements[1]; //12
			var regionIdRaw = cookieElements[2]; //12_53
			
			if (countyIdRaw == regionIdRaw){ //county or nation			
				if (regionIdRaw == 0){ //nation
					if (state == "Polls" || isValg == false)
						selectNationPolls(false);
					else
						toggleState('Nation', false);
				}
	            else { //county                    
				    //state = (state == 'Polls') ? 'Polls' : 'County';
				    //(state == 'Polls' || isValg == false) ? selectNationPolls(false) : toggleState('County', false);
				    
				    var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;
					
					if (isValg == false){
						$('#topmenuButtonPolls').addClass('activeMMBtn');
						toggleState('countyPolls',false);
					}else {
						$('#topmenuButtonValg').addClass('activeMMBtn');
						toggleState('County',false);
					}	
					selectCounty(countyId, name, false);
				}
			}
			else{ //municipality, todo: district
			    //state = (state == 'Polls') ? 'Polls' : 'Municipality';
			    //					        url = "toggleState('Polls', false);pollMenuCounties.selectedIndex = " + ((regionIdElements[0] > 12) ? regionIdElements[0] - 1 : regionIdElements[0]) + ";selectCounty('" + countyId + "', '', true);var index = getIndexFromValue('" + regionIdElements[0] + "', '" + regionIdElements[1] + "');pollMenuMunicipalities.selectedIndex = index;selectPollRegion();closeSearchResults(false)";

			    //(state == 'Polls') ? selectNationPolls(false) : toggleState('Municipality', false);

			    if (state != 'Polls'){
					$('#topmenuButtonValg').addClass('activeMMBtn');
			        toggleState('Municipality', false);
			    }
			    //bug-fix 17.11.2011, Fix goto Oslo via widget, Oslo doesn't need special handling
			   // if (regionIdRaw == '3_1'){ //special case for Oslo
			    //    selectCounty('00m0301',name,false);
				//}
			   // else
			        selectMunicipality(regionIdRaw, name);
			}
		}
	}
}

function setWidgetRegion(){
	var value = 'Velg favoritt';	
	
	var cookie = getCookie('widget');
	if (cookie){
		var cookieElements = cookie.split("|");
		if (cookieElements.length == 3)
			value = cookieElements[0].toUpperCase();		
	}	
	
	var favLabel = document.getElementById('favouriteLabel');
	if (favLabel){
		favLabel.innerHTML = value;
	}	
}

function openFavouriteSelector(){
	//open selector
	
	document.getElementById('blackOverlay').style.display='block';	
	document.getElementById('favouriteSelector').style.display='block';
	
	fadeInFavouriteSelector();
}
function fadeInFavouriteSelector(){
	$("#favouriteSelector").animate({opacity:1}, 400);
}
function closeFavouriteSelector(save){
	$("#favouriteSelector").animate({opacity:0}, 400,	
		function (){
			document.getElementById('blackOverlay').style.display='none';
			document.getElementById('favouriteSelector').style.display='none';
		
			if (save)
				setWidgetRegion();
		}
	);
}
function removeFromFavourites(id) {
	    var favourites = prefs.load();

	    if ($.isArray(favourites)) {

	        for (var i = 0; i < favourites.length; i++) {
	            var fav = favourites[i];
	            if (i == id) {
	                favourites.splice(i, 1);
	                break;
	            }
	        }

	        prefs.data = serialize(favourites);
	        prefs.save('valg2013Fav');
	        parseFavouritesWeb();
	    }
		var headerHeight = $("#header").height();
		var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
		var ContentTop =  $("#header").height() + menyHeight;
		$(function(){
			$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
		});
}
function removeFromFavourites2(name) {
	    var favourites = prefs.load();

	    if ($.isArray(favourites)) {

	        for (var i = 0; i < favourites.length; i++) {
	            var fav = favourites[i];
	            if (favourites[i].name.toUpperCase() == name) {
	                favourites.splice(i, 1);
	                break;
	            }
	        }

	        prefs.data = serialize(favourites);
	        prefs.save('valg2013Fav');
			if (state == 'Fav') {
				parseFavouritesWeb();
			}else {
			
			var myregion = getRegionId(regionName);
			if (state == 'countyPolls' || state == 'County'){
			$('#favHolderButton').html("<div id='isFavorite' onclick=\"saveFavouriteSelector('"+myregion+"','"+regionName.toUpperCase()+"','true')\"><img src='images/webFavOff.png' /></div>");
			}else {
				$('#favHolderButton').html("<div id='isFavorite' onclick=\"saveFavouriteSelector('"+myregion+"','"+regionName.toUpperCase()+"','false')\"><img src='images/webFavOff.png' /></div>");
			}
			$('#topmenuButtonCountyLink').removeClass('isMyFav');
			$('#topmenuButtonMunicipalityLink').removeClass('isMyFav');
			$('#topmenuButtonCountyPollLink').removeClass('isMyFav');
			
			}
	}
}
function saveFavouriteSelector(favId, favName, isFavCounty){
	var favourites = prefs.load();
	//var countyId = favouriteSelectorCounties.selectedIndex;
	//var muniId = favouriteSelectorMunicipalities.selectedIndex;
	var name = favName;
	var id = favId;
	var muniId = favId;
	var isCounty = isFavCounty;
	var cookieValue = '';
	//if (favouriteSelectorMunicipalities.selectedIndex != 0){
	if ( isCounty == 'false' || isCounty == false) { //bianca
	
       // muniId = favouriteSelectorMunicipalities.value;
       // name = favouriteSelectorMunicipalities.options[favouriteSelectorMunicipalities.selectedIndex].text;
        
       // var muniElements = muniId.split('_');
		var muniElements = id.split('_');
        if (muniElements.length == 2 || muniElements.length == 3 ){
        	id = muniElements[0];
        }
        if ($.isArray(favourites)){
		
			var newFav = {name: name , countyId: id, muniId: muniId};
			if(!eksistsInArrayWeb(newFav, favourites))
				favourites.unshift(newFav);
				
				
		}else {
        //cookieValue = name + '|' + countyId + '|' + muniId;
			favourites = [{name: name , countyId: id, muniId: muniId}];
			
		}
	}
   // else if (favouriteSelectorCounties.selectedIndex != 0) {
	else if (isCounty == 'true' || isCounty == true) {
        //if (favouriteSelectorCounties.selectedIndex != 3){
		if (name != 'Oslo'){
        	//countyId = parseInt(favouriteSelectorCounties.value,10);
        	//name = favouriteSelectorCounties.options[favouriteSelectorCounties.selectedIndex].text;
        	
			if ($.isArray(favourites)){
			var newFav = {name: name , countyId: id, muniId: id};
			if(!eksistsInArrayWeb(newFav, favourites))
				favourites.unshift(newFav);
			}else {
			
				favourites = [{name: name , countyId: id, muniId: id}];
			}
			//cookieValue = name + '|' + countyId + '|' + countyId;
        }
        else{ //Oslo special treatment
        	id = '00m0301';
        	//name = favouriteSelectorCounties.options[favouriteSelectorCounties.selectedIndex].text;
        	
			if ($.isArray(favourites)){
			var newFav = {name: name, countyId: id , muniId:id};
			if(!eksistsInArrayWeb(newFav, favourites))
				favourites.unshift(newFav);
			}else {
			
				favourites = [{name: name, countyId: id , muniId:id}];
			}
        }        
    }

prefs.data = serialize(favourites);
prefs.save('valg2013Fav');
	closeFavouriteSelector(true);
	if (state != 'Fav'){
		if (state == 'County' || state == 'countyPolls'|| state == 'Municipality' )
			$('#favHolderButton').html("<div id='isFavorite' onclick=\"removeFromFavourites2('"+regionName.toUpperCase()+"')\"><img src='images/webFavOn.png' /></div>");
			if (state == 'Municipality')
				$('#topmenuButtonMunicipalityLink').addClass('isMyFav');
			else if (state == 'countyPolls')
				$('#topmenuButtonCountyPollLink').addClass('isMyFav');
			else
				$('#topmenuButtonCountyLink').addClass('isMyFav');
	}else {
		openFavPanel();
	
		var headerHeight = $("#header").height();
		var menyHeight = $('.favWrapper').height() + $(".favouriteContainer").height() + 50;
		var ContentTop =  $("#header").height() + menyHeight;
		$(function(){
			$("#mainCircle").animate({top: ContentTop},{duration:300,queue: false});
			$("#wrapperUnderMenu").animate({height:menyHeight}, {duration:300,queue:false});
		});
	}
}

function setCookie (name, value, exp_y, exp_m, exp_d, path, domain, secure){
	var cookie_string = name + "=" + value;

	if (exp_y){
    	var expires = new Date ( exp_y, exp_m, exp_d );
    	cookie_string += "; expires=" + expires.toGMTString();
  	}	

  	if (path)
    	cookie_string += "; path=" + escape ( path );

  	if (domain)
        cookie_string += "; domain=" + escape ( domain );
  
  	if (secure)
        cookie_string += "; secure";

  	document.cookie = cookie_string;
}
function getCookie (cookie_name){
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

  if ( results )
    return ( unescape(results[2]) );
  else
    return null;
}
function deleteCookie (cookie_name){
	var cookie_date = new Date ( );  // current date & time
  	cookie_date.setTime ( cookie_date.getTime() - 1 );
  	document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}

//function showLastWeekPolls(){
function gotoSelectLastWeekPoll(value, pollText){
	
	//value = ID-1200;20110629;kommune;5_28
	//pollText = Kommunestyrevalg i �stre Toten (Sentio for Oppland Arbeiderblad)
	var hL = pollText;
	var pollElements = value.split(";");
	if (pollElements.length == 4){
		var pollId = pollElements[0];
		var pollDateRaw = pollElements[1];
		var pollType = pollElements[2];
		var regionIdRaw = pollElements[3];
			
		//fix url
		switch (pollType){
			case 'kommune':
				//regionIdRaw = 12_65	or 1_6
				//regionIdRaw = 0 special case - spurt om kommunevalg i hele landet
				if (regionIdRaw == '0'){
				
					pollMenuMunicipalities.selectedIndex = 0;
					pollMenuCounties.selectedIndex = 0;
					selectNationPolls(false);					
				}
				else{
					var regionIdElements = regionIdRaw.split('_');											
					if (regionIdElements.length == 2){
						var countyId = regionIdElements[0].length > 1 ? regionIdElements[0] : '0' + regionIdElements[0];							
						if (countyId == '03') 
							countyId = '00';						
						toggleState('Polls', false);
						pollMenuCounties.selectedIndex = ((regionIdElements[0] > 12)? regionIdElements[0] - 1:regionIdElements[0]);
						selectCounty(countyId, '', true);
						
						var index = getIndexFromValue(regionIdElements[0], regionIdElements[1]);
						pollMenuMunicipalities.selectedIndex = index;
						selectPollRegion();						
					}					
				}				
				break;
			case 'fylke':
				//regionIdRaw = 1 or 12
				var countyId = regionIdRaw.length > 1 ? regionIdRaw : '0' + regionIdRaw;					
				toggleState('Polls', false);
				pollMenuMunicipalities.selectedIndex = 0;
				pollMenuCounties.selectedIndex = ((regionIdRaw > 12)? regionIdRaw - 1:regionIdRaw);
				selectPollRegion();				
				break;
			case 'riks':
				pollMenuMunicipalities.selectedIndex = 0;
				pollMenuCounties.selectedIndex = 0;
				selectNationPolls(false);					
				break;
		}
	}
}


var _id;
function showMainCircle(id){
	var promise = '';
	_id = id;
	
	var headerHeight = $("#header").height();
		var menyHeight = headerHeight;

		$("#mainCircle").stop();
		$("#wrapperUnderMenu").stop();
		promise = $(function(){
			$("#mainCircle").animate({top: menyHeight},{duration:100,queue: false});
			$("#wrapperUnderMenu").animate({height:0}, {duration:100,queue:false});
			document.getElementById('circleContainer').style.display='none';
			$(".underMenu").attr("style","display: none;");
		}).promise(); //.done(function(){});	
	promise.done(function() {	

	if(id == 'kart'|| id == 'votes' || id == 'mandates' ||id == 'parties'|| id == 'history'||id == 'table'){
		$("#valgMaalingWrapper").attr("style","display: block;");
		$("#storLeftBtn").attr("style","display: none;");
		$("#storRightBtn").attr("style","display: none;");
		$("#valgLeftBtn").attr("style","display: block;");
		$("#valgRightBtn").attr("style","display: block;");
		fadeInMainPanel($('#valgMaalingWrapper'));
		
	}else if ( id == 'mandatesBlock'|| id == 'gallerier'|| id == 'Partier') {
		$("#reloadableComponent").html('');	
		$("#stortingWrapper").attr("style","display: block;");
		fadeInMainPanel($('#stortingWrapper'));	
		$("#valgLeftBtn").attr("style","display: none;");
		$("#valgRightBtn").attr("style","display: none;");
		$("#storRightBtn").attr("style","display: block;");
		$("#storLeftBtn").attr("style","display: block;");
	}
	
	if(id == 'kart'){

		$('#valgMaalingWrapper').animate({left:'0',right:'0'});
	}else if (id == 'votes'){
		
		if (state == 'Municipality' || state == 'countyPolls'){
			
			$('#valgMaalingWrapper').animate({left:'0',right:'0'});
		}else {
			$('#valgMaalingWrapper').animate({left:'-100%',right:'100%'});
		}
	}else if (id == 'mandates'){
		if (state == 'countyPolls')
			$('#valgMaalingWrapper').animate({left:'-100%',right:'100%'});
		else
			$('#valgMaalingWrapper').animate({left:'-200%',right:'200%'});
	}else if (id == 'parties'){
		if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601'))
			$('#valgMaalingWrapper').animate({left:'-300%',right:'300%'});
		else 
			$('#valgMaalingWrapper').animate({left:'-400%',right:'400%'});
	}else if (id == 'history'){
		if (state == 'Municipality'){
			$('#valgMaalingWrapper').animate({left:'-100%',right:'100%'});
		}else if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')){
			$('#valgMaalingWrapper').animate({left:'-200%',right:'200%'});
		}else {	
			$('#valgMaalingWrapper').animate({left:'-300%',right:'300%'});
		}
	}else if (id == 'table'){
		if (region.length > 8){
			$('#valgMaalingWrapper').animate({left:'-100%',right:'100%'});
		}else if (state == 'Municipality'){
			$('#valgMaalingWrapper').animate({left:'-200%',right:'200%'});
		}else if ((region == 'c11m1103') || (region == 'c12m1201') || (region == 'c16m1601')){
			$('#valgMaalingWrapper').animate({left:'-400%',right:'400%'});
		}else {	
			$('#valgMaalingWrapper').animate({left:'-500%',right:'500%'});
		}
	}
	
	if (id == 'mandatesBlock'){
		$('#stortingWrapper').animate({left:'0',right:'0'});
		
	}else if (id == 'gallerier') {
		$('#stortingWrapper').animate({left:'-100%',right:'100%'});
		
	}else if (id == 'Partier') {
		$('#stortingWrapper').animate({left:'-200%',right:'200%'});
		
	}
	var circleCurrent = document.getElementById('circleCurrent');
	
	if (_id == 'history'){
			
		    openHistory();
		}
	toggleHeaders();
	//fadeInMainCircle();
	});
}

function fadeInMainCircle(){
	//$("#mainPanel").attr("style","display: none;");
	//fadeOutMainPanel();
	/*$("#mainCircle").animate(
	{
		//width: '678px', height: '678px', 'margin-left': '-339px', 'margin-top': '-339px'
		width:'100%'
	}, 500, finishedFadeInMainCircle);*/
}

function finishedFadeInMainCircle(){	
	$("#" + _id).animate({opacity:1}, 400);
	$("#mainCircleHeaderText").animate({opacity:1}, 400);
	$("#mainCircleRegionText").animate({opacity:1}, 400);
	$("#mainCircleDescriptionText").animate({opacity:1}, 400);

	var pollPanelSelect = document.getElementById("pollSelect_" + _id);	
	if (pollPanelSelect){
		
		if (_id == 'votes' || _id == 'mandates')
			pollPanelSelect.style.display = "block";
		else
			pollPanelSelect.style.display = "none";			
	}
	
	$("#circleCurrent").animate({opacity:1}, 400);
	$("#divCloseButton").animate({opacity:0.8}, 400, animateMainComponent);
	
}

function hideMainCircle(){	
	/*$("#" + _id).animate({opacity:0}, 400);
	$("#mainCircleHeaderText").animate({opacity:0}, 400);
	$("#mainCircleDescriptionText").animate({opacity:0}, 400);
	$("#mainCircleRegionText").animate({opacity:0}, 400);
	$("#circleCurrent").animate({opacity:0}, 400);	
	$("#divCloseButton").animate({opacity:0}, 400, fadeOutMainCircle);*/
}

function fadeOutMainCircle(){

	/*$("#mainCircle").animate(
	{
		width: '0px', height: '0px', 'margin-left': '0px', 'margin-top': '0px'
	}, 500, function (){document.getElementById('blackOverlay').style.display='none';document.getElementById('mainCircle').style.display='none';document.getElementById('circleCurrent').style.display='none'});	
	*/
	//document.getElementById('mainCircle').style.display='none';
}

function fadeOutMainPanel(){
	//$("#mainPanel").animate({opacity: 0}, 1, function(){toggleCircleButtons();});
}
function fadeInMainPanel(activeDiv){
	//document.getElementById('mainPanel').style.display='block';
	if ($(activeDiv).hasClass('activeWindow')){
	
	}else {
	$(".divMainComponent").animate({opacity: 0}, 1);
	
	$(activeDiv).animate({opacity: 1}, 500);
	$(".divMainComponent").attr('style','z-index:-100;');
	$(".divMainComponent").removeClass('activeWindow');
	$(activeDiv).attr('style','z-index:1;');
	$(activeDiv).addClass('activeWindow');
	}
}


/* -------- Animation for components --------*/
function animateMainComponent(){

	switch (_id){
		case 'votes':
			animateVotesComponent();
			break;
		case 'mandates':
			animateMandatesComponent();
			break;
		case 'history':
			break;
		case 'parties':
			break;
		case 'table':
			break;
	}
}

function getMaalingColor(nr){
	
	switch (nr){
		case '0':
			 return '#EC1C24';
			break;
		case '1':
			 return '#DD5E74';
			break;
		case '2':
			 return '#009A4C';
			break;
		case '3':
			 return '#FFDD2E';
			break;
		case '4':
			 return '#0091CC';
			break;
		case '5':
			 return '#005E96';
			break;
		case '6':
			  return '#58595B';
			break;
		case '7':
			 return '#C8C5A8';
			break;
	}
}
function animateVotesComponent(){
	//alert(document.getElementById('column3'));	
}
function animateMandatesComponent(){
}

/* --------Animation for votes counted component --------*/
var t;
var maxValue = 0;
var startValue = 0;
var nrOfLoops = 10;
var increment = 0;
function animateVotesCounted(lines){
	var percentCounted = parseFloat(lines[4]);
	var widthEmpty = (100 - percentCounted);	
	
	maxValue = percentCounted;
	startValue = 0;
	increment = maxValue / nrOfLoops;
	//alert(nrOfLoops + ' - ' + increment + ' - ' + maxValue + ' - ' + startValue );
	
	clearInterval(t);
	//t = setInterval(animate, 20);
}
function animate() {
    startValue += increment;
	if(startValue < maxValue) {
	    var fillDiv = document.getElementById('votesCountedFilled');
	    if (fillDiv) {
	        fillDiv.style.width = startValue.toString() + "%";
		}
	} 
	else {
		clearInterval(t); 
	}	
}

/*function writeDebugInfo(info) {
    var debug = document.getElementById('debuginfo');
    if (debug)
        debug.innerHTML = info;
}*/

function selectParty(partyId) {
    selectParty(partyId, null);
}

function selectParty(partyId, partyResults) {
    currentSelectedPartyId = partyId;

    //ensure visibility
   $('.partyElectionMenuElement').attr('style','display:;');
   var header = document.getElementById('selectedPartyHeader');
    var logo = document.getElementById('partyHeaderImage' + partyId);
    var party = parties[partyId];
    
    if (party) {
        if (header) header.innerHTML = party.name;
       /* if (logo) {
            logo.src = 'images/logo/' + partyId + '_480_on.png';
            if (logo.parentNode) logo.parentNode.className = 'partyHeaderSelected';   
        }*/
    }

    //reset all other parties
    for (var i = 1; i < 10; i++) {

        if (i != partyId) {
            var logoOff = document.getElementById('partyHeaderImage' + i);
            if (logoOff) {
                logoOff.src = 'images/logo/' + i + '_480_off.png';
                if (logoOff.parentNode) logoOff.parentNode.className = 'partyHeader';
            }
        }
    }

    if (partyId != 9)
        selectPartyComponent(currentSelectedPartyComponent);

    if (partyId == 9 && partyResults != null)
        selectPartyOther(partyResults);
}

function selectPartyOther(partyResultsString) {
    var currentCountyId = (state == 'County') ? parseInt(region.substring(1)) : 0;

    var partyHeader = $('.selectedPartyHeader');
    if (partyHeader) $(partyHeader).attr('style','display:none;');
	
	$('.partyElectionMenuElement').attr('style','display:none;');
   /* var partyMenuTable = document.getElementsByClassName('partyMenuElection');
    if (partyMenuTable) partyMenuTable.style.display = 'none';*/

    var reloadDiv = $('.reloadablePartyComponent');
    if (reloadDiv) {
        var innerHTML = "";

        var backgroundColor = '#E4E4E4';//'#FFFFFF';

        innerHTML += "<table id='tableContainer' style='margin-top:-5px;border:7px solid #FFFFFF;margin-left:auto;margin-right:auto;' border='0' cellpadding='0' cellspacing='0'><tbody>";
        innerHTML += "<tr>";
        innerHTML += "<td class='tableHeader' style='background-color:#A3A3A3;font-weight:bold;font-size:15px;padding-left:4px;'>Parti</td>";
        //innerHTML += "<td class='tableHeader' style='padding-left:4px;'>Fylkesrep.</td>";
        //innerHTML += "<td class='tableHeader' style='padding-left:4px;border-right:1px #E4E4E4 solid;'>Kommunerep.</td>";
        innerHTML += "</tr>";
		$.each(parties,function() {
			var partyId = this.id; 
				if (partyId <= 9)
					return true;
				
                    var partyName = this.name;
                    if (backgroundColor == '#FFFFFF')
                        backgroundColor = '#E4E4E4';//'#D8E9F2';
                    else if (backgroundColor == '#E4E4E4')//'#D8E9F2')
                        backgroundColor = '#FFFFFF';

                    innerHTML += "<tr style='background-color:" + backgroundColor + "'>";
                    innerHTML += "<td class='tableData' style='text-align:left;padding-left:4px;border-bottom:1px #E4E4E4 solid;'>" + partyName + "</td>";
                    
                    innerHTML += "</tr>";
		});
        innerHTML += "</tbody></table>";

        $(reloadDiv).html(innerHTML) ;
    }
}


function selectPartyComponent(componentId) {
    currentSelectedPartyComponent = componentId;
    
    var menuElement = $('.partyElectionMenuElement' + componentId);
	$('.partyElectionMenuElement').removeClass('active');
    if (menuElement) {
        $(menuElement).addClass('active');
    }

    //reset all other menuelements
    /*for (var i = 1; i < 5; i++) {
        if (i != componentId) {
            var menuOff = document.getElementById('partyElectionMenuElement' + i);
            if (menuOff) {
                menuOff.style.color = '#C0C0C0';
            }            
        }
    }*/
	
    reloadPartyComponent();
}

function reloadPartyComponent() {

    var dataAccuArray;
    var dataArray;
    
        dataArray = partyDataSets[currentSelectedPartyId - 1 ];

    if (dataArray) {
	
        switch (currentSelectedPartyComponent) {
            case 1:
                reloadPartyReps();
                break;
            case 2:
                reloadPartyMandates(dataArray[4], dataArray[5], dataArray[6], dataArray[7]);
                break;
            case 3:
                reloadPartyChange(dataArray[0], dataArray[1], dataArray[2], dataArray[3]);
                break;
            case 4:
                reloadPartyFacts();
                break;
            default:
        }        
    }
}

function reloadPartyReps(dataAccuArray) {

    var reloadDiv = document.getElementById('reloadablePartyComponent');
    if (reloadDiv) {
        var innerHTML = "";

        if (partyAccuDataSets[currentSelectedPartyId]) {
            var maxArray = partyAccuDataSets['Max'];
            var muniMax = (maxArray) ? ((maxArray.length > 0) ? maxArray[0] : 0) : 0;
            var countyMax = (maxArray) ? ((maxArray.length > 1) ? maxArray[1] : 0) : 0;
            
            var countySeats = parseInt(partyAccuDataSets[currentSelectedPartyId][0]);
            var countySeatsChange = parseInt(partyAccuDataSets[currentSelectedPartyId][1]);
            var countyWidth = getSeatLabelWidth(countySeats, countyMax);
            var muniSeats = parseInt(partyAccuDataSets[currentSelectedPartyId][2]);
            var muniSeatsChange = parseInt(partyAccuDataSets[currentSelectedPartyId][3]);
            var muniWidth = getSeatLabelWidth(muniSeats, muniMax);
            
            innerHTML += "<table style='width:100%;' border='0' cellpadding='0' cellspacing='0'>";
            innerHTML += "<tr><td style='text-align:left;font-weight:normal;' colspan='2'>Kommunestyrepresentanter</td></tr>";
            innerHTML += "<tr><td><div class='partyRegionValueBox' style='padding-right:10px;text-align:right;width:" + muniWidth + "px;'>" + muniSeats + "</div></td><td style='padding-left:10px;width:100%;color:" + ((muniSeatsChange > 0) ? '#04B9DD' : (muniSeatsChange < 0) ? '#FF1F26' : '#000000') + "'>" + ((muniSeatsChange > 0) ? '+' : '') + muniSeatsChange + "</td></tr>";
            innerHTML += "<tr><td style='padding-top:20px;text-align:left;font-weight:normal;' colspan='2'>Fylkestingsrepresentanter</td></tr>";
            innerHTML += "<tr><td><div class='partyRegionValueBox' style='padding-right:10px;text-align:right;width:" + countyWidth + "px;'>" + countySeats + "</div></td><td style='padding-left:10px;width:100%;;color:" + ((countySeatsChange > 0) ? '#04B9DD' : (countySeatsChange < 0) ? '#FF1F26' : '#000000') + "'>" + ((countySeatsChange > 0) ? '+' : '') + countySeatsChange + "</td></tr>";
            innerHTML += "</table>";            
        }

        reloadDiv.innerHTML = innerHTML;

        
    }
}
function reloadPartyMandates(greatest, lowest, max, min) {
if (state != 'Polls' && isValg == true){
    var reloadDiv = $('.reloadablePartyComponent');
    if (reloadDiv) {
        var innerHTML = "";
        var leftTable = "";
        var rightTable = "";

        leftTable += "<table class='partyRegionTable' border='0' cellpadding='0' cellspacing='0'>";
        for (var i = 0; i < greatest.length; i++ ) {
            var grElements = greatest[i].split(";");
            if (grElements.length == 2) {
                var calcWidth = roundNumber(getLabelWidth(parseFloat(grElements[1]), max, min),2);

                leftTable += "<tr class='"+(((i%2) == 0) ? 'hasBackground' : '')+"' style='" + ((i == 0) ? 'font-weight:bold;' : '') + "'><td class='partyRegionName'>" +  getRegionName((state == 'Nation' || state == 'Storting') ? ('c' + grElements[0]) :(region == 'c00m0301' ) ? ("3_1_"+grElements[0]) :(parseRegionId(grElements[0]))).toUpperCase() + "</td>";
                leftTable += "<td class='partyRegionValue'><div class='partyRegionValueBox' style='width:" + calcWidth + "px;' >" + roundNumber(parseFloat(grElements[1]), 1) + "%</div></td></tr>";
            }
        } 
//getRegionName( (state == 'Nation' || state == 'Storting') ? ('c' + regionId) : (parseRegionId(regionId)));		
        leftTable += "</table>";

        rightTable += "<table class='partyRegionTable' border='0' cellpadding='0' cellspacing='0'>";
        for (var j = 0; j < lowest.length; j++) {
            var loElements = lowest[j].split(";");
            if (loElements.length == 2) {
                var calcWidth = roundNumber(getLabelWidth(parseFloat(loElements[1]), max, min),2);

                rightTable += "<tr class='"+(((j%2) == 0) ? 'hasBackground' : '')+"' style='" + ((j == 0) ? 'font-weight:bold;' : '') + "'><td class='partyRegionName'>" +  getRegionName((state == 'Nation' || state == 'Storting') ? ('c' + loElements[0]) :(region == 'c00m0301' ) ? ("3_1_"+loElements[0]) : (parseRegionId(loElements[0]))).toUpperCase()  + "</td>";
                rightTable += "<td class='partyRegionValue'><div class='partyRegionValueBox' style='width:" + calcWidth + "px;' >" + roundNumber(parseFloat(loElements[1]), 1) + "%</div></td></tr>";
            }
        }
		var partiName =getPartyName(currentSelectedPartyId);
		//.substring(0,10)
        rightTable += "</table>";
        innerHTML += /* <hr class='menyHR'/>*/"<div class='tablePartiHeader'>"+partiName.toUpperCase()+"</div>";
        innerHTML += "<table style='width:100%;' border='0' cellpadding='0' cellspacing='0'>";
        innerHTML += "<tr>";
        innerHTML += "<td style='text-align:left' class='partyRegionValueHeader displayBold'>ST&Oslash;RST</td>";
        innerHTML += "<td style='text-align:left;border-right:1px #000000 solid;'><img src='images/biggest.png' /></td>";
		innerHTML += "<td style='text-align:left;border-left:1px #000000 solid;' class='partyRegionValueHeader'>MINST</td>";
        innerHTML += "<td style='text-align:left;'><img src='images/smallest.png' /></td>";
       
        innerHTML += "</tr>";
        innerHTML += "<tr>";
        innerHTML += "<td colspan='2' style='border-right:1px #000000 solid;padding-right:10px;'>" + leftTable + "</td>";
        innerHTML += "<td colspan='2' style='border-left:1px #000000 solid;padding-left:10px;'>" + rightTable + "</td>";
        innerHTML += "</tr>";
        innerHTML += "</table>";

        $(reloadDiv).html(innerHTML);
    }
	}else {}
}
function reloadPartyChange(best, poorest, max, min) {
    var reloadDiv = $('.reloadablePartyComponent');
    if (reloadDiv) {
        var innerHTML = "";
        var leftTable = "";
        var rightTable = "";

        leftTable += "<table class='partyRegionTable' border='0' cellpadding='0' cellspacing='0'>";
        for (var i = 0; i < best.length; i++) {
            var bestElements = best[i].split(";");
            if (bestElements.length == 2) {
                var calcWidth = roundNumber(getLabelWidth(parseFloat(bestElements[1]), max, min), 2);

                if (parseFloat(bestElements[1]) < 0) {
                    if (calcWidth != 0) {
                        if (calcWidth < 40)
                            calcWidth = 40;
                    }
                    leftTable += "<tr class='"+(((i%2) == 0) ? 'hasBackground' : '')+"' style='" + ((i == 0) ? 'font-weight:bold;' : '') + "'><td class='partyRegionValue'><div class='partyRegionValueBoxRight' style='float:right;width:" + calcWidth + "px;' >" + roundNumber(parseFloat(bestElements[1]), 1) + "%</div></td>";
                    leftTable += "<td class='partyRegionName' style='text-align:left;'>" +getRegionName((state == 'Nation' || state == 'Storting') ? ('c' + bestElements[0]) :(region == 'c00m0301' ) ? ("3_1_"+bestElements[0]): (parseRegionId(bestElements[0]))).toUpperCase() + "</td></tr>";
                }
                else {
                    leftTable += "<tr class='"+(((i%2) == 0) ? 'hasBackground' : '')+"' style='" + ((i == 0) ? 'font-weight:bold;' : '') + "'><td class='partyRegionName'>" +getRegionName((state == 'Nation' || state == 'Storting') ? ('c' + bestElements[0]) :(region == 'c00m0301' ) ? ("3_1_"+bestElements[0]) : (parseRegionId(bestElements[0]))).toUpperCase() + "</td>";
                    leftTable += "<td class='partyRegionValue'><div class='partyRegionValueBox' style='width:" + calcWidth + "px;' >" + roundNumber(parseFloat(bestElements[1]), 1) + "%</div></td></tr>";                    
                }
            }
        }
        leftTable += "</table>";

        rightTable += "<table class='partyRegionTable' border='0' cellpadding='0' cellspacing='0'>";
        for (var j = 0; j < poorest.length; j++) {
            var poElements = poorest[j].split(";");
            if (poElements.length == 2) {
                var calcWidth = roundNumber(getLabelWidth(parseFloat(poElements[1]), max, min),2);

                if (parseFloat(poElements[1]) < 0) {
                    
                    if (calcWidth != 0) {
                        if (calcWidth < 40)
                            calcWidth = 40;
                    }
					
                    rightTable += "<tr class='"+(((j%2) == 0) ? 'hasBackground' : '')+"' style='" + ((j == 0) ? 'font-weight:bold;' : '') + "'><td class='partyRegionValueRight'><div class='partyRegionValueBoxRight' style='float:right;width:" + calcWidth + "px;' >" + roundNumber(parseFloat(poElements[1]), 1) + "%</div></td>";
                    rightTable += "<td class='partyRegionName' style='text-align:left;'>" + getRegionName((state == 'Nation' || state == 'Storting') ? ('c' + poElements[0]) :(region == 'c00m0301' ) ? ("3_1_"+poElements[0]) : (parseRegionId(poElements[0]))).toUpperCase() + "</td></tr>";
                }
                else {
                    rightTable += "<tr class='"+(((j%2) == 0) ? 'hasBackground' : '')+"' style='" + ((j == 0) ? 'font-weight:bold;' : '') + "'><td class='partyRegionName'>" + getRegionName((state == 'Nation' || state == 'Storting') ? ('c' + poElements[0])  :(region == 'c00m0301' ) ? ("3_1_"+poElements[0])  : (parseRegionId(poElements[0]))).toUpperCase() + "</td>";
                    rightTable += "<td class='partyRegionValue'><div class='partyRegionValueBox' style='width:" + calcWidth + "px;' >" + roundNumber(parseFloat(poElements[1]), 1) + "%</div></td></tr>";                    
                }
            }
        }
        rightTable += "</table>";       
		var partiName =getPartyName(currentSelectedPartyId);		
        innerHTML += /*<hr class='menyHR'/>*/"<div class='tablePartiHeader'>"+partiName.toUpperCase()+"</div>";
        innerHTML += "<table style='width:100%;' border='0' cellpadding='0' cellspacing='0'>";
        innerHTML += "<tr>";
        innerHTML += "<td style='text-align:left;border-right:1px #000000 solid;' colspan='2' class='partyRegionValueHeader'>MEST FRAM<img src='images/pad/arrow_up.png' /></td>";
        //innerHTML += "<td style='text-align:left;'></td>";
        innerHTML += "<td style='text-align:left;border-left:1px #000000 solid;' colspan='2' class='partyRegionValueHeader'>MEST TILBAKE<img src='images/pad/arrow_down.png' /></td>";
		//innerHTML += "<td style='text-align:left;'></td>";
        
        innerHTML += "</tr>";
        innerHTML += "<tr>";
        innerHTML += "<td colspan='2' style='border-right:1px #000000 solid;padding-right:10px;'>" + leftTable + "</td>";
        innerHTML += "<td colspan='2' style='border-left:1px #000000 solid;padding-left:10px;'>" + rightTable + "</td>";
        innerHTML += "</tr>";
        innerHTML += "</table>";

        $(reloadDiv).html(innerHTML);


    }
}
function reloadPartyFacts() {
    var currentCountyId = (state == 'County') ? parseInt(region.substring(1)) : 0;
    var reloadDiv = $('.reloadablePartyComponent');
	
    if (reloadDiv) {
        var innerHTML = "";
        var infoTable = "";
		var partiName =getPartyName(currentSelectedPartyId);		
        innerHTML += /*<hr class='menyHR'/>*/"<div class='tablePartiHeader'>"+partiName.toUpperCase()+"</div>";
        infoTable += "<table style='width:100%;font-weight:normal;font-size:13px;' border='0' cellpadding='0' cellspacing='0'>";
        infoTable += "<tr>";
        infoTable += "<td class='partyFactLabel'>Leder:</td>";
        infoTable += "<td class='partyFactValue'>" + /*((state == 'Nation' || state == 'Storting' ) ? */leaders[currentSelectedPartyId - 1].toString() /*: countyLeaders[currentCountyId - 1][currentSelectedPartyId - 1].toString())*/ + "</td>";
        infoTable += "</tr>";
        infoTable += "<tr>";
        infoTable += "<td class='partyFactLabel'>Stiftet:</td>";
        infoTable += "<td class='partyFactValue'>" + foundedYears[currentSelectedPartyId - 1].toString() + "</td>";
        infoTable += "</tr>";
        infoTable += "<tr>";
        infoTable += "<td class='partyFactLabel'>Beste stortingsvalg:</td>";
        infoTable += "<td class='partyFactValue'>" + /*((state == 'Nation' || state == 'Storting' ) ? */bestElections[currentSelectedPartyId - 1].toString() /*: (countyBestElections[currentCountyId - 1][currentSelectedPartyId - 1].toString() + "% (" + countyBestElectionsYear[currentCountyId - 1][currentSelectedPartyId - 1].toString() + ")"))*/ + "</td>";
        infoTable += "</tr>";
        infoTable += "<tr>";
        infoTable += "<td class='partyFactLabel'>D&aring;rligste stortingsvalg:</td>";
        infoTable += "<td class='partyFactValue'>" + /*((state == 'Nation' || state == 'Storting' ) ?*/ poorestElections[currentSelectedPartyId - 1].toString()/* : (countyPoorestElections[currentCountyId - 1][currentSelectedPartyId - 1].toString() + "% (" + countyPoorestElectionsYear[currentCountyId - 1][currentSelectedPartyId - 1].toString() + ")"))*/ + "</td>";
        infoTable += "</tr>";
        infoTable += "<tr>";
        infoTable += "<td class='partyFactLabel'>Oppslutning i 2009:</td>";
        infoTable += "<td class='partyFactValue'>" + /*((state == 'Nation' || state == 'Storting') ?*/ results2007[currentSelectedPartyId - 1].toString()/* : countyResults2007[currentCountyId - 1][currentSelectedPartyId - 1].toString())*/ + "</td>";
        infoTable += "</tr>";
        
        //if (state == 'Nation' ) {
            infoTable += "<tr>";
            infoTable += "<td class='partyFactLabel'>Slagord:</td>";
            infoTable += "<td class='partyFactValue'>" + slogans[currentSelectedPartyId - 1].toString() + "</td>";
            infoTable += "</tr>";            
        //}
        
        infoTable += "</table>";

        innerHTML += "<table style='width:100%;' border='0' cellpadding='0' cellspacing='0'>";
        innerHTML += "<tr>";
        innerHTML += "<td><img style='width:104px;' src='images/pad/leaders/p" + currentSelectedPartyId + ".jpg' /></td>"; //bianca add </td>
		//innerHTML += "<td>" + ((state == 'Nation' ) ? " <img style='width:104px;' src='images/pad/leaders/p" + currentSelectedPartyId + ".jpg' /></td>" : "&nbsp; </td>"); //bianca add </td>
        innerHTML += "<td style='vertical-align:top;'>" + infoTable + "</td>";
        innerHTML += "</tr>";
        innerHTML += "</table>";

       // reloadDiv.innerHTML = innerHTML;
	   $(reloadDiv).html(innerHTML) ;
    }
}

function getSeatLabelWidth(val, maxValue) {
    if (val == maxValue)
        return (componentWidth - 75); //make space for changelabel

    var newWidth = (componentWidth - 75); //make space for changelabel

    var percent = val / maxValue;
    newWidth = newWidth * percent;

    return newWidth;    
}


function getLabelWidth(val, maxValue, minValue) {

    if ((minValue * -1) > maxValue)
        maxValue = minValue * -1;

    if (val == maxValue)
        return 100;

    var width = 100;

    var percent = val / maxValue;
    width = width * percent;

    if (width < 0)
        width = width * -1; //always positive width

    return width;
}

var timeoutTT;
function displayFylkeTT(e,nr) {
	var posx = 0;
	var posy = 0;
	if (!e) var e = window.event;
	if (e.pageX || e.pageY) 	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY) 	{
		posx = e.clientX + document.body.scrollLeft
			+ document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop
	}	
	if (timeoutTT)
		clearTimeout(timeoutTT);
	var heightHeader = $('#header').height();	
	var div = document.getElementById("fylkeTT");
	if (div){
		div.style.visibility = "visible";
		//div.style.opacity = 1;
		div.style.left = posx + "px";
		div.style.top = posy + "px";	
		div.style.border = "2px solid #A3A3A3";
		div.innerHTML = "<b>" + getRegionName(nr).toUpperCase() + "</b>";
	}		
	timeoutTT = setTimeout("hideTooltip2()",2000);
}
function openAllFylke(nr, athis) {
	
	$('.repTT').animate({height:"0px"});
	$(".repholderBig").removeClass('activeRep');
	$(".repholder").removeClass('activeRep');
	var innerHTML = '';
	var obj = athis;
	var newObj = $(obj).closest('div').nextAll('.repGroupTT')[0]; 
	if ($(newObj).hasClass('TTsmall')){
		var container = $('div.repInnerWrapper');
	}else {
		var container = $('div.repInnerWrapperBig');
	}
	container.scrollTop(0);
	innerHTML += "<div class='dunnoWrapper'>";
	innerHTML += "<div class='bildWrapper'><img src='images/fylke/"+nr+"_small.png' /></div>";
	innerHTML += "<div class='closeMeBtn' onclick='closeGroup();'></div>";
	innerHTML += "<div class='dunWrapper'>";
	
	
	$.each(inRepres , function() {
		
		if (this.CountyId == nr) {
			innerHTML += generatePBoxF(this);
		}
	});
	innerHTML += "</div></div><div style='width:100%;display:block;padding-bottom:10px;'></div>";
	$(newObj).html(innerHTML);
	
	$(newObj).attr('style','display:block;');
	$(newObj).animate({height:"1%"},"fast",function(){
		container.scrollTop(
		container.scrollTop() + $(newObj).offset().top - container.offset().top - 35 );
		checkScrollPosition(container);	
	});
	
}
function closeGroup() {
	$('.repGroupTT').animate({height:"0px"}, function(){$('.repGroupTT').attr('style','');
	
		var container = $('div.repInnerWrapper');
		
		container.scrollTop(0);
		checkScrollPosition(container);
		});
}
function closeRepTT() {
	$('.repTT').animate({height:"0px"},function(){
	
	var container = $('div.repInnerWrapper');
		//var container2 = $('div.repInnerWrapperBig');
		container.scrollTop(0);
		//container2.scrollTop(0);
	if ($('.repTT').hasClass('TTbig')) container = $('div.repInnerWrapperBig');
		
	checkScrollPosition(container);
	});
	$(".repholderBig").removeClass('activeRep');
	$(".repholder").removeClass('activeRep');
}
function openAllParti(nr, athis){
	$('.repTT').animate({height:"0px"});
	$(".repholderBig").removeClass('activeRep');
	$(".repholder").removeClass('activeRep');
	var innerHTML = '';
	var obj = athis;
	
	var newObj = $(obj).closest('div').nextAll('.repGroupTT')[0];
	if ($(newObj).hasClass('TTsmall')){
		var container = $('div.repInnerWrapper');
	}else {
		var container = $('div.repInnerWrapperBig');
	}
	container.scrollTop(0);
	innerHTML += "<div class='dunnoWrapper'>";
	innerHTML += "<div class='bildWrapper'><img src='images/logo/parti"+nr+".png' /></div>";
	innerHTML += "<div class='closeMeBtn' onclick='closeGroup();'></div>";
	innerHTML += "<div class='dunWrapper'>";
	
	$.each(inRepres , function() {
		
		if (this.PartiOrganizationId == nr) {
			innerHTML += generatePBox(this);
		}
	});
	innerHTML += "</div></div><div style='width:100%;display:block;padding-bottom:10px;'></div>";
	$(newObj).html(innerHTML);
	
	$(newObj).attr('style','display:block;');
	$(newObj).animate({height:"1%"},"fast",function(){
		container.scrollTop(
		container.scrollTop() + $(newObj).offset().top - container.offset().top - 35 );
		checkScrollPosition(container);	
	});
		
}
function generatePBox(rep){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
	if((rep.ParliamentPeriods == '')){
		
			extraMHTML += "<img src='images/rep/grueneEckeBig.png'  />";
		}	
	var myURL = "images/rep/big/"+(rep.Picture != '' ? (rep.Picture+'_org200.png') : 'dummy.jpg' );
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == rep.CountyId){ // county fit
				if ( rep.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( rep.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
					}
					if ( rep.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='images/rep/uManBig.png' />";
					}	
				}
			}
		}
		
	}
	
	extraMHTML +="</div>";
	
	
	
	innerHTML += "<div class='partiGroup' onclick=\"openRepBox('candidate_"+rep.CandidateId+"');\" ><div class='repImgBG' style='background-image:url("+myURL+");'><div class='blender'><img src='images/rep/repCoverE.png' /></div>"+extraMHTML+"</div><div class='partiGroupName'>"+rep.FirstName.toUpperCase()+"<br/>" +rep.LastName.toUpperCase() +"<br/>"+rep.CountyName.toUpperCase()+"</div></div>";
	return innerHTML;
}
function generatePBoxF(rep){
	var innerHTML = '';
	var extraMHTML = "<div class='specialMandat'>";
	// valgmode
	if((rep.ParliamentPeriods == '')){
		
			extraMHTML += "<img src='images/rep/grueneEckeBig.png' />";
		}	
	var myURL = "images/rep/big/"+(rep.Picture != '' ? (rep.Picture+'_org200.png') : 'dummy.jpg' );
	if (isValg  ){	 
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == rep.CountyId){ // county fit
				if ( rep.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
					 if ( rep.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
						extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
					}
					if ( rep.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
						extraMHTML += "<img src='images/rep/uManBig.png' />";
					}	
				}
			}
		}
		
	}
	
	extraMHTML +="</div>";
	//style='background-image:url("+myURL+");'
	var myURL = "images/rep/big/"+(rep.Picture != '' ? (rep.Picture+'_org200.png') : 'dummy.jpg' );
	
	innerHTML += "<div class='partiGroup' onclick=\"openRepBox('candidate_"+rep.CandidateId+"');\" ><div class='repImgBG' style='background-image:url("+myURL+");'><div class='blender'><img src='images/rep/repCoverE.png' /></div>"+extraMHTML+"</div><div class='partiGroupName'>"+rep.FirstName.toUpperCase()+"<br/>" +rep.LastName.toUpperCase() +"<br/>"+rep.PoliticalPartyCode.toUpperCase()+"</div></div>";
	return innerHTML;
}
function openRepBox(repId,athis) {

	$('.repGroupTT').animate({height:"0px"}, function(){$('.repGroupTT').attr('style','');});
	
	var innerHTML = '';
	var obj = athis;
	$(".repTT").attr('style','');
	var newObj = $('#'+repId).closest('div').nextAll('.repTT')[0];
	$(newObj).html() == '';
	innerHTML += "<div class='closeMeBtn' onclick='closeRepTT();'></div>";
	if ($(newObj).hasClass('TTsmall')){
		var container = $('div.repInnerWrapper');
	}else {
		var container = $('div.repInnerWrapperBig');
	}
	container.scrollTop(0);

	$.getJSON('repData/'+repId+'_metainfo.json', function(data){
		innerHTML += "<div class='newlogoBox'>"; // DIV OPEN
		
		if (isValg  ){	 // valgmode
		
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							innerHTML += "<img src='images/rep/usikker.png' />";
						}
						if ( data.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
							//extraMHTML += "<img src='images/rep/uManBig.png' />";
						}	
					}
				}
			}
		}
		if(data.ParliamentPeriods == ''){
			innerHTML += "<img class='isNewImage' src='images/rep/isNew.png' />";
		}
		innerHTML += "</div>"; //CLOSE OPEN DIV
		innerHTML += "<div class='repBox'>";
		innerHTML += "<div class='repHeadline'>";
		innerHTML += "<div class='repPartiLogo partiLogo1'><img src='images/logo/parti"+data.PartiOrganizationId +".png' /></div>"
		innerHTML += "<div class='repName'>"+data.FirstName.toUpperCase() + " " + data.LastName.toUpperCase() +"<br/><span style='font-size:14px;display:block;margin-top:-5px;' >"+getPartyName(data.PartiOrganizationId) +" i " + data.CountyName +"</span>";
		for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							//innerHTML += "<img src='images/rep/usikker.png' />";
						}
						if ( data.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
							innerHTML += "<span style='display:block;font-size:10px;margin-top:-7px;'>Utjevningsmandat</span>";
						}	
					}
				}
			}
		innerHTML += "</div></div>";
		innerHTML += "<div class='repContent'>";
		
		if ($(newObj).hasClass('TTsmall')){
			var myURL = "images/rep/big/"+(data.Picture != '' ? (data.Picture+'_org200.png') : 'dummy_big.jpg' );
			innerHTML += "<div class='repBild'><img style='width:71px;border-radius:5px;' src='"+myURL+"' /></div>"; 
		}else {}
		
		innerHTML += "<div class='repInfo'>";
		if (data.DateOfBirth != ''){
			var birthDate = data.DateOfBirth.split('T');
			birthDate = birthDate[0].split('-');
			innerHTML += '<div class="lineWrapper"><div class="repLeft">Født:</div><div class="repRight"> '+ birthDate[2] + '.'+ birthDate[1] +'.'+ birthDate[0] +'</div></div>';
		}
		if (data.Occupation != ''){
			innerHTML += '<div class="lineWrapper"> <div class="repLeft">Stilling:</div><div class="repRight"> '+ data.Occupation +'</div> </div>';
		}
		if (data.Municipality != ''){
			var text = '';
			text = data.Municipality;
			if ( text != undefined){
			innerHTML += '<div class="lineWrapper"><div class="repLeft">Bosted:</div><div class="repRight"> '+ text +'</div></div>';
			}
		}
		
		if (data.Municipality == '' || data.Occupation == '')
			innerHTML += '<div class="lineWrapper"> &nbsp; </div>';
			
		innerHTML += "<br/>";
		if (data.Education != ''){
			innerHTML += '<div class="blockWrapper"><div class="repFL"><span class="bold">Utdanning/Yrkesbakgrunn:</span> '+ data.Education +'</div>';
		}
		if (data.Description != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.Description +'</div>';
		}
		/*if (data.ParliamentPeriods != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.ParliamentPeriods +'</div>';
		}*/
		/*if (data.Email != ''){
			innerHTML += '<div class="repFL"><span class="bold">E-Post:</span> '+ data.Email +'</div>';
		}*/
		if (data.SocialConnections != '' && data.SocialConnections != null && data.SocialConnections != 'null'){
			for (var i=0; i < data.SocialConnections.length;i++){
				//
				if (data.SocialConnections[i].Name.toUpperCase() == 'TWITTER') {
					if ( data.SocialConnections[i].UserName.charAt(0) === '@')
						var userName = data.SocialConnections[i].UserName.substr(1);
					
					innerHTML += '<br /><a href="https://twitter.com/'+ userName +'" class="twitter-follow-button" data-show-count="false" target="_blank" >Følg @'+ userName +'</a>';
				//	<a href="https://twitter.com/twitter" class="twitter-follow-button" data-show-count="false">Follow @twitter</a>
					

				}else {
					innerHTML += '<div class="repFL"><span class="bold">'+data.SocialConnections[i].Name+': </span> '+ data.SocialConnections[i].UserName +'</div>';
				}
				
				//data.SocialConnections[i].Name
			}
		}
		innerHTML += "</div>";
		innerHTML += "</div>";
		innerHTML += "</div>";
		
		if (isValg  ){	 // valgmode
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
			
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							for (var l = 0; l < herausforderer.length; l++) {
									
									if ( (herausforderer[l].CountyId == 301 ? 3 : herausforderer[l].CountyId) == data.CountyId) {
									innerHTML += "<div class='herrausforderer' >";
									innerHTML += "<div class='herrausfordererleft'>";
									innerHTML += "<img class='herrausfordererimg' src='images/rep/gruenKleinerAls.png' />";
									innerHTML += "<div class='herrausforderertext'>Mangler "+countySpecialM[i].vinneStemmer +
									" stemmer</div>";
									innerHTML += "</div>";
									innerHTML += "<div class='herrausfordererright'>";
									innerHTML += "<div class='herrausfordererrighttext'>UTFORDRER</div>";
									innerHTML += "<img id='candidate_"+ herausforderer[l].CandidateId+"' onclick=\"openRepBoxH('candidate_"+ herausforderer[l].CandidateId+"',this);\" class='herrausfordererrightimg' style='width:71px; border-radius:5px;' src='images/rep/big/"+herausforderer[l].Picture+"_org200.png' />";
									innerHTML += "</div>";
									innerHTML += "<div class='herrausfordererbottom'>";
									innerHTML += "<img class='herrausfordererpartyimg' src='images/logo/parti"+herausforderer[l].PartiOrganizationId+".png' />";
									innerHTML += "<div class='herrausfordererbottomtext'>"+herausforderer[l].FirstName+" "+herausforderer[l].LastName+"</div>";
									//<div>"+herausforderer[l].FirstName+", "+herausforderer[l].LastName+", "+getPartyName(herausforderer[l].PartiOrganizationId)+", "+countySpecialM[i].vinneStemmer+"</div>
									innerHTML += "</div>";
									innerHTML += "</div>";
										
									}
								}
							
						}
					}
				}
			}
		}
		innerHTML += "</div>";
		innerHTML += "</div>";
		
			$(newObj).html(innerHTML);
			twttr.widgets.load();
			$(newObj).attr('style','display:block;margin-top:-5px;');
			
			if ($(newObj).hasClass('TTsmall')){
				
			$(newObj).animate({height:"1%"},"fast",function(){
		container.scrollTop(
		container.scrollTop() + $(newObj).offset().top - container.offset().top - 35 );
		checkScrollPosition(container);
		
	});
	}else {
		
		$(newObj).animate({height:"1%"},"fast",function(){
		container.scrollTop(
		container.scrollTop() + $(newObj).offset().top - container.offset().top - 115 );
		checkScrollPosition(container);
	});
	}
	})
	.done(function(){/*console.log("second sucess  rep");*/})
	.fail(function(){/*console.log("error rep");*/})
	.always(function(){/*console.log("complete rep");*/});
	
}
function openRepBoxH(repId,athis) {
	$(".repholderBig").removeClass('activeRep');
	$(".repholder").removeClass('activeRep');
		
	var innerHTML = '';
	innerHTML += "<div class='closeMeBtn' onclick='closeRepTT();'></div>";
	var obj = athis;
	$(".repTT").attr('style','');
	
	var newObj = $('#'+repId).parent().parent().parent();
	//$(".repTT").html() == '';
	//$(newObj).html() = '';
	//var innerHTML = '';
	var obj = athis;
	
	
	if ($(newObj).hasClass('TTsmall')){
		var container = $('div.repInnerWrapper');
	}else {
		var container = $('div.repInnerWrapperBig');
	}
	container.scrollTop(0);

	$.getJSON('repData/'+repId+'_metainfo.json', function(data){
		innerHTML += "<div class='newlogoBox'>"; // DIV OPEN
		
		if (isValg  ){	 // valgmode
		
			for (var i=0; i< countySpecialM.length; i++){	// loop specialmandater array
				if ((countySpecialM[i].countyId== 301 ? 3 :countySpecialM[i].countyId) == data.CountyId){ // county fit
					if ( data.CountyPosition == countySpecialM[i].normalMandatforParti ){	// representanter position is as big as  then the normal amount of mandater + 1
						if ( data.PartiOrganizationId  ==  countySpecialM[i].mistMandat ) { // same parti
							//extraMHTML += "<img src='images/rep/roteEckeBig.png' />";
							innerHTML += "<img src='images/rep/usikker.png' />";
						}
						if ( data.PartiOrganizationId  ==  countySpecialM[i].umandatParti ) { // same parti
							//extraMHTML += "<img src='images/rep/uManBig.png' />";
						}	
					}
				}
			}
		}
		if(data.ParliamentPeriods == ''){
			innerHTML += "<img class='isNewImage' src='images/rep/isNew.png' />";
		}
		innerHTML += "</div>"; //CLOSE OPEN DIV
		innerHTML += "<div class='repBox'>";
		innerHTML += "<div class='repHeadline'>";
		innerHTML += "<div class='repPartiLogo partiLogo1'><img src='images/logo/parti"+data.PartiOrganizationId +".png' /></div>"
		innerHTML += "<div class='repName'>"+data.FirstName.toUpperCase() + " " + data.LastName.toUpperCase() +"<br/><span style='font-size:14px;display:block;margin-top:-5px;' >"+getPartyName(data.PartiOrganizationId) +" i " + data.CountyName +"</span></div>";
		
		innerHTML += "</div>";
		innerHTML += "<div class='repContent'>";
		
		if ($(newObj).hasClass('TTsmall')){
			var myURL = "images/rep/big/"+(data.Picture != '' ? (data.Picture+'_org200.png') : 'dummy_big.jpg' );
			innerHTML += "<div class='repBild'><img style='width:71px;border-radius:5px;' src='"+myURL+"' /></div>"; 
		}else {var myURL = "images/rep/big/"+(data.Picture != '' ? (data.Picture+'_org200.png') : 'dummy_big.jpg' );
			innerHTML += "<div class='repBild'><img style='width:71px;border-radius:5px;' src='"+myURL+"' /></div>"; 
		}
		if ($(newObj).hasClass('TTsmall')){
			innerHTML += "<div class='repInfo'>";
		}else {
			innerHTML += "<div class='repInfo' style='padding-left: 80px;width: 100%;'>";
		}
		if (data.DateOfBirth != ''){
			var birthDate = data.DateOfBirth.split('T');
			var birthDate = birthDate[0].split('-');
			innerHTML += '<div class="lineWrapper"><div class="repLeft">Født:</div><div class="repRight"> '+ birthDate[2] + '.'+ birthDate[1] +'.'+ birthDate[0] +'</div></div>';
		}
		if (data.Occupation != ''){
			innerHTML += '<div class="lineWrapper"> <div class="repLeft">Stilling:</div><div class="repRight"> '+ data.Occupation +'</div> </div>';
		}
		if (data.Municipality != ''){
			var text = '';
			text = data.Municipality;
			if ( text != undefined){
			innerHTML += '<div class="lineWrapper"><div class="repLeft">Bosted:</div><div class="repRight"> '+ text +'</div></div>';
			}
		}
		innerHTML += "<br/>";
		if (data.Education != ''){
			innerHTML += '<div class="blockWrapper"><div class="repFL"><span class="bold">Utdanning/Yrkesbakgrunn:</span> '+ data.Education +'</div>';
		}
		if (data.Description != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.Description +'</div>';
		}
		/*if (data.ParliamentPeriods != ''){
			innerHTML += '<div class="repFL"><span class="bold">Politisk karriere:</span> '+ data.ParliamentPeriods +'</div>';
		}*/
		/*if (data.Email != ''){
			innerHTML += '<div class="repFL"><span class="bold">E-Post:</span> '+ data.Email +'</div>';
		}*/
		if (data.SocialConnections != '' && data.SocialConnections != null && data.SocialConnections != 'null'){
			for (var i=0; i < data.SocialConnections.length;i++){
				innerHTML += '<div class="repFL"><span class="bold">'+data.SocialConnections[i].Name+': </span> '+ data.SocialConnections[i].UserName +'</div>';
			}
		}
		innerHTML += "</div>";
		innerHTML += "</div>";
		innerHTML += "</div>";
		
		
		innerHTML += "</div>";
		innerHTML += "</div>";
			
			
			
		setTimeout(function(){
			$(newObj).html(innerHTML);
			$(newObj).attr('style','display:block;margin-top:-5px;');
			if ($(newObj).hasClass('TTsmall')){
				
				$(newObj).animate({height:"1%"},"fast",function(){
					container.scrollTop(
					container.scrollTop() + $(newObj).offset().top - container.offset().top - 35 );
					checkScrollPosition(container);
				});
			}else {
		
				$(newObj).animate({height:"1%"},"fast",function(){
					container.scrollTop(
					container.scrollTop() + $(newObj).offset().top - container.offset().top - 115 );
					checkScrollPosition(container);
				});
			}
		},350);
	})
	.done(function(){/*console.log("second sucess  rep");*/})
	.fail(function(){/*console.log("error rep");*/})
	.always(function(){/*console.log("complete rep");*/});
	
}

function openMapTooltip(id, parseRegion){
var size = ((window.innerWidth >= 681) ? '100' : '80');
	$(".mapTooltip").attr("style", "visibility:hidden;");
	$(".mapItem").removeAttr("style");
	$("."+id).attr("style","visibility:visible;");
	$(".map_"+parseRegion+" img").attr("style","transform:scale(0.9);");
	$(".map_"+parseRegion).attr("style","background-image: url('images/kart/"+parseRegion+"_FFDD2E_"+ size +".png');background-repeat:no-repeat; background-position: top left;");
}

function closeMapTooltip(){
	$(".mapTooltip").attr("style", "visibility:hidden;");
	$(".mapItem").removeAttr("style");
	$(".mapItem img").removeAttr("style");
}
/*** test to load svg instead ofother ma stuff ***/
var c01,c02,c03,c04,c05,c06,c07,c08,c09,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20;

function openPartiInfoTT(id) {
	var container = $('div#partiInnerWrapper');
	container.scrollTop(0);
	$("div .pLogoWrapper").removeClass('activeParty');
	$('.pTT').attr('style','display:none;');
	$("div .pImg"+id).addClass('activeParty');
	$('.parti'+id+'Info').attr('style','height:0px;display:block;');
	var myHeight = (id == 9) ? 400:260;
	$('.parti'+id+'Info').animate({height:myHeight+"px"},"fast",function(){
		container.scrollTop(
		container.scrollTop() + $('.parti'+id+'Info').offset().top - container.offset().top );
	});
	
	return false;
}
function closePartiInfoTT(id) {
	$('.pTT').slideUp("fast");
	$('.pLogoWrapper').removeClass('activeParty');
	
	
}
function scrollTable(dir) {
	var container;
	if (dir == 'up') {
		container =  $('div#tableInnerWrapper');
		container.scrollTop(0);
	}else if (dir == 'down') {
		container =  $('div#tableInnerWrapper');
		var pos = container.scrollTop();
		pos += 48;
		container.scrollTop(pos);
	}
	checkScrollPosition2(container);
}
function checkScrollPosition2 (scrollContainer) {
	var container = scrollContainer;
	if ( container.scrollTop() == 0 ) {
		$('.upButton').attr('style','display:none;');
	}else if (container.scrollTop() != 0) {
		$('.upButton').attr('style','');
	}
	if(container.scrollTop() >= $('#tableContainer').height() - container.height() + (window.innerWidth < 700 ? 20 : 0) ){
		$('.downButton').attr('style','display:none;');
	}else {
		$('.downButton').attr('style','');
	}

}

function scrollRepWindow(dir) {
	/*** up upBig down downBig ***/
	var container;
	if (dir == 'up') {
		container =  $('div.repInnerWrapper');
		container.scrollTop(0);
	}else if (dir == 'down') {
		container =  $('div.repInnerWrapper');
		var pos = container.scrollTop();
		pos += 33;
		container.scrollTop(pos);
	}else if ( dir == 'upBig') {
		container = $('div.repInnerWrapperBig');
		var pos = container.scrollTop();
		pos -= 270; 
		if (pos < 0) pos = 0;
		if (pos < 130 ) pos = 0;
		container.scrollTop(pos);
	}else if (dir == 'downBig') {
		container = $('div.repInnerWrapperBig');
		var pos = container.scrollTop();
		pos += 270;
		container.scrollTop(pos);
	}
	checkScrollPosition(container);
}
function checkScrollPosition (scrollContainer) {
	var container = scrollContainer;
	if ( container.scrollTop() == 0 ) {
		$('.upButton').attr('style','display:none;');
	}else if (container.scrollTop() != 0) {
		$('.upButton').attr('style','');
	}
	if(container.scrollTop() >= $('.pusher').height() - container.height() + (window.innerWidth < 700 ? 20 : 0) ){
		$('.downButton').attr('style','display:none;');
	}else {
		$('.downButton').attr('style','');
	}

}
function animateRotate (startAngle, endAngle, duration, easing, complete){
    //return this.each(function(){
        var elem = $('.pfeil');

        $({deg: startAngle}).animate({deg: endAngle}, {
            duration: duration,
            easing: easing,
            step: function(now){
                elem.css({
                  '-moz-transform':'rotate('+now+'deg)',
                  '-webkit-transform':'rotate('+now+'deg)',
                  '-o-transform':'rotate('+now+'deg)',
                  '-ms-transform':'rotate('+now+'deg)',
                  'transform':'rotate('+now+'deg)'
                });
            },
            complete: complete || $.noop
        });
	//});
}		
function testValgIds() {
/*** check the kommune kretser ids ***/
	var counter = 0;
	var subRegionsArray = subRegions.split('|');
    for (var i = 0; i < subRegionsArray.length; i++) {
		var elements = subRegionsArray[i].split(';');
		if (elements[2].substring(0, 3) == '29_'){
			counter++
			//console.log(subRegionsArray[i]);
		}
	}
   // console.log(counter);
}
