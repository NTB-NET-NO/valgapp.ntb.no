﻿
	/*var parties = [
		{color:'#B01116', id:1, shortName:'RV', name:'Rødt'},
		{color:'#EB1C24', id:2, shortName:'SV', name:'Sosialistisk venstreparti'},
		{color:'#DC5E74', id:3, shortName:'AP', name:'Arbeiderpartiet'},
		{color:'#01994C', id:4, shortName:'SP', name:'Senterpartiet'},
		{color:'#98B94E', id:5, shortName:'V', name:'Venstre'},
		{color:'#FFDC2E', id:6, shortName:'KRF', name:'Kristelig folkeparti'},
		{color:'#0090CC', id:7, shortName:'H', name:'Høyre'},
		{color:'#005E94', id:8, shortName:'FRP', name:'Fremskrittspartiet'},
		{color: '#C8C4A7', id:9, shortName: 'A', name: 'Andre' },
	    {color:'#C8C4A7', id:101, shortName:'AFI', name:'Alt For Innbyggerne'},
        {color:'#C8C4A7', id:102, shortName:'AGV', name:'Askers Grønne Venner'},
        {color:'#C8C4A7', id:103, shortName:'AND', name:'Andøylista'},
        {color:'#C8C4A7', id:104, shortName:'ANFL', name:'Andørja fellesliste'},
        {color:'#C8C4A7', id:105, shortName:'AP_SV_TY', name:'Arbeiderpartiet og SV'},
        {color:'#C8C4A7', id:106, shortName:'APRU', name:'Fellesl. Ap, Rødt og uavhengige velgere'},
        {color:'#C8C4A7', id:107, shortName:'APSV', name:'Valgliste for AP og SV'},
        {color:'#C8C4A7', id:108, shortName:'ARJA', name:'Árja'},
        {color:'#C8C4A7', id:109, shortName:'ARNØY', name:'Arnøylista'},
        {color:'#C8C4A7', id:110, shortName:'ASKIMP', name:'Askimpartiet'},
        {color:'#C8C4A7', id:111, shortName:'ATPG', name:'Det Norske Arbeiderparti og Tverrpolitisk gruppe'},
        {color:'#C8C4A7', id:112, shortName:'AUKRA', name:'Aukralista – tverrpolitisk liste'},
        {color:'#C8C4A7', id:113, shortName:'B', name:'Bryggjalista'},
        {color:'#C8C4A7', id:114, shortName:'BAL', name:'Bindal Alternative Liste'},
        {color:'#C8C4A7', id:115, shortName:'BB', name:'Bygdalista i Bjugn'},
        {color:'#C8C4A7', id:116, shortName:'BBL', name:'By- og bygdelista'},
        {color:'#C8C4A7', id:117, shortName:'BEBL', name:'Beiarn Bygdeliste'},
        {color:'#C8C4A7', id:118, shortName:'BFE', name:'Berg fellesliste'},
        {color:'#C8C4A7', id:119, shortName:'BFKH', name:'Bygdelista Fiskefjord - Kongsvik - Hårvik'},
        {color:'#C8C4A7', id:120, shortName:'BFLEB', name:'Borgerlig Fellesliste'},
        {color:'#C8C4A7', id:121, shortName:'BHBV', name:'Fellesliste for Høgre og Venstre'},
        {color:'#C8C4A7', id:122, shortName:'BIV', name:'Bygdalista i Verran'},
        {color:'#C8C4A7', id:123, shortName:'BJAFL', name:'Bjarkøy Kommunes Politiske Fellesliste'},
        {color:'#C8C4A7', id:124, shortName:'BLBU', name:'Buviklista'},
        {color:'#C8C4A7', id:125, shortName:'BLFLÅ', name:'Bygdeliste for Flå'},
        {color:'#C8C4A7', id:126, shortName:'BLFY', name:'Bygdelista'},
	    {color:'#C8C4A7', id:127, shortName:'BLLOM', name:'Bygdalista i Lom'},
        {color:'#C8C4A7', id:128, shortName:'BLM', name:'Bygdelista i Modum'},
        {color:'#C8C4A7', id:129, shortName:'BLSTA', name:'Bygdelista for Stange'},
        {color:'#C8C4A7', id:130, shortName:'BLTS', name:'Bygdalista for tverrpolitisk samarbeid'},
        {color:'#C8C4A7', id:131, shortName:'BLV', name:'Bygdelista'},
        {color:'#C8C4A7', id:132, shortName:'BLVS', name:'Bygdeliste'},
        {color:'#C8C4A7', id:133, shortName:'BMB', name:'Bylisten mot bomring'},
        {color:'#C8C4A7', id:134, shortName:'BMG', name:'Bygdeliste for Midtre Gauldal'},
        {color:'#C8C4A7', id:135, shortName:'BNL', name:'Bygdelista for Nordre Land'},
        {color:'#C8C4A7', id:136, shortName:'BOM', name:'Nøtterøylisten mot Bomring'},
        {color:'#C8C4A7', id:137, shortName:'BS', name:'Bygdalist i Skjåk'},
        {color:'#C8C4A7', id:138, shortName:'BSAM', name:'Berg samlingsliste'},
        {color:'#C8C4A7', id:139, shortName:'BSE', name:'Bygdelista i Stor-Elvdal'},
        {color:'#C8C4A7', id:140, shortName:'BUTV', name:'Bygdeutviklingslista'},
        {color:'#C8C4A7', id:141, shortName:'BYB', name:'Bygland Bygdeliste'},
        {color:'#C8C4A7', id:142, shortName:'BYG', name:'Bygdalista'},
        {color:'#C8C4A7', id:143, shortName:'BYG_NOR', name:'Bygdeliste for Norheimsund'},
        {color:'#C8C4A7', id:144, shortName:'BYGH', name:'Bygdeliste for Høylandet'},
        {color:'#C8C4A7', id:145, shortName:'BYGS', name:'Bygdalista'},
        {color:'#C8C4A7', id:146, shortName:'BYGVÅL', name:'Bygdelista'},
        {color:'#C8C4A7', id:147, shortName:'BYLUFT', name:'Byluftlisten'},
        {color:'#C8C4A7', id:148, shortName:'BYMILJØ', name:'Bymiljølista'},
        {color:'#C8C4A7', id:149, shortName:'BYN', name:'By og Nærmiljøpartiet'},
        {color:'#C8C4A7', id:150, shortName:'BØA', name:'Bygdelista – for øvre del av Audnedal'},
        {color:'#C8C4A7', id:151, shortName:'BØHAP', name:'Høyre og Det Norske Arbeiderparti'},
        {color:'#C8C4A7', id:152, shortName:'BØL', name:'Bøgdalesta'},
        {color:'#C8C4A7', id:153, shortName:'BÅTL', name:'BÅTSFJORDLISTA'},
        {color:'#C8C4A7', id:154, shortName:'DEMN', name:'Demokratene i Norge'},
        {color:'#C8C4A7', id:155, shortName:'DLF', name:'Det Liberale Folkepartiet'},
        {color:'#C8C4A7', id:156, shortName:'EDP', name:'Eidsvoll Demokratiske Bygdeliste'},
        {color:'#C8C4A7', id:157, shortName:'FBU', name:'Felleslista for bygdeutvikling'},
        {color:'#C8C4A7', id:158, shortName:'FD', name:'Felleslista for Dyrøy'},
        {color:'#C8C4A7', id:159, shortName:'FDL', name:'Flakstad distriktsliste'},
        {color:'#C8C4A7', id:160, shortName:'FDS', name:'Flora Demokratiske Solidaritet'},
        {color:'#C8C4A7', id:161, shortName:'FERJA', name:'Ferja/Stigedalen (Ferjelista)'},
        {color:'#C8C4A7', id:162, shortName:'FJORD', name:'Fjordfolket'},
        {color:'#C8C4A7', id:163, shortName:'FKBL', name:'Fredvang og Krystad bygdeliste'},
        {color:'#C8C4A7', id:164, shortName:'FKRFSPV', name:'Fellesliste KrF, SP, Venstre'},
        {color:'#C8C4A7', id:165, shortName:'FLFA', name:'Felleslista'},
        {color:'#C8C4A7', id:166, shortName:'FLHOLT', name:'Felleslista for KrF, SP, V'},
        {color:'#C8C4A7', id:167, shortName:'FLISTO', name:'Fitjarlisto'},
        {color:'#C8C4A7', id:168, shortName:'FLOKT', name:'Felleslista - Oktasaslista'},
        {color:'#C8C4A7', id:169, shortName:'FLRRR', name:'Felleslista for Ringvassøy, Reinøy og Rebbenesøy'},
        {color:'#C8C4A7', id:170, shortName:'FLSP', name:'Folkelista/Senterpartiet'},
        {color:'#C8C4A7', id:171, shortName:'FLSPKRF', name:'Fellesliste for Senterpartiet og Kristelig Folkeparti'},
        {color:'#C8C4A7', id:172, shortName:'FLUT', name:'Utsira Fellesliste'},
        {color:'#C8C4A7', id:173, shortName:'FLÅSAM', name:'Samarbeidsliste for SP og KrF'},
        {color:'#C8C4A7', id:174, shortName:'FMD_SV', name:'Fellesliste Miljøpartiet dei Grøne og SV'},
        {color:'#C8C4A7', id:175, shortName:'FOHAR', name:'Folkelista for Hareid kommune'},
        {color:'#C8C4A7', id:176, shortName:'FRBB', name:'Folkets Røst By- og Bygdeliste'},
        {color:'#C8C4A7', id:177, shortName:'FRP_H', name:'Fellesliste Fremskrittspartiet/Høyre'},
        {color:'#C8C4A7', id:178, shortName:'FS', name:'Folkets stemme'},
        {color:'#C8C4A7', id:179, shortName:'FV', name:'Felleslista for Værøy'},
        {color:'#C8C4A7', id:180, shortName:'GDL', name:'Kautokeino Fastboendes liste'},
        {color:'#C8C4A7', id:181, shortName:'GRANB', name:'Gran Bygdeliste'},
        {color:'#C8C4A7', id:182, shortName:'GRUEFL', name:'Fellesliste mellom H, KRF og V'},
        {color:'#C8C4A7', id:183, shortName:'H_KRF_SI', name:'Fellesliste for Høyre og KrF'},
        {color:'#C8C4A7', id:184, shortName:'H_V', name:'Fellesliste Høyre og Venstre'},
        {color:'#C8C4A7', id:185, shortName:'HADSELFL', name:'Hadsel Fellesliste'},
        {color:'#C8C4A7', id:186, shortName:'HE_HVSP', name:'Fellesliste for Høyre, Venstre og SP'},
        {color:'#C8C4A7', id:187, shortName:'HEMNELIS', name:'Hemnelista'},
        {color:'#C8C4A7', id:188, shortName:'HEORD', name:'Hemnes samfunnsparti'},
        {color:'#C8C4A7', id:189, shortName:'HKVU', name:'Felleslista for Høyre, Kristelig Folkeparti, Venstre og Uavhengige'},
        {color:'#C8C4A7', id:190, shortName:'HTL', name:'Hinnøysiden tverrpolitiske liste'},
        {color:'#C8C4A7', id:191, shortName:'IL', name:'Innbyggerlista'},
        {color:'#C8C4A7', id:192, shortName:'INNB', name:'Innbyggjarpartiet'},
        {color:'#C8C4A7', id:193, shortName:'JL', name:'Kautokeino Flyttesameliste'},
        {color:'#C8C4A7', id:194, shortName:'KARLIS', name:'Karasjok lista Kàràsjot’ Lista'},
        {color:'#C8C4A7', id:195, shortName:'KBYG', name:'Kåfjord bygdeliste'},
        {color:'#C8C4A7', id:196, shortName:'KIU', name:'Kvænangen i utvikling'},
        {color:'#C8C4A7', id:197, shortName:'KJS', name:'Karasjoga Johttisamiid searvvi listu/ Karasjok flyttsamelagets liste'},
        {color:'#C8C4A7', id:198, shortName:'KOM', name:'Kommunelista'},
        {color:'#C8C4A7', id:199, shortName:'KRF_V_SO', name:'Fellesliste for KrF/Venstre'},
        {color:'#C8C4A7', id:200, shortName:'KRFFLFB', name:'Kristelig Folkeparti, Fellesliste og Frie borgerlige'},
        {color:'#C8C4A7', id:201, shortName:'KRFVO', name:'KRF_Venstre'},
        {color:'#C8C4A7', id:202, shortName:'KRISTNE', name:'De kristne'},
        {color:'#C8C4A7', id:203, shortName:'KRLB', name:'Bjoa bygdeliste'},
        {color:'#C8C4A7', id:204, shortName:'KRSL', name:'Kretsliste for området Russelv - Sør-Lenangsbotn'},
        {color:'#C8C4A7', id:205, shortName:'KSP', name:'Kristent Samlingsparti'},
        {color:'#C8C4A7', id:206, shortName:'KTL', name:'Kvam Tverrpolitiske liste'},
        {color:'#C8C4A7', id:207, shortName:'KYST', name:'Kystpartiet'},
        {color:'#C8C4A7', id:208, shortName:'LB', name:'Lierne Bygdeliste'},
        {color:'#C8C4A7', id:209, shortName:'LENVIK', name:'Lenviklista'},
        {color:'#C8C4A7', id:210, shortName:'LHV', name:'Liste for Høyre og Venstre'},
        {color:'#C8C4A7', id:211, shortName:'LILLEHL', name:'Lillehammerlista'},
        {color:'#C8C4A7', id:212, shortName:'LOMSL', name:'Lomslista'},
        {color:'#C8C4A7', id:213, shortName:'LTV', name:'Lovund Tverrpolitiske Liste'},
        {color:'#C8C4A7', id:214, shortName:'LVH', name:'Lørenskog i våre hjerter'},
        {color:'#C8C4A7', id:215, shortName:'LVS', name:'Liste for Venstre og Senterpartiet'},
        {color:'#C8C4A7', id:216, shortName:'MDG', name:'Miljøpartiet De Grønne'},
        {color:'#C8C4A7', id:217, shortName:'METPBL', name:'Meråker Tverrpolitiske Bygdeliste'},
        {color:'#C8C4A7', id:218, shortName:'MKBYGD', name:'Bygdelista i Moskenes'},
        {color:'#C8C4A7', id:219, shortName:'MOS', name:'Melbu og omegn samarbeidsliste'},
        {color:'#C8C4A7', id:220, shortName:'MOSKFL', name:'Moskenes fellesliste'},
        {color:'#C8C4A7', id:221, shortName:'MR', name:'Miljølisten Rana'},
        {color:'#C8C4A7', id:222, shortName:'MVL', name:'Sør-Varangerlista'},
        {color:'#C8C4A7', id:223, shortName:'NEBL', name:'Nes bygdeliste'},
        {color:'#C8C4A7', id:224, shortName:'NKP', name:'Norges Kommunistiske Parti'},
        {color:'#C8C4A7', id:225, shortName:'NOLI', name:'Norddalslista'},
        {color:'#C8C4A7', id:226, shortName:'NYODDA', name:'Nye Odda'},
        {color:'#C8C4A7', id:227, shortName:'NÆ', name:'Nærbølista'},
        {color:'#C8C4A7', id:228, shortName:'OL', name:'Orkdalslista'},
        {color:'#C8C4A7', id:229, shortName:'OPTI', name:'Optimum'},
        {color:'#C8C4A7', id:230, shortName:'PP', name:'Pensjonistpartiet'},
        {color:'#C8C4A7', id:231, shortName:'PULS', name:'Politisk Uavhengig Liste i Solund'},
        {color:'#C8C4A7', id:232, shortName:'RADS', name:'Radikale sosialister'},
        {color:'#C8C4A7', id:233, shortName:'RE_H_KRF', name:'Høyre og Kristelig Folkeparti'},
        {color:'#C8C4A7', id:234, shortName:'RFL', name:'Rødøy fellesliste'},
        {color:'#C8C4A7', id:235, shortName:'RSL', name:'Ringsakerlista'},
        {color:'#C8C4A7', id:236, shortName:'RV_SV', name:'Felleslista RV/SV'},
        {color:'#C8C4A7', id:237, shortName:'RV_SV_SD', name:'R, SV, og Sosialdemokrater i Meland'},
        {color:'#C8C4A7', id:238, shortName:'RØL', name:'Røroslista'},
        {color:'#C8C4A7', id:239, shortName:'SAMDEM', name:'Samfunnsdemokratane'},
        {color:'#C8C4A7', id:240, shortName:'SAME', name:'Samefolkets Parti'},
        {color:'#C8C4A7', id:241, shortName:'SAMH', name:'Samarbeidslista'},
        {color:'#C8C4A7', id:242, shortName:'SAMLIST', name:'Samlingslista'},
        {color:'#C8C4A7', id:243, shortName:'SAMVE', name:'Samarbeidsliste'},
        {color:'#C8C4A7', id:244, shortName:'SB', name:'Småbylista Orkdal'},
        {color:'#C8C4A7', id:245, shortName:'SBL', name:'Sømna Bygdeliste'},
        {color:'#C8C4A7', id:246, shortName:'SBLRØ', name:'Samarbeidslista i Røyrvik  Fellesliste mellom frie velgere, H, KrF, Sp og V'},
        {color:'#C8C4A7', id:247, shortName:'SELTL', name:'Tverrpolitisk liste'},
        {color:'#C8C4A7', id:248, shortName:'SENTRUM', name:'Sentrumslista (Kystpartiet, Venstre og partipolitisk uavhengige velgere'},
        {color:'#C8C4A7', id:249, shortName:'SET', name:'Seter krets'},
        {color:'#C8C4A7', id:250, shortName:'SFBY', name:'Sør-Fron Bygdaliste'},
        {color:'#C8C4A7', id:251, shortName:'SFP_NSR', name:'Sámeálbmot Bellodat/NSR Deatnu'},
        {color:'#C8C4A7', id:252, shortName:'SFSBL', name:'Sandsøy og Fenes/Skjellesvik bygdeliste'},
        {color:'#C8C4A7', id:253, shortName:'SLB', name:'Søndre Land Bygdeliste'},
        {color:'#C8C4A7', id:254, shortName:'SLHY', name:'Samlingslista'},
        {color:'#C8C4A7', id:255, shortName:'SLMO', name:'Samlingslista for Modalen'},
        {color:'#C8C4A7', id:256, shortName:'SOLA', name:'Solidaritetslista'},
        {color:'#C8C4A7', id:257, shortName:'SOLID', name:'SOLIDARITETSLISTA AV SV, RØDT OG UAVHENGIGE'},
        {color:'#C8C4A7', id:258, shortName:'SOLR', name:'Solrenningslista'},
        {color:'#C8C4A7', id:259, shortName:'SOT', name:'SotraLista'},
        {color:'#C8C4A7', id:260, shortName:'SP_SV', name:'Liste Sp/SV'},
        {color:'#C8C4A7', id:261, shortName:'SP_V', name:'Samlingslista Senterpartiet/Venstre'},
        {color:'#C8C4A7', id:262, shortName:'SP_V_KRF', name:'Fellesliste for Senterpartiet, Venstre og Kristelig folkeparti'},
        {color:'#C8C4A7', id:263, shortName:'SPKRF', name:'Senterpartiet/Kristelig Folkeparti'},
        {color:'#C8C4A7', id:264, shortName:'STØKURS', name:'Stø kurs'},
        {color:'#C8C4A7', id:265, shortName:'SVS', name:'Sør-Varanger Solidaritetslista'},
        {color:'#C8C4A7', id:266, shortName:'SØBYG', name:'Søgne bygdeliste'},
        {color:'#C8C4A7', id:267, shortName:'TAL', name:'Tanangerlisten'},
        {color:'#C8C4A7', id:268, shortName:'TB', name:'Tverrpolitisk bygdeliste'},
        {color:'#C8C4A7', id:269, shortName:'TBY', name:'Tverrpolitisk Bygdeliste'},
        {color:'#C8C4A7', id:270, shortName:'TE', name:'Team Elverum – Politikk for by og bygd'},
        {color:'#C8C4A7', id:271, shortName:'TFL', name:'Tranøy Folkeliste'},
        {color:'#C8C4A7', id:272, shortName:'TFV', name:'Frie Velgere'},
        {color:'#C8C4A7', id:273, shortName:'TJL', name:'Tjømelista'},
        {color:'#C8C4A7', id:274, shortName:'TLG', name:'Tverrpolitisk liste'},
        {color:'#C8C4A7', id:275, shortName:'TLNE', name:'Tverrpolitisk liste'},
        {color:'#C8C4A7', id:276, shortName:'TPK', name:'Tverrpolitisk Kommuneliste'},
        {color:'#C8C4A7', id:277, shortName:'TPKR', name:'Tverrpolitisk bygdeliste'},
        {color:'#C8C4A7', id:278, shortName:'TPLH', name:'Tverrpolitisk liste'},
        {color:'#C8C4A7', id:279, shortName:'TPLS', name:'Tverrpolitisk liste Sømna'},
        {color:'#C8C4A7', id:280, shortName:'TPLTY', name:'Tverrpolitisk liste for Tysfjord'},
        {color:'#C8C4A7', id:281, shortName:'TPÅS', name:'Tverrpolitisk bygdeliste'},
        {color:'#C8C4A7', id:282, shortName:'TSBO', name:'Tverrpolitisk samlingsliste-bygdaliste for Os'},
        {color:'#C8C4A7', id:283, shortName:'TTL', name:'Tvedestrand Tverrpolitiske Liste'},
        {color:'#C8C4A7', id:284, shortName:'TVB', name:'Tverrpolitisk bygdeliste'},
        {color:'#C8C4A7', id:285, shortName:'TVLKS_V', name:'Tverrpolitisk liste for Kvamsøy, Sandsøy og Voksa'},
        {color:'#C8C4A7', id:286, shortName:'TVLN', name:'Tverpolitisk liste for Norddal'},
        {color:'#C8C4A7', id:287, shortName:'TVLRING', name:'Tverrpolitisk liste'},
        {color:'#C8C4A7', id:288, shortName:'TVPÅL', name:'Tverrpolitisk liste for Ålesund'},
        {color:'#C8C4A7', id:289, shortName:'TVPAG', name:'Tverrpolitisk liste'},
        {color:'#C8C4A7', id:290, shortName:'TVØ', name:'Tverrpolitisk Valliste for Øygarden (TVØ)'},
        {color:'#C8C4A7', id:291, shortName:'TYDL', name:'Tydalslista, bygdeliste for utvikling'},
        {color:'#C8C4A7', id:292, shortName:'TYSBYG', name:'Tysfjord bygdeliste'},
        {color:'#C8C4A7', id:293, shortName:'TYV', name:'Tynsetlista frie velgere'},
        {color:'#C8C4A7', id:294, shortName:'UAVH', name:'Sulalista, partipolitisk uavhengig liste for Sula'},
        {color:'#C8C4A7', id:295, shortName:'UF', name:'Uavhengig folkevalgte'},
        {color:'#C8C4A7', id:296, shortName:'UNGLIST', name:'Ungdomslista for Utsira'},
        {color:'#C8C4A7', id:297, shortName:'UVH', name:'Uavhengig valliste for Haram'},
        {color:'#C8C4A7', id:298, shortName:'VPP', name:'Verdipolitisk Parti'},
        {color:'#C8C4A7', id:299, shortName:'VV', name:'Vårt Valg'},
        {color:'#C8C4A7', id:300, shortName:'VØHK', name:'Valgliste for Øvre-Høylandet og Kongsmoen krets'},
        {color:'#C8C4A7', id:301, shortName:'ØR_KRF_V', name:'Ørskog KRF - Ørskog V'},
        {color:'#C8C4A7', id:302, shortName:'ØTL', name:'Øksnes Tverrpolitiske liste'},
        {color:'#C8C4A7', id:303, shortName:'VL', name:'Vestfoldlisten mot bomringer'},
        { color: '#C8C4A7', id:304, shortName: 'SUNML', name: 'Sunnmørslista/Tverrpolitisk liste for Sunnmøre' },
        {color:'#C8C4A7', id:305, shortName:'SAMF', name:'Samfunnspartiet'}
	];*/


/*function makeGoogleCall(lat, lon) {
    var url = "/default.aspx?lat=" + lat + "&lon=" + lon;
    
    var xmlhttp;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function () {

        if (xmlhttp.readyState == 4){
            if (xmlhttp.status == 200)
                parsePosition(xmlhttp.responseText);
            else
                alert(xmlhttp.status); //document.getElementById(divId).innerHTML = "<div style='width:320px;text-align:center;'>Beklager ingen data funnet</div>";
        }
    };

    xmlhttp.open("GET", url);
    xmlhttp.send();
       
}

function parsePosition(responseText) {
    var xmlDoc;
    var parser;
    try { //Internet Explorer
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(responseText);
    }
    catch (e) { //Firefox et. all
        try {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(responseText, "text/xml");
        }
        catch (e) {
            alert(e.message);
        }
    }

    if (xmlDoc) {

        var addresses = xmlDoc.getElementsByTagName("address_component");

        for (var i = 0; i < addresses.length; i++) {
            var address = addresses[i];
            var children = address.childNodes;
            var muniName = '';

            //iterate 7 or 9 nodes
            for (var j = 0; j < children.length; j++) {
                var childNode = children[j];

                if (childNode.nodeType == 1) { //element
                    if (childNode.childNodes.length == 1) {
                        var textChild = childNode.childNodes[0];
                        if (childNode.tagName == 'type' && textChild.nodeValue == 'administrative_area_level_2') {
                            var longNameNodeList = childNode.parentNode.getElementsByTagName("long_name");
                            if (longNameNodeList.length > 0) {
                                var long_name_text = (longNameNodeList[0].childNodes.length > 0) ? longNameNodeList[0].childNodes[0] : null;

                                if (long_name_text) {
                                    muniName = long_name_text.nodeValue;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (muniName != '') {
                handleMuni(muniName);
                break;
            }
        }
    }
}*/



/* Smartphone */
@media screen and (min-device-width : 321px) { /*(max-device-width : 480px){*/
	#header{
		/*color:#FF0000;*/
		height:150px;
		font-size:33pt;
		background-image:url('../images/urne_480.png');
		margin: 0 0 0 18px;			
	}
	#headerContent{
		position: absolute; 
		bottom: 17px; 
		left: 132px;
	}
	.button{
		background-image:url('../images/mainbtn_1x70.png');
		height:70px;
		line-height:70px;
		padding-bottom:3px;
		font-size:21pt;
	}
		
	#divNation{
		min-height:256px;
	}
	#divCounty{
		min-height:291px;
	}
	#divMunicipality{
		min-height:300px;/*291px;*/
	}
	#divSearch{
		min-height:291px;
	}
		
	#footer{
		height:100px;
		line-height:100px;
		background-image:url('../images/logo_480.png');
		background-repeat:no-repeat;
		background-position:center center;
	}
	
	/* County selectbox */
	.divSelect{
		padding-top:15px;	
	}
	.divSelectLeft{
		padding-left:40px;
	}

	.styled{
		width:200px;
		text-align:center;
		height:37px;
		font-size:18pt; /*21pt*/
		font-weight:bold;
		color:#FFFFFF;
		-webkit-appearance: listbox;
	}	

	/* Buttonbar */	
	.divButtonBar{
		padding-top:15px;
		padding-left:18px;
	}
	
	.buttonBar{
		width:446px;
	}
	
	.divButtonBarTwoButtons{
		padding-top:15px;
		padding-left:90px;
	}
	
	.buttonBarButton1{
		background-repeat:no-repeat;
		background-image:url('../images/blokk_480_off.png');
		width:144px;
		height:63px;
	}
	.buttonBarButton2{
		background-repeat:no-repeat;
		background-image:url('../images/stemmer_480_off.png');
		width:159px;
		height:63px;
	}
	.buttonBarButton3{
		background-repeat:no-repeat;
		background-image:url('../images/repr_480_off.png');
		width:144px;
		height:63px;
	}
	.doubleButtonBarButton2{
		background-repeat:no-repeat;
		background-image:url('../images/stemmerbig_320_off.png');
		width:221px;
		height:42px;
	}
	.doubleButtonBarButton3{
		background-repeat:no-repeat;
		background-image:url('../images/mandaterbig_320_off.png');
		width:225px;
		height:42px;
	}
	
	.dataDiv{
		padding-top:15px;	
	}			
	
	/* Majority container*/
	#majorityContainer{
		font-size:18pt;
		padding-top:21px;
	}
	
	/* Votes counted*/
	#votesCountedContainer{
		font-size:18pt;
		padding-top:21px;
		padding-left:5px;
		padding-right:5px;
	}
	
	.votesCountedHeader{
		padding-top:6px;
	}
	
	/* Votes maincomponent*/
	#votesContainer{
		font-size:18pt;
	}

	.votesLogo{
		padding-top:3px;
		width:52px;
	}
	.votesValue{
		padding-top:3px;
		padding-bottom:3px;	
	}
	.votesDiff{
		padding-top:3px;
		padding-bottom:3px;	
	}
	.votesDirection{
		padding-top:3px;
	}
	
	/* Last updated component*/
	#lastUpdatedContainer{
		font-size:14pt;
		font-weight:normal;
		padding-top:21px;
		width:100%;			
	}	
	#lastUpdatedImage{
		width:35%;
		text-align:right;
	}
	#lastUpdatedText{
		 width:65%;
		 text-align:left;
		 padding-left:6px;		
	}
		
	/* Columns */
	#columnsContainer{
		width:100%;
		position:relative;
		font-size:6pt;
		text-align:center;		
		padding-top:4px;		
	}
	#columnsBackground{
		font-size:4pt;
		font-weight:normal;
		font-style:italic;
		color:#9197A6;
		width:100%;
	}
	.columnsBackgroundRow{
		border-bottom:1px #E7E7E7 solid;
	}

	/* Search */	
	#searchBox{
		color:#C8C8C8;
		font-size:21pt;
		-webkit-appearance: textfield;
	}	
	
}
